<?php
/**
 * Add a user.
 * Form to add a new user.
 *
 * @package Elgg
 * @subpackage Core
 */
?>


<!--
<div>
	<button id="generate-invite-url-button" class="btn btn-default">Generate Invite URL</button>
	<br/>
	URL = <span id="invite-url">press the button!</span>
</div>
-->

Hey dude, if you want to bulk register users, go <a href="/pg/approvals">Here</a>

<script>
	$('#generate-invite-url-button').click(function() {
		$.ajax({
			type: 'GET',
			url: '/services/api/rest/json?method=secret.generate_invite_url',
			dataType: 'json',
			success: function(data) {
				$('#invite-url').html('<a href="' + data.result + '">' + data.result + '</a>');
			}
		});
	});
</script>
