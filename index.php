<?php
/**
 * Elgg index page for web-based applications
 *
 * @package Elgg
 * @subpackage Core
 */ 
 
/**
 * Start the Elgg engine
 */
define('externalpage',true);
require_once(dirname(__FILE__) . "/engine/start.php");

if (!trigger_plugin_hook('index', 'system', null, FALSE)) {
	/**
	 * Check to see if user is logged in, if not display login form
	 **/

	if (isloggedin()) {
		forward('pg/miigle');
	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <link rel="shortcut icon" href="/favicon.png">

    <title>Miigle - The Social Network for Innovators | Join Us</title>
    <meta name="description" content="Miigle is the online community and marketplace 
    for entrepreneurs and startups. We help innovators worldwide share their ideas 
    and connect with people interested to help.">
    <meta name="keywords" content="miigle, miiglers, social network, social innovation, 
    crowdsourcing, crowdfunding, josh fester, luc berlin, foster ideas, startup, 
    entrepreneurship, entrepreneur community">

    <!-- Bootstrap core CSS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="/mod/miigle_theme/vendor/nouislider/jquery.nouislider.min.css" rel="stylesheet"/>

    <!-- clone of arial rounded font -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="/mod/miigle_theme/index.css" rel="stylesheet">

    <!-- Bootstrap -->
    <script src="/mod/miigle_dependencies/js/jquery.min.js"></script>
    <script src="/mod/miigle_dependencies/js/bootstrap.js" async="true"></script>
  </head>
  <body>
    <!--[if lte IE 9]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--============================================
    |
    |   Static navbar
    |
    ============================================-->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><img src="/mod/miigle_theme/graphics/miigle-logo.png" alt="Miigle" /></a>
        </div>
        <div class="navbar-collapse collapse">
          <a href="/pg/login" class="pull-right btn btn-default btn-inverse navbar-btn">Log in</a>
          <!--<a href="/mod/miigle_user_actions/login.html" class="pull-right btn btn-default btn-inverse navbar-btn">Log in</a>-->
          <button id="header_join" data-toggle="modal" href="#modal-request" type="button" class="pull-right btn btn-danger navbar-btn">Join The Beta</button>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
    <!--============================================
    |
    |   Splash
    |
    ============================================-->
    <section id="splash">
        <div class="container">
          <div class="row">
            <div class="col-md-12 jumbotron">
              <h1>Innovate better, <span class="yellow">together</span>.</h1>
              <p>Miigle helps startups worldwide showcase and market their products while collaborating with each other and the general public. <b>Join us!</b></p>
              <p>
                <button id="body_join" data-toggle="modal" href="#modal-request" type="button" class="btn-lg btn btn-danger">Get an Invite</button>
              </p>            
            </div>
          </div>
          <div class="clearfix"></div>
        </div> 
    </section>


    <div class="modal fade" id="pitch-modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <iframe width="560" height="315" src="//www.youtube.com/embed/PrnaG9glVkY" frameborder="0" allowfullscreen></iframe>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!--============================================
    |
    |   How it works
    |
    ============================================-->
    <section id="how">
        <div class="container">         
            <div class="row">
                <div class="col-md-12">
                    
                    <h2>How It Works</h2>
                    <h3 class="text-muted">Hello, ZERO legwork. Goodbye wasted time, money, and energy.</h3>
                                        
                    <div id="carousel-how" class="carousel slide carousel-fade" data-interval="false">              
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="/mod/miigle_theme/graphics/home-slide-1.png" alt="">                              
                            </div>
                            <div class="item">
                                <img src="/mod/miigle_theme/graphics/home-slide-2.png" alt="">
                            </div>
                            <div class="item">
                                <img src="/mod/miigle_theme/graphics/home-slide-3.png" alt=""> 
                            </div>
                        </div>  
                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-how" data-slide="prev">
                            <img src="/mod/miigle_theme/graphics/chevron-left.png" alt="Prev">
                        </a>
                        <a class="right carousel-control" href="#carousel-how" data-slide="next">
                            <img src="/mod/miigle_theme/graphics/chevron-right.png" alt="Next">
                        </a>                            
                    </div><!--/.carousel -->
                    
                    <div class="row captions">
                        <div class="col-sm-4">
                            <div class="custom-caption slide-0 active" data-slide="0">
                                <span class="indicator">1</span>
                                <h4>Post your startup or idea</h4>
                                <p class="text-muted">Take 5 minutes to create your profile and post your startup,
                                app, or idea description as well as any needs or questions you may have.</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="custom-caption slide-1" data-slide="1">
                                <span class="indicator">2</span>
                                <h4>Meet others in your network</h4>
                                <p class="text-muted">Miigle's algorithm searches and identifies members worldwide whose interests, experiences,
                                and resources match your needs.</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="custom-caption slide-2" data-slide="2">
                                <span class="indicator">3</span>
                                <h4>Start collaborating</h4>
                                <p class="text-muted">Your project is introduced to highly relevant people who are willing to collaborate, become
                                users, or help contribute resources you need.</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>                                  
        </div>
    </section>
    
    <!--============================================
    |
    |   Video
    |
    ============================================-->
    <section id="video">
        <div class="container">         
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="overlay">
                        <h2>See how together we can change the world</h2>
                        <a href="#" class="btn-play"><img src="/mod/miigle_theme/graphics/play-video.png" alt="Play" /></a>
                    </div>
                    
                    <iframe src="//player.vimeo.com/video/77944311" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> 
                    
                </div>
            </div>
        </div>
    </section>
    
     <!--============================================
    |
    |   Team
    |
    ============================================-->
    <section id="team">
        <div class="container">
    
          <h2>The Team</h2>
          <h3 class="text-muted">Experienced, but still hungry and foolish.</h3>
          
          <div class="row">
            
            <div class="col-md-4">
                <div class="person">
                    <img src="/mod/miigle_theme/graphics/luc.jpg" alt="Luc Berlin" />
                    <h4>Luc Berlin, <i>Cofounder &amp; CEO</i></h4>
                    <p class="text-muted">8+ years in business &amp; digital strategy with an expertise in user aquisition
                        and revenue generation. Pepperdine MBA - Global Business.</p>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="person">
                    <img src="/mod/miigle_theme/graphics/josh.jpg" alt="Josh Fester" />
                    <h4>Josh Fester, <i>Cofounder &amp; CTO</i></h4>
                    <p class="text-muted">8+ years developing business and media applications. Missouri State University 
                        B.S. CIS &amp; Entrepreneurship.</p>
                </div>
            </div>
	            
	            <div class="col-md-4">
	                <div class="person">
	                    <img src="/mod/miigle_theme/graphics/john.jpg" alt="John Pavlick" />
	                    <h4>John Pavlick, <i>Cofounder &amp; CSO</i></h4>
	                    <p class="text-muted">6+ years in software development and hardware hacking. Purdue University B.S. Computer Engineering.</p>
	                </div>
	            </div>
             
          </div><!-- //row -->
        			
        			<hr class="margin-bottom-20">
        			
	        <div class="row ">
	            
	           <div class="col-md-4 col-md-offset-2">
	                <div class="person">
	                    <img src="/mod/miigle_theme/graphics/yuki.jpg" alt="Yuki Rosene" />
	                    <h4>Yuki Rosene, <i>Front-end Developer</i></h4>
	                    <p class="text-muted">14+ years experience in Web and Print Graphic Design, with an emphasis on optimizing web platforms through user friendly design.</p>
	                </div>
	            </div> 
	            
	            <div class="col-md-4">
	                <div class="person">
	                    <img src="/mod/miigle_theme/graphics/janne.jpg" alt="Janne Koivistoinen" />
	                    <h4>Janne Koivistoinen, <i>Graphic Designer</i></h4>
	                    <p class="text-muted">9+ years in User Interface and Graphic Design. Baptized hardest name to pronounce on the Miigle team. Finnish. Wannabe Phil Dunphy.</p>
	                </div>
	            </div>
	            
	       </div><!-- //row -->
        			
        			<hr class="margin-bottom-20">
        			
	        <div class="row ">
	            
	            <div class="col-md-4 col-md-offset-2">
	                <div class="person">
	                    <img src="/mod/miigle_theme/graphics/jose.jpg" alt="Jose Manriquez" />
	                    <h4>Jose Manriquez, <i>Web Operations</i></h4>
	                    <p class="text-muted">Studying Computer Science at Texas A&M. Passionate about hackathons and programming.</p>
	                </div>
	            </div>
	            
	            <div class="col-md-4">
	                <div class="person">
	                    <img src="/mod/miigle_theme/graphics/rafa.jpg" alt="Rafael Moreno Cesar" />
	                    <h4>Rafael Moreno Cesar, <i>Web Operations</i></h4>
	                    <p class="text-muted">Hails from Mexico City. Studying Computer Science at Texas A&M.</p>
	                </div>
	            </div>
	            	            
	          </div><!-- //row -->      
        </div>
    </section>
 
    <!--============================================
    |
    |   Career
    |
    ============================================-->
    <section id="career">
        <div class="container">
    
          <h2>We Are Growing Our Team, Join Us!</h2>
          <h3 class="text-muted">Investing passion, skills, and laughter to change the world.</h3>
          
          <div class="row">
            
            <div class="col-md-10 col-md-offset-1" style="padding:0 5%;">
                <div>
                    <p>We created Miigle to use our passions and talents to create a world and company in which we'd be proud to be a part. If you have an insatiable hunger to make an impact in the world and would love to have fun while doing so <a href="#contact">contact us</a> below.</p>

                    <p>We are currently seeking passionate programmers, designers, and marketers.</p>
                    <p><a href="http://miiglers.tumblr.com/post/75132992254/weve-made-a-dent-and-are-looking-for-awesome-people" target="_blank">Learn more about how our story began, and the skills we are looking for.</a></p>

                </div>
            </div>
             
          </div><!-- //row -->
             
        </div>
    </section>
    
    <!--============================================
    |
    |   Latest News
    |
    ============================================-->
    <section id="news">
        <div class="container">         
                    
            <header>
                <h2>Latest News</h2>
                <span class="hidden-sm year">2014</span>
            </header>           
            <div class="row">
                <div class="col-md-6 left-side">                    
                    <a target="_blank" href="//miiglers.tumblr.com/post/77925879581/fueling-global-innovation-why-silicon-valley-got-it" class="post">
                        <div class="arrow"></div>
                        <h3>Fueling global innovation: Why Silicon Valley got it wrong!</h3>
                        <p>Following our demo, there was a short Q&A session with an impressive judge panel that included: 
                           Adeo Ressi, Jay Levi, Joyce Kim, Don Dodge, and Vivek Wadhwa. </p>
                    </a>
                    <a target="_blank" href="//miiglers.tumblr.com/post/65417847202/how-is-miigle-different-from-angellist-or-y-combinator" class="post">
                        <div class="arrow"></div>
                        <h3>How is Miigle different from AngelList or Y Combinator?</h3>
                        <p>As any person who’s experienced it would tell you, being the “new kid on the block” is rarely 
                            fun (perhaps not if you’re a boys band). That is until you show everyone that you’re better than...</p>
                    </a>
                </div>
                <div class="col-md-6 right-side">                   
                    <a target="_blank" href="//blog.miigle.com/post/76272504735/if-youre-an-innovator-your-life-just-got-a-whole-lot" class="post">
                        <div class="arrow"></div>
                        <h3>If you’re an innovator, your life just got a whole lot easier!</h3>
                        <p>It’s exciting times for us here at Miigle! 
                           After months of tirelessly working on our product we are finally ready to unveil 
                           the first phase to the public! 
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </section>
    
    <!--============================================
    |
    |   Contact Us
    |
    ============================================-->
    <section id="contact">
        <div class="container"> 
            
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2>Contact Us</h2>
                    <h3 class="text-muted">Got questions, comments, suggestions, or just want
                        to say Hello? Drop us a note.</h3>
                    <form class="form-validate" id="contact_form" action="/mod/miigle_theme/contact.php" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group form-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-user text-muted"></span></span>
                                    <input required type="text" class="form-control" name="name" placeholder="Full Name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group form-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-envelope text-muted"></span></span>
                                    <input required type="email" class="form-control" name="email" placeholder="Email Address">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="subject" placeholder="Subject">
                        </div>
                        <div class="form-group">
                            <textarea required class="form-control" name="message" placeholder="Message"></textarea>
                        </div>
                        <div class="row submit">
                            <div class="col-md-6">
                                <h3 class="text-muted">Slide right to submit:</h3>
                            </div>
                            <div class="col-md-6">
                                <img class="pull-right ajax-loader" src="/mod/miigle_theme/graphics/ajax-loader.gif" alt="ajax loader" />
                                <div class="noUiSlider"></div>                              
                            </div>
                        </div>
                        <div class="hidden success">
                            <h4>Thank you!</h4>
                            <p><span class="glyphicon glyphicon-ok"></span></p>
                            <p class="text-muted">Your message has been sent.</p>
                        </div>
                        <input type="hidden" name="human" class="human" value="0">
                    </form>                 
                </div>
            </div>          
                    
        </div>
    </section>
    
    
    
    <!--============================================
    |
    |   Social Links
    |
    ============================================-->
    <section id="social">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a target="_blank" href="http://www.linkedin.com/company/miigle"><img src="/mod/miigle_theme/graphics/linkedin.png" alt="Linked In"></a>
                    <a target="_blank" href="https://www.facebook.com/miiglers"><img src="/mod/miigle_theme/graphics/facebook.png" alt="Facebook"></a>
                    <a target="_blank" href="https://twitter.com/miiglers"><img src="/mod/miigle_theme/graphics/twitter.png" alt="Twitter"></a>
                    <a target="_blank" href="https://plus.google.com/111652768429793362697/posts"><img src="/mod/miigle_theme/graphics/google.png" alt="Google"></a>
                    <a target="_blank" href="http://vimeo.com/miiglers"><img src="/mod/miigle_theme/graphics/vimeo.png" alt="Vimeo"></a>
                    <a target="_blank" href="http://www.youtube.com/user/miigleTV"><img src="/mod/miigle_theme/graphics/youtube.png" alt="Youtube"></a>
                </div>
            </div>
        </div>
    </section>
    
    
    
    <!--============================================
    |
    |   Footer
    |
    ============================================-->
    <section id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="pull-left">
                        <li>Company</li>
                        <!--<li>
                            <a href="/about.php">About</a>
                        </li>-->
                        <li>
                            <a href="http://miiglers.tumblr.com/">Blog</a>
                        </li>
                    </ul>
                    <ul class="pull-left">
                        <li>Stay in Touch</li>
                        <li>
                            <a target="_blank" href="http://www.linkedin.com/company/miigle">LinkedIn</a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.facebook.com/miiglers">Facebook</a>
                        </li>
                        <li>
                            <a target="_blank" href="https://twitter.com/miiglers">Twitter</a>
                        </li>
                        <li>
                            <a target="_blank" href="https://plus.google.com/111652768429793362697/posts">Google+</a>
                        </li>
                        <li>
                            <a target="_blank" href="http://vimeo.com/miiglers">Vimeo</a>
                        </li>
                        <li>
                            <a target="_blank" href="http://www.youtube.com/user/miigleTV">Youtube</a>
                        </li>
                    </ul>
                    <button id="footer_join" data-toggle="modal" href="#modal-request" type="button" class="pull-right btn btn-danger navbar-btn">Join The Beta</button>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        &copy; 2013 <strong>Miigle</strong>. All rights reserved.
                        Header image by <a href="//www.heisenbergmedia.com/">Heisenberg Media</a>
                        <span class="pull-right">
                            <a href="/terms.html">Terms of Service</a>
                            &amp; <a href="/privacy.html">Privacy Policy</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>      
    </section>
    
    
    
    <!--============================================
    |
    |   Request Invite form
    |
    ============================================-->
    <div id="modal-request" class="modal fade">
        <div class="modal-dialog">
            <form class="form-validate" id="request_form" action="/mod/miigle_theme/request_invite.php" method="post" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title">Join Miigle!</h3>
                        <h4 class="text-muted">Promote your startup and ideas to the world.</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fname">First Name</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-user text-muted"></span></span>
                                        <input required type="text" class="form-control" name="fname" placeholder="i.e. Bruce">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lname">Last Name</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-user text-muted"></span></span>
                                        <input required type="text" class="form-control" name="lname" placeholder="i.e. Wayne">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope text-muted"></span></span>
                                <input required type="email" class="form-control" name="email" placeholder="i.e. Bruce.Wayne@WayneEnterprises.com">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="url">Website or Project URL e.g. AngelList, Kickstarter, IndieGogo, etc.<small class="text-muted">(optional)</small></label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-link text-muted"></span></span>
                                <input type="url" class="form-control" name="url" placeholder="i.e. http://angel.co/batmobile">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="comment">Comments <small class="text-muted">(optional)</small></label>
                            <textarea rows="3" name="comment" type="text" class="form-control" placeholder="i.e. I would like to introduce to the world a revolutionary vehicle that will change the way superheroes travel and fight crime. I call it the Bat Mobile."></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="error-request_form"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">                              
                                <h4 class="text-muted">Slide right to submit:</h4>
                            </div>
                            <div class="col-md-6">
                                <img class="pull-right ajax-loader" src="/mod/miigle_theme/graphics/ajax-loader.gif" alt="ajax loader" />
                                <div class="noUiSlider"></div>
                            </div>
                        </div>
                    </div>                  
                </div><!-- /.modal-content -->
                <div class="hidden success">
                        <h4>Thank you!</h4>
                        <p><span class="glyphicon glyphicon-ok"></span></p>
                        <p class="text-muted">We've received your submission. Stay tuned.</p>
                    </div>
                    <input type="hidden" name="human" class="human" value="0">
            </form>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    
    <!-- jquery validation -->
    <script src="/mod/miigle_dependencies/js/jquery.validate.js"></script>
    
    <!-- jquery form -->
    <script src="/mod/miigle_dependencies/js/jquery.form.min.js"></script>
    
    <!-- jquery mobile slider -->
    <script src="/mod/miigle_theme/vendor/nouislider/jquery.nouislider.min.js"></script>
    
    <script>
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-22362993-1']);
      _gaq.push(['_setDomainName', 'miigle.com']);
      _gaq.push(['_setAllowLinker', true]);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>
    
    <script src="/mod/miigle_theme/index.js"></script>

    <!-- AddThis Smart Layers BEGIN -->
    <!-- Go to http://www.addthis.com/get/smart-layers to customize -->
    <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52713b1954f95f2f"></script>
    <script>
      addthis.layers({
        'theme' : 'transparent',
        'share' : {
          'position' : 'right',
          'numPreferredServices' : 5
        }   
      });
    </script>
    <!-- AddThis Smart Layers END -->
    
  </body>
</html>
