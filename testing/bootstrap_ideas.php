<?php

date_default_timezone_set('UTC');

require_once('../engine/start.php');

/*
    public static function EditIdea($idea_guid,
                                    $idea_title, 
                                    $idea_type_int, 
                                    $idea_website, 
                                    $idea_category_int, 
                                    $idea_pitch, 
                                    $idea_tags_string, 
                                    $idea_description,
                                    $idea_question_community_int, 
                                    $idea_question, 
                                    $idea_videos_list,
                                    $location_dict,
                                    $stakeholders_dict,
                                    $image_files_dict) {
                                    */

global $SESSION;

$SESSION['user'] = get_user_by_username('joshfester');
$action_token_dict = MiigleGetFormToken();
$ts = $action_token_dict['ts'];
$token = $action_token_dict['token'];

set_input('__elgg_ts', $ts);
set_input('__elgg_token', $token);

$image_files_dict = array(
    'idea_icon' => '',
    'idea_images_list' => array(),
    'stakeholder_icons_dict' => array(),
);

MiigleIdea::EditIdea(0,
                     'Miigle',
                     1,
                     'http://miigle.com',
                     1,
                     'this is the miigle pitch',
                     json_encode(array('tag1', 'tag2')),
                     'this is the super long miigle description',
                     1,
                     'some miigle question',
                     json_encode(array()),
                     json_encode(MiigleLocationLookup('Los Angeles, CA, United States')),
                     array(),
                     $image_files_dict);

$SESSION['user'] = get_user_by_username('joshfester');
                                                
MiigleIdea::EditIdea(0,
                     'Facebook',
                     1,
                     'http://facebook.com',
                     1,
                     'this is the facebook pitch',
                     json_encode(array('tag1', 'tag2')),
                     'this is the super long facebook description',
                     1,
                     'some facebook question',
                     json_encode(array()),
                     json_encode(MiigleLocationLookup('San Francisco, CA, United States')),
                     array(),
                     $image_files_dict);

$SESSION['user'] = get_user_by_username('joshfester');
                                                
MiigleIdea::EditIdea(0,
                     'Dropbox',
                     1,
                     'http://facebook.com',
                     1,
                     'this is the dropbox pitch',
                     json_encode(array('tag1', 'tag2')),
                     'this is the super long dropbox description',
                     1,
                     'some dropbox question',
                     json_encode(array()),
                     json_encode(MiigleLocationLookup('San Francisco, CA, United States')),
                     array(),
                     $image_files_dict);

$SESSION['user'] = get_user_by_username('lucberlin');
        
MiigleIdea::EditIdea(0,
                     'Baugettes Inc',
                     1,
                     'http://baugettesinc.com',
                     3,
                     'this is the baugettes pitch',
                     json_encode(array('tag1', 'tag2')),
                     'this is the super long baugettes description',
                     1,
                     'some baugettes question',
                     json_encode(array()),
                     json_encode(MiigleLocationLookup('Paris, IL, France')),
                     array(),
                     $image_files_dict);

$SESSION['user'] = get_user_by_username('johnpavlick');

MiigleIdea::EditIdea(0,
                     'Tea Inc',
                     3,
                     'http://teainc.com',
                     7,
                     'this is the tea pitch',
                     json_encode(array('tag1', 'tag2')),
                     'this is the super long tea description',
                     1,
                     'some tea question',
                     json_encode(array()),
                     json_encode(MiigleLocationLookup('London, EN, United Kingdom')),
                     array(),
                     $image_files_dict);

$SESSION['user'] = get_user_by_username('johnpavlick');

$return_dict = MiigleIdea::EditIdea(0,
                     'Africa Inc',
                     2,
                     'http://africainc.com',
                     1,
                     'this is the Africa pitch',
                     json_encode(array('tag1', 'tag2')),
                     'this is the super long Africa description',
                     1,
                     'some Africa question',
                     json_encode(array()),
                     json_encode(MiigleLocationLookup('Cape Town, WC, South Africa')),
                     array(),
                     $image_files_dict);

sleep(1);
//following something
/*
$idea = get_entity($return_dict['guid']);
$SESSION['user'] = get_user_by_username('johnpavlick');
MiigleIdea::FollowIdea($SESSION['user']->getGUID(), $idea->getGUID());
 */


