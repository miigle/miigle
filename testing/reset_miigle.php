<?php

date_default_timezone_set('UTC');

require_once('../engine/settings.php');

global $CONFIG;

function TearDownMiigle() {
    global $CONFIG;
    $output_lines = array();

    $command_string = sprintf('echo drop database %s | mysql -h %s -u %s --password=%s',
        $CONFIG->dbname,
        $CONFIG->dbhost,
        $CONFIG->dbuser,
        $CONFIG->dbpass);
    exec($command_string, $output_lines);
    array_map('var_dump', $output_lines);

    $command_string = sprintf('echo create database %s | mysql -h %s -u %s --password=%s',
        $CONFIG->dbname,
        $CONFIG->dbhost,
        $CONFIG->dbuser,
        $CONFIG->dbpass);
    exec($command_string, $output_lines);
    array_map('var_dump', $output_lines);

    $command_string = 'sudo rm -rf ../../data';
    exec($command_string, $output_lines);
    array_map('var_dump', $output_lines);

    $command_string = 'mkdir ../../data';
    exec($command_string, $output_lines);
    array_map('var_dump', $output_lines);

    $command_string = 'sudo chown "$USER":www-data ../../data';
    exec($command_string, $output_lines);
    array_map('var_dump', $output_lines);

    $command_string = 'sudo chmod -R g+w ../../data';
    exec($command_string, $output_lines);
    array_map('var_dump', $output_lines);

    $command_string = 'wget --spider http://localhost 2> /dev/null';
    exec($command_string, $output_lines);
    array_map('var_dump', $output_lines);
    sleep(2);

    $command_string = 'phantomjs bootstrap.js';
    var_dump($command_string);
    exec($command_string, $output_lines);
    array_map('var_dump', $output_lines);
    sleep(2);

    return; //investigate this later
    //$command_string = 'wget --spider http://localhost/upgrade.php 2> /dev/null';
    //$command_string = 'wget --no-check-certificate --spider https://localhost/upgrade.php';
    var_dump($command_string);
    exec($command_string, $output_lines);
    array_map('var_dump', $output_lines);
    sleep(2);
}

TearDownMiigle();
sleep(5);

