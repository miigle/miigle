<?php

date_default_timezone_set('UTC');

function PermissionsOverride($hook, $entity_type, $returnvalue, $params) {
    if ($entity_type == 'user') {
        return true;
    } else {
        return $returnvalue;
    }
}

function PreventForward() { 
    return false; 
}

require_once('../engine/start.php');


$plugin_hooks = array(
    'action_gatekeeper:permissions:check',
    'permissions_check:metadata', 
    'permissions_check', 
    'container_permissions_check'
);

foreach ($plugin_hooks as $plugin_hook) {
    register_plugin_hook($plugin_hook, 'all', 'PermissionsOverride');
}
register_plugin_hook('forward', 'system', 'PreventForward', 0);

$full_names = array(
    'Kevin Systrom', 
    'Evan Sharp',
    'Steve Jobs',
    'Sir Ken Robinson',
    'Mark Pincus',
    'Reid Hoffman',
    'Sean Combs',
    'Beyonce Knowles',
    'Shawn Carter',
    'Ashton Kutcher',
    'Justin Timberlake',
    'Peter Thiel',
    'Dave McClure',
    'Pete Cashmore',
    'Angela Benton',
    'Guion Bluford',
    'Mae Jemison',
    'Michael Jackson',
    'George Lucas',
    'Walt Disney',
    'Steven Spielberg',
    'Tim Cook',
    'Tim Westergren',
    'Will Glaser',
    'Jon Kraft',
    'Larry Page',
    'Sergey Brin',
    'Marissa Mayer',
    'Jerry Yang',
    'David Filo',
    'Steve Case',
    'Tom Anderson',
    'Michael Arrington',
    'Saran Kaba Jones',
    'Marie Curie',
    'Ada Lovelace',
    'Nelson Mandela',
    'Robin Chase',
    'Angelina Jolie',
    'Marilyn Monroe',
    'Rashmi Sinha',
    'Gabriel Chanel',
    'Al Gore',
);

foreach ($full_names as $full_name) {
    global $CONFIG;
    list($first_name, $last_name) = explode(' ', $full_name);
    $username = strtolower($first_name . '.' . $last_name);

    set_input('password', 'DEMO2014');
    set_input('password2', 'DEMO2014');
    set_input('email', "$username@miigle.com");
    set_input('fname', $first_name);
    set_input('lname', $last_name);

    require($CONFIG->pluginspath . 'loginbyemailonly/actions/register.php');


    /*

    $guid = register_user($username, 'password', $user_name, "$username@localhost.localdomain", false, 0, 0);
    set_user_validation_status($guid, TRUE, 'email');
    $user = get_entity($guid);
    $user->enable();
    $user->set('fname', $name_parts[0]);
    $user->set('lname', $name_parts[1]);
    MiigleUser::BootstrapElggUser($user);

    var_dump($user_dict);
    $user_dict['user_icon_filename'] = sprintf('%s/test_data/%s.jpg', getcwd(), strtolower($name_parts[0]));
    $result = MiigleUser::EditUser($user_dict);
    var_dump($result);
     */
}

global $ENTITY_SHOW_HIDDEN_OVERRIDE;
$ENTITY_SHOW_HIDDEN_OVERRIDE = true;
$users_list = elgg_get_entities(array(
    'type' => 'user',
    'subtype' => 'miigle_user',
    'limit' => 99999
));
$ENTITY_SHOW_HIDDEN_OVERRIDE = false;

foreach ($users_list as $user) {
    $user->enable();
    set_user_validation_status($user->guid, TRUE, 'email');
    $user->save();

}

foreach ($plugin_hooks as $plugin_hook) {
    unregister_plugin_hook($plugin_hook, 'all', 'PermissionsOverride');
}
unregister_plugin_hook('forward', 'system', 'PreventForward');
