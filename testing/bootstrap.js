var page = require('webpage').create();
var enable_all_link;

var steps = [
    function() {
        page.open('http://localhost/');
    },
    function() {
        page.evaluate(function() {
            document.getElementsByName('sitename')[0].value = 'MiigleDev';
            document.getElementsByName('sitedescription')[0].value = 'Miigle Development Site';
            document.getElementsByName('siteemail')[0].value = 'miigle_dev_email@localhost.localdomain';
            document.getElementsByName('wwwroot')[0].value = 'http://localhost/';
            var miigle_dir = document.getElementsByName('path')[0].value;
            miigle_dir = miigle_dir.slice(0, -1);
            var dir_parts = miigle_dir.split('/');
            dir_parts.pop();
            var pwd = dir_parts.join('/');
            var data_dir = pwd + '/data/';
            document.getElementsByName('dataroot')[0].value = data_dir;
        });
    },
    function() {
        page.evaluate(function() {
            var form = document.getElementsByTagName('form')[0];
            form.submit();
            return form;
        });
    },
    function() {
        page.open('http://localhost/pg/register');
    },
    function() {
        page.evaluate(function() {
            document.getElementsByName('name')[0].value = 'Administrator';
            document.getElementsByName('email')[0].value = 'admin@localhost.localdomain';
            document.getElementsByName('username')[0].value = 'admin';
            document.getElementsByName('password')[0].value = 'password';
            document.getElementsByName('password2')[0].value = 'password';
        });
    },
    function() {
        page.evaluate(function() {
            document.getElementsByTagName('form')[0].submit.click();
        });
    },
    function() {
        page.open('http://localhost/index_old.php');
    },
    function() {
        page.evaluate(function() {
            document.getElementsByName('username')[0].value = 'admin';
            document.getElementsByName('password')[0].value = 'password';
            document.getElementsByTagName('form')[0].submit();
        });
    },
    function() {
        enable_all_link = page.evaluate(function() {
            return document.getElementsByClassName('enableallplugins')[0].href;
        });
    },
    function() {
        page.open(enable_all_link);
    },
    function() {
        phantom.exit();
    },
]

var i = 0;
setInterval(function() {
    steps[i]();
    i += 1;
}, 200);
