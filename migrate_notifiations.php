<?php
date_default_timezone_set('UTC');

function PermissionsOverride($hook, $entity_type, $returnvalue, $params) {
    if ($entity_type == 'user') {
        return true;
    } else {
        return $returnvalue;
    }
}

function PreventForward() { 
    return false; 
}

require_once('engine/start.php');

$plugin_hooks = array(
    'action_gatekeeper:permissions:check',
    'permissions_check:metadata', 
    'permissions_check', 
    'container_permissions_check'
);

foreach ($plugin_hooks as $plugin_hook) {
    register_plugin_hook($plugin_hook, 'all', 'PermissionsOverride');
}
register_plugin_hook('forward', 'system', 'PreventForward', 0);

elgg_set_ignore_access(true);

$notifications = elgg_get_entities(array(
    'type'=>'object',
    'subtype'=>'notification',
    'limit'=>9999999
));

$time_created_dict = array();

foreach ($notifications as $notification) {
    unset($user_guid);
    unset($object_guid);
    unset($subject_guid);
    unset($notification_type);
    unset($notification_payload);

    $username = preg_replace('/^<a href=".*\/pg\/profile\//', '', $notification->description);
    $username = preg_replace('/".*$/', '', $username);

    if (preg_match('/is following you$/', $notification->description)) {
        $notification_type = 'follow';
    } else if (preg_match('/^.*cheered for /', $notification->description)) {
        $notification_type = 'cheer';
        $idea_title = preg_replace('/^.*cheered for /', '', $notification->description);
    } else if (preg_match('/^.*rated /', $notification->description)) {
        $notification_type = 'rate';
        $idea_title = preg_replace('/^.*rated /', '', $notification->description);
    } else {
        throw new Exception('I do not understand this');
    }
    $subject = MiigleUser::GetUserFromUsername($username);

    $user_guid = intval($notification->owner_guid);
    $subject_guid = $subject['guid'];
    //have nottype

    global $CONFIG;

    if ($notification_type != 'follow') {
        $results = elgg_get_entities(array(
            'types' => 'object',
            'subtypes' => 'idea',
            'wheres' => array(
                "oe.title='$idea_title'",
            ),
            'joins' => array(
                "JOIN {$CONFIG->dbprefix}objects_entity oe ON e.guid = oe.guid",
            ),
        ));

        if (!sizeof($results)) {
            throw new Exception('I do not understand this');
        }

        $idea_dict = MiigleIdea::GetIdea($results[0]->guid);
        $object_guid = $idea_dict['guid'];
    } else {
        $object_guid = $user_guid;
    }

    //have oguid
    //
    if ($notification_type == 'rate') {
        $river_items = get_river_items($subject_guid, $object_guid, 
                                       $subject_relationship='', $type='',
                                       $subtype='', 
                                       $action_type='user:rate::idea', 
                                       $limit = 999999, $offset = 0, 
                                       $posted_min = 0, $posted_max = 0);
        if (sizeof($river_items) > 0) {
            $river_item = TransformRiverItem(array_shift($river_items));
            if (abs($notification->time_created - $river_item['posted']) > 5) {
                $notification->delete();
                continue;
            }
            $rating = get_entity($river_item['action_payload'][0]);
            $rating_dict = $rating->toDict();
            $rating_dict = $rating_dict['rating_dict']; //lol names
            //fuck functional programming for now
            $sum = 0;
            $total = 0;
            foreach ($rating_dict as $rating_name=>$rating) {
                $sum += $rating;
                $total += 10;
            }
            $num_stars = 5 * ($sum / $total);
            $notification_payload = json_encode(array(
                'idea_rating' => $num_stars));
        }
    }

    if (!isset($notification_payload)) {
        $notification_payload = '';
    }

    //var_dump($user_guid, $subject_guid, $object_guid, $notification_type, $notification_payload);
    $x = array(
        'user_guid' => $user_guid,
        'subject_guid' => $subject_guid,
        'object_guid' => $object_guid,
        'notification_type' => $notification_type,
        'notification_payload' => $notification_payload,
    );

    $new_notification = MiigleNotification::CreateNotification(
                                           $user_guid, $subject_guid, 
                                           $object_guid, $notification_type,
                                           $notification_payload);
    $time_created_dict[$new_notification->guid] = $notification->time_created;
    $notification->delete();

    unset($user_guid);
    unset($object_guid);
    unset($subject_guid);
    unset($notification_type);
    unset($notification_payload);
}

var_dump($time_created_dict);

foreach ($time_created_dict as $guid=>$time_created) {
    update_data("UPDATE {$CONFIG->dbprefix}entities SET time_created=$time_created WHERE guid=$guid;");
}
