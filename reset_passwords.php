<?php
/* This sets every user's password as the admin's password */
date_default_timezone_set('UTC');

function PermissionsOverride($hook, $entity_type, $returnvalue, $params) {
    if ($entity_type == 'user') {
        return true;
    } else {
        return $returnvalue;
    }
}

function PreventForward() { 
    return false; 
}

require_once('engine/start.php');

$plugin_hooks = array(
    'action_gatekeeper:permissions:check',
    'permissions_check:metadata', 
    'permissions_check', 
    'container_permissions_check'
);

foreach ($plugin_hooks as $plugin_hook) {
    register_plugin_hook($plugin_hook, 'all', 'PermissionsOverride');
}
register_plugin_hook('forward', 'system', 'PreventForward', 0);

elgg_set_ignore_access(true);

$users = elgg_get_entities(array(
    'type'=>'user',
    'limit'=>9999999
));

$admin_user = array_shift(array_filter($users, function($user) {
    return $user->username === 'admin';
}));

foreach ($users as $user) {
    $user->salt = $admin_user->salt;
    $user->password = $admin_user->password;
    $user->save();
}
