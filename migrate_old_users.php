<?php
date_default_timezone_set('UTC');
function PermissionsOverride($hook, $entity_type, $returnvalue, $params) {
    if ($entity_type == 'user') {
        return true;
    } else {
        return $returnvalue;
    }
}

function PreventForward() { 
    return false; 
}

require_once('engine/start.php');


$plugin_hooks = array(
    'action_gatekeeper:permissions:check',
    'permissions_check:metadata', 
    'permissions_check', 
    'container_permissions_check'
);

foreach ($plugin_hooks as $plugin_hook) {
    register_plugin_hook($plugin_hook, 'all', 'PermissionsOverride');
}
register_plugin_hook('forward', 'system', 'PreventForward', 0);

$user = get_entity(6);
$user->delete();
