$(document).ready(function() {
    $('#login-form').submit(function(event) {
        event.preventDefault();
        LoginEmailUser();
    });
    $('#register-form').submit(function(event) {
        event.preventDefault();
        RegisterEmailUser();
    });
});

//register_type = 'email', 'facebook', or 'linkedin'
var BLANK_REGISTER_DICT = {
    first_name: '', 
    last_name: '', 
    email_address: '',
    password: '', 
    password2: '',
    register_type: '', 
    facebook_signed_request_string: '',
}
var BLANK_LOGIN_DICT = {
    email_address: '',
    password: '', 
    register_type: '', 
    facebook_signed_request_string: '',
}

function GenerateRandomPassword() {
    return [Math.random().toString(36), Math.random().toString(36)].join('');
}

function RedirectToIdeaFeed() {
    window.location.replace('http://' + window.location.host + '/pg/miigle');
}

function ParseURLArgs() {
    var args = {};
    var query_string = window.location.search.substring(1);
    var queries_list = query_string.split('&');

    if (query_string === '') {
        return args;
    }

    var args = queries_list.reduce(function(args, query) {
        var parts = query.split('=');
        args[parts[0]] = parts[1];
        return args;
    }, Object.create(null));

    return args;
}

function RegisterUser(args_dict) {
    var url_args = ParseURLArgs();
    Object
        .keys(url_args)
        .forEach(function(key) {
            args_dict[key] = url_args[key];
        });

    return;
    $.ajax({
        type: 'POST',
        url: '/services/api/rest/json?method=users.register',
        data: args_dict,
        complete: function(jqxhr, text_status) {
            var response_dict = JSON.parse(jqxhr.responseText);

            if (response_dict.status == 0) {
                if (args_dict.register_type == 'email') {
                    alert('Check your email, dude');
                } else {
                    RedirectToIdeaFeed();
                }
            } else {
                alert(response_dict.message);
            }
        },
    });
}

function LoginUser(args_dict) {
    $.ajax({
        type: 'POST',
        url: '/services/api/rest/json?method=users.login',
        data: args_dict,
        complete: function(jqxhr, text_status) {
            var response_dict = JSON.parse(jqxhr.responseText);

            if (response_dict.status == 0) {
                RedirectToIdeaFeed();
            } else {
                alert(response_dict.message);
            }
        },
    });
}

function RegisterFacebookUser(me_dict, signed_request_string) {
    alert('Coming soon!');
    return;
    var args_dict = Object.create(BLANK_REGISTER_DICT);

    args_dict.first_name = me_dict.first_name;
    args_dict.last_name = me_dict.last_name;
    args_dict.email_address = me_dict.email;
    args_dict.password = GenerateRandomPassword();
    args_dict.password2 = args_dict.password;
    args_dict.register_type = 'facebook';
    args_dict.facebook_signed_request_string = signed_request_string;

    RegisterUser(args_dict);
}
function RegisterLinkedInUser(linkedin_dict) {
    alert('Coming soon!');
    return;
    var args_dict = Object.create(BLANK_REGISTER_DICT);

    args_dict.first_name = linkedin_dict.firstName;
    args_dict.last_name = linkedin_dict.lastName;
    args_dict.email_address = linkedin_dict.emailAddress;
    args_dict.password = GenerateRandomPassword();
    args_dict.password2 = args_dict.password;
    args_dict.register_type = 'linkedin';

    RegisterUser(args_dict);
}
function RegisterEmailUser() {
    alert('Coming soon!');
    return;
    var args_dict = Object.create(BLANK_REGISTER_DICT);

    //if $('#register-form').valid();

    args_dict.first_name = $('input[name="fname"]').val();
    args_dict.last_name = $('input[name="lname"]').val();
    args_dict.email_address = $('input[name="email"]').val();
    args_dict.password = $('input[name="password"]').val();
    args_dict.password2 = $('input[name="password2"]').val();
    args_dict.register_type = 'email';

    RegisterUser(args_dict);
}

/*
function LoginFacebookUser(me_dict, signed_request_string) {
    var args_dict = Object.create(BLANK_LOGIN_DICT);

    args_dict.email_address = me_dict.email;
    args_dict.password = GenerateRandomPassword();
    args_dict.register_type = 'facebook';
    args_dict.facebook_signed_request_string = signed_request_string;

    LoginUser(args_dict);
}
function LoginLinkedInUser(linkedin_dict) {
    var args_dict = Object.create(BLANK_LOGIN_DICT);

    args_dict.email_address = me_dict.email;
    args_dict.password = GenerateRandomPassword();
    args_dict.register_type = 'linkedin';

    LoginUser(args_dict);
}
*/
function LoginEmailUser() {
   var args_dict = Object.create(BLANK_LOGIN_DICT);

    //if $('#register-form').valid();

    args_dict.email_address = $('input[name="email"]').val();
    args_dict.password = $('input[name="password"]').val();
    args_dict.register_type = 'email';

    LoginUser(args_dict);   
}

function AuthenticateWithLinkedIn(callback) {
    alert('Coming soon!');
    return;
    IN.User.authorize(function() {
        IN.API.Profile('me')
            .fields('firstName', 'lastName', 'emailAddress')
            .result(function(me) {
                var linkedin_dict = me.values[0];
                callback(linkedin_dict);
            });
    });
}

function AuthenticateWithFacebook(callback) {
    alert('Coming soon!');
    return;
    FB.login(function(login_response) {
        FB.api('/me', function(me_response) {
            if (login_response.authResponse != null) {
                callback(me_response, login_response.authResponse.signedRequest);
            } // else they clicked "Cancel" on the facebook popup
        });
    }, 
    {scope: 'email'});
}

//down from here needs to be exposed
function RegisterWithLinkedIn() {
    AuthenticateWithLinkedIn(RegisterLinkedInUser);
}
function LoginWithLinkedIn() {
    AuthenticateWithLinkedIn(RegisterLinkedInUser);
}
function RegisterWithFacebook() {
    AuthenticateWithFacebook(RegisterFacebookUser);
}
function LoginWithFacebook() {
    AuthenticateWithFacebook(RegisterFacebookUser);
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : window.FACEBOOK_APP_ID, 
        status     : true, // check login status
        cookie     : true, // enable cookies to allow the server to access the session
        xfbml      : true  // parse XFBML
    });
};
