<?php require('login_register_header.php'); ?>
    <!--[if lte IE 9]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <!--============================================
    |
    | Static navbar
    |
    ============================================-->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/index.php"><img src="/mod/miigle_theme/graphics/miigle-logo.png" alt="Miigle" /></a>
        </div>
        <div class="navbar-collapse collapse">
          <a href="/pg/login" class="pull-right btn btn-default btn-inverse navbar-btn">Log in</a>
          <button id="header_join" data-toggle="modal" href="#modal-request" type="button" class="pull-right btn btn-danger navbar-btn">Join The Beta</button>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <section id="splash">
      <div class="container">      
        <div class="row">
          <div class="col-md-5" style="float: none; margin: 0 auto;">
            
                <style type="text/css">
                    .account_forgottenPassword {
                        text-align:center;
                    }
                    #forgotPassword {
                        max-width:300px;
                        margin:0 auto;
                    }
                </style>
                <div class="account_forgottenPassword" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                <img src="/mod/miigle_theme/graphics/forgot_password.jpg" alt="Forgot Password">
                            </p>        
                            <h2>What was it!?</h2>    
                            <p>Enter your email address below to reset your password</p>
                            <form id="forgotPassword" action="/action/loginbyemailonly/requestnewpassword" method="post">
                                <input type="hidden" name="__elgg_token" value="" id="__elgg_token" />
                                <input type="hidden" name="__elgg_ts" value="" id="__elgg_ts" />
                                <div class="form-group">
                                    <input type="text" name="username" class="form-control" placeholder="Email">
                                </div>                
                                <button type="submit" class="btn btn-default">Reset My Password</button>
                            </form>
                        </div>
                    </div>
                </div>
                <script>
                  (function() {
                    $.ajax({
                      url:'/services/api/rest/json/?method=form.get_token',
                      dataType: "json",
                      success:function(xhr){
                        var ts = xhr.result.ts;
                        var token = xhr.result.token;
                        $('form #__elgg_ts').val(ts);
                        $('form #__elgg_token').val(token);
                      }
                    });
                  })();
                </script>        
          </div>
        </div>
      </div>
    </section>
<?php require('login_register_footer.php'); ?>
