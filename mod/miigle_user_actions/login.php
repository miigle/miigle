<?php require('login_register_header.php'); ?>
    <!--[if lte IE 9]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <!--============================================
    |
    | Static navbar
    |
    ============================================-->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/index.php"><img src="/mod/miigle_theme/graphics/miigle-logo.png" alt="Miigle" /></a>
        </div>
        <div class="navbar-collapse collapse">
          <!--<a href="/mod/miigle_user_actions/login.html" class="pull-right btn btn-default btn-inverse navbar-btn">Log in</a>-->
          <button id="header_join" data-toggle="modal" href="#modal-request" type="button" class="pull-right btn btn-danger navbar-btn">Join The Beta</button>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <section id="splash">
      <div class="container">      
        <div class="row">
          <div class="col-md-5" style="float: none; margin: 0 auto;">
            <h2>Welcome Back!</h2>
            <p class="text-muted">Please login below.</p>
            <form id="login-form">
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-envelope"></span>
                  </span>
                  <input type="text" name="email" class="form-control" placeholder="Enter email address" />
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-lock"></span>
                  </span>
                  <input type="password" name="password" class="form-control" placeholder="Enter password" />   
                </div>
              </div>
              <div class="form-group hidden">
                <div class="checkbox pull-left">
                  <input type="checkbox" id="persistent" name="persistent" checked="checked"> 
                  <label for="persistent"></label>
                </div>
                <span class="pull-left remember-me">Remember Me</span>
              </div>
              <div class="clearfix"></div>
              <div class="form-group submit">
                <input type="submit" class="btn btn-lg btn-default btn-miigle btn-block" value="SIGN IN!"/>
                <p style="margin-bottom: 0px; padding-top: 15px; font-size: 75%;">
                  By clicking SIGN IN, you agree to Miigle's <a href="/terms.html">Terms of Use</a> and <a href="/privacy.html">Privacy Policy</a>.
                </p>
              </div>
              <a href="/pg/forgotpassword">Forgot it, Help!</a>
            </form>
            <!--
            <div class="row">
              <div class="col-md-10 col-md-offset-1" style="margin-bottom: 15px">
                &nbsp;
                <div style="height: 2px; background-color: #ccc; text-align: center">
                  <span class="text-muted" style="background-color: white; position: relative; top: -0.7em;">
                    &nbsp;or&nbsp;
                  </span>
                </div>
                &nbsp;
              </div>
            </div>

            <div class="row" id="social-login-button-div">
              <div class="col-md-6" style="padding-right: 5px">
                <input type="image" src="/mod/miigle_theme/graphics/signinwithfacebook.png"
                       onclick="LoginWithFacebook()"/>
              </div>
              <div class="col-md-6" style="padding-left: 5px">
                <input type="image" src="/mod/miigle_theme/graphics/signinwithlinkedin.png"
                       onclick="LoginWithLinkedIn()"/>
              </div>
            </div>
            -->
          </div>
        </div>
      </div>
    </section>
<?php require('login_register_footer.php'); ?>
