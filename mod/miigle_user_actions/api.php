<?php
global $CONFIG;
require_once($CONFIG->pluginspath . 'miigle_profile/api.php');
require_once($CONFIG->pluginspath . 'miigle_cache/api.php');

// Log out function copied from Elgg default actions
function MiigleLogout() {
   return logout();
}

/**
 * Get tokens for a <form>
 *
 * @return array() with timestamp and token
 */
function MiigleGetFormToken() {
    $ts = time();
    $token = generate_action_token($ts);
    return array('ts'=>$ts, 'token'=>$token);
}

// Expose MiigleGetFormToken()
expose_function('form.get_token', 
    'MiigleGetFormToken', 
     null,
     'Returns a security token for forms',
     'GET',
     false,
     false
);

//maps code => is_valid
class InviteCodeKeyValueStore extends NamespacedKeyValueStore {
    public function __construct() {
        parent::__construct('invite_codes', new ElggDatabaseKeyValueStore());
    }
}

function GenerateInviteCode() {
    $code = md5(time() . rand());

    $kv_store = new InviteCodeKeyValueStore();
    $kv_store->set($code, true);

    return $code;
}

function IsInviteCodeValid($code) {
    $kv_store = new InviteCodeKeyValueStore();
    return ($kv_store->get($code) == true);
}

function UseInviteCode($code) {
    $kv_store = new InviteCodeKeyValueStore();

    if (IsInviteCodeValid($code)) {
        $kv_store->set($code, false);
        return true;
    } else {
        return false;
    }
}

function GenerateInviteURL() {
    global $CONFIG;
    return $CONFIG->url . 'pg/register?code=' . GenerateInviteCode();
}
expose_function('secret.generate_invite_url', 'GenerateInviteURL',
    array(),
    '',
    'GET',
    false,
    false
);

function RegisterUser($first_name, $last_name, $email_address, $password, $password2, $register_type, $social_login_id='', $code='') {
    global $CONFIG;
    $first_name = trim($first_name);
    $last_name = trim($last_name);
    $email_address = trim($email_address);
    $full_name = "$first_name $last_name";
    $password = trim($password);
    $password2 = trim($password2);

    if (!IsInviteCodeValid($code)) {
        //throw new Exception('Invalid invite code');
    }

    if (!in_array($register_type, array('email', 'facebook', 'linkedin', 'invite'))) {
        throw new Exception("Invalid registration type $register_type");
    }
    if (strlen($email_address) < 3 || !is_email_address($email_address)) {
        throw new Exception("Invalid email address $email_address");
    }
    if ($password !== $password2) {
        throw new Exception("Passwords do not match");
    }
    if (strlen($password) < 3) {
        throw new Exception("Password too short");
    }
    if (strlen($first_name) < 1) {
        throw new Exception("First name too short");
    }
    if (strlen($last_name) < 1) {
        throw new Exception("Last name too short");
    }

    //check to see if we're adding 
    $existing_user = get_user_by_email($email_address);
    if ($existing_user) {
        if ($register_type == 'email') {
            throw new Exception("Email address $email_address already registered");
        } else {
            try {
                return LoginUser($email_address, $password, $register_type, $social_login_id);
            } catch (Exception $e) {}
        }
    }
    //generate username
    $base_username = $first_name . $last_name;
    $base_username = strToLower(preg_replace("/[^a-zA-Z]/", "", $base_username));
    $user_number = '';

    do {
        $username = $base_username . $user_number;
        $user_number += 1;
    } while (get_user_by_username($username) !== false);

    //actually register the user
    $guid = register_user($username, $password, $full_name, $email_address, true);

    if (!$guid) {
        throw new Exception("Unknown error in registration");
    }

    $new_user = get_entity($guid);
    $new_user->fname = $first_name;
    $new_user->lname = $last_name;
    if ($social_login_id) {
        $new_user->set($register_type . '_id', strval($social_login_id));
    }
    MiigleUser::BootstrapElggUser($new_user);
    
    switch ($register_type) {
        case 'email':
            request_user_validation($guid);
            $new_user->disable('new_user', false);
            
        case 'invite':
            $new_user->enable();
            set_user_validation_status($new_user->guid, true, $register_type);
            $new_user->save();
        default:
            $new_user->enable();
            set_user_validation_status($new_user->guid, true, $register_type);
            $new_user->save();
            login($new_user, true);
    }

    $new_user->save();

    UseInviteCode($code);

    return $new_user->guid;
}

function RegisterUserProxy($first_name, $last_name, $email_address, $password, $password2, $register_type, $facebook_signed_request_string='', $code='') {
    global $CONFIG;

    switch ($register_type) {
        case 'email':
            RegisterUser($first_name, $last_name, $email_address, $password, $password2, $register_type, '', $code);
            break;

        case 'facebook':
            $facebook_id = ValidateFacebookAuthResponseString($facebook_signed_request_string);

            if ($facebook_id) {
                RegisterUser($first_name, $last_name, $email_address, $password, $password2, $register_type, $facebook_id, $code);
            } else {
                throw new Exception('Invalid Facebook signedRequest');
            }
            break;

        case 'linkedin':
            $linkedin_oauth_dict = json_decode($_COOKIE["linkedin_oauth_$CONFIG->LINKEDIN_API_KEY"], true);
            $linkedin_id = ValidateLinkedInOauthDict($linkedin_oauth_dict);

            if ($linkedin_id) {
                RegisterUser($first_name, $last_name, $email_address, $password, $password2, $register_type, $linkedin_id, $code);
            } else {
                throw new Exception('Invalid LinkedIn OAuth cookie');
            }
            break;

        default: 
            throw new Exception("Invalid register type $register_type");
            break;
    }

    return true;
}
expose_function('users.register', 'RegisterUserProxy',
    array(
        'first_name' => array('type'=>'string', 'required'=>true),
        'last_name' => array('type'=>'string', 'required'=>true),
        'email_address' => array('type'=>'string', 'required'=>true),
        'password' => array('type'=>'string', 'required'=>true),
        'password2' => array('type'=>'string', 'required'=>true),
        'register_type' => array('type'=>'string', 'required'=>true),
        'facebook_signed_request_string' => array('type'=>'string', 'required'=>false, 'default'=>''),
        'code' => array('type'=>'string', 'required'=>false, 'default'=>''),
    ),
    'Register a new user',
    'POST',
    false,
    false
);

function LoginUser($email_address, $password, $register_type, $social_login_id='') {
    global $CONFIG;
    $email_address = trim($email_address);
    $password = trim($password);

    if ($register_type == 'email') {
        $user = authenticate_by_email($email_address, $password); 
        
        if ($user) {
            return login($user, true);
        } else {
            throw new Exception('Invalid email/password');
        }
    } else { //social login
        $user = elgg_get_entities_from_metadata(array(
            'type' => 'user',
            'subtype' => 'miigle_user',
            'metadata_name_value_pairs'=>array(
                array(
                    'name' => $register_type . '_id',
                    'value' => $social_login_id,
                ),
            ),
        ));

        if ($user) {
            $user = $user[0]; //fuck off elgg
            return login($user, true);
        } else {
            throw new Exception("What am I doing? $register_type $social_login_id");
        }
    }

    error_log('how did I fall to this?');
}

function LoginUserProxy($email_address, $password, $register_type, $facebook_signed_request_string='') {
    switch ($register_type) {
        case 'email':
            return LoginUser($email_address, $password, $register_type);

        case 'facebook':
            $facebook_id = ValidateFacebookAuthResponseString($facebook_signed_request_string);

            if ($facebook_id) {
                return RegisterUser($email_address, $password, $register_type, $facebook_id);
            } else {
                throw new Exception('Invalid Facebook signedRequest');
            }
            break;

        case 'linkedin':
            $linkedin_oauth_dict = json_decode($_COOKIE["linkedin_oauth_$CONFIG->LINKEDIN_API_KEY"], true);
            $linkedin_id = ValidateLinkedInOauthDict($linkedin_oauth_dict);

            if ($linkedin_id) {
                return RegisterUser($email_address, $password, $register_type, $linkedin_id);
            } else {
                throw new Exception('Invalid LinkedIn OAuth cookie');
            }
            break;

        default: 
            throw new Exception("Invalid register type $register_type");
    }
}

expose_function('users.login', 'LoginUserProxy',
    array(
        'email_address' => array('type'=>'string', 'required'=>true),
        'password' => array('type'=>'string', 'required'=>true),
        'register_type' => array('type'=>'string', 'required'=>true),
        'facebook_signed_request_string' => array('type'=>'string', 'required'=>false),
    ),
    'Login a user',
    'POST',
    false,
    false
);

/**
 * Modified version of Elgg's send_new_password_request()
 *
 * @param int $user_guid
 */
function MiigleSendApprovalPasswordLink($user_guid) {
    global $CONFIG;

    $user_guid = (int)$user_guid;
    $user = get_entity($user_guid);
    
    if ($user) {
        // generate code
        $code = generate_random_cleartext_password();
        //create_metadata($user_guid, 'conf_code', $code,'', 0, ACCESS_PRIVATE);
        set_private_setting($user_guid, 'passwd_conf_code', $code);

        // generate link
        $link = $CONFIG->site->url . "pg/newpassword?u=$user_guid&c=$code";

        // generate email
        $subject = "You’re Invited to Miigle!";
        $header = "The wait is <strong>over</strong>.";
        $email = <<<EOD
        
            Dear {$user->fname},

            I’m delighted to extend to you an invitation to join our global community of innovators! 

            Miigle was created to make it easier for innovators like yourself to showcase their startups or projects and remove the wasted time, money, and energy it takes for you to connect with potential contributors, investors, and users worldwide.

            Here’s how to get started:

            1. Click on the following link: <a href="{$link}">{$link}</a>
            2. Choose your password and SIGN IN
            3. Create your account, post your startup, invite friends, and start Miigling!

            We’re still in Private Beta so your suggestions are extremely valuable to us. Please use the “Feedback” icon at the bottom right corner of the site to share your thoughts with us. 

            You may also contact me directly via email at <a href="mailto:luc.berlin@miigle.com">luc.berlin@miigle.com</a>.

            On behalf of the Miigle team, Thank You. We look forward to Miigling with you and contributing to your success.

            Warmly,

            Luc Berlin
            Founder & CEO at <a href="http://www.miigle.com">Miigle.com</a>
EOD;

        return notify_user($user->guid, $CONFIG->site->guid, $subject, $email, array('header'=>$header));
    } else throw new Exception("Could not get user $user_guid");
}

/**
 * Approve a user who has requested an invite. Register them as a user, validate them,
 * and send them an email
 *
 * @param string $email
 */
function ApproveUser($email) {
    // Check for existing user
    $existing_user = get_user_by_email($email);    
    if ($existing_user) {
        //$existing_user[0]->delete(); //this needs to get removed after testing
        throw new Exception("User with email $email already exists");
    } 
    
    // Get the user info from invite_requests table
    $sql = "SELECT * FROM invite_requests WHERE email = '$email' LIMIT 1";
    $res = mysql_query($sql) or die(mysql_error());
    
    // Make sure we found a user
    $rows = mysql_num_rows($res);
    if (!$rows) {
        //throw new Exception("No invite request with email $email found");
    } else {
        while ($row = mysql_fetch_assoc($res)) {
            $first_name = $row['fname'];
            $last_name = $row['lname'];
        }    
        mysql_free_result($res);
    }
    
    
    // Create a password and user account
    $password = generate_random_cleartext_password();
    $user_guid = RegisterUser($first_name, $last_name, $email, $password, $password, 'invite');
    
    // Send the bastard a link to reset their password
    if ($user_guid) {
        return MiigleSendApprovalPasswordLink($user_guid);
    } else throw new Exception("Could not get user");
}

expose_function('users.approve', 'ApproveUser',
    array(
        'email' => array('type'=>'string', 'required'=>true)
    ),
    'Approve a user who has requested an invite',
    'POST',
    false,
    false
);

/**
 * Approve multiple users who have requested an invite
 *
 * @param string $email_list
 */
function ApproveBulkUsers($emails) {
    $email_list = array_filter(explode(",", $emails), function($email) {
        return $email != '';
    });

    foreach ($email_list as $email) {
        ApproveUser(trim($email));
    }
    return true;
}

expose_function('users.approve.bulk', 'ApproveBulkUsers',
    array(
        'emails' => array('type'=>'string', 'required'=>true)
    ),
    'Approve multiple users who have requested an invite',
    'POST',
    false,
    false
); 

/**
 * Update a user password via a private code sent in email
 *
 * @param string $user_guid
 * @param string $password1
 * @param string $password2
 * @param string $passwd_conf_code
 */
function MiigleUpdatePassword($user_guid, $password1, $password2, $passwd_conf_code) {
    if ($password1 !== $password2) {
        throw new Exception("Passwords do not match");
    }
    
    $user = get_entity($user_guid);
    $saved_code = get_private_setting($user_guid, 'passwd_conf_code');

    if ($user && $saved_code && $saved_code == $passwd_conf_code) {
        $user->salt = generate_random_cleartext_password();
        $user->password = generate_user_password($user, $password1);
        $user->save();
        return login($user, true);
    } else throw new Exception('Invalid user or code');
}

expose_function('users.set.password', 'MiigleUpdatePassword',
    array(
        'user_guid' => array('type'=>'int', 'required'=>true),
        'password1' => array('type'=>'string', 'required'=>true),
        'password2' => array('type'=>'string', 'required'=>true),        
        'passwd_conf_code' => array('type'=>'string', 'required'=>true)
    ),
    'Update a user password via a private code sent in email',
    'POST',
    false,
    false
);
