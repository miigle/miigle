<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <link rel="shortcut icon" href="/favicon.png">

    <title>Miigle - The Social Network for Innovators | Join Us</title>
    <meta name="description" content="Miigle is the online community and marketplace 
    for entrepreneurs and startups. We help innovators worldwide share their ideas 
    and connect with people interested to help.">
    <meta name="keywords" content="miigle, miiglers, social network, social innovation, 
    crowdsourcing, crowdfunding, josh fester, luc berlin, foster ideas, startup, 
    entrepreneurship, entrepreneur community">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    
    <!-- Bootstrap core CSS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="/mod/miigle_theme/vendor/nouislider/jquery.nouislider.min.css" rel="stylesheet"/>

    <!-- clone of arial rounded font -->
    <link href="//fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/mod/miigle_theme/index.css" rel="stylesheet">

    <div class="hidden">
      <script type="IN/Login"></script>
    </div>

    <style>
      section#splash p {
        margin-top: 0 !important;
        margin-bottom: 20px;
      }
      section#splash img {
        max-width: 100%;
      }
      section#splash {
        background-image: none;
        background: white;
      }
      section#splash .remember-me {
        line-height:40px;
      }
      .form-group {
        margin-bottom:15px;        
      }
      #social-login-button-div input {
        max-width: 100%;
      }
    </style>

    <script>
      /* Once SSL kicks in we can use this
      if (window.location.protocol !== 'https:') {
          window.location.href = window.location.href.replace('http', 'https');
      }
      */
    </script>
  </head>
  <body>
  <div id="fb-root"></div>
