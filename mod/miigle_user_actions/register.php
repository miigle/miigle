<?php require('login_register_header.php'); ?>
<?php forward(); ?>
    <!--[if lte IE 9]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <!--============================================
    |
    | Static navbar
    |
    ============================================-->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/index.php"><img src="/mod/miigle_theme/graphics/miigle-logo.png" alt="Miigle" /></a>
        </div>
        <div class="navbar-collapse collapse">
          <a href="/pg/login" class="pull-right btn btn-default btn-inverse navbar-btn">Log in</a>
          <!--<button id="header_join" data-toggle="modal" href="#modal-request" type="button" class="pull-right btn btn-danger navbar-btn">Join The Beta</button>-->
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <section id="splash">
      <div class="container">      
        <div class="row">
          <div class="col-md-5" style="float: none; margin: 0 auto;">
            <h2>Get Started Now.</h2>
            <br/>
            <p class="text-muted">Sign up in 30 seconds.</p>
            <form id="register-form">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                      <input type="text" name="fname"class="form-control" placeholder="First Name" required />
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                      <input type="text" name="lname"class="form-control" placeholder="Last Name" required />
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                  <input type="email" name="email"class="form-control" placeholder="Email" required />
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                  <input type="password" name="password" class="form-control" placeholder="Password" required minlength="3" />        
                  <input type="password" name="password2" class="form-control" placeholder="Confirm Password" required minlength="3" />       
                </div>
                <input type="hidden" name="invitecode">
                <input type="hidden" name="friend_guid">
              </div>
              <div class="form-group" id="messageBox"></div>
              <div class="form-group submit">
                <input type="hidden"  name="friend_guid"  value="" />
                <input type="hidden"  name="invitecode"  value="" />
                <input type="hidden"  name="action"  value="register" />
                <input type="submit" class="btn btn-lg btn-default btn-miigle btn-block" value="SIGN ME UP!"/>
                <p style="margin-bottom: 0px; padding-top: 15px; font-size: 75%;">
                  By clicking SIGN ME UP, you agree to Miigle's <a href="/terms.html">Terms of Use</a> and <a href="/privacy.html">Privacy Policy</a>.
                </p>
              </div>
            </form>
            <!--
            <div class="row">
              <div class="col-md-10 col-md-offset-1" style="margin-bottom: 15px">
                &nbsp;
                <div style="height: 2px; background-color: #ccc; text-align: center">
                  <span class="text-muted" style="background-color: white; position: relative; top: -0.7em;">
                    &nbsp;or&nbsp;
                  </span>
                </div>
                &nbsp;
              </div>
            </div>

            <div class="row" id="social-login-button-div">
              <div class="col-md-6" style="padding-right: 5px">
                <input type="image" src="/mod/miigle_theme/graphics/signupwithfacebook.png"
                       onclick="RegisterWithFacebook()"/>
              </div>
              <div class="col-md-6" style="padding-left: 5px">
                <input type="image" src="/mod/miigle_theme/graphics/signupwithlinkedin.png" 
                       onclick="RegisterWithLinkedIn()"/>
              </div>
            </div>
            -->
          </div>
        </div>
      </div>
    </section>
<?php require('login_register_footer.php'); ?>
