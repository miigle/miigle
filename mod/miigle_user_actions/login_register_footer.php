  <!--============================================
    |
    | Social Links
    |
    ============================================-->
    <section id="social" class="hidden">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <a target="_blank" href="http://www.linkedin.com/company/miigle"><img src="/mod/miigle_theme/graphics/linkedin.png" alt="Linked In"></a>
            <a target="_blank" href="https://www.facebook.com/miiglers"><img src="/mod/miigle_theme/graphics/facebook.png" alt="Facebook"></a>
            <a target="_blank" href="https://twitter.com/miiglers"><img src="/mod/miigle_theme/graphics/twitter.png" alt="Twitter"></a>
            <a target="_blank" href="https://plus.google.com/111652768429793362697/posts"><img src="/mod/miigle_theme/graphics/google.png" alt="Google"></a>
            <a target="_blank" href="http://vimeo.com/miiglers"><img src="/mod/miigle_theme/graphics/vimeo.png" alt="Vimeo"></a>
            <a target="_blank" href="http://www.youtube.com/user/miigleTV"><img src="/mod/miigle_theme/graphics/youtube.png" alt="Youtube"></a>
          </div>
        </div>
      </div>
    </section>

  <!--============================================
    |
    | Footer
    |
    ============================================-->
    <section id="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <ul class="pull-left">
              <li>Company</li>
              <!--<li>
                <a href="/about.php">About</a>
              </li>-->
              <li>
                <a href="http://miiglers.tumblr.com/">Blog</a>
              </li>
            </ul>
            <ul class="pull-left">
              <li>Stay in Touch</li>
              <li>
                <a target="_blank" href="http://www.linkedin.com/company/miigle">LinkedIn</a>
              </li>
              <li>
                <a target="_blank" href="https://www.facebook.com/miiglers">Facebook</a>
              </li>
              <li>
                <a target="_blank" href="https://twitter.com/miiglers">Twitter</a>
              </li>
              <li>
                <a target="_blank" href="https://plus.google.com/111652768429793362697/posts">Google+</a>
              </li>
              <li>
                <a target="_blank" href="http://vimeo.com/miiglers">Vimeo</a>
              </li>
              <li>
                <a target="_blank" href="http://www.youtube.com/user/miigleTV">Youtube</a>
              </li>
            </ul>
            <button id="footer_join" data-toggle="modal" href="#modal-request" type="button" class="pull-right btn btn-danger navbar-btn">Join The Beta</button>
          </div>
        </div>
      </div>
      <div class="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              &copy; 2013 <strong>Miigle</strong>. All rights reserved.
                <span class="pull-right">
                    <a href="/terms.html">Terms of Service</a>
                    &amp; <a href="/privacy.html">Privacy Policy</a>
                </span>
            </div>
          </div>
        </div>
      </div>    
    </section>

  <!--============================================
    |
    | Request Invite form
    |
    ============================================-->
    <div id="modal-request" class="modal fade">
      <div class="modal-dialog">
        <form class="form-validate" id="request_form" action="/mod/miigle_theme/request_invite.php" method="post" enctype="multipart/form-data">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h3 class="modal-title">Join Miigle!</h3>
              <h4 class="text-muted">Promote your startup and ideas to the world.</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="fname">First Name</label>
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-user text-muted"></span></span>
                      <input required type="text" class="form-control" name="fname" placeholder="i.e. Bruce">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="lname">Last Name</label>
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-user text-muted"></span></span>
                      <input required type="text" class="form-control" name="lname" placeholder="i.e. Wayne">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <div class="input-group">
                  <span class="input-group-addon"><span class="glyphicon glyphicon-envelope text-muted"></span></span>
                  <input required type="email" class="form-control" name="email" placeholder="i.e. Bruce.Wayne@WayneEnterprises.com">
                </div>
              </div>
              <div class="form-group">
                <label for="url">AngelList or Kickstarter URL <small class="text-muted">(optional)</small></label>
                <div class="input-group">
                  <span class="input-group-addon"><span class="glyphicon glyphicon-link text-muted"></span></span>
                  <input type="url" class="form-control" name="url" placeholder="i.e. http://angel.co/batmobile">
                </div>
              </div>
              <div class="form-group">
                <label for="comment">Comments <small class="text-muted">(optional)</small></label>
                <textarea rows="3" name="comment" type="text" class="form-control" placeholder="i.e. I would like to introduce to the world a revolutionary vehicle that will change the way superheroes travel and fight crime. I call it the Bat Mobile."></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <div class="row">
                <div class="col-md-12">
                  <div id="error-request_form"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">                
                  <h4 class="text-muted">Slide right to submit:</h4>
                </div>
                <div class="col-md-6">
                  <img class="pull-right ajax-loader" src="/mod/miigle_theme/graphics/ajax-loader.gif" alt="ajax loader" />
                  <div class="noUiSlider"></div>
                </div>
              </div>
            </div>          
          </div><!-- /.modal-content -->
          <div class="hidden success">
            <h4>Thank you!</h4>
            <p><span class="glyphicon glyphicon-ok"></span></p>
            <p class="text-muted">We've received your submission. Stay tuned.</p>
          </div>
          <input type="hidden" name="human" class="human" value="0">
        </form>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Bootstrap -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="/mod/miigle_user_actions/js/auth.js"></script >

    <!-- jquery validation -->
    <script src="/mod/miigle_dependencies/js/jquery.validate.js"></script>

    <!-- jquery form -->
    <script src="/mod/miigle_dependencies/js/jquery.form.min.js"></script>

    <!-- jquery mobile slider -->
    <script src="/mod/miigle_theme/vendor/nouislider/jquery.nouislider.min.js"></script>

    <script type="text/javascript" src="//platform.linkedin.com/in.js" async>
        authorize: true
        credentials_cookie: true
        api_key: <?php echo $CONFIG->LINKEDIN_API_KEY; ?>
    </script>
    <?php require(dirname(dirname(dirname(__FILE__))) . '/engine/settings.php'); ?>
    <script type="text/javascript">
        var FACEBOOK_APP_ID = <?php echo json_encode($CONFIG->FACEBOOK_APP_ID); ?>;
    </script>

    <script>
        (function(d){
         var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement('script'); js.id = id; js.async = true;
         js.src = "//connect.facebook.net/en_US/all.js";
         ref.parentNode.insertBefore(js, ref);
        }(document));
    </script>
    <script>
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-22362993-1']);
      _gaq.push(['_setDomainName', 'miigle.com']);
      _gaq.push(['_setAllowLinker', true]);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>
    <script src="/mod/miigle_theme/index.js"></script>
  </body>
</html>
