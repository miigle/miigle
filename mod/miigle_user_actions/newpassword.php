<?php require('login_register_header.php'); ?>
    <!--[if lte IE 10]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <!--============================================
    |
    | Static navbar
    |
    ============================================-->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/index.php"><img src="/mod/miigle_theme/graphics/miigle-logo.png" alt="Miigle" /></a>
        </div>
        <div class="navbar-collapse collapse">
          <!--<a href="/mod/miigle_user_actions/login.html" class="pull-right btn btn-default btn-inverse navbar-btn">Log in</a>-->
          <button id="header_join" data-toggle="modal" href="#modal-request" type="button" class="pull-right btn btn-danger navbar-btn">Join The Beta</button>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <section id="splash">
      <div class="container">       
<div class="row">
    <div class="col-md-6 col-md-offset-3" style="text-align:center;">
        <p>
            <img style="border-radius:5px;" src="/mod/miigle_theme/graphics/chameleon.jpg" alt="Chameleons">
        </p>
        <h1>Almost there!</h1>
        <p>Do you smell that? Take a deep breath. It's innovation.</p>
        
        <form id="newpassword">
            <div class="form-group input-group">
              <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
              <input id="password1" type="password" name="password1" class="form-control" placeholder="Enter a Password" required="" minlength="4">        
            </div>
            <div class="form-group input-group">
              <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
              <input id="password2" type="password" name="password2" class="form-control" placeholder="Confirm Password" required="" minlength="4">       
            </div>
              <p style="margin-bottom: 15px; font-size: 75%;">
                By clicking SIGN IN, you agree to Miigle's <a href="/terms.html">Terms of Use</a> and <a href="/privacy.html">Privacy Policy</a>.
              </p>
            <input type="submit" class="btn btn-default btn-miigle btn-lg btn-block" value="SIGN IN">
        </form>

    </div>
</div>

<script>
	function ParseURLArgs() {
	    var args = {};
	    var query_string = window.location.search.substring(1);
	    var queries_list = query_string.split('&');
        var blank_dict;

        if (Object.create) {
            blank_dict = Object.create(null);
        } else {
            blank_dict = {};
        }

	    if (query_string === '') {
            alert('Something is wrong with your URL. Try opening the email link again');
	        return args;
	    }

	    var args = queries_list.reduce(function(args, query) {
	        var parts = query.split('=');
	        args[parts[0]] = parts[1];
	        return args;
	    }, blank_dict);

	    return args;
	}

    $(document).ready(function(){
        $('form#newpassword').validate({
            //debug: true,
            validClass: 'has-success',
            errorClass: 'error has-error',
            rules: {
                password2: {
                    required: true,
                    equalTo: '#password1',
                },
            },
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').addClass(validClass).removeClass(errorClass);
            },
            submitHandler: function(form){
            	var form_data = $(form).serializeArray();
            	var url_args = ParseURLArgs();
                console.log(url_args);

                if (url_args['u'] === undefined || url_args['c'] === undefined) {
                    alert('Something is wrong with your URL. Try opening the email link again');
                    return;
                }

            	form_data.push({
            		name: 'user_guid', 
            		value: url_args['u'],
            	});
            	form_data.push({
            		name: 'passwd_conf_code',
            		value: url_args['c'],
            	});
                console.log(form_data);

                $.ajax({
                    type: 'POST',
                    url: '/services/api/rest/json/?method=users.set.password',
                    data: form_data,
                    complete: function(jqxhr, text_status) {
                        var response_dict = JSON.parse(jqxhr.responseText);

                        if (response_dict.status == 0) {
                            window.location.replace('http://' + window.location.hostname + '/pg/miigle');
                        } else {
                            alert(response_dict.message);
                        }
                    },
                });
            }
        });
    });
</script>
 </div>
    </section>
<?php require('login_register_footer.php'); ?>
