<?php
/**
 * New Password page to finish beta user approval
 * 
 * @package MiigleUserActions
 */
 
//get the page
$page = elgg_view('miigle_user_actions/approvals');

//do whatcha want, whatcha want with my body
$body = elgg_view_layout('one_column', $page);

//get title
$title = elgg_echo('User Approvals');

//draw page
page_draw($title, $body);

?>
