<?php

require_once(dirname(__FILE__) . '/api.php');

/** 
* Page handler for register and login
*/
function MiigleUserActionsInit(){
    register_page_handler('login', 'MiigleLoginPageHandler');
    register_page_handler('register', 'MiigleRegisterPageHandler');
    register_page_handler('newpassword', 'MiigleNewpasswordPageHandler');
    register_page_handler('approvals', 'MiigleApprovalsPageHandler');
    register_page_handler('forgotpassword', 'MiigleForgotPasswordPageHandler');
    register_page_handler('inviterequests', 'MiigleInviteRequestsHandler');
    register_page_handler('betanosignups', 'MiigleBetaNoSignupsHandler');
    if (get_context() == 'admin' && isadminloggedin()) {
    	global $CONFIG;
    	add_submenu_item('Invite Requests', $CONFIG->wwwroot . 'pg/inviterequests');
    	add_submenu_item('Beta No Signups', $CONFIG->wwwroot . 'pg/betanosignups');
    }
}

function MiigleBetaNoSignupsHandler($page) {
    if (!isadminloggedin()) {
        forward();
    }

    return include(dirname(__FILE__) . '/betanosignups.php');
}

function MiigleInviteRequestsHandler($page) {
    if (!isadminloggedin()) {
        forward();
    }

    return include(dirname(__FILE__) . '/inviterequests.php');
}

/**
 * login page handler
 *
 * @param array $page From the page_handler function
 * @return true|false Depending on success
 */
function MiigleLoginPageHandler($page) {
    // Make sure they aren't already logged in
    if (isloggedin()) {
        MiigleLogout();  
    }
        
    // I just took all the html from login.html and put it in here
    include(dirname(__FILE__) . '/login.php');

    return true;
}

/**
 * register page handler
 *
 * @param array $page From the page_handler function
 * @return true|false Depending on success
 */
function MiigleRegisterPageHandler($page) {
    // Make sure they aren't already logged in
    if (isloggedin()) {
        MiigleLogout();  
    }
        
    // I just took all the html from login.html and put it in here        
    include(dirname(__FILE__) . '/register.php');
    
    return true;
}

/**
 * forgot password page handler
 *
 * @param array $page From the page_handler function
 * @return true|false Depending on success
 */
function MiigleNewpasswordPageHandler($page) {             
    include(dirname(__FILE__) . '/newpassword.php');
    
    return true;
}

/**
 * approvals page handler
 *
 * @param array $page From the page_handler function
 * @return true|false Depending on success
 */
function MiigleApprovalsPageHandler($page) {  
    // THOU SHALL NOT PASS
    admin_gatekeeper();
    
    include(dirname(__FILE__) . '/approvals.php');

    return true;
}

function MiigleForgotPasswordPageHandler($page) {
    return include(dirname(__FILE__) . '/forgotten_password.php');
}

register_elgg_event_handler('init', 'system', 'MiigleUserActionsInit');
