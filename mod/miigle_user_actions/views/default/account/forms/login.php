<?php
/**
 * Elgg login form
 *
 * @package Elgg
 * @subpackage Core
 */

global $CONFIG;

$login_url = $vars['url'];
if ((isset($CONFIG->https_login)) && ($CONFIG->https_login)) {
    $login_url = str_replace("http://", "https://", $vars['url']);
}

$username = elgg_view('input/text', array('internalname' => 'username', 'class' => 'form-control', 'extra_tags'=>'placeholder="Enter email address"'));
$password = elgg_view('input/password', array('internalname' => 'password', 'class' => 'form-control', 'extra_tags'=>'placeholder="Enter password"'));
$submit = elgg_view('input/submit', array('value' => elgg_echo('SIGN IN!'), 'class'=>'btn btn-lg btn-default btn-miigle btn-block'));
?>

<form action="<?php echo $login_url; ?>action/login" method="post" class="whitebox">
    <input type="hidden" name="__elgg_token" value="" id="__elgg_token" />
    <input type="hidden" name="__elgg_ts" value="" id="__elgg_ts" />
    <div class="form-group">
        <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
            <?php echo $username; ?>
        </div>
    </div>
    <div class="form-group">
        <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
            <?php echo $password; ?>
        </div>
    </div>    
    <div class="form-group">
        <div class="checkbox pull-left">
            <input type="checkbox" id="persistent" name="persistent" checked="checked"> 
            <label for="persistent"></label>
        </div>    
        <span class="pull-left remember-me">Remember Me</span>
    </div>
    <div class="clearfix"></div>
    <div class="form-group submit"><?php echo $submit; ?></div>        
    <a href="<?php echo $vars['url']; ?>account/forgotten_password.php">Forgot it, Help!</a>
</form>

