<?php
/**
 * Elgg forgotten password.
 *
 * @package Elgg
 * @subpackage Core
 */

?>
<style type="text/css">
    .account_forgottenPassword {
        text-align:center;
    }
    #forgotPassword {
        max-width:300px;
        margin:0 auto;
    }
</style>
<div class="account_forgottenPassword" class="container">
    <div class="row">
        <div class="col-md-12">
            <p>
                <img src="/mod/miigle_theme/graphics/forgot_password.jpg" alt="Forgot Password">
            </p>        
            <h2>What was it!?</h2>    
            <p>Enter your email address below to reset your password</p>
            <form id="forgotPassword" action="/action/loginbyemailonly/requestnewpassword" method="post">
                <input type="hidden" name="__elgg_token" value="" id="__elgg_token" />
                <input type="hidden" name="__elgg_ts" value="" id="__elgg_ts" />
                <div class="form-group">
                    <?php echo elgg_view('input/text', array('internalname' => 'username', 'class'=>'form-control', 'extra_tags'=>'placeholder="Email"')); ?>
                    <?php echo elgg_view('input/captcha'); ?>
                </div>                
                <button type="submit" class="btn btn-default">Reset My Password</button>
            </form>
        </div>
    </div>
</div>
<script>
  (function() {
    $.ajax({
      url:'/services/api/rest/json/?method=form.get_token',
      dataType: "json",
      success:function(xhr){
        var ts = xhr.result.ts;
        var token = xhr.result.token;
        $('form #__elgg_ts').val(ts);
        $('form #__elgg_token').val(token);
      }
    });
  })();
</script>