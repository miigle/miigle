<?php
/**
 * Elgg register form
 *
 * @package Elgg
 * @subpackage Core
 */

$username = get_input('u');
$email = get_input('e');
$name = get_input('n');

$admin_option = false;
$loggedin_user = get_loggedin_user();

if ($loggedin_user && $loggedin_user->isAdmin() && isset($vars['show_admin'])) {
    $admin_option = true;
}

$fname_input = elgg_view('input/text' , array('internalname' => 'fname', 'class' => "form-control", 'value' => $name, 'extra_tags'=>'placeholder="First Name" required'));
$lname_input = elgg_view('input/text' , array('internalname' => 'lname', 'class' => "form-control", 'value' => $name, 'extra_tags'=>'placeholder="Last Name" required'));

$email_input = elgg_view('input/email' , array('internalname' => 'email', 'class' => "form-control", 'value' => $email, 'extra_tags'=>'placeholder="Email" required'));
//$form_body .= "<label>" . elgg_echo('username') . "<br />" . elgg_view('input/text' , array('internalname' => 'username', 'class' => "general-textarea", 'value' => $username)) . "</label><br />";
$password_input = elgg_view('input/password' , array('internalname' => 'password', 'class' => "form-control", 'extra_tags'=>'placeholder="Password" required minlength="3"'));
//$form_body .= "<label>" . elgg_echo('passwordagain') . "<br />" . elgg_view('input/password' , array('internalname' => 'password2', 'class' => "general-textarea")) . "</label><br />";

// view to extend to add more fields to the registration form
//$form_body .= elgg_view('register/extend');

// Add captcha hook
$hiddens = elgg_view('input/captcha');
$hiddens .= elgg_view('input/hidden', array('internalname' => 'friend_guid', 'value' => $vars['friend_guid']));
$hiddens .= elgg_view('input/hidden', array('internalname' => 'invitecode', 'value' => $vars['invitecode']));
$hiddens .= elgg_view('input/hidden', array('internalname' => 'action', 'value' => 'register'));
$submit .= elgg_view('input/submit', array('internalname' => 'submit', 'value' => elgg_echo('SIGN ME UP!'), 'class'=>'btn btn-lg btn-default btn-miigle btn-block')) . "</p>";
?>

<form action="<?php echo $vars['url']; ?>action/register" method="post" class="whitebox">
    <input type="hidden" name="__elgg_token" value="" id="__elgg_token" />
    <input type="hidden" name="__elgg_ts" value="" id="__elgg_ts" />
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                    <?php echo $fname_input; ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                    <?php echo $lname_input; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
            <?php echo $email_input; ?>
        </div>
    </div>
    <div class="form-group">
        <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
            <?php echo $password_input; ?>
        </div>
    </div>    
    <div class="form-group" id="messageBox"></div>
    <div class="form-group submit"><?php echo $hiddens.$submit; ?></div>        
</form>

<script>
$(document).ready(function(){
    $('form').validate({
      showErrors: function(errorMap, errorList) {
          $.each(errorList, function(key, value){
              $(value.element).parents('.form-group').addClass('has-error');
          });
          this.defaultShowErrors();
      },
      errorLabelContainer: "#messageBox",
      unhighlight:function(element, errorClass, validClass){
          $(element).parents('.form-group').removeClass('has-error');
      },
      messages: {
          ideawebsite: 'Please enter a valid URL (http://www.example.com)'
      }
    });
});
</script>