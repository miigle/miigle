<div class="row">
    <div class="col-md-6 col-md-offset-3" style="text-align:center;">
    
        <p>
            <img style="border-radius:5px;" src="/mod/miigle_theme/graphics/chameleon.jpg" alt="Chameleons">
        </p>
        <h1>Almost there!</h1>
        <p>Do you smell that? Take a deep breath. It's innovation.</p>
        
        <form id="newpassword">
            <input type="hidden" name="user_guid" value="<?php echo $vars['user_guid']; ?>">
            <input type="hidden" name="passwd_conf_code" value="<?php echo $vars['code']; ?>">
            <?php echo elgg_view('input/securitytoken'); ?>
            <div class="form-group input-group">
              <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
              <input id="password1" type="password" name="password1" class="form-control" placeholder="Enter a Password" required="" minlength="4">        
            </div>
            <div class="form-group input-group">
              <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
              <input id="password2" type="password" name="password2" class="form-control" placeholder="Confirm Password" required="" minlength="4">       
              <span style="padding-bottom: 15px; padding-top: 10px; font-size: 75%;">
                By clicking SIGN ME UP, you agree to Miigle's <a href="/terms.html">Terms of Use</a> and <a href="/privacy.html">Privacy Policy</a>.
              </span>
            </div>
            <input type="submit" class="btn btn-default btn-miigle btn-lg btn-block" value="SIGN IN">
        </form>

    </div>
</div>

<script>
    $(document).ready(function(){
        $('form#newpassword').validate({
            //debug: true,
            validClass: 'has-success',
            errorClass: 'error has-error',
            rules: {
                password2: {
                    required: true,
                    equalTo: '#password1',
                },
            },
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').addClass(validClass).removeClass(errorClass);
            },
            submitHandler: function(form){
                $.ajax({
                    type: 'POST',
                    url: '/services/api/rest/json/?method=users.set.password',
                    data: $(form).serialize(),
                    complete: function(jqxhr, text_status) {
                        var response_dict = JSON.parse(jqxhr.responseText);

                        if (response_dict.status == 0) {
                            window.location.replace('http://' + window.location.hostname + '/pg/miigle');
                        } else {
                            alert(response_dict.message);
                        }
                    },
                });
            }
        });
    });
</script>