<div class="row">
    <div class="col-md-6 col-md-offset-3" style="text-align:center;">
        
        <img src="http://www.reactiongifs.com/wp-content/gallery/dance-party/swag_dance.gif">
        
        <h1>User Approvals</h1>
        
        <form id="approvals">
            <input type="hidden" name="user_guid" value="<?php echo $vars['user_guid']; ?>">
            <input type="hidden" name="passwd_conf_code" value="<?php echo $vars['code']; ?>">
            <div class="form-group">
                <textarea name="emails" class="form-control" placeholder="Comma separated list of emails to approve"></textarea>
            </div>
            <input type="submit" class="btn btn-primary" value="Approve dem bitches">
        </form>
    
    </div>
</div>

<script>
    $(document).ready(function(){
        $('form#approvals').validate({
            debug: true,
            validClass: 'has-success',
            errorClass: 'error has-error',
            submitHandler: function(form){
                $.ajax({
                    type: 'POST',
                    url: '/services/api/rest/json/?method=users.approve.bulk',
                    data: $(form).serialize(),
                    beforeSend: function(){
                        $(form).find('input.btn').attr('disabled', true);
                    },
                    complete: function(jqxhr, text_status) {
                        var response_dict = JSON.parse(jqxhr.responseText);

                        if (response_dict.status == 0) {
                            $(form).find('input.btn').attr('disabled', false);
                            $(form).find('textarea').html('');
                            alert('success!');
                        } else {
                            alert(response_dict.message);
                        }
                    },
                });
            }
        });
    });
</script>
