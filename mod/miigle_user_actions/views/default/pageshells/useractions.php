<?php

    /*
    *
    * Miigle User Actions pageshell
    * The standard HTML page shell that everything else fits into
    *
    * @package MiigleUserActions
    * @subpackage Core
    * @author Josh Fester
    *
    * @uses    $vars['config'] The site configuration settings, imported
    * @uses    $vars['title'] The page title
    * @uses    $vars['body'] The main content of the page
    *
    */
    
// Set the content type 
header("Content-type: text/html; charset=UTF-8");

// Set title
if (empty($vars['title'])) {
        $title = $vars['config']->sitename;
} else if (empty($vars['config']->sitename)) {
        $title = $vars['title'];
} else {
        $title = $vars['config']->sitename . ": " .    $vars['title'];
}
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo $CONFIG->wwwroot;?>mod/miigle_theme/graphics/favicon.ico">

        <!-- jquery -->
        <script src="/mod/miigle_dependencies/js/jquery.min.js"></script>
        <!--// jquery -->
        
        <!-- Bootstrap -->
        <link href="/mod/miigle_dependencies/css/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="/mod/miigle_dependencies/js/bootstrap.js"></script>
        <!--// Bootstrap -->
        
        <!-- jquery validation -->
        <script src="/mod/miigle_dependencies/js/jquery.validate.js"></script>
        <!--// jquery validation -->
        
        <!-- include the default css file -->
        <link rel="stylesheet" href="/_css/css.css" />
        <style>
            body {padding-top:30px;}
            .bg {
                background:black;
                border-top:4px solid #F1D200;
                height:100%;
                width:100%;
                position:absolute;
                top:0;
                z-index:-999;
            }
            @media (min-width:992px) { .bg {height:45%;} }
            header {text-align:center;color:white;}
            .tagline {margin:25px 0;}
            .whitebox {padding:20px;}
            span.remember-me {margin-left:25px;}
            .input-group-addon {background:white;border-right-color:transparent;}
            .input-group input {border-left:none;}
            .form-group.submit {margin-top:15px;}
        </style>
        
        <!-- get the security token -->
        <script>
            $(document).ready(function(){
                $.ajax({
                    url:'/services/api/rest/json/?method=form.get_token',
                    dataType: "json",
                    success:function(xhr){
                        var ts = xhr.result.ts;
                        var token = xhr.result.token;
                        $('form #__elgg_ts').val(ts);
                        $('form #__elgg_token').val(token);
                    }
                });
            });
        </script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="bg"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <header>
                        <img src="/mod/miigle_user_actions/assets/logo.png" />
                        <div class="tagline">Share, discover, &amp; foster startups. Worldwide.</div>
                    </header>
                    <?php echo    $vars['body'];  ?>
                </div>
            </div>
        </div>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. 
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>-->
    </body>
</html>
