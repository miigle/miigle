<?php
/**
 * Miigle user account settings plugin
 *
 * @package ElggUserSettings
 */

/**
 * Miigle UserSettings init function; sets up the UserSettings functions
 *
 */
function miigle_user_settings_init() {
    // Get config
    global $CONFIG;

    // Register the page handler
    register_page_handler('miigle_user_settings','miigle_user_settings_page_handler');

    //elgg_extend_view('css',"miigle_user_settings/css");
}


/**
 * settings page handler
 *
 * @param array $page Array of page elements, forwarded by the page handling mechanism
 */
function miigle_user_settings_page_handler($page) {
    global $CONFIG;

    // The $page[0] is the file we're getting
    if (isset($page[0])) {
        switch ($page[0])
        {
        case 'edit' :
            include($CONFIG->pluginspath . "miigle_user_settings/edit.php"); 
            break;
        default:
            // Include the standard index
            include($CONFIG->pluginspath . "miigle_user_settings/index.php");
            break;
        }
    }
    else{
        // Include the standard index
        include($CONFIG->pluginspath . "miigle_user_settings/index.php");
    }
}

// Make sure the profile initialisation function is called on initialisation
register_elgg_event_handler('init','system','miigle_user_settings_init');

// Register actions
global $CONFIG;
register_action("miigle_user_settings/edit",true,$CONFIG->pluginspath . "miigle_user_settings/actions/edit.php");
