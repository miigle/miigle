<?php

    /**
     * 
     * 
     */

    //get user
    $user = $vars["entity"];
?>
<div class="whitebox" id="usersettings-edit">
    <div class="whitebox-header">
        <?php echo elgg_view('account/tabs', array('active'=>'Account')); ?>
    </div>
    <div class="whitebox-body">
        <div class="page-header">
            <h2>Account Settings</h2>
        </div>
        
        <form action="<?php echo $vars['url']; ?>action/miigle_user_settings/edit" method="post">
            <?php echo elgg_view('input/securitytoken') ?>
            <div style="margin:0;">
                <div class="form-group">
                    <label for="username">
                        <?php echo elgg_echo("Username") ?>
                    </label>
                    <?php echo elgg_view("input/text",array(
                        'internalname' => 'username',
                        'value' => $user->username,
                        'class' => 'form-control',
                        'id' => 'username'
                    )); ?>
                </div>
                <div class="form-group">
                    <label for="email">
                        <?php echo elgg_echo("Email address") ?>
                    </label>
                    <?php echo elgg_view("input/text",array(
                        'internalname' => 'email',
                        'value' => $user->email,
                        'class' => 'form-control',
                        'id' => 'email'
                    )); ?>
                </div>
            </div>
            <?php echo elgg_view("input/submit", array(
                    'internalname' => 'submit',
                    'value' => "Save Changes",
                    'class' => 'btn btn-default btn-miigle'
                ));?>
            <div class="page-header">
                <h2>Change Password</h2>
            </div>
                <div class="form-group">
                    <label for="current_password">
                        <?php echo elgg_echo("Current Password") ?>
                    </label>
                    <div class="secondcol">
                        <?php echo elgg_view("input/password",array(
                                                                'internalname' => 'password',
                                                                'value' => '',
                                                                'class'=>'form-control',
                                                                'extra_tags'=>'autocomplete="off"')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password1">
                        <?php echo elgg_echo("New Password") ?>
                    </label>
                    <?php echo elgg_view("input/password",array(
                        'internalname' => 'password1',
                        'value' => '',
                        'extra_tags'=>'autocomplete="off"',
                        'class' => 'form-control',
                        'id' => 'password1'
                    )); ?>
                </div>
                <div class="form-group">
                    <label for="password2">
                        <?php echo elgg_echo("Confirm New Password") ?>
                    </label>
                    <?php echo elgg_view("input/password",array(
                        'internalname' => 'password2',
                        'value' => '',
                        'extra_tags'=>'autocomplete="off"',
                        'class' => 'form-control',
                        'id' => 'password2'
                    )); ?>

                </div>
                <?php echo elgg_view("input/submit", array(
                    'internalname' => 'submit',
                    'value' => "Save Changes",
                    'class' => 'btn btn-default btn-miigle'
                ));?>
        </form>
    </div>
</div>
