<?php

    /**
     * Elgg profile index
     * 
     * @package ElggProfile
     */

    // Get the Elgg engine
    require_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

    // Get the user
    $user = get_input('user');
    
    $body = "";
    
    //get the loggedin user
    $user = get_loggedin_user();
    if($user){
        if ($user->isBanned() && !isadminloggedin()){
            forward();
            exit;
        }
        $body = elgg_view("miigle_user_settings/edit", array('entity' => $user));
        $title = $user->name;
        $body = elgg_view_layout('one_column',$body);
    } 
    else {
        //user not logged in go to index page
        forward();
    }

    page_draw($title, $body);
        
?>
