<?php
    /**
     * 
     * @description change miigle_user_settings
     */
    // Load configuration
    global $CONFIG;

    //check access
    gatekeeper();

    //get logged in user
    $user = get_loggedin_user();
    set_input('username', $user->email);

    if(!$user){
        forward();
    }
    
    $success = true;
    $change = false;
    
    try{
        $username = sanitise_string(get_input("username"));
        $email = sanitise_string(get_input("email"));
        if(!empty($username) && $user->username != $username){
            //verify that this username is unique
            if(get_user_by_username($username)===false){
                $user->username = $username;
                $change = true;
                if(!$user->save()){
                    $success = false;
                }
            }else{
                throw new Exception("Username already exists in the system, please select a different one.");
            }
        }
        if(!empty($email)){
            if(validate_email_address($email)){
                $user->email = $email;
                $change = true;
                if(!$user->save()){
                    $success = false;
                }
            }else{
                throw new Exception("Invalid email address");
            }
        }
        
        $currpass = sanitise_string(get_input("password"));
        $newpass = sanitise_string(get_input("password1"));
        $confirmnewpass = sanitise_string(get_input("password2"));
        if(authenticate($username, $currpass) === false) {
            echo("Invalid current password");
            exit;
        }
        //check if currpass is correct
        if(!empty($newpass)){
            //validate the new password
            if(validate_password($newpass)){
                //check if newpass and confirm pass match
                if($newpass==$confirmnewpass){
                    //generate new salt
                    $user->salt = generate_random_cleartext_password();
                    //generate md5 hash of the user password and update
                    $user->password = generate_user_password($user, $newpass);
                    $change = true;
                    if(!$user->save()){
                        $success = false;
                    }
                }else{
                    //throw error, passwords dont match
                    throw new Exception("Passwords don't match");
                }
            }else{
                //throw error, invalid password
                throw new Exception("Invalid Password");
            }
        }
    }catch(Exception $e){
        echo($e->getMessage());
        exit;
    }
    if($change && $success){
        echo 'success';
    }elseif($change && !$success){
        echo("Failed to save changes");
    }
    exit;
