<?php
    /**
     * list down notifications
     * 
     * @uses $vars['entities'] notifications objects to list
     */

    $entities = $vars['entities'];
    $limit = $vars['limit'];
    $offset = $vars['offset'];
    $count = $vars['count'];
    $baseurl = $vars['baseurl'];
    $pagination = $vars['pagination'];

    save_last_viewed_notification_ts(get_loggedin_user(), time());

    //echo '<pre>';
    //var_dump($entities);

    $notifications_list = array();
    foreach ($entities as $entity) {
      $notification_dict = array();

      $username = preg_replace('/^<a href=".*\/pg\/profile\//', '', $entity->description);
      $username = preg_replace('/".*$/', '', $username);

      if (preg_match('/is following you$/', $entity->description)) {
        $notification_type = 'follow';
      } else if (preg_match('/^.*cheered for /', $entity->description)) {
        $notification_type = 'cheer';
        $idea_title = preg_replace('/^.*cheered for /', '', $entity->description);
      } else if (preg_match('/^.*rated /', $entity->description)) {
        $notification_type = 'rate';
        $idea_title = preg_replace('/^.*rated /', '', $entity->description);
      } else {
        continue;
      }

      $notification_dict['notification_type'] = $notification_type;
      $notification_dict['user_dict'] = MiigleUser::GetUserFromUsername($username);
      $notification_dict['time_created'] = $entity->time_created;

      global $CONFIG;

      if ($notification_dict['notification_type'] != 'follow') {
          $results = elgg_get_entities(array(
            'types' => 'object',
            'subtypes' => 'idea',
            'wheres' => array(
              "oe.title='$idea_title'",
            ),
            'joins' => array(
              "JOIN {$CONFIG->dbprefix}objects_entity oe ON e.guid = oe.guid",
            ),
          ));

          if (!sizeof($results)) {
            continue;
          }

          $notification_dict['idea_dict'] = MiigleIdea::GetIdea($results[0]->guid);
      }

      if ($notification_dict['notification_type'] == 'rate') {
        //
        $river_items = get_river_items($notification_dict['user_dict']['guid'], $notification_dict['idea_dict']['guid'], $subject_relationship = '', $type = '',
                                       $subtype = '', $action_type = 'user:rate::idea', $limit = 999999, $offset = 0, $posted_min = 0, $posted_max = 0);
        if (sizeof($river_items) > 0) {
          $river_item = TransformRiverItem(array_shift($river_items));
          $rating = get_entity($river_item['action_payload'][0]);
          if (abs($notification_dict['time_created'] - $river_item['posted']) > 5) { //ugly
              continue;
          }
          $rating_dict = $rating->toDict();
          $rating_dict = $rating_dict['rating_dict']; //lol names
          //fuck functional programming for now
          $sum = 0;
          $total = 0;
          foreach ($rating_dict as $rating_name=>$rating) {
            $sum += $rating;
            $total += 10;
          }
          $num_stars = 5 * ($sum / $total);
        }
      }

      $notifications_list[] = $notification_dict;
    }
    
    
    $nav = "";
    if ($pagination){
        $nav .= elgg_view('navigation/pagination',array(
            'baseurl' => $baseurl,
            'offset' => $offset,
            'count' => $count,
            'limit' => $limit,
        ));
    }
?>
<!--- Please Add Below Style to CSS file -->

