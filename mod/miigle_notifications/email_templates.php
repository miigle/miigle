<?php

$notification_email_templates = array(
	'cheer' => array(
		'subject' => 'You have a new Cheer!',
		'header'  => 'You have a new Cheer!',
		'body'    => <<<EOD
%s cheered for %s.

	To view %s's profile <a href="%s">click here</a>.

Thank You,

- The Miigle Team
EOD
	),

	'comment' => array(
		'subject' => 'An idea you follow has a new comment!',
		'header'  => 'An idea you follow has a new comment!',
		'body'    => <<<EOD
%s has also left a comment on %s.

	"%s"

	To view all comments for %s <a href="%s">click here</a>.

Thank You,

- The Miigle Team
EOD
	),

	'comment_reply' => array(
		'subject' => 'You received a reply to your comment',
		'header'  => 'You received a reply to your comment',
		'body'    => <<<EOD
%s replied to your comment made on %s

	"%s"

	To view all comments for %s <a href="%s">click here</a>.

Thank You,

- The Miigle Team
EOD
	),

	'comment_owner' => array(
		'subject' => 'You have a new comment!',
		'header'  => 'You have a new comment!',
		'body'    => <<<EOD
%s has left a comment on %s

	"%s"

	To view all comments for %s <a href="%s">click here</a>.

Thank You,

- The Miigle Team
EOD
	),

	'rate' => array (
		'subject' => 'You have a new rating!',
		'header'  => 'You have a new rating!',
		'body'    => <<<EOD
%s rated %s.

	To view their rating <a href="%s">click here</a>.

	To view %s's average rating <a href="%s">click here</a>.

Thank You,

- The Miigle Team
EOD
	),

	'follow' => array(
		'subject' => 'You have a new fan!',
		'header'  => 'You have a new fan!',
		'body'    => <<<EOD
%s has followed you.

	To view %s's profile <a href="%s">click here</a>.

Thank you,

- The Miigle Team
EOD
	),

	'message' => array(
		'subject' => 'You have a new message!',
		'header'  => 'You have a new message!',
		'body'    => <<<EOD
"%s"

	To reply to this message <a href="%s">click here</a>.

	To view %s's profile <a href="%s">click here</a>.

Thank You,

- The Miigle Team
EOD
	),
);