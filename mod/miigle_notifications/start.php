<?php
require_once(dirname(__FILE__) . '/api.php');

/**
 * Elgg MiigleNotifications plugin
 * 
 * @package MiigleNotifications
 */

/**
 * initialisation
 *
 * These parameters are required for the event API, but we won't use them:
 * 
 * @param unknown_type $event
 * @param unknown_type $object_type
 * @param unknown_type $object
 */

function miigle_notifications_init() {
    // Load system configuration
    global $CONFIG;
    //extend css
    elgg_extend_view("css", "miigle_notifications/css");
    //register page handler for all notifications
    //register_page_handler("notifications", "notifications_page_handler");

    register_entity_type('object', 'miigle_notification');
    add_subtype('object', 'miigle_notification', 'MiigleNotification');
}

/**
 * page handler for notifications
 * 
 */
function notifications_page_handler($page){
    if(isset($page[0])){
        global $CONFIG;
        switch($page[0]){
            case 'all':
                if(!include_once($CONFIG->pluginspath . "/miigle_notifications/all.php")){
                    register_error("Can not find the page you were looking for.");
                    forward(REFERER);
                }
                break;
            default:
                register_error("Can not find the page you were looking for.");
                forward(REFERER);
                break;
        }
    }
    
    return true;
}


/**
 * send notification to user
 */
function miigle_notify_user($user, $msg="", $for_obj_guid=0, $notification_type="message"){
    $success = false;
    if($user){
        try{
            $ignore = elgg_set_ignore_access(true);
            $notification = new ElggObject();
            $notification->subtype = "notification";
            $notification->access_id = ACCESS_PUBLIC;
            $notification->title = "";
            $notification->description = $msg;
            $notification->owner_guid = $user->guid;
            $notification->container_guid = $user->guid;
            $notification->notification_type = $notification_type;
            $notification->unread = 1;
            $notification->for_obj_guid = $for_obj_guid;
            $success = $notification->save();
            if($success){
                $notification->access_id = ACCESS_PRIVATE;
                $notification->creation_ts = $notification->time_created;
                $success = $notification->save();
            }
            elgg_set_ignore_access($ignore);
            global $CONFIG;
            notify_user($user->email, $CONFIG->site->guid, $msg, $msg);
        }catch(Exception $ex){
            echo $ex->getMessage();exit;
        }
    }
    return $success;
}

/**
 * 
 * Enter description here ...
 */
function miigle_remove_notification($for_relation_guid){
    $user = get_loggedin_user();
    $metadata_name_value_pairs = array('name' => 'for_obj_guid', 'value' => $for_relation_guid, 'operand' => '=', 'case_sensitive' => FALSE);
    $res = elgg_get_entities_from_metadata(array(
        'types' => 'object',
        'subtypes' => 'notification',
        'owner_guid' => $user->guid,
        'metadata_name_value_pairs' => $metadata_name_value_pairs,
        'limit' => 0,
        'offset' => 0
    ));
    //delete notification object, since action has been taken on it
    return delete_entity($res[0]->guid);
}

/**
 * 
 * get users notifications count
 *
 * @params $user
 * 
 * @return integer
 * 
 */
function get_unviewed_notifications_count($user){
    $last_viewed_notification_ts = 0;
    if(isset($user->last_viewed_notification_ts)){
        $last_viewed_notification_ts = $user->last_viewed_notification_ts;
    }
    $unread_msgs_count = elgg_get_entities_from_metadata(array(
        'types' => 'object',
        'subtypes' => 'notification',
        'owner_guid' => $user->guid,
        'wheres' => array("e.time_created > $last_viewed_notification_ts"),
        'order_by' => 'e.time_created DESC',
        'limit' => $limit,
        'offset' => 0,
        'count' => true
    ));
    return $unread_msgs_count;
}


/**
 * 
 * get users latest notifications
 *
 * @params $user
 * @params $limit, number of objects to return, default is 10
 * @params $offset, offset to start from, default is 0
 * 
 * @return array
 * 
 */
function get_latest_notifications($user, $limit=10, $offset=0, $count=false){
    $metadata_name_value_pairs = array();
    $res = elgg_get_entities_from_metadata(array(
                'types' => 'object',
                'subtypes' => 'notification',
                'owner_guid' => $user->guid,
                'order_by' => 'e.time_created DESC',
                'limit' => $limit,
                'offset' => $offset,
                'count' => $count
            ));
    return $res;
}

/**
 * save user last notification read timestamp
 */
function save_last_viewed_notification_ts($user, $ts){
    $user = get_loggedin_user();
    $user->last_viewed_notification_ts = $ts;
    if(!$user->save()){
        register_error("could not save last read time");
        return false;
    }
    return true;
}

/**
 * save user last notification read timestamp
 */
function save_last_viewed_message_ts($user, $ts){
    $user->last_viewed_messages_ts = $ts;
    if(!$user->save()){
        register_error("could not save last read time");
        return false;
    }
    return true;
}


// Make sure the miigle_notifications plugin initialisation function is called on initialisation
register_elgg_event_handler('init','system','miigle_notifications_init');
    
//Register actions
global $CONFIG;
/*
register_action("miigle_notifications/notify",false,$CONFIG->pluginspath . "miigle_notifications/actions/notify.php", false);
register_action("miigle_notifications/update_viewed_notifications",false,$CONFIG->pluginspath . "miigle_notifications/actions/update_viewed_notifications.php", false);
register_action("miigle_notifications/update_viewed_messages",false,$CONFIG->pluginspath . "miigle_notifications/actions/update_viewed_messages.php", false);
*/