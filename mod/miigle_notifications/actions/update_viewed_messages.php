<?php
    /**
     * 
     */

    //access check
    action_gatekeeper();
    
    //get loggedin user
    $user = get_loggedin_user();
    
    //update last read notification timestamp to currrent
    if(save_last_viewed_message_ts($user, time())){
        echo 'success';
    }else{
        echo 'failed';
    }
    exit();
?>