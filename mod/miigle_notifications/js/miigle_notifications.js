angular.module('miigle.stub', ['miigle_star_rating', 'ngRoute'])
	.controller('StubController', function($routeParams) {
		var routes = Object.keys($routeParams);
		var new_url = '/pg/miigle#';

		if (routes.length > 0) {
			routes.forEach(function(route_key) {
				new_url += '/' + $routeParams[route_key];
			});
            window.history.pushState(null, '', window.location.origin + window.location.pathname);
			window.location.href = window.location.origin + new_url;
		}
	})
    .config(function($routeProvider) {
        $routeProvider
        	.when('/', {
        		controller: 'StubController',
        		template: '<span></span>',
        	})
        	.when('/:a', {
        		controller: 'StubController',
        		template: '<span></span>',
        	})
        	.when('/:a/:b', {
        		controller: 'StubController',
        		template: '<span></span>',
        	})
        	;
    })
	;