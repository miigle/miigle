<?php
    /**
     * 
     * 
     */

    //authorize
    gatekeeper();
    
    //get user
    $user = get_loggedin_user();
    
    //get input if any
    $limit = get_input("limit", 20);
    $offset = get_input("offset", 0);
    
    //count all notifications
    $count = get_latest_notifications($user, 0, 0, true);
    
    //fetch data
    $arrNotifications = get_latest_notifications($user, $limit, $offset);
    
    $params = array(
                        "entities" => $arrNotifications,
                        "limit" => $limit,
                        "offset" => $offset,
                        "count" => $count,
                        "baseurl" => $_SERVER['REQUEST_URI'],
                        "pagination"=>true
                );
    
    //get page view
    $page = elgg_view("miigle_notifications/list", $params);
    
    //title
    $title = elgg_view_title("All Notifications");
    
    //fill into layout
    $body = elgg_view_layout("one_column", $title.$page);
    
    //draw page
    page_draw("All Notifications", $body);
?>
