<?php
class MiigleNotification extends ElggObject {
    public function __construct($guid=null) {
        parent::__construct($guid);

        if (!$guid) {
            $this->set('subtype', 'miigle_notification');
            $this->set('access_id', ACCESS_PRIVATE);
        }
    }

    public function toDict() {
        return array(
            'guid'                 => intval($this->guid),
            'subject_guid'         => intval($this->subject_guid),
            'object_guid'          => intval($this->object_guid),
            'notification_type'    => strval($this->notification_type),
            'notification_payload' => json_decode($this->notification_payload, 
                                                  true),
            'read'                 => ($this->read) ? true : false,
            'time_created'         => intval($this->time_created),
        );
    }
    
    public static function CreateNotification($user_guid, $subject_guid, 
                                              $object_guid, $notification_type, 
                                              $notification_payload='') {
        $ignore = elgg_set_ignore_access(true);
        $notification = new MiigleNotification();

        if (!in_array($notification_type, 
                      array('cheer', 'follow', 'rate', 'comment', 'message',
                            'comment_reply'))) {
            throw new Exception("Invalid notification type $notification_type");
        }

        $notification->set('owner_guid', $user_guid);
        $notification->set('container_guid', $user_guid);
        $notification->set('subject_guid', $subject_guid);
        $notification->set('object_guid', $object_guid);
        $notification->set('notification_type', $notification_type);
        $notification->set('notification_payload', $notification_payload);
        $notification->set('read', 0);

        $notification->notifyUser();

        $notification->save();

        elgg_set_ignore_access($ignore);

        return $notification;
    }

    public function notifyUser() {
        global $CONFIG;

        $notification_dict = $this->toDict();
        $notification_type = $notification_dict['notification_type'];
        require('email_templates.php');
        $template = $notification_email_templates[$notification_type];

        $subject = MiigleUser::GetUserFromGUID(
                $notification_dict['subject_guid']);

        $subject_full_name = sprintf('%s %s', $subject['first_name'], 
                                              $subject['last_name']);
        $subject_url = sprintf('%spg/miigle#/user/%s', $CONFIG->wwwroot, 
                                                        $subject['username']);
        $notifications_url = sprintf('%spg/miigle#/notifications', 
                $CONFIG->wwwroot);

        if (in_array($notification_type, array('cheer', 'rate', 'comment', 
                                               'comment_reply'))) {
            $object = MiigleIdea::GetIdea(
                    $notification_dict['object_guid']);
            $object_title = $object['title'];
            $object_url = sprintf('%spg/miigle#/idea/%s', $CONFIG->wwwroot, 
                                                          $object['guid']);
        }

        switch ($notification_type) {
            case 'cheer':
                $body = sprintf($template['body'], $subject_full_name, 
                                $object_title, $subject_full_name, 
                                $subject_url);

                return notify_user($this->owner_guid, 
                                   $CONFIG->site->guid, 
                                   $template['subject'], 
                                   $body,
                                   array('header'=>$template['header']));

            case 'follow':
                $body = sprintf($template['body'], $subject_full_name, 
                                $subject_full_name, $subject_url);
                return notify_user($this->owner_guid, 
                                   $CONFIG->site->guid, 
                                   $template['subject'], 
                                   $body,
                                   array('header'=>$template['header']));

            case 'rate':
                $body = sprintf($template['body'], $subject_full_name, 
                                $object_title, $notifications_url, 
                                $object_title, $object_url);
                return notify_user($this->owner_guid, 
                                   $CONFIG->site->guid, 
                                   $template['subject'], 
                                   $body,
                                   array('header'=>$template['header']));

            case 'comment':
                if ($this->owner_guid == $object['owner_guid']) {
                    $template = $notification_email_templates['comment_owner'];
                }
            case 'comment_reply':
                $comment = $notification_dict['notification_payload'];
                $body = sprintf($template['body'], $subject_full_name, 
                                $object_title, $comment, $object_title, 
                                $object_url);
                return notify_user($this->owner_guid, 
                                   $CONFIG->site->guid, 
                                   $template['subject'], 
                                   $body,
                                   array('header'=>$template['header']));

            case 'message':
                $conversation_url = sprintf('%spg/miigle#/messages/inbox/%s', 
                        $CONFIG->wwwroot, $subject['username']);
                $message = $notification_dict['notification_payload'];
                $body = sprintf($template['body'], $message, $conversation_url, 
                                $subject_full_name, $subject_url);

                return notify_user($this->owner_guid, $CONFIG->site->guid,
                                   $template['subject'], $body, 
                                   array('header'=>$template['header']));

            default:
                throw new Exception("Unknown notification_type {$notification_dict['notification_type']}");
        }
    }

    public static function GetNotifications($user_guid, $offset=0, $limit=10) {
        $notifications = elgg_get_entities(array(
            'type' => 'object',
            'subtype' => 'miigle_notification',
            'owner_guid' => intval($user_guid),
            'offset' => $offset,
            'limit' => $limit,
        ));

        if (!$notifications) {
            $notifications = array();
        }

        return array_map('DictifyEntity', $notifications);
    }

    public static function SetNotificationAsRead($guid) {
        $notification = get_entity($guid);

        if (!$notification) {
            return false;
        }
        $notification->read = 1;
        return $notification->save();
    }

    public static function SetNotificationsAsRead($guid_list) {
        return array_map('MiigleNotification::SetNotificationAsRead', 
                         $guid_list);
    }
}

function GetNotificationsProxy($offset, $limit) {
    return MiigleNotification::GetNotifications(get_loggedin_userid(), 0, 9999999);
}
function SetNotificationsAsReadProxy($guids_list) {
    $guids_list = json_decode($guids_list, true);

    MiigleNotification::SetNotificationsAsRead($guids_list);
    return true;
}

expose_function('notifications.get', 'GetNotificationsProxy',
    array(
        'offset' => array('type'=>'int', 'required'=>false, 'default'=>0),
        'limit'  => array('type'=>'int', 'required'=>false, 'default'=>10),
    ),
    '',
    'POST',
    false,
    false
);
expose_function('notifications.set_read', 'SetNotificationsAsReadProxy',
    array(
        'guids_list' => array('type'=>'string', 'required'=>true),
    ),
    '',
    'POST',
    false,
    false
);