<?php

/**
 * Elgg profile icon
 * 
 * @package ElggProfile
 * 
 * @uses $vars['entity'] The user entity. If none specified, the current user is assumed.
 * @uses $vars['size'] The size - small, medium or large. If none specified, medium is assumed. 
 */

// Get entity
if (empty($vars['entity']))
    $vars['entity'] = $vars['user'];

if ($vars['entity'] instanceof ElggUser) {

    $full_name = $vars['entity']->fname . ' ' . $vars['entity']->lname;
    $full_name = htmlentities($full_name, ENT_QUOTES, 'UTF-8');
    $username = $vars['entity']->username;

    if ($icontime = $vars['entity']->icontime) {
        $icontime = "{$icontime}";
    } else {
        $icontime = "default";
    }

    // Get size
    if (!in_array($vars['size'],array('small','medium','large','tiny','master','topbar')))
        $vars['size'] = "medium";

    // Get any align and js
    if (!empty($vars['align'])) {
        $align = " align=\"{$vars['align']}\" ";
    } else {
        $align = "";
    }

    $entity_url = $vars['entity']->getURL();
    $entity_icon = elgg_format_url($vars['entity']->getIcon($vars['size']));

    // Override
    //What the hell is override?
    if (isset($vars['override']) && $vars['override'] == true) {
        $override = true;
    } else $override = false;

    if (!$override) {
?>

<div class="usericon">
    <div class="avatar_menu_button">
        <img src="<?php echo $vars['url']; ?>_graphics/spacer.gif" border="0" width="15" height="15" /></div>

        <div class="sub_menu">
            <h3><a href="<?php echo $entity_url ?>"><?php echo $full_name; ?></a></h3>
            <?php
            if (isloggedin()) {
                $actions = elgg_view('profile/menu/actions',$vars);
                if (!empty($actions)) {
                    echo "<div class=\"item_line\">{$actions}</div>";
                }
                /* If you're looking at your own profile, put an "Edit Profile" button in there */
                if ($vars['entity']->getGUID() == $vars['user']->getGUID()) {
                    echo elgg_view('profile/menu/linksownpage',$vars);
                } else {
                    echo elgg_view('profile/menu/links',$vars);
                }					
            } else {
                echo elgg_view('profile/menu/links',$vars);
            }
            ?>
        </div>	
        <?php
        if ((isadminloggedin()) || (!$vars['entity']->isBanned())) {
        ?><a href="<?php echo $entity_url ?>" class="icon">
        <?php
        }
    }
        ?>

    <img src="<?php echo $entity_icon; ?>" 
         class="icon"
         border="0" 
         <?php echo $align; ?> 
         alt="<?php echo $full_name; ?>" 
         title="<?php echo $full_name; ?>" 
         <?php echo $vars['js']; ?> 
    />

    <?php 
    if (!$override) {
    ?>
    </a></div>
    <?php
    }
}
    ?>
