<?php
require_once (dirname(dirname(__FILE__)) . '/miigle_cache/api.php');

function GetBlankMiigleLocation() {
    return array(
        'country' => '',
        'region' => '',
        'city' => '',
        'fqcn' => '',
        'cia_map_reference' => '',
        'currency_code' => '',
        'latitude' => 0.0,
        'longitude' => 0.0,
        'timezone' => '',
    );
}

abstract class GeoBytesKeyValueStore implements KeyValueStore {
    abstract public function get($key);

    public function set($key, $value) {
        return false;
    }
    public function delete($key) {
        return false;
    }

    public function contains($key) {
        return true; //results exist for every query. Sometimes they're [] or an empty object, though.
    }

    protected function _getResponse($url) {
        return utf8_encode(file_get_contents($url));
    }
}

class GeoBytesFullyQualifiedCityNameKeyValueStore extends GeoBytesKeyValueStore {
    public function get($search_term) {
        $json_response_string = $this->_getResponse('http://gd.geobytes.com/AutoCompleteCity?q=' . urlencode($search_term));

        if ($json_response_string == '[""]') {
            $json_response_string = '[]';
        }
        return $json_response_string;
    }
}
class GeoBytesLocationDetailsKeyValueStore extends GeoBytesKeyValueStore {
    public function get($fully_qualified_city_name) {
        $json_response_string = $this->_getResponse('http://gd.geobytes.com/GetCityDetails?fqcn=' . urlencode($fully_qualified_city_name));
        $response_dict = json_decode($json_response_string, true);

        $return_dict = GetBlankMiigleLocation();

        $return_dict['country']           = $response_dict['geobytescountry'];
        $return_dict['region']            = $response_dict['geobytesregion'];
        $return_dict['city']              = $response_dict['geobytescity'];
        $return_dict['fqcn']              = $response_dict['geobytesfqcn'];
        $return_dict['cia_map_reference'] = $response_dict['geobytesmapreference'];
        $return_dict['currency_code']     = $response_dict['geobytescurrencycode'];
        $return_dict['latitude'] = floatval($response_dict['geobyteslatitude']);
        $return_dict['longitude']= floatval($response_dict['geobyteslongitude']);
        $return_dict['timezone']          = $response_dict['geobytestimezone'];

        return json_encode($return_dict);
    }
}

class GeoBytesSearchTermToLocationDetailsResolver implements KeyValueStore {
    private $search_term_to_fqcn_resolver; //returns a list of fqcn for a search term
    private $fqcn_to_location_details_resolver;

    //$key_value_store_class_name = of class name that I will use to cache my results I pull from the web
    public function __construct($key_value_store_class_name) {
        $search_term_to_fqcn_cache = new NamespacedKeyValueStore('search_term_to_fqcn', new $key_value_store_class_name());
        $this->search_term_to_fqcn_resolver = new KeyValueCache($search_term_to_fqcn_cache, new GeoBytesFullyQualifiedCityNameKeyValueStore());

        $fqcn_to_location_details_cache = new NamespacedKeyValueStore('fcqn_to_location_details', new $key_value_store_class_name());
        $this->fqcn_to_location_details_resolver = new KeyValueCache($fqcn_to_location_details_cache, new GeoBytesLocationDetailsKeyValueStore());
    }

    public function get($search_term) {
        $location_details_list = array();
        $fqcn_list = json_decode($this->search_term_to_fqcn_resolver->get($search_term), true);

        foreach ($fqcn_list as $fqcn) {
            $location_details_dict = json_decode($this->fqcn_to_location_details_resolver->get($fqcn), true);
            $location_details_list[] = $location_details_dict;
        }

        return json_encode($location_details_list);
    }

    public function set($key, $value) {
        return false;
    }
    public function contains($key) {
        return false;
    }
    public function delete($key) {
        return false;
    }
}

function MiigleLocationSearch($search_term) {
    //$lookup_service = new GeoBytesSearchTermToLocationDetailsResolver('InMemoryKeyValueStore');
    $lookup_service = new GeoBytesSearchTermToLocationDetailsResolver('ElggDatabaseKeyValueStore');
    //$lookup_service = new GeoBytesSearchTermToLocationDetailsResolver('JSONFileKeyValueStore');
    return json_decode($lookup_service->get($search_term), true);
}

//this is the same as above but makes sure it only returns one result.
//we have to do this because searching for 'san francisco' gives two results:
//san francisco and south san francisco
function MiigleLocationLookup($lookup_term) {
    $search_results = MiigleLocationSearch($lookup_term);

    if (sizeof($search_results) > 0) {
        foreach ($search_results as $search_result_dict) {
            if ($search_result_dict['fqcn'] === $lookup_term) {
                return $search_result_dict;
            }
        }
    } else {
        return false;
    }
}

expose_function('location.autocomplete', 'MiigleLocationSearch',
    array(
        'search_term' => array('type'=>'string', 'required'=>'true'),
    ),
    'blah',
    'GET',
    false,
    false
);
expose_function('location.lookup', 'MiigleLocationLookup',
    array(
        'search_term' => array('type'=>'string', 'required'=>'true'),
    ),
    'blah',
    'GET',
    false,
    false
);
