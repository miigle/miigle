<?php
interface KeyValueStore {
    public function get($key);
    public function set($key, $value);
    public function delete($key);
    public function contains($key);
}

//$persistence_store is used on $cache_store cache misses
class KeyValueCache implements KeyValueStore {
    private $cache_store;
    private $persistence_store;

    public function __construct(KeyValueStore $cache_store, KeyValueStore $persistence_store) {
        $this->cache_store       = $cache_store;
        $this->persistence_store = $persistence_store;
    }

    public function get($key) {
        $value = $this->cache_store->get($key);

        //if cache miss
        if ($value === false) {
            $value = $this->persistence_store->get($key);
            $this->cache_store->set($key, $value);
        }

        return $value;
    }

    public function set($key, $value) {
        return ($this->cache_store->set($key, $value) && 
                $this->persistence_store->set($key, $value));
    }

    public function delete($key) {
        return ($this->cache_store->delete($key) &&
                $this->persistence_store->delete($key));
    }

    public function contains($key) {
        return ($this->cache_store->contains($key) ||
                $this->persistence_store->contains($key));
    }
}

//This is more like a mixin
class NamespacedKeyValueStore implements KeyValueStore {
    private $store;
    private $namespace;

    public function __construct($namespace, KeyValueStore $store) {
        $this->namespace = $namespace;
        $this->store = $store;
    }

    private function _transformKey($key) {
        return sprintf('%s:%s', $this->namespace, $key);
    }

    public function get($key) {
        $key = $this->_transformKey($key);
        return $this->store->get($key);
    }

    public function set($key, $value) {
        $key = $this->_transformKey($key);
        return $this->store->set($key, $value);
    }

    public function delete($key) {
        $key = $this->_transformKey($key);
        return $this->store->delete($key);
    }

    public function contains($key) {
        $key = $this->_transformKey($key);
        return $this->store->contains($key);
    }
}

//probably shouldn't use this naked. Probably safest to wrap it in a NamedspacedKeyValueStore
class ElggDatabaseKeyValueStore implements KeyValueStore {
    private function _getEntity($key) {
    	global $CONFIG;
        $key = mysql_real_escape_string($key);

        $entity = elgg_get_entities(array(
            'type' => 'object',
            'subtype' => 'miigle_kv_store_object',
            'limit' => 1,
            'wheres' => array("title = '$key'"),
            'joins' => array("JOIN {$CONFIG->dbprefix}objects_entity o on o.guid = e.guid"),
        ));

        if ($entity) {
        	return $entity[0];
        } else {
        	return false;
        }
    }

    //good
    public function get($key) {
        $entity = $this->_getEntity($key);

        if ($entity === false) {
            return false;
        } else {
            return strval($entity->description);
        }
    }

    //good
    public function set($key, $value) {
        $entity = $this->_getEntity($key);

        if ($entity === false) {
            global $CONFIG;

            $entity = new ElggObject();

            $entity->subtype = 'miigle_kv_store_object';
            $entity->access_id = ACCESS_PUBLIC;

            //$entity->site_guid = $CONFIG->site_guid;
            $entity->owner_guid = 0;
            $entity->container_guid = 0;

            $entity->title = strval($key);
            $entity->description = strval($value);

            return $entity->save() != false;
        } else {
            $entity->description = strval($value);
            return $entity->save();
        }
    }

    //good
    public function delete($key) {
        $entity = $this->_getEntity($key);

        if ($entity === false) {
            return false;
        } else {
            return $entity->delete() !== false;
        }
    }

    //good
    public function contains($key) {
        return $this->get($key) !== false;
    }
}

//mostly just for testing
class InMemoryKeyValueStore implements KeyValueStore {
    private $store;

    public function __construct($store=array()) {
        $this->store = $store;
    }

    public function get($key) {
        if ($this->contains($key)) {
            return $this->store[$key];
        } else {
            return false;
        }
    }

    public function set($key, $value) {
        $this->store[$key] = $value;
        return true;
    }

    public function contains($key) {
        return isset($this->store[$key]);
    }

    public function delete($key) {
        unset($this->store[$key]);
        return true;
    }

    protected function _getStore() {
        return $this->store;
    }
}

//poor man's sqlite
class JSONFileKeyValueStore extends InMemoryKeyValueStore {
    private $json_file;

    public function __construct($filename='json_cache.json') {
        $this->json_file = new ElggFile();
        $this->json_file->setFilename($filename);

        if (!$this->json_file->exists()) {
            $this->json_file->open('write');
            $this->json_file->write('{}');
            $this->json_file->close();
        }

        $this->json_file->open('read');
        $file_contents = $this->json_file->grabFile();
        $this->json_file->close();
        $json_dict = json_decode($file_contents, true);

        parent::__construct($json_dict);
    }

    public function __destruct() {
        $this->json_file->open('write');
        $this->json_file->write(json_encode($this->_getStore()));
        $this->json_file->close();
    }
}

//This is the magic. Being able to swap out KeyValueStore implementations easily by
//writing code to the KeyValueStore interface.
/*
class MemcacheKeyValueStore implements KeyValueStore {}

class RedisKeyValueStore implements KeyValueStore {}

class CassandraKeyValueStore implements KeyValueStore {}
*/
