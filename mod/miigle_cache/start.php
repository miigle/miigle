<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/engine/start.php');
require_once(dirname(__FILE__) . '/api.php');

function MiigleCacheInit() {
    register_entity_type('object', 'miigle_kv_store_object');
	register_plugin_hook('search', 'object:miigle_kv_store_object', 'SearchKeyValueStoreHook');
}

function SearchKeyValueStoreHook($hook, $type, $value, $params) {
	return false;
}

register_elgg_event_handler('init', 'system', 'MiigleCacheInit');
