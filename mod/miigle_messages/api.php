<?php
require_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

function _GetMessageEntitiesBetweenUsers($owner_guid, $other_user_guid, $trash, $limit, $offset, $count=false) {
    $options = array(
        'type' => 'object',
        'subtype' => 'messages',
        'metadata_name_value_pairs'=>array(
            array(
                'name' => 'other_user_guid',
                'value' => $other_user_guid,
            ),
            array(
                'name' => 'in_trash',
                'value' => intval($trash),
            ),
        ),
        'owner_guid' => $owner_guid,
        'limit' => $limit,
        'offset' => $offset,
        'count' => $count,
    );

    $results = elgg_get_entities_from_metadata($options);
    if (!$results) {
        return array();
    } else {
        return $results;
    }
}

function GetUnreadMessagesBetweenUsersCount($owner_guid, $other_user_guid, $trash=false) {
    return elgg_get_entities_from_metadata(array(
        'type' => 'object',
        'subtype' => 'messages',
        'metadata_name_value_pairs' => array(
            array(
                'name' => 'other_user_guid',
                'value' => $other_user_guid,
            ),
            array(
                'name'  => 'is_read',
                'value' => 0
            ),
            array(
                'name'  => 'in_trash',
                'value' => intval($trash),
            ),
        ),
        'owner_guid' => $owner_guid,
        'limit' => 99999999,
        'count' => true
    ));
}

//These two functions use almost identical options arrays. Make sure you change them in tandem.
function GetUnreadMessagesCount($owner_guid, $trash=false) {
    return elgg_get_entities_from_metadata(array(
        'type' => 'object',
        'subtype' => 'messages',
        'metadata_name_value_pairs' => array(
            array(
                'name'  => 'is_read',
                'value' => 0
            ),
            array(
                'name'  => 'in_trash',
                'value' => intval($trash),
            ),
        ),
        'owner_guid' => $owner_guid,
        'limit' => 99999999,
        'count' => true
    ));
}

function GetMessagesBetweenUsersCount($owner_guid, $other_user_guid, $trash=false) {
    return _GetMessageEntitiesBetweenUsers($owner_guid, $other_user_guid, false, 9999999, 0, true);
}

//Do these always come back sorted? Yes. 
function GetMessagesBetweenUsers($owner_guid, $other_user_guid, $trash=false, $limit=10, $offset=0) {
    $message_entity_list = _GetMessageEntitiesBetweenUsers($owner_guid, $other_user_guid, $trash, $limit, $offset);
    $message_list = array();

    foreach($message_entity_list as $message) {
        $message_object = array(
            'message_guid' => intval($message->guid),
            'message_text' => strval($message->description),
            'author_guid' => intval($message->author_guid),
            'other_user_guid' => intval($other_user_guid),
            'message_timestamp' => intval($message->time_created),
            'is_read' => ($message->is_read) ? true : false,
        );

        $message_list[] = $message_object;
    }

    return $message_list;
} 


//this is awful
function GetConversationsForUserCount($owner_guid, $trash=false) {
    return sizeof(GetConversationsForUser($owner_guid, $trash, 99999999));
}

function _CompareMessageTimestamps($message_one, $message_two) {    
    return ($message_one->time_created < $message_two->time_created) ? 1 : -1;
}

//this is awful
function GetConversationsForUser($owner_guid, $trash=false, $limit=10, $offset=0) {
     // Getting all messages that $user_guid owns. This is *not* scalable.
    $options = array(
        'type' => 'object',
        'subtype' => 'messages',
        'owner_guid' => $owner_guid,
        'metadata_name_value_pairs' => array(
            array(
                'name' => 'in_trash',
                'value' => intval($trash),
            ),
        ),
        'limit' => 9999999,
    );

    $messages = elgg_get_entities_from_metadata($options); //This function call here is *monster* expensive!
    if ($messages == false) {
        $messages = array();
    }

    //maps other_user_guid=>most_recent_message
    $most_recent_messages_dict = array();

    foreach($messages as $message) {
        $other_user_guid = $message->other_user_guid;
        if (!$other_user_guid) {
            //hackstorm. forgetting deleted users
            continue;
        }

        if (!isset($most_recent_messages_dict[$other_user_guid])
            || ($most_recent_messages_dict[$other_user_guid]->time_created < $message->time_created)) {
            $most_recent_messages_dict[$other_user_guid] = $message;
        }
    }

    uasort($most_recent_messages_dict, '_CompareMessageTimestamps');
    //now, $most_recent_messages_dict has the most recent message "last"

    $conversation_list = array();
    foreach($most_recent_messages_dict as $other_user_guid=>$message) {
        $conversation_list[] = array(
            'other_user_guid' => $other_user_guid,
            'latest_message' => array(
                'message_text' => $message->description,
                'message_timestamp' => intval($message->time_created),
                'is_reply' => ($message->author_guid == $owner_guid) ? true : false,
            ),
            'unread_count' => GetUnreadMessagesBetweenUsersCount($owner_guid, $other_user_guid, $trash),
            'total_count'  => GetMessagesBetweenUsersCount($owner_guid, $other_user_guid, $trash),
        );
    }

    return array_splice($conversation_list, $offset, $limit);
}

function GetShortMessageText($message_text) {
    if (strlen($message_text) > 50) {
        return substr($message_text, 0, 50) . '...';
    } 
    return $message_text;
}

function SendMiigleMessage($from_user_guid, $to_user_guid, $body, $notify=false) {
    global $message_send_flag;
    global $messages_pm;

    $message_send_flag = 1;
    
    if ($notify) {
        $messages_pm = 1;
    } else {
        $messages_pm = 0;
    }
    
    $outgoing_message = new ElggObject();
    $incoming_message = new ElggObject();

    // Tell the system it's a message
    $outgoing_message->subtype = "messages";
    $incoming_message->subtype = "messages";

    // Set its owner to the current user
    $outgoing_message->owner_guid = $from_user_guid;
    $outgoing_message->container_guid = $from_user_guid;
    $incoming_message->owner_guid = $to_user_guid;
    $incoming_message->container_guid = $to_user_guid;

    // For now, set its access to public (we'll add an access dropdown shortly)
    //Why in the name of god do we need to do this?
    $outgoing_message->access_id = ACCESS_PUBLIC;
    $incoming_message->access_id = ACCESS_PUBLIC;

    // Set its description appropriately
    $outgoing_message->description = $body;
    $incoming_message->description = $body;

    // set the metadata
    $outgoing_message->author_guid = $from_user_guid;
    $outgoing_message->is_read = 1;
    $outgoing_message->other_user_guid = $to_user_guid;
    $outgoing_message->in_trash = 0;

    $incoming_message->author_guid = $from_user_guid;
    $incoming_message->is_read = 0;
    $incoming_message->other_user_guid = $from_user_guid;
    $incoming_message->in_trash = 0;


    $incoming_success = $incoming_message->save();
    $outgoing_success = $outgoing_message->save();

    $outgoing_message->access_id = ACCESS_PRIVATE;
    $incoming_message->access_id = ACCESS_PRIVATE;
    
    $incoming_message->save();
    $outgoing_message->save();
    $message_send_flag = 0;    

    if ($outgoing_message->access_id != ACCESS_PRIVATE ||
        $incoming_message->access_id != ACCESS_PRIVATE) {
        $outgoing_message->delete();
        $incoming_message->delete();
        return false;
    }

    MiigleNotification::CreateNotification($to_user_guid, $from_user_guid, 
                                           $to_user_guid, 'message', 
                                           json_encode($body));

    return $outgoing_success && $incoming_success;
}

function IsUserOneFollowingUserTwo($user_guid_one, $user_guid_two) {
    return check_entity_relationship($user_guid_one, 'following_user', $user_guid_two) != false;
}

function AreUsersMutualFollowers($user_guid_one, $user_guid_two) {
    return (IsUserOneFollowingUserTwo($user_guid_one, $user_guid_two) && 
            IsUserOneFollowingUserTwo($user_guid_two, $user_guid_one));
}

function SendMiigleMessageProxy($to_user_guid, $message_string) {
    $from_user_guid = intval(get_loggedin_userid());
    $to_user_guid = intval($to_user_guid);

    if (!$from_user_guid) {
        return false;
    }

    $other_user = get_entity($to_user_guid);
    if (!$other_user || $other_user->getSubtype() != 'miigle_user') {
        return false;
    }
    //do mutual follow check
    if (!AreUsersMutualFollowers($from_user_guid, $to_user_guid)) {
        return false;
    }

    if (!SendMiigleMessage($from_user_guid, $to_user_guid, $message_string)) {
        return false;
    }

    //actually a list of 1 message - the message we just sent
    $latest_messages = GetMessagesBetweenUsers($from_user_guid, $to_user_guid, false, 1, 0);
    $latest_message = array_pop($latest_messages);
    return $latest_message;
}

function UpdateMessages($user_guid, $guids_list, $box_type, $action) {
    $trash = ($box_type == 'trash') ? true : false;

    if (!in_array($box_type, array('inbox', 'trash'))) {
        throw new Exception("Invalid box_type: $box_type");
    }
    if (!in_array($action, array('move-trash', 'move-inbox', 'mark-read', 'mark-unread', 'delete'))) {
        throw new Exception("Invalid action: $action");
    }

    $messages_list = array();
    $entities = array_map('get_entity', $guids_list);

    foreach ($entities as $e) {
        $type_string = sprintf('%s:%s', $e->getType(), $e->getSubtype());

        if ($type_string == 'object:messages') {
            $messages_list[] = $e;
        } else if ($type_string == 'user:miigle_user') {
            $messages_list = array_merge(
                    $messages_list,
                    _GetMessageEntitiesBetweenUsers($user_guid, $e->guid, $trash, 9999999, 0, false));
        } else {
            throw new Exception("Unknown type: $type_string");
        }
    }

    switch ($action) {
        case 'move-inbox':
            foreach($messages_list as $message) {
                $message->in_trash = 0;
            }
            break;

        case 'move-trash':
            foreach($messages_list as $message) {
                $message->in_trash = 1;
            }
            break;

        case 'mark-unread':
            foreach($messages_list as $message) {
                $message->is_read = 0;
            }
            break;

        case 'mark-read':
            foreach($messages_list as $message) {
                $message->is_read = 1;
            }
            break;

        case 'delete':
            foreach($messages_list as $message) {
                $message->delete();
            }
            break;

        default:
            throw new Exception("Invalid action: $action");
    }

    return true;
}

function GetConversationsForUserProxy($trash, $limit, $offset) {
    return GetConversationsForUser(get_loggedin_userid(), $trash=$trash, $limit, $offset);
}
function GetMessagesBetweenUsersProxy($other_user_guid, $trash, $limit, $offset) {
    return GetMessagesBetweenUsers(get_loggedin_userid(), $other_user_guid, $trash, $limit, $offset);
}
function GetUnreadMessagesCountProxy($trash) {
    return GetUnreadMessagesCount(get_loggedin_userid(), $trash);
}
function UpdateMessagesProxy($guids_list, $box_type, $action) {
    $user_guid = get_loggedin_userid();
    $guids_list = json_decode($guids_list, true);
    return UpdateMessages(get_loggedin_userid(), $guids_list, $box_type, $action);
}

expose_function('messages.send', 'SendMiigleMessageProxy',
    array(
        'to_user_guid' => array('type'=>'int', 'required'=>true),
        'message' => array('type'=>'string', 'required'=>true),
    ),
    'Send a message',
    'POST',
    false,
    false
);
expose_function('messages.get_conversations', 'GetConversationsForUserProxy',
    array(
        'trash' => array('type'=>'bool', 'required'=>true),
        'limit' => array('type'=>'int',  'required'=>false, 'default'=>10),
        'offset' => array('type'=>'int', 'required'=>false, 'default'=>0),
    ),
    'Get conversations',
    'POST',
    false,
    false
);
expose_function('messages.get_conversation', 'GetMessagesBetweenUsersProxy',
    array(
        'other_user_guid' => array('type'=>'int',  'required'=>true),
        'trash'           => array('type'=>'bool', 'required'=>true),
        'limit'           => array('type'=>'int',  'required'=>false, 'default'=>10),
        'offset'          => array('type'=>'int',  'required'=>false, 'default'=>0),
    ),
    'Get conversation',
    'POST',
    false,
    false
);
expose_function('messages.get_unread_messages_count', 'GetUnreadMessagesCountProxy',
    array(
        'trash' => array('type'=>'bool', 'required'=>false, 'default'=>'false'),
    ),
    'Get unread messages count',
    'POST',
    false,
    false
);
expose_function('messages.update', 'UpdateMessagesProxy',
    array(
        'guids_list' => array('type'=>'string', 'required'=>true),
        'box_type'   => array('type'=>'string', 'required'=>true),
        'action'     => array('type'=>'string', 'required'=>true)
    ),
    'Update messages',
    'POST',
    false,
    false
);
