$(document).ready(function() {
    //don't use any of this 

    /* Setting Ajax submit for sending message */
    $('#modal_send_message_form').submit(function(event) {
        var $this = $(this);
        var post_url = $this.attr('action');
        var post_data = $this.serialize();
        var in_messages = ($('#messages-list').length > 0) ? true : false;
        var $ajax_loader = $('<img src="/mod/miigle_theme/graphics/ajax-loader.gif">');

        if (in_messages) {
            $ajax_loader.css('display', 'block');
            $ajax_loader.css('margin', '20px auto auto auto');
            $ajax_loader.hide();
            $ajax_loader.insertBefore($('#messages-list').children()[0]);
            $ajax_loader.slideToggle();
        } else {
            $('#send-message-button').text('Sending...');
        }

        $.ajax({
            type: 'POST',
            url: post_url,
            data: post_data,
            success: function(data, status, xhr) {
                if (in_messages) {
                    var $new_message = $(data);
                    $new_message.css('display', 'none');
                    $new_message.insertBefore($('#messages-list').children()[0]);

                    $ajax_loader.slideToggle();
                    setTimeout(function() {
                        $new_message.slideToggle();
                    }, 400);
                } else {
                    $('#send-message-button').text('Sent!');
                }
            },
            error: function(xhr, status, error) {
                //refreshing the page to show the error. 
                location.reload();
            },
        });

        //clear the text area so if the user clicks reply again they don't see the text they just sent
        $('#send_message_textarea').val(''); 

        setTimeout("$('#modal_send_message').modal('hide');", 50);
        event.preventDefault();
    });
    /* Setting Ajax submit for sending message */

    /* Ajax update-action form submit */
    $('#update-form').submit(function(event) {
        $('#ajax-loader').css('display', '');

        var $this = $(this);
        var post_url = $this.attr('action');
        var post_data = $this.serialize();

        $.ajax({
            type: 'POST',
            url: post_url,
            data: post_data,
            success: function(data, status, xhr) {
                console.log('win');
            },
            error: function(xhr, status, error) {
                console.log(xhr.status);
                console.log(status, error);
            },
            complete: function() {
                $('#ajax-loader').css('display', 'none');
                location.reload();
            },
        });
        event.preventDefault();
    });
    /* Ajax update-action form submit */

});