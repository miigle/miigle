var miigle_messages = angular.module('miigle.messages', ['miigle.messages.send_message', 'miigle.stub']);

var miigle_messages_send_message = angular.module(
    'miigle.messages.send_message', ['miigle']);


miigle_messages_send_message.directive('messagemodal', function() {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/mod/miigle_messages/templates/send_message_form.html',
    controller: 'SendMessageModalController',
    scope: {
      sendToUserGuid: '=',
    },
  };
});

miigle_messages_send_message.controller('SendMessageModalController', 
  ['$scope', '$element', 'MessagesFactory',
  function($scope, $element, MessagesFactory) {
    'use strict';

    var init;

    init = function() {
      $scope.message_body = '';
      $scope.other_user_dict = miigle.getEntity($scope.sendToUserGuid);

      $scope.$on('message:show-modal', function(event) {
        $element.modal('show');
      });

      /* Setting message text box to auto-focux on show */
      $element.on('shown.bs.modal', function(event) {
        $element.find('textarea').focus();
      });

      /* Setting shift enter to send message */
      $scope.watchForKeyCombos = function($event) {
        if ($event.keyCode == 13 && $event.shiftKey) {
          $event.preventDefault();
          $scope.sendMessage();
        }
      };
    };

    $scope.sendMessage = function() {
      if ($scope.message_body.length > 0) {
        $element.modal('hide');
        MessagesFactory.sendMessage($scope.sendToUserGuid, $scope.message_body);
        $scope.message_body = '';
      }
    };

    init();
  }
]);