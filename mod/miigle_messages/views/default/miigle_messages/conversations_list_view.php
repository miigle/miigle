<div ng-if="conversations_view_list.length == 0" style="padding-top: 20px; padding-bottom: 20px; text-align: center;">Empty motherfucker</div>

<table class="table table-hover" style="margin-bottom: 0">
	<tr ng-repeat="conversation in conversations_view_list" 
      class="message_read_0"
      href="#/messages/{{box_type}}/{{conversation.other_user_dict.username}}">
      this is a convo {{conversation}}
		<!--checkbox-->
          <td style="width: 50px; vertical-align: middle;" class="message-select">
              <div class="checkbox">
                  <!--eh...-->
                  <input id="conversation_{{conversation.other_user_guid}}" type="checkbox" name="message_id[]" value="{{conversation.other_user_guid}}" />    
                  <label for="conversation_<?php echo $other_user_guid; ?>"></label>
              </div>
          </td>

          <!--icon-->
          <td style="width: 125px;">
              <img src="/pg/image/<?php echo $other_user->icon_guid; ?>?w=100&h=100" class="icon">
          </td>

          <!--other user name and job-->
          <?php if ($other_user): //get the icon of the user who owns the message ?>
              <td class="message-details">
                  <b><?php echo $other_user->fname . ' ' . $other_user->lname; ?>
                  <?php if ($conversation['unread_count'] > 0) echo '(' . $conversation['unread_count'] . ')'; ?>
                  </b>

                  <br>
                  <p class="text-muted">                                            
                      <?php if ($reply): ?>
                          <span class="glyphicon glyphicon-share-alt"></span>
                      <?php endif; ?>
                      <?php echo $message_text; ?>
                  </p>                                  
              </td>
          <?php else: // deleted sender ?>
              <td>
                  <strong><?php echo elgg_echo('messages:deleted_sender'); ?></strong><br>    
                  <?php echo $message_text; ?>
              </td>
          <?php endif; ?>                                

          <td class="message-delete text-muted">
              <div class="message-date"><small><?php echo elgg_view_friendly_time($timestamp); ?></small></div>
          </td>
	</tr>
</table>

<style>
	td:not(.message-select)  {
		cursor: pointer;
	}
  td > a > .icon:hover {
    -webkit-box-shadow: 0px 0px 5px 2px rgba(255,224,0,1);
    -moz-box-shadow: 0px 0px 5px 2px rgba(255,224,0,1);
    box-shadow: 0px 0px 5px 2px rgba(255,224,0,1);
  }
</style>
<script>
	$('td:not(.message-select)').click(function() {
		var $this = $(this);
		window.location = $this.parent().attr('href');
	});
</script> 






