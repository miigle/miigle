<?php
$messages_list = $vars['messages_list'];
?>

<?php 
foreach($messages_list as $message) {
    $user = get_entity($message['author_guid']);
    $top_right_view = elgg_view('miigle_messages/message/message_top_right_view', array(
        'message_guid'=>$message['message_guid'],
        'time_created'=>elgg_view_friendly_time($message['message_timestamp']),
    ));
    $body_view = elgg_view('miigle_messages/message/message_body_view', array(
        'message_text'=>$message['message_text'],
    ));

    $user_full_name = $user->fname . ' ' . $user->lname;
    $work_info_dict = json_decode($user->work_info_dict, true);
?>


<div class="whitebox" style="margin: 20px;">
    <div class="whitebox-content <?php if (!$message['is_read']) echo 'message_read_0'; ?>">
        <div class="whitebox-header" style="padding-left: 10px; background: #f7f7f7;">
            <?php echo $top_right_view; ?>
            <div class="media" style="margin-top: 5px;">
                <div class="pull-left">
                    <input style="display: none;" class="message-select message-checkbox" id="message_<?php echo $message['message_guid']; ?>" 
                           type="checkbox" name="message_id[]" value="<?php echo $message['message_guid']; ?>" />    
                    <label for="message_<?php echo $message['message_guid']; ?>"></label>
                    <img src="/pg/image/<?php echo $user->icon_guid; ?>?w=50&h=50" class="icon">
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><a href="/pg/user/<?php echo $user->username; ?>"><?php echo $user_full_name; ?></a></h4>
                    <strong class="text-muted">
                        <?php 
                        if ($work_info_dict['current_title']) {
                            echo $work_info_dict['current_title'];
                        }
                        if ($work_info_dict['current_title'] && $work_info_dict['current_employer']) {
                            echo ' at ';
                        }
                        if ($work_info_dict['current_employer']) {
                            echo $work_info_dict['current_employer'];
                        }
                        ?>
                    </strong>
                </div>                                                  
            </div>
        </div><!--class-header-->

        <div class="whitebox-body">                              
            <?php echo $body_view; ?> 
        </div>
    </div><!-- /.class-content -->
</div><!-- /.class-dialog -->    

<?php
} //closing the foreach loop curly brace