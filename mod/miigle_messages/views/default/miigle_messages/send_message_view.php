<form id="modal_send_message_form">
    <?php echo elgg_view('input/securitytoken'); ?>
    <input type="hidden" name="send_to" value="<?php echo $vars['user']->guid; ?>" />

    <div class="modal fade modal-message-reply" id="modal_send_message" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="send-message-close" type="button" class="close" aria-hidden="true">&times;</button>
                    <div class="media" style="margin-top: 5px;">
                        <div class="pull-left">
                            <img src="/pg/image/0?h=50&w=50">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="/pg/user/joshfester">Josh Fester</a></h4>
                            <strong class="text-muted">CTO at Miigle</strong>
                        </div>                                                  
                    </div>
                </div><!--modal-header-->

                <div class="modal-body">                              
                    <textarea id="send_message_textarea" class="form-control" name="message"></textarea>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-default btn-miigle">Send</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</form>
