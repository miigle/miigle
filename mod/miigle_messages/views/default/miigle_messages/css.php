.modal-open {
    overflow: auto;
}

.unread-count-display {
    background: red;
    color: white;

    position: absolute;
    top: 15px;
    right: 5px;

    font-weight: bold;
    font-size: 10px;
    border-radius: 2px;
    line-height: 14px;
    height: 14px;
    min-width: 10px;
    text-align: center;
    padding-left: 1px;
    padding-right: 1px;

    background-image: -webkit-gradient(
    linear,
    left top,
    left bottom,
    color-stop(0, #FA3C45),
    color-stop(1, #DC0D17)
    );
    background-image: -o-linear-gradient(bottom, #FA3C45 0%, #DC0D17 100%);
    background-image: -moz-linear-gradient(bottom, #FA3C45 0%, #DC0D17 100%);
    background-image: -webkit-linear-gradient(bottom, #FA3C45 0%, #DC0D17 100%);
    background-image: -ms-linear-gradient(bottom, #FA3C45 0%, #DC0D17 100%);
    background-image: linear-gradient(to bottom, #FA3C45 0%, #DC0D17 100%);

    text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);

    -webkit-box-shadow: 0px 1px 1px 0px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 1px 1px 0px rgba(0,0,0,0.75);
    box-shadow: 0px 1px 1px 0px rgba(0,0,0,0.75);
}

#message_dropdown {
    min-width: 400px;
}
#message_dropdown > li > a:hover,
#message_dropdown > li > a:focus {
  color: #000000;
  background-color: #EDEDED;
}
#message_dropdown > li > .unread {
    background-color: #F3F3F3;
}

.message-links {
    margin-top:5px;
}
.message-links a {
    margin-right:5px;
}
.message-links a:hover, 
.message-links a.active {
    color:#333;
    text-decoration:none;
}
#messages .table tbody>tr:first-child>td {
    border-top:none;
}
#messages .table tr td:first-child {
    padding-left:20px;
    padding-right:20px;
}
#messages .table tr td:last-child {
    padding-right:20px;
}
#messages .message_read_1 {
    opacity:0.8;
    filter:alpha(opacity=80);
}
#messages .message_read_1:hover {
    opacity:1;
    filter:alpha(opacity=100);
}
#messages .message_read_0 {
    background:#f0f0f0;
}
#messages .message_read_0 .message-details a {
    font-weight:bold;
}
#messages .message-details a,
#messages .message-details a:hover {
    color:#333;
}
#messages .table>tbody>tr.active>td {
    background:#fffce9;
}
#messages .table td.message-delete {
    text-align:right;    
    min-width:120px;
}
.modal-message-reply .modal-header {
    font-size:13px;
}
.modal-message-reply textarea {
    height:100px;
}
#messages .pagination .page-count {
    margin-right: 10px;
}
@media (min-width:992px) {
    #messages .table td {
        height:100px;
    }
    #messages .table td.message-delete {
        min-width:180px;
        vertical-align:top;
    }
    #messages .table td.message-delete .message-date {
        margin:10px 0 30px 0;
    }
}

.messages_error {
	border:4px solid #D3322A;
	background:#F7DAD8;
	color:#000000;
	padding:3px 10px 3px 10px;
	z-index: 8000;
	margin:0;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 10px;
    max-width: 950px;
    /*
	position:fixed;
	top:30px;
    */
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	cursor: pointer;
}

.message-links .active {
    border-style: solid;
    border-left: 0;
    border-right: 0;
    border-top: 0;
    border-bottom: 2px solid #333;
    padding-bottom: 12px;
}

