<?php 
//pagination
$baseurl = $_SERVER['REQUEST_URI'];
$nav = '';
$offset = $vars['offset'];
$limit = $vars['limit'];
$current_page_number = ($offset / $limit) + 1;
$num_pages = ceil(intval($vars['total_message_count']) / $limit);

if ($offset > 0) {
    $newoffset = $offset - $limit;
    if ($newoffset < 0)
        $newoffset = 0;

    $prevurl = elgg_http_add_url_query_elements($baseurl, array('offset' => $newoffset, 'limit'=>$limit));

    $nav .= '<a class="btn btn-default" href="' . $prevurl . '"><span class="glyphicon glyphicon-arrow-left"></span></a> ';
}

if ($current_page_number < $num_pages) {
    $newoffset = $offset + $limit;
    $nexturl = elgg_http_add_url_query_elements($baseurl, array('offset' => $newoffset, 'limit'=>$limit));

    $nav .= '<a class="btn btn-default" href="' . $nexturl . '"><span class="glyphicon glyphicon-arrow-right"></span></a> ';
}
?>

<script>
var miigle_messages = {};
<?php if ($vars['other_user']): ?>
    miigle_messages.other_user_dict = <?php echo json_encode($vars['other_user']->toDict()); ?>;
<?php endif; ?>
</script>

<!-- this forms handles the actions. move to trash, delete, mark as read/unread, etc -->
<form id="update-form" method="POST" action="<?php echo $vars['url'] ?>action/miigle_messages/update"
      ng-app="miigle.messages" ng-controller="MiigleMessagesController">
    <div ng-view></div>
    <?php echo elgg_view('input/securitytoken'); ?>
    <input type="hidden" name="message-folder" value="<?php echo $vars['messages_folder']; ?>">
    <input id="update-action" type="hidden" name="update-action" value="">
    <input id="update-action-type" type="hidden" name="update-action-type" value="">

    <div id="messages" class="whitebox"><!-- start the main messages wrapper div -->
        <h1 class="whitebox-header nav" style="padding-bottom: 0;">
            <ul id="inbox_trash_tabs" style="border-bottom: 0;" class="nav nav-tabs pull-right">
                <li class="<?php if ($vars['messages_folder'] == 'inbox' && $vars['view_type'] != 'messages_list') echo 'active'; ?>" >
                    <a class="text-muted" href="/pg/messages/inbox">
                    Inbox <?php if ($vars['unread_count'] > 0) echo "({$vars['unread_count']})"; ?>
                    </a>
                </li>
                <li class="<?php if ($vars['messages_folder'] == 'trash' && $vars['view_type'] != 'messages_list') echo 'active'; ?>" >
                    <a class="text-muted" href="/pg/messages/trash">Trash</a>
                </li>
            </ul>
            <div id="messages-envelope-header" style="padding-top: 10px;">
                <span class="glyphicon glyphicon-envelope"></span> Messages
            </div>
        </h1>

        <style>
        #inbox_trash_tabs > li.active > a {
            background: #f7f7f7;
        }
        @media (max-width: 450px) {
            #messages-envelope-header {
                display: none;
            }
        }
        </style>

    	<div class="message-controls whitebox-subhead">
    	    <div class="pull-right">
                <?php if ($vars['view_type'] == 'messages_list' && 
                          $vars['messages_folder'] == 'inbox' && 
                          AreUsersMutualFollowers($vars['other_user']->guid, $vars['page_owner'])): ?>
                    <a id="send-message-button" class="btn btn-default" data-toggle="modal" style="padding-left: 12px;" ng-click="showModal()">
                        <i class="fa fa-reply"></i>
                    </a>
                    <messagemodal send-to-user-dict="other_user_dict" message-send-callback="messageSendCallback"
                                  message-before-send="messageBeforeSend"></messagemodal>
                <?php endif; ?>
    	    </div>    
    	    <div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-check-square-o">&nbsp;&nbsp;</i><i class="fa fa-caret-down"></i>
                    </button>
                    <ul id="select-list" class="dropdown-menu" role="menu">
                        <li><a id="select-all" href="#">All</a></li>
                        <li><a id="select-none"href="#">None</a></li>

                        <?php if ($vars['view_type'] == 'messages_list'): ?>
                            <li><a id="select-some" href="#">Some</a></li>
                        <?php else: ?>
                            <li><a id="select-unread" href="#">Unread</a></li>
                            <li><a id="select-read" href="#">Read</a></li>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-cog"></i>&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                    </button>
                    <ul id="action-list" class="dropdown-menu" role="menu">
                        <?php if ($vars['messages_folder'] == 'trash'): ?>
                            <li><a id="move-to-inbox" href="#">Move to Inbox</a></li>
                            <li><a id="delete-permanently" href="#">Delete Permanently</a></li>
                        <?php else: ?>
                            <?php if ($vars['view_type'] != 'messages_list'): ?>
                                <li><a id="mark-as-read" href="#">Mark as Read</a></li>
                                <li><a id="mark-as-unread" href="#">Mark as Unread</a></li>
                            <?php endif; ?>
                            <li><a id="move-to-trash" href="#">Move to Trash</a></li>
                        <?php endif; ?>
                    </ul>
                </div>
                <img id="ajax-loader" height="31px" style="display: none;" src="/mod/miigle_theme/graphics/ajax-loader.gif" />
    	    </div>
    	</div>   

        <div id="messages-list">
        	<?php echo $vars['body']; ?>
        </div>

        <div id="footer-div" class="whitebox-footer">
            <?php if (!empty($nav)): ?>
                <div class="pull-right">
                    <span class="text-muted page-count"><?php echo $current_page_number; ?> / <?php echo $num_pages; ?></span>&nbsp
                    <?php echo $nav; ?>
                </div>
            <?php endif; ?>

            <div id="rows-select-dropdown" class="btn-group dropup">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <?php echo $limit; ?> Rows &nbsp<span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                    <?php 
                    foreach(array(5, 10, 25, 50) as $limit_option) {
                        $new_url = elgg_http_add_url_query_elements($baseurl, array('limit'=>$limit_option, 'offset'=>0));
                        if ($limit_option != $limit) {
                            echo "<li><a href=\"$new_url\">$limit_option Rows</a></li>";
                        }
                    }
                    ?>
                </ul>
            </div><!--btn-group dropup-->
            
            <button id="to-top-button" class="btn btn-default" style="display: none;">Top</button>
        </div>
        <span id="find-me"></span>
    </div><!--#messages-->
</form>