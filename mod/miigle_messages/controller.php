<?php
require_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
require_once(dirname(__FILE__) . '/api.php');

// You need to be logged in!
gatekeeper();

// /messages is shorthand for messages/inbox
if (!isset($page[0])) {
	forward('pg/messages/inbox');
}

// Get offset and limit
$offset = get_input('offset', 0);
$limit = get_input('limit', 10);
if ($limit <= 0) {
	$limit = 10;
}

// Get the logged in user, you can't see other peoples messages so use session id
$page_owner = get_loggedin_user();
set_page_owner($page_owner->guid);

//messages_folder is an awkward name, but I couldn't think of anything better
//messages_folder should be either 'inbox' or 'trash'
$messages_folder = $page[0];
if (($messages_folder != 'inbox') && ($messages_folder != 'trash')) {
	forward('pg/messages/inbox');
}
$trash = ($messages_folder == 'trash') ? true : false;

//page[1] is a username, if given. that means message_list
$view_type = (isset($page[1])) ? 'messages_list' : 'conversations_list';

// /inbox or /trash
if ($view_type == 'conversations_list') {
	$conversations_list = GetConversationsForUser($page_owner->guid, $trash, $limit, $offset);
	$body = elgg_view('miigle_messages/conversations_list_view', array(
		'conversations_list' => $conversations_list,
		'messages_folder' => $messages_folder,
	));
	$total_message_count = GetConversationsForUserCount($page_owner->guid, $trash);
} else { // /inbox/username or /trash/username
	$other_user_username = $page[1];

	//If the username coming in is all numbers, it is a deleted user
	if (is_numeric($other_user_username)) {
		$other_user_guid = $other_user_username;
	} else {
		$other_user = get_user_by_username($other_user_username);
		if (!$other_user) {
			forward('pg/messages/inbox');
		}
		$other_user_guid = $other_user->guid;
	}

	$messages_list = GetMessagesBetweenUsers($page_owner->guid, $other_user_guid, $trash, $limit, $offset);
	$body = elgg_view('miigle_messages/messages_list_view', array(
		'messages_list' => $messages_list,
	));

	$total_message_count = GetMessagesBetweenUsersCount($page_owner->guid, $other_user_guid, $trash);
}

$unread_count = GetUnreadMessagesCount($page_owner->guid);
$entire_messages_view = elgg_view('miigle_messages/miigle_messages_shell', array (
	'offset' => $offset,
	'limit' => $limit,
	'total_message_count' => $total_message_count,
	'messages_folder' => $messages_folder,
	'view_type' => $view_type,
	'unread_count' => $unread_count,
	'body' => $body,
	'other_user' => $other_user,
	'page_owner' => $page_owner,
));

//these are not used...
$title = 'notitle';
$sidebar = 'nosidebar';

$page_body = elgg_view_layout('one_column', $entire_messages_view);
page_draw($title, $page_body, $sidebar);