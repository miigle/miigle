<?php
require_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
require_once(dirname(__FILE__) . '/api.php');

function MiigleMessagesInit() {
    elgg_extend_view('js', 'miigle_messages/js');
    elgg_extend_view('css', 'miigle_messages/css');
    register_page_handler('messages', 'MiigleMessagesPageHandler');
	register_plugin_hook('permissions_check:metadata', 'object', 'MiigleMessagesCanEditMetadata');
}

function MiigleMessagesPageHandler($page) {
    if (!include_once(dirname(__FILE__) . '/controller.php')) {
        return false;
    }
    return true; 
}

register_elgg_event_handler('init', 'system', 'MiigleMessagesInit');

function MiigleMessagesSiteNotifyHandler(ElggEntity $from, ElggUser $to, $subject, $message, array $params = NULL) {
	global $CONFIG;
	
	if (!$from)
		throw new NotificationException(sprintf(elgg_echo('NotificationException:MissingParameter'), 'from'));
		 
	if (!$to)
		throw new NotificationException(sprintf(elgg_echo('NotificationException:MissingParameter'), 'to'));
		
	/* wtf is happening here
	global $messages_pm;
	if (!$messages_pm)
		return messages_send($message,$to->guid,$from->guid,0,false,false);
	else return true;
	*/

	return true;
}

/**
 * Override the canEditMetadata function to return true for messages
 *
 */
function MiigleMessagesCanEditMetadata($hook_name, $entity_type, $return_value, $parameters) {
	global $message_send_flag;
	//return true;
	
	if ($message_send_flag == 1) {
		$entity = $parameters['entity'];
		if ($entity->getSubtype() == "messages") {
			return true;
		}
	}
	
	return $return_value;
	
}
/**
 * Override the canEdit function to return true for messages within a particular context.
 *
 */
function MiigleMessagesCanEditContainer($hook_name, $entity_type, $return_value, $parameters) {
	//return true;
	global $message_send_flag;
	return $message_send_flag || $return_value;
}

/**
 * Override the canEdit function to return true for messages within a particular context.
 *
 */
function MiigleMessagesCanEdit($hook_name, $entity_type, $return_value, $parameters) {
	global $message_send_flag;

	if ($message_send_flag == 1) {
		$entity = $parameters['entity'];
		if ($entity->getSubtype() == "messages") {
			return true;
		}
	}
	
	return $return_value;
	
}

// Register a notification handler for site messages
register_notification_handler('site', 'MiigleMessagesSiteNotifyHandler');

register_plugin_hook('permissions_check', 'object', 'MiigleMessagesCanEdit');
register_plugin_hook('container_permissions_check', 'object', 'MiigleMessagesCanEditContainer');
