<?php

/*
 * Here we handle moving messages between folders, (inbox, trash) marking messages as read and unread, and deleting
 * messages permanently.
 */
require_once(dirname(dirname(__FILE__)) . '/api.php');

$valid_update_actions = array(
    'move-to-inbox',
    'move-to-trash',
    'delete-permanently',
    'mark-as-read',
    'mark-as-unread',
);

$valid_update_action_types = array(
    'conversations',
    'messages',
);

$valid_message_folders = array(
    'inbox',
    'trash',
);

// Need to be logged in to do this
gatekeeper();
action_gatekeeper();

$owner = get_loggedin_user();

$guid_array = get_input('message_id', array());
if (!is_array($guid_array)) {
    $guid_array = array($guid_array);
}

$update_action = get_input('update-action', 'none-supplied'); 
$update_action_type = get_input('update-action-type', 'none-supplied');
$message_folder = get_input('message-folder', 'none-supplied');
$trash = ($message_folder == 'trash') ? true : false;

if (!in_array($update_action, $valid_update_actions)) {
    register_error("Invalid update-action $update_action");
    http_response_code(400);
    exit;
}

if (!in_array($update_action_type, $valid_update_action_types)) {
    register_error("Invalid update-action-type $update_action_type");
    http_response_code(400);
    exit;
}

if (!in_array($message_folder, $valid_message_folders)) {
    register_error("Invalid message-folder $message_folder");
    http_response_code(400);
    exit;
}

$message_list = array();
if ($update_action_type == 'messages') {
    //Saying, "get me all the messages with these guids
    $message_list = elgg_get_entities(array(
        'wheres' => 'guid IN (' . join(',', $guid_array) . ')',
        'limit' => 99999999,
    ));
} else {
    foreach($guid_array as $user_guid) {
        //In Python this would read $message_list.extend(_GetMessages....)
        //example - $message_list = [1, 2, 3]
        //_GetMessage... = [4, 5, 6]
        //After merge, $message_list = [1, 2, 3, 4, 5, 6]
        $message_list = array_merge(
            $message_list, 
            _GetMessageEntitiesBetweenUsers($owner->guid, $user_guid, $trash, 99999999, 0, false)
        );
    }
}

switch ($update_action) {
    case 'move-to-inbox':
        foreach($message_list as $message) {
            $message->in_trash = 0;
        }
        break;

    case 'move-to-trash':
        foreach($message_list as $message) {
            $message->in_trash = 1;
        }
        break;

    case 'mark-as-unread':
        foreach($message_list as $message) {
            $message->is_read = 0;
        }
        break;

    case 'mark-as-read':
        foreach($message_list as $message) {
            $message->is_read = 1;
        }
        break;

    case 'delete-permanently':
        foreach($message_list as $message) {
            $message->delete();
        }
        break;

    default:
        break;  
}

forward($_SERVER['HTTP_REFERER']);
?>
