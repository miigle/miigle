<?php
require_once(dirname(dirname(dirname(dirname(__FILE__)))) . "/engine/start.php");
require_once(dirname(dirname(__FILE__)) . '/api.php');

gatekeeper();
action_gatekeeper();

$from_user_guid = get_loggedin_user()->guid;

// Get input data
$message_contents = get_input('message'); // the message
$to_user_guid = get_input('send_to'); // this is the user guid to whom the message is going to be sent

if (empty($to_user_guid)) {
	register_error(elgg_echo("messages:user:blank"));
	http_response_code(400);
	exit;
}

$user = get_user($to_user_guid);
if (!$user) {
	register_error(elgg_echo("messages:user:nonexist"));
	http_response_code(400);
	exit;
}

if (!AreUsersMutualFollowers($to_user_guid, $from_user_guid)) {
	register_error(elgg_echo('messages:notmutualfollowers'));
	http_response_code(400);
	exit;
}

// Make sure the message field is not blank
if (empty($message_contents)) {
	register_error(elgg_echo("messages:blank"));
	http_response_code(400);
	exit;
}

$result = SendMiigleMessage($from_user_guid, $to_user_guid, $message_contents);
if (!$result) {
	http_response_code(500);
	exit;
}

//actually a list of 1 message - the message we just sent
$latest_message = GetMessagesBetweenUsers($from_user_guid, $to_user_guid, false, 1, 0);

$new_message_view = elgg_view('miigle_messages/messages_list_view', array(
	'messages_list' => $latest_message,
));

echo $new_message_view;

exit;
?>
