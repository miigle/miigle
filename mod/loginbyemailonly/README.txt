Login by Email Only 0.5
Copyright (c) 2010 Lenny Urbanowski

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  
USA


** ABOUT **

This plugin replaces the username login system with an e-mail login system.

Login by Email Only is released under the GNU Public License (GPL), which
is supplied in this distribution as LICENSE.

This plugin and the contained documents are HIGHLY based off of the work of
Pedro Prez on the plugin "Login By Email". The majority of the code on this project
is from his original project. I just modified it to eliminate the need for usernames
all together.


Changes include

 - Chaged to E-mail login / registration only
 - Removed username field from registration form
 - Removed username label on login form
 - Removed code that allowed username login
 - Added feature to generate a username based on the display name



** CONTRIBUTORS **

See CONTRIBUTORS.txt for development credits.


** LICENSE INFORMATION **

This software is governed under rights, privileges, and restrictions in 
addition to those provided by the GPL v2.  Please carefully read the
LICENSE.txt file for more information.


** INSTALLATION **

	* Unzip the file to the elgg/mods/ directory.

	* Go to your Elgg tools administration section, find the new tool, and 
	  enable it.
	  
	* Enjoy.
	  
	 
** TODO **
	  
	* Nothing TODO at the moment.
	
	
** CHANGES **

v0.5 (7/6/2010)
	Initial Public Release
	
v0.65 (9/2/2010)
	Adjustments for 1.7.2


