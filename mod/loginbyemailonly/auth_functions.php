<?php
/**
 * Login by Email Only
 * 
 * @author Lenny Urbanowski
 * @link http://community.elgg.org/pg/profile/itsLenny
 * @copyright (c) Lenny Urbanowski 2010
 * @license GNU General Public License (GPL) version 2
 *
 *
 * Based on: Login by Email
 * 
 * @author Pedro Prez
 * @link http://community.elgg.org/pg/profile/pedroprez
 * @copyright (c) Keetup 2009
 * @link http://www.keetup.com/
 * @license GNU General Public License (GPL) version 2
 */
global $CONFIG;


#PAM AUTHENTIFICATION
function pam_auth_emailpass($credentials = NULL) {
    if (is_array($credentials) && ($credentials['email']) && ($credentials['password'])) {
        if ($user = get_user_by_email($credentials['email'])) {
            if(is_array($user)) {
                $user = array_shift($user);
                
                // Let admins log in without validating their email, but normal users must have validated their email
                if ((!$user->admin) && (!$user->validated) && (!$user->admin_created)) {
                    return false;
                }   
                
                if ($user->password == generate_user_password($user, $credentials['password'])) { 
                    return true;
                }   
            }
        }
    }
    return false;
}
    
#AUTHENTIFICATION BY MAIL
function authenticate_by_email($email, $password) {
    if (pam_auth_emailpass(array('email' => $email, 'password' => $password))) {
        $user = get_user_by_email($email);
        if(is_array($user)) {
            return array_shift($user);
        }
    }
    return false;
}

function ValidateLinkedInOauthDict($linkedin_oauth_dict) {
    global $CONFIG;

    if ($linkedin_oauth_dict != NULL && 
        $linkedin_oauth_dict['signature_version'] == 1 &&
        $linkedin_oauth_dict['signature_order'] &&
        is_array($linkedin_oauth_dict['signature_order'])) {

        $base_string = '';
        foreach ($linkedin_oauth_dict['signature_order'] as $key) { 
            if (isset($linkedin_oauth_dict[$key])) {
                $base_string .= $linkedin_oauth_dict[$key];
            }
        }

        $signature = base64_encode(hash_hmac('sha1', $base_string, $CONFIG->LINKEDIN_SECRET_KEY, true));

        if ($signature == $linkedin_oauth_dict['signature']) {
            return $linkedin_oauth_dict['member_id'];
        }
    }

    return false;
}

function Base64URLDecode($input) {
    return base64_decode(strtr($input, '-_', '+/'));
}

function ValidateFacebookAuthResponseString($facebook_signed_request_string) {
    global $CONFIG;
    list($encoded_signature, $payload) = explode('.', $facebook_signed_request_string, 2); 

    $signature = Base64URLDecode($encoded_signature);
    $expected_signature = hash_hmac('sha256', $payload, $CONFIG->FACEBOOK_APP_SECRET, $raw=true);

    if ($signature !== $expected_signature) {
        return false;
    } else {
        $data = json_decode(Base64URLDecode($payload), true);
        return $data['user_id'];
    }
}