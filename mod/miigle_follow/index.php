<?php

    /**
     * Elgg ideas index page
     * 
     * @package ElggIdeas
     */

    // Load Elgg engine
        require_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

    // access check
    gatekeeper();
        
    // Get the current page's owner
        $page_owner = page_owner_entity();
        if ($page_owner === false || is_null($page_owner)) {
            
            // guess that logged in user is the owner - if no logged in send to login page
            if (!isloggedin()) {
                forward('/');
            }
            
            $page_owner = $_SESSION['user'];
            set_page_owner($_SESSION['guid']);
        }

    //set ideas page title
        if ($page_owner == $_SESSION['user']) {
            $area2 = elgg_view_title(elgg_echo('idea:your'));
        } else {
            $area2 = elgg_view_title(sprintf(elgg_echo('idea:user'),$page_owner->name));
        }

        $offset = (int)get_input('offset', 0);
        
    // Get a list of blog posts
        $area2 .= elgg_list_entities(array('type' => 'object', 'subtype' => 'idea', 'container_guid' => page_owner(), 'limit' => 10, 'offset' => $offset, 'full_view' => FALSE, 'view_type_toggle' => FALSE));
        
    // Display them in the page
        $body = elgg_view_layout("three_column", '', $area2, $area3);
        
    // Display page
        page_draw(sprintf(elgg_echo('blog:user'),$page_owner->name),$body);
        
?>