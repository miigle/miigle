<?php
    /**
     * list all followers of a user
     */

    //check access
    gatekeeper();
    
    //get username
    $username = get_input("username", "");
    
    //get display mode
    $mode = get_input("mode", "");
    
    //get user
    $user = get_user_by_username($username);
    
    if(!$user){
        $msg = "User not found.";
        if($mode==="widget"){
            die($msg);
        }else{
            register_error($msg);
            forward(REFERER);
        }
    }
    
    //everything seems ok, get list of all followers
    $arrFollowers = get_user_followers($user->guid);
    
    $title = count($arrFollowers) . " people are Following " . ucwords($user->name);
        
    $body = elgg_view("users/list", array("entities"=>$arrFollowers, "title"=>$title));
    
    if($mode === "widget"){
        die($body);
    }else{
        $body = elgg_view_layout("three_column", "", $body);
        $title = "Followers of ".ucwords($user->name);
        page_draw($title, $body);
    }
?>