<?php

/**
 * Elgg Follow API
 * 
 * @package ElggFollow
 */
 

/**
 * Follow a user
 *
 * @param int $follower_guid 
 * @param int $following_guid 
 */
function api_follow_user($follower_guid, $following_guid){
    try {
        // Make sure we're logged in (send us to the front page if not)
        gatekeeper();
        action_gatekeeper();
        
        $follower = get_entity($follower_guid);
        $following = get_entity($following_guid);
        
        //check if user is already following follow user
        if(!check_entity_relationship($follower_guid, "following_person", $following_guid)){
            //create relationship of following with follow user
            if(add_entity_relationship($follower_guid, 'following_person', $following_guid)){
                //create inverse relation with follow user
                if(add_entity_relationship($following_guid, 'person_follower', $follower_guid)){                
                    ////notifications
                    $msg = '<a href="'.$follower->getURL().'">'.ucwords($follower->name).'</a> is following you';
                    miigle_notify_user($following, $msg);
                    return 'success';
                } 
            } 
        } else return 'already following';
    }
    catch (Exception $e){
        return $e->getMessage();
    }
}
expose_function("follow.follow_user", 
    "api_follow_user", 
    array(
        "follower_guid" => array('type' => 'int'),
        "following_guid" => array('type' => 'int')
    ),
    'Follow a user',
    'POST',
    false,
    false
);



/**
 * Un-Follow a user
 *
 * @param int $follower_guid 
 * @param int $following_guid 
 */
function api_unfollow_user($follower_guid, $following_guid){
    try {
        // Make sure we're logged in (send us to the front page if not)
        gatekeeper();
        action_gatekeeper();
        
        //check if user is already following follow user
        if(check_entity_relationship($follower_guid, "following_person", $following_guid)){
            //remove relationship of following with follow user
            remove_entity_relationship($follower_guid, 'following_person', $following_guid);
            //remove inverse relation with follow user
            remove_entity_relationship($following_guid, 'person_follower', $follower_guid);
            return 'success';
        } else return 'already unfollowed';
    }
    catch (Exception $e){
        return $e->getMessage();
    }
}
expose_function("follow.unfollow_user", 
    "api_unfollow_user", 
    array(
        "follower_guid" => array('type' => 'int'),
        "following_guid" => array('type' => 'int')
    ),
    'Follow a user',
    'POST',
    false,
    false
);



/**
 * Get a user's followers
 *
 * @param int $user_guid 
 * @param int $offset
 * @param int $limit
 * @param bool $infinite - whether to return a link to next set of content
 */
//don't use this
function api_get_user_followers($user_guid, $offset=0, $limit=20, $infinite=false){
    try {
        // Make sure we're logged in (send us to the front page if not)
        gatekeeper();
        
        //get the followers
        $return = false;
        $followers_q = elgg_get_entities_from_relationship(array(
                'relationship' => 'person_follower',
                'relationship_guid' => $user_guid,
                'types' => 'user',
                'limit' => $limit,
                'offset' => $offset
            ));
        foreach($followers_q as $follower){
            $followers['guid'] = $follower->guid;
            $followers['fname'] = $follower->fname;
            $followers['lname'] = $follower->lname;
            $followers['email'] = $follower->email;
            $followers['url'] = $follower->getURL();
            $followers['icon'] = $follower->getIcon('small');
            
            $return[] = $followers;
        }
        return $return;
        
    }
    catch (Exception $e){
        return $e->getMessage();
    }
}
expose_function("follow.get_user_followers", 
    "api_get_user_followers", 
    array(
        "user_guid" => array('type' => 'string'),
        "offset" => array('type' => 'int', 'required'=>false),
        "limit" => array('type' => 'int', 'required'=>false),
        "infinite" => array('type' => 'bool', 'required'=>false)
    ),
    'Get a user\'s followers',
    'GET',
    false,
    false
);
