<?php
    /**
     * list all followers of a user
     */

    //check access
    gatekeeper();
    
    //get input
    $username = get_input("username", "");
    
    //get display mode
    $mode = get_input("mode", "");
    
    //get user
    $user = get_user_by_username($username);
    
    if(!$user){
        $msg = "User not found.";
        if($mode==="widget"){
            die($msg);
        }else{
            register_error($msg);
            forward(REFERER);
        }
    }
    
    //everything seems ok, get list of all followers
    $arrFollowing = get_users_following($user->guid);
    
    $title = ucwords($user->name) . " is Following " . count($arrFollowing) . " people";
        
    $body = elgg_view("users/list", array("entities"=>$arrFollowing, "title"=>$title));
    
    if($mode === "widget"){
        die($body);
    }else{
        $body = elgg_view_layout("three_column", "", $body);
        $title = ucwords($user->name) . " is Following";
        page_draw($title, $body);
    }
?>