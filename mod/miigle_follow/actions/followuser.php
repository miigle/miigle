<?php
    /**
     * 
     * Cheer an idea
     * 
     */

    // Make sure we're logged in (send us to the front page if not)
    gatekeeper();
    action_gatekeeper();
    
    global $CONFIG;
    
    $forward_url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "pg/dashboard";
    
    //get logged in user
    $user = get_loggedin_user();

    //get user guid to follow
    $followuser_guid = get_input('u');
    
    //follow user entity
    $followuser = get_user($followuser_guid);
    
    //check if user is already following follow user
    if(!check_entity_relationship($user->guid, "following_person", $followuser_guid)){
        //create relationship of following with follow user
        if(add_entity_relationship($user->guid, 'following_person', $followuser_guid)){
            //create inverse relation with follow user
            if(add_entity_relationship($followuser_guid, 'person_follower', $user->guid)){
                system_message("You are following ".ucwords($followuser->name)." now.");
                
                ////notifications
                $msg = '<a href="'.$user->getURL().'">'.ucwords($user->name).'</a> follows <a href="'.$followuser->getURL().'">"'.$followuser->name.'\'s profile</a>"';
                if(!miigle_notify_user($followuser, $msg)){
                    //register_error("notification not sent");
                }
                ///////
                
                forward($forward_url);
            }else{
                register_error("Error creating inverse relationship with user");
                forward($forward_url);
            }
        }else{
            register_error("Error creating relationship with user");
            forward($forward_url);
        }
    }else{
        system_message("You are following ".ucwords($followuser->name)." now.");
        //echo 'success';
        forward($forward_url);
    }
    
    forward($forward_url);
?>