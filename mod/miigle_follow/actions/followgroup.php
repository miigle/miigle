<?php
    /**
     * 
     * Follow a Network
     * 
     */

    // Make sure we're logged in (send us to the front page if not)
    gatekeeper();
    action_gatekeeper();
    
    global $CONFIG;
    
    $forward_url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "pg/dashboard";
    
    //get logged in user
    $user = get_loggedin_user();

    //get group_guid to follow
    $group_guid = get_input('i');
    
    //group entity
    $group = get_entity($group_guid);
    
    //group owner
    $owner_guid = $group->owner_guid;
    
    //owner can not follow group
    if($owner_guid == $user->guid){
        register_error("You can not follow your own Network.");
        forward($forward_url);
    }
    
    //check if user is already following the group
    if(!check_entity_relationship($user->guid, "following", $group_guid)){
        //create relationship between user and group as following
        if(add_entity_relationship($user->guid, 'following', $group_guid)){
            //create inverse relation
            if(!add_entity_relationship($group_guid, 'follower', $user->guid)){
                register_error("Error creating inverse relation.");
                forward($forward_url);
            }else{
                system_message("You are following the network now.");
                
                ////notifications
                $group_owner = get_entity($group->owner_guid);
                $msg = '<a href="'.$user->getURL().'">'.ucwords($user->name).'</a> follows the network <a href="'.$group->getURL().'">"'.$group->name.'</a>"';
                if(!miigle_notify_user($group_owner, $msg)){
                    //register_error("notification not sent");
                }
                ///////
                
                //add to river
                add_to_river('river/follow/network', 'follow', $user->guid, $group_guid);
                forward($forward_url);
            }
        }else{
            register_error("Error following network.");
            forward($forward_url);
        }
    }else{
        register_error("You are already following the network.");
        forward($forward_url);
    }
    
    forward($forward_url);
?>