<?php
    /**
     * 
     * Follow an idea
     * 
     */

    // Make sure we're logged in (send us to the front page if not)
    gatekeeper();
    action_gatekeeper();
    
    global $CONFIG;
    
    $forward_url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "pg/dashboard";
    
    //get logged in user
    $user = get_loggedin_user();

    //get idea_guid to follow
    $idea_guid = get_input('i');
    
    //idea entity
    $idea = get_entity($idea_guid);
    
    //idea owner
    $owner_guid = $idea->owner_guid;
    
    //check if user is already following the idea
    if(!check_entity_relationship($user->guid, "following", $idea_guid)){
        //create relationship between user and idea as following
        if(add_entity_relationship($user->guid, 'following', $idea_guid)){
            //create inverse relation
            if(add_entity_relationship($idea_guid, 'follower', $user->guid)){
                //check relation with owner
                if(!check_entity_relationship($user->guid, "following_person", $owner_guid)){
                    //create relationship of following with idea owner
                    if(add_entity_relationship($user->guid, 'following_person', $owner_guid)){
                        //create inverse relation with idea owner
                        if(add_entity_relationship($owner_guid, 'person_follower', $user->guid)){

                            ////notifications
                            $idea_owner = get_entity($idea->owner_guid);
                            $msg = '<a href="'.$user->getURL().'">'.ucwords($user->name).'</a> follows the idea <a href="'.$idea->getURL().'">"'.$idea->title.'</a>"';
                            if(!miigle_notify_user($idea_owner, $msg)){
                                //register_error("notification not sent");
                            }
                            ///////

                            system_message("You are following the idea now.");
                            //echo 'success';
                            forward($forward_url);
                        }else{
                            register_error("Error creating inverse relationship with idea owner");
                            forward($forward_url);
                        }
                    }else{
                        register_error("Error creating relationship with idea owner");
                        forward($forward_url);
                    }
                }else{
                    system_message("You are following the idea now.");
                    //echo 'success';
                    forward($forward_url);
                }
            }
        }else{
            register_error("Error following idea.");
            //echo 'failure';
            forward($forward_url);
        }
    }else{
        system_message("You are already following the idea.");
        //echo 'success';
        forward($forward_url);
    }
    
    forward($forward_url);
?>