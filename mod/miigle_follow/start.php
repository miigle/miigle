<?php

    /**
     * Elgg Follow plugin
     * 
     * @package ElggFollow
     */

    /**
     * initialisation
     *
     * These parameters are required for the event API, but we won't use them:
     * 
     * @param unknown_type $event
     * @param unknown_type $object_type
     * @param unknown_type $object
     */

    function follow_init() {
        // Load system configuration
        global $CONFIG;
            
        // Extend system CSS with our own styles, which are defined in the idea/css view
        //elgg_extend_view('css','follow/css');
            
        // Register a page handler, so we can have nice URLs
        //register_page_handler('follow','follow_page_handler');        
    }
    
    // Expose API functions
    include(dirname(__FILE__) . "/api.php");
    
    /**
     * Follow page handler; allows the use of fancy URLs
     *
     * @param array $page From the elgg page_handler function
     * @return true|false Depending on success
     */
    function follow_page_handler($page) {
        switch ($page[0]) {
            case "view":
                // show an idea in detail
                set_input('idea_guid', $page[1]);
                include(dirname(__FILE__) . "/view.php");
                break;
            case "followers":
                // show list of all follow by this user
                if(isset($page[1])){
                    set_input('username', $page[1]);
                }
                if(isset($page[2])){
                    $viewtype = $page[2];
                    set_input("viewtype", $viewtype);
                }
                if(isset($page[3])){
                    $viewtype = $page[3];
                    set_input("mode", $page[3]);
                }
                include(dirname(__FILE__) . "/followers.php");
                break;
            case "following":
                // show list of all following by this user
                if(isset($page[1])){
                    set_input('username', $page[1]);
                }
                if(isset($page[2])){
                    $viewtype = $page[2];
                    set_input("viewtype", $viewtype);
                }
                if(isset($page[3])){
                    $viewtype = $page[3];
                    set_input("mode", $page[3]);
                }
                include(dirname(__FILE__) . "/following.php");
                break;
            default:
                return false;
        }

        return true;
    }
    
    /**
     * Get idea follow action url
     * 
     * @param $idea_guid
     */
    function get_idea_follow_url($idea_guid){
        global $CONFIG;
        $follow_url = $CONFIG->wwwroot."action/follow/followidea?i=".$idea_guid;
        return elgg_add_action_tokens_to_url($follow_url);
    }
    
    /**
     * Get user follow action url
     * 
     * @param $followuser_guid
     */
    function get_user_follow_url($followuser_guid){
        global $CONFIG;
        $follow_url = $CONFIG->wwwroot."action/follow/followuser?u=".$followuser_guid;
        return elgg_add_action_tokens_to_url($follow_url);
    }
    
    /**
     * Get idea cheer action url
     * 
     * @param $idea_guid
     */
    function get_idea_unfollow_url($idea_guid){
        global $CONFIG;
        $unfollow_url = $CONFIG->wwwroot."action/follow/unfollowidea?i=".$idea_guid;
        return elgg_add_action_tokens_to_url($unfollow_url);
    }
    
    /**
     * Get followers of this user's ideas
     * 
     */
    function get_user_followers($user_guid, $limit=0, $offset=0){
        $user = get_entity($user_guid);
        return elgg_get_entities_from_relationship(array(
                'relationship' => 'person_follower',
                'relationship_guid' => $user_guid,
                'types' => 'user',
                'limit' => $limit,
                'offset' => $offset
            ));
    }
    
    /**
     * Get who this user is following
     * 
     */
    function get_users_following($user_guid, $limit=0, $offset=0){
        return elgg_get_entities_from_relationship(array(
                'relationship' => 'following_person',
                'relationship_guid' => $user_guid,
                'types' => 'user',
                'limit' => $limit,
                'offset' => $offset
            ));
    }

     /**
      * Get ideas followed by user
      * 
      * @param $user_guid
      */
     function get_followed_ideas($user_guid, $limit=0, $offset=0){
        return elgg_get_entities_from_relationship(array(
                'relationship' => 'following',
                'relationship_guid' => $user_guid,
                'types' => 'object',
                'subtypes' => 'idea',
                'limit' => $limit,
                'offset' => $offset
            ));
     }

     /**
      * Get users following this idea
      * 
      * @param $idea_guid
      */
     function get_users_following_idea($idea_guid, $limit=0, $offset=0){
        return elgg_get_entities_from_relationship(array(
                'relationship' => 'follower',
                'relationship_guid' => $idea_guid,
                'types' => 'user',
                'limit' => $limit,
                'offset' => $offset
            ));
     }
     
    /**
     * Get network follow action url
     * 
     * @param $group_guid
     */
    function get_group_follow_url($group_guid){
        global $CONFIG;
        $follow_url = $CONFIG->wwwroot."action/follow/followgroup?i=".$group_guid;
        return elgg_add_action_tokens_to_url($follow_url);
    }
     
     /**
     * Get group followers
     * 
     */
    function get_group_followers($group, $limit=0, $offset=0, $count=false){
        return elgg_get_entities_from_relationship(array(
                'relationship' => 'follower',
                'relationship_guid' => $group->guid,
                'types' => 'user',
                'limit' => $limit,
                'offset' => $offset,
                'count' => $count
            ));
    }
    
     /**
     * Get group followers count
     * 
     */
    function get_group_followers_count($group, $limit=0, $offset=0){
        return get_group_followers($group, 0, 0, true);
    }
    
    
    // Make sure the follow plugin initialisation function is called on initialisation
        //register_elgg_event_handler('init','system','follow_init');
        
    // Register actions
        global $CONFIG;
        /*
        register_action("follow/followidea",false,$CONFIG->pluginspath . "follow/actions/followidea.php");
        register_action("follow/unfollowidea",false,$CONFIG->pluginspath . "follow/actions/unfollowidea.php");
        register_action("follow/followuser",false,$CONFIG->pluginspath . "follow/actions/followuser.php");
        register_action("follow/followgroup",false,$CONFIG->pluginspath . "follow/actions/followgroup.php");
        */