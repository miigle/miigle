<?php
    $performed_by = get_entity($vars['item']->subject_guid); // $statement->getSubject();
    $object = get_entity($vars['item']->object_guid);
    $url = "<a href=\"{$performed_by->getURL()}\">{$performed_by->name}</a>";
    
    if($object->getSubtype()=="group"){
        $groupname = ucwords($object->name);
        $groupurl = '<a href="' . $object->getURL() . '">' . $groupname . '</a>';
        $string = sprintf("%s has bid on idea \"%s\" ",$url, $groupurl);
    }
?>

<?php echo $string; ?>