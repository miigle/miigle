<?php
//john.pavlick@miigle.com wrote this. email me for help.

class StubUser extends ElggObject {
    public static function create($first_name, $last_name, $url, $icon_guid=0) {
        $user = new StubUser();

        $user->set('subtype', 'stub_user');
        $user->set('access_id', ACCESS_PUBLIC);
        $user->set('owner_guid', get_loggedin_userid());
        $user->set('fname', strval($first_name));
        $user->set('lname', strval($last_name));
        $user->set('icon_guid', intval($icon_guid));
        $user->set('url', $url);

        return $user;
    }

    public function delete() {
        $icon = get_entity($this->get('icon_guid'));
        if ($icon) {
            $icon->delete();
        }

        return parent::delete();
    }

    public function getURL() {
        return $this->url;
    }
}

class IdeaStakeholder extends ElggObject {
    private static function _create($user_guid, $stakeholder_id, $role) {
        $stakeholder = new IdeaStakeholder();

        $user = get_entity($user_guid);
        if (!$user) {
            return false;
        }

        $stakeholder->set('subtype', 'idea_stakeholder');
        $stakeholder->set('owner_guid', $user->get('owner_guid'));
        $stakeholder->set('access_id', ACCESS_PUBLIC);

        $stakeholder->set('stakeholder_id', intval($stakeholder_id));
        $stakeholder->set('role', strval($role));
        $stakeholder->set('user_guid', intval($user_guid));

        return $stakeholder;
    }

    public function GetStakeholderDict($stakeholder_guid) {
        $stakeholder = get_entity($stakeholder_guid);
        
        if ($stakeholder) {
            return $stakeholder->toDict();
        } else {
            return false;
        }
    }

    public function delete() {
        //delete the stub user and their icon if they have one
        $user = get_entity($this->get('user_guid'));
        if ($user && $user->getSubtype() == 'stub_user') {
            $user->delete();
        }

        return parent::delete();
    }

    public function toDict() {
        $user = get_entity($this->get('user_guid'));

        return array(
            'guid'           => intval($this->get('guid')),
            'stakeholder_id' => intval($this->get('stakeholder_id')),
            'role'           => strval($this->get('role')),
            'first_name'     => strval($user->fname),
            'last_name'      => strval($user->lname),
            'url'            => strval($user->getURL()),
            'icon_guid'      => intval($user->get('icon_guid')),
        );
    }

    //factory functions
    public static function CreateUserStakeholder($stakeholder_id, $user_guid, $role) {
        $stakeholder = IdeaStakeholder::_create($user_guid, $stakeholder_id, $role);
        if (!$stakeholder->save()) {
            return false;
        } else {
            return $stakeholder;
        }
    }

    public static function CreateNonUserStakeholder($stakeholder_id, $first_name, $last_name, $role, $url='', $icon_contents=null) {
        $user = StubUser::create($first_name, $last_name, $url);

        if ($icon_contents != null) {
            $user_icon_guid = CreateMiigleImage($icon_contents);
            if ($user_icon_guid !== false) {
                $user->set('icon_guid', intval($user_icon_guid));
            }
        }

        $user_guid = $user->save();
        if ($user_guid === false) {
            return false; //shit
        }

        $stakeholder = IdeaStakeholder::_create($user_guid, $stakeholder_id, $role);

        if (!$stakeholder->save()) {
            return false;
        } else {
            return $stakeholder;
        }
    }
}
