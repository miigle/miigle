<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/engine/start.php');
require_once(dirname(__FILE__) . '/api.php');

function MiigleStakeholdersInit() {
    register_entity_type('object', 'idea_stakeholder');
    add_subtype('object', 'idea_stakeholder', 'IdeaStakeholder');

    register_entity_type('object', 'stub_user');
    add_subtype('object', 'stub_user', 'StubUser');

	register_plugin_hook('search', 'object:idea_stakeholder', 'SearchIdeaStakeholdersHook');
}

function SearchIdeaStakeholdersHook($hook, $type, $value, $params) {
	return false;
}

register_elgg_event_handler('init', 'system', 'MiigleStakeholdersInit');
