<?php
/**
 * Idea page handler; allows the use of fancy URLs
 *
 * @param array $page From the elgg page_handler function
 * @return true|false Depending on success
 */
function MiigleIdeasPageHandler($page) {

    switch ($page[0]) {
        // /idea/new
        case 'new':
            include(dirname(__FILE__) . '/routes/edit_route.php');
            return true;

        default:
            break;
    }

    $idea_guid = intval($page[0]);
    set_input('idea_guid', $idea_guid);

    if ($idea_guid === 0) {
        forward('pg/miigle');
    }

    switch ($page[1]) {
        // /idea/:idea_guid/edit
        case 'edit':
            include(dirname(__FILE__) . '/routes/edit_route.php');
            break;

        // /idea/:idea_guid or
        // /idea/:idea_guid/view
        case 'view': 
        default:
            include(dirname(__FILE__) . '/routes/view_route.php');
            break;
    }

    return true;
}
?>
