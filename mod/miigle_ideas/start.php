<?php
/**
 * Elgg Ideas plugin
 * 
 * @package ElggIdeas
 */

require_once(dirname(dirname(dirname(__FILE__))) . '/engine/start.php');
require_once(dirname(__FILE__) . '/api.php');
require_once(dirname(__FILE__) . '/router.php');

function MiigleIdeasInit() {
    // Load system configuration
    global $CONFIG;
        
    // Extend system CSS with our own styles, which are defined in the idea/css view
    elgg_extend_view('css', 'ideas/css');
    //elgg_extend_view('miigle_dependencies/footer_js', 'ideas/js');
        
    // Register a page handler, so we can have nice URLs
    //register_page_handler('idea', 'MiigleIdeasPageHandler');
    //register_page_handler('dashboard', 'MiigleIdeasPageHandler');
        
    register_entity_type('object', 'idea');
    add_subtype('object', 'idea', 'MiigleIdea');
    register_entity_type('object', 'idea_rating');
    add_subtype('object', 'idea_rating', 'MiigleIdeaRating');

    register_entity_type('object', 'idea_video');
    add_subtype('object', 'idea_video', 'IdeaVideo');

    register_entity_type('object', 'miigle_object');
    add_subtype('object', 'miigle_object', 'MiigleObject');

    register_entity_url_handler('MiigleIdeasGetURL', 'object', 'idea');
    
    // Register icon hook to get idea icons
    register_plugin_hook('entity:icon:url', 'object', 'MiigleIdeasIconHook');
    register_plugin_hook('permissions_check:metadata', 'object', 'MiigleIdeasMetadataPermissionCheck');
    register_plugin_hook('search', 'object:idea_rating', 'SearchIdeaRatingsHook');
    register_plugin_hook('search', 'object:idea_video', 'SearchIdeaVideosHook');
    register_plugin_hook('search', 'object:miigle_object', 'SearchIdeaVideosHook');
    register_plugin_hook('search', 'object:idea', 'SearchMiigleIdeasHook');
}

function SearchIdeaRatingsHook($hook, $type, $value, $params) {
    return false;
}
function SearchIdeaVideosHook($hook, $type, $value, $params) {
    return false;
}

//WHY THE FUCK...I CAN'T EVEN...WHY DO I NEED THIS?!?!?
function MiigleIdeasMetadataPermissionCheck($hook, $type, $return_value, $params) {
    $entity = $params['entity'];
    $user = $params['user'];
    $metadata = $params['metadata'];

    if (in_array($metadata->name, array('cheer_count', 'rating_count', 'total_rating_points'))) {
        return true;
    } else {
        return $return_value;
    }
}

//this shouldn't be happening server side
function MiigleIdeasIconHook($hook, $type, $return_value, $params) {
    if ($params['entity']->getSubtype() === 'idea') {
        //stripping off the beginning '/'
        $url = CreateImageURL($params['entity']->icon_guid);
        return ltrim($url, '/');
    }
}

function MiigleIdeasGetURL($idea) {
    global $CONFIG;
    return $CONFIG->wwwroot . 'pg/miigle#/idea/' . $idea->guid;
}
    

/**
 * Get all comment objects for an idea
 *
 * @param ElggEntity $idea
 *
 * @return comment objects array
 *
 */
 function idea_get_comments(){
    return 0;
 }

 
 /**
  * get recent idea matching user's interest
  * 
  * @param $user
  */
 //idk why I'm leaving this uncommented for now...
 function get_matching_ideas($user, $limit=0, $offset=0){
     return array();
     //get interests
    $interests = json_decode($user->interests, true);
    $arr_ideas = array();
    $count_res = 0;
    $interest_name = "";
    if($interests){
        foreach($interests as $interest){
            $interest_arr = json_decode($interest, true);
            if(!empty($interest_name)){
                $interest_name .= " " . strtolower(trim($interest_arr['interest_title']));
            }else{
                $interest_name = strtolower(trim($interest_arr['interest_title']));
            }
        }
        $params = array(
            'query' => $interest_name,
            'offset' => $offset,
            'limit' => $limit,
            'sort' => "created",
            'order' => "desc",
            'search_type' => "tags",
            'type' => "object",
            'subtype' => "idea",
        //    'tag_type' => $tag_type,
            'owner_guid' => ELGG_ENTITIES_ANY_VALUE,
            'container_guid' => ELGG_ENTITIES_ANY_VALUE,
            'pagination' => TRUE
        );

        $results = trigger_plugin_hook('search', "tags", $params, array());
        return $results;
//            if (is_array($results['entities']) && $results['count']) {
//                $arr_ideas = array_merge($arr_ideas, $results['entities']);
//            }
    }
    if(count($arr_ideas)>0){
        return $arr_ideas;
    }else{
        return false;
    }
 }

register_elgg_event_handler('init', 'system', 'MiigleIdeasInit');
    
