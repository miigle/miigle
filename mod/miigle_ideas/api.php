<?php
require_once(dirname(__FILE__) . '/lib/vendor/autoload.php');
require_once(dirname(__FILE__) . '/scraper.php');
use \Fhaculty\Graph\Graph as Graph;
use \Fhaculty\Graph\Algorithm\ShortestPath\BreadthFirst as BreadthFirst;
/* email john.pavlick@miigle.com for questions. He wrote this file */

//Why put these in a database when you could put them here? Don't overcomplicate things.
function GetIdeaCategories() {
    return array(
        1 => 'Arts & Design', 
        2 => 'Business & Entrepreneurship', 
        3 => 'Computers & Internet', 
        4 => 'Education',
        5 => 'Entertainment & Gaming',
        6 => 'Environment & Wildlife',
        7 => 'Finance & Investing', 
        8 => 'Government', 
        9 => 'Health',
        10 => 'Lifestyle & Fashion',
        11 => 'Media',
        12 => 'Non-Profit & Social Good',
        13 => 'Science',
        14 => 'Technology',
        15 => 'Travel & Culture',
    );
}

function GetIdeaQuestionCommunities() {
    return array(
        1 => 'Advertising',
        2 => 'Business',
        3 => 'Engineering',
        4 => 'Finance',
        5 => 'Legal',
        6 => 'Marketing',
        7 => 'Operations',
        8 => 'Sales',
        9 => 'Web Development',
        10 => 'Design',
        11 => 'Manufacturing',
        12 => 'Non Profit',
        13 => 'Software Development',
        14 => 'Mobile Development',
    );
}

function GetIdeaTypes() {
    return array(
        1 => 'Startup',
        2 => 'Project',
        3 => 'App',
    );
}

/*
function GetContinents() {
    return array(
        1 => 'America',
        2 => 'Europe',
        3 => 'Africa',
        4 => 'Oceania',
        5 => 'Asia',
    );
}
 */

class IdeaVideo extends ElggObject {
    public static function create($video_id, $video_type) {
        $video = new IdeaVideo();

        $video->set('subtype', 'idea_video');
        $video->set('owner_guid', get_loggedin_userid());
        $video->set('access_id', ACCESS_PUBLIC);
        $video->set('video_id', strval($video_id));
        $video->set('video_type', strval($video_type));

        return $video;
    }

    public static function GetVideoDict($video_guid) {
        $video = get_entity($video_guid);
        if ($video) {
            return $video->toDict();
        } else {
            return false;
        }
    }

    public function toDict() {
        return array(
            'guid'       => $this->get('guid'),
            'video_id'   => $this->get('video_id'),
            'video_type' => $this->get('video_type'),
        );
    }
}

class MiigleIdeaRating extends ElggObject {
    private static function _authorize() {
        gatekeeper();
        if (!validate_action_token(false)) {
            throw new APIException('unauthorized');
        }
    }

    public function __construct($guid=null) {
        parent::__construct($guid);

        if (!$guid) {
            $this->set('subtype', 'idea_rating');
            $this->set('access_id', ACCESS_PUBLIC);
        }
    }

    public function toDict() {
        return array(
            'guid' => intval($this->guid),
            'idea_guid' => intval($this->idea_guid),
            'user_guid' => intval($this->owner_guid),
            'rating_dict' => json_decode($this->rating_dict, true),
        );
    }

    public function getTotalRatingValue() {
        $this_dict = $this->toDict();
        $this_rating_dict = $this_dict['rating_dict'];

        return array_sum(array_map('intval', array_values($this_rating_dict)));
    }

    public function getRatingMaxValue() {
        $this_dict = $this->toDict();
        $this_rating_dict = $this_dict['rating_dict'];

        return 10 * sizeof($this_rating_dict);
    }

    public function getRatingNumberOfStars() {
        return 5 * ($this->getTotalRatingValue() / $this->getRatingMaxValue());
    }

    private static function _GetUserIdeaRating($user_guid, $idea_guid) {
        $idea = get_entity($idea_guid);
        $user = get_entity($user_guid);

        if (!$idea || !$user) {
            return false;
        }

        $user_idea_ratings = elgg_get_entities_from_metadata(array(
            'type' => 'object',
            'subtype' => 'idea_rating',
            'owner_guid' => intval($user_guid),
            'metadata_name_value_pairs' => array(
                array(
                    'name' => 'idea_guid',
                    'value' => intval($idea_guid),
                ),
            ),
        ));

        if (!$user_idea_ratings) {
            return false;
        } else {
            return $user_idea_ratings[0];
        }
    }

    public static function GetUserIdeaRatingDict($user_guid, $idea_guid) {
        $user_idea_rating = MiigleIdeaRating::_GetUserIdeaRating($user_guid, $idea_guid);

        if (!$user_idea_rating) {
            return false;
        } else {
            return $user_idea_rating->toDict();
        }
    }

    public static function DeleteUserIdeaRating($user_guid, $idea_guid) {
        $user_idea_rating = MiigleIdeaRating::_GetUserIdeaRating($user_guid, $idea_guid);
        if ($user_idea_rating) {
            return $user_idea_rating->delete();
        }
    }

    public static function CreateUserIdeaRating($user_guid, $idea_guid, $rating_dict) {
        $idea = get_entity($idea_guid);
        $user = get_entity($user_guid);

        if (!$idea || !$user) {
            return false;
        }

        //if a rating exists, do NOT create a new one, but do not make assumptions of how to deal with it
        //just gtfo
        if (MiigleIdeaRating::_GetUserIdeaRating($user_guid, $idea_guid)) {
            error_log('rating exists?');
            return false;
        }

        $RATING_NAMES = array(
            'impact',
            'originality',
            'monetizable',
            'green',
        );

        $rating_names = array_keys($rating_dict);

        $missing_keys = array_diff($RATING_NAMES, $rating_names);
        $extra_keys   = array_diff($rating_names, $RATING_NAMES);
        if (sizeof($missing_keys) || sizeof($extra_keys)) { //do something better
            error_log('keys problem');
            return false;
        }

        function GreaterThanTen($num) { return $num > 10; }
        function LessThanZero($num)   { return $num < 0; }

        $rating_values = array_map('intval', array_values($rating_dict));
        $bad_values = array_merge(
            array_filter($rating_values, 'GreaterThanTen'),
            array_filter($rating_values, 'LessThanZero')
        );

        if (sizeof($bad_values)) { //do something better
            error_log('bad values');
            return false;
        }

        $user_idea_rating = new MiigleIdeaRating();
        $user_idea_rating->set('owner_guid', intval($user_guid));
        $user_idea_rating->set('idea_guid', intval($idea_guid));
        $user_idea_rating->set('rating_dict', json_encode($rating_dict));

        foreach ($rating_dict as $rating_name=>$rating_value) {
            $user_idea_rating->set($rating_name, intval($rating_value));
        }

        if (!$user_idea_rating->save()) {
            return false;
        } else {
            return $user_idea_rating;
        }
    }

    public static function UnrateIdea($user_guid, $idea_guid) {
        MiigleIdeaRating::_authorize();

        $idea = get_entity($idea_guid);
        $user = get_entity($user_guid);

        if (!$idea || !$user) {
            return false;
        } else {
            $current_rating_object = MiigleIdeaRating::_GetUserIdeaRating($user_guid, $idea_guid);

            if ($current_rating_object) {
                $idea->rating_count -= 1;
                $idea->total_rating_points -= $current_rating_object->getTotalRatingValue();
                remove_entity_relationship($current_rating_object->guid, 'rating_for', $idea_guid);
                $idea->save();
            }

            MiigleIdeaRating::DeleteUserIdeaRating($user_guid, $idea_guid); //delete rating object
            remove_entity_relationship($user_guid, 'rated_idea', $idea_guid);

            return true;
        }
    }

    public static function RateIdea($user_guid, $idea_guid, $rating_dict) {
        if (!MiigleIdeaRating::UnrateIdea($user_guid, $idea_guid)) { //does the guids check
            return false;
        }

        $idea = get_entity($idea_guid);
        $user = get_entity($user_guid);

        $rating_object = MiigleIdeaRating::CreateUserIdeaRating($user_guid, $idea_guid, $rating_dict);
        add_entity_relationship($user_guid, 'rated_idea', $idea_guid);
        add_entity_relationship($rating_object->guid, 'rating_for', $idea_guid);
        CreateFeedItem($user_guid, 'user:rate::idea', json_encode(array($rating_object->guid)), $idea_guid);

        $idea->rating_count += 1;
        $idea->total_rating_points += $rating_object->getTotalRatingValue();
        $idea->save();

        $notification_payload = json_encode(array(
            'idea_rating'=>$rating_object->getRatingNumberOfStars(),
        ));
        MiigleNotification::CreateNotification($idea->owner_guid, $user_guid, 
                                               $idea_guid, 'rate', 
                                               $notification_payload);

        return MiigleIdea::GetIdea($idea_guid);
    }
}

class MiigleIdea extends ElggObject {
    private static function _authorize() {
        gatekeeper();
        if (!validate_action_token(false)) {
            throw new APIException('unauthorized');
        }
    }


    public static function CheerIdea($user_guid, $idea_guid) {
        MiigleIdea::_authorize();

        $idea = get_entity($idea_guid);
        $user = get_entity($user_guid);

        if (!$idea || !$user) {
            return false;
        }

        if (check_entity_relationship($user_guid, 'cheering_idea', $idea_guid) == false) {
            $user->addRelationship($idea->guid, 'cheering_idea'); 
            CreateFeedItem($user_guid, 'user:cheer::idea', '[]', $idea_guid);
            $idea->cheer_count += 1;
            $idea->save();

            MiigleNotification::CreateNotification($idea->owner_guid, $user_guid, 
                                                   $idea_guid, 'cheer');

            MiigleUser::FollowUser($user_guid, $idea->owner_guid);
        }

        return MiigleIdea::GetIdea($idea->guid);
    }

    public static function UncheerIdea($user_guid, $idea_guid) {
        MiigleIdea::_authorize();

        $idea = get_entity($idea_guid);
        $user = get_entity($user_guid);

        if (!$idea || !$user) {
            return false;
        }

        if (remove_entity_relationship($user_guid, 'cheering_idea', $idea_guid)) {
            $idea->cheer_count -= 1;
            $idea->save();
        }

        return MiigleIdea::GetIdea($idea->guid);
    }

    private function _getCheeringUsers() {
        $cheerers = elgg_get_entities_from_relationship(array(
            'relationship'         => 'cheering_idea',
            'relationship_guid'    => $this->getGUID(),
            'inverse_relationship' => true,
            'limit' => 99999,
        ));

        if ($cheerers == false) {
            return array();
        } else {
            return $cheerers;
        }
    }

    //get the guids of the users who are cheering this idea
    public function getCheeringUsersGUIDs() {
        return array_map('GetObjectGUID', $this->_getCheeringUsers());
    }

    public static function GetBlankIdea() {
        return array(
            'guid' => 0,
            'owner_guid' => 0,
            'icon_guid' => 0,
            'time_created' => 0,
            'title' => '',
            'pitch' => '',
            'description' => '',
            'website' => '',
            'type_int' => 0,
            'category_int' => 0,
            'question_community_int' => 0,
            'question' => '',
            'foster_url' => '',

            'tags_list' => array(), //of the form ['tag1', 'tag2']
            'image_guids_list' => array(), //of the form [guid_one, guid_two, etc]
            'videos_list' => array(), //of the form [{video_id: 'blah', video_type: 'vimeo'}, ...]
            'stakeholders_dict' => (object)array(), 
            /* of the form 
            {1: {first_name: 'john',
                 last_name: 'pavlick',
                 icon_guid: 15,
                 role: 'web dev',
                 linkedin_url: 'http://blah'},
             2: etc
            }

            The keys of the outer dict are the "stakeholder_id"s 
            They're necessary to associate stakeholder icon and stakeholder_dict
            during the POST when uploading images. If you want to know more, ask 
            john.pavlick@miigle.com because it's complicated, and I'd rather not write out
            a book here.
            */

            'location_dict' => GetBlankMiigleLocation(),
            'cheering_guids_list' => array(),
            'cheer_count' => 0,
            'rating_count' => 0,
            'total_rating_points' => 0,
            'needed_expertise_list' => array(),
            'women_founders' => false,
        );
    }

    //awful
    //this should be done entirely in sql
    public static function GetIdeaLocationCounts() {
        $ideas = elgg_get_entities(array(
            'type' => 'object',
            'subtype' => 'idea',
            'limit' => 999999,
        ));

        $cia_map_references = array_map(function($idea) {
            return strval($idea->cia_map_reference);
        }, $ideas);

        $reference_to_count_dict = array_count_values($cia_map_references);

        return $reference_to_count_dict;
    }
    public static function GetIdeaCategoryCounts() {
        $ideas = elgg_get_entities(array(
            'type' => 'object',
            'subtype' => 'idea',
            'limit' => 999999,
        ));

        if ($ideas == false) {
            $ideas = array();
        }

        $category_ints = array_map(function($idea) {
            return intval($idea->category_int);
        }, $ideas);

        $women_founder_ideas = array_values(array_filter($ideas, function($idea) {
            return json_decode($idea->women_founders);
        }));

        $category_int_to_count_dict = array_count_values($category_ints);
        $category_int_to_count_dict[-1] = sizeof($women_founder_ideas);


        return $category_int_to_count_dict;
    }

    public static function GetIdea($idea_guid, $get_blank=true) {
        $idea = get_entity($idea_guid);
        $blank_idea = MiigleIdea::GetBlankIdea();

        static $result_cache;

        if (!$result_cache) {
            $result_cache = array();
        } else if (isset($result_cache[$idea->guid])) {
            return $result_cache[$idea->guid];
        }

        if ($idea) {
            //$idea = new MiigleIdea($idea->guid);
            $blank_idea['guid'] = intval($idea->guid);
            $blank_idea['owner_guid'] = intval($idea->owner_guid);
            $blank_idea['icon_guid'] = intval($idea->icon_guid);
            $blank_idea['title'] = strval($idea->title);
            $blank_idea['pitch'] = strval($idea->pitch);
            $blank_idea['website'] = strval($idea->website);
            $blank_idea['description'] = strval($idea->description);
            $blank_idea['time_created'] = intval($idea->time_created);
            $blank_idea['type_int'] = intval($idea->type_int);
            $blank_idea['category_int'] = intval($idea->category_int);
            $blank_idea['question_community_int'] = intval($idea->question_community_int);
            $blank_idea['question'] = strval($idea->question);
            $blank_idea['foster_url'] = strval($idea->foster_url);

            //json_decode($blah, true) the true is necessary becase otherwise, you'll get stdClass objects back
            //instead of associative arrays (dictionaries)
            $blank_idea['tags_list'] = json_decode($idea->tags_list, true);
            $blank_idea['videos_list'] = $idea->getVideoDicts();
            $blank_idea['stakeholders_dict'] = $idea->getStakeholdersDict();
            $blank_idea['location_dict'] = json_decode($idea->location_dict, true);

            $blank_idea['image_guids_list'] = $idea->getImageGUIDs();
            $blank_idea['icon_guid'] = intval($idea->getIconGUID());
            $blank_idea['cheering_guids_list'] = $idea->getCheeringUsersGUIDs();
            $blank_idea['cheer_count'] = intval($idea->cheer_count);
            $blank_idea['rating_count'] = intval($idea->rating_count);
            $blank_idea['total_rating_points'] = intval($idea->total_rating_points);
            $blank_idea['needed_expertise_list'] = json_decode($idea->needed_expertise_list, true);
            $blank_idea['women_founders'] = json_decode($idea->women_founders);

            foreach ($blank_idea['videos_list'] as $video_dict) {
                if ($video_dict['video_type'] == 'vimeo') {
                    $blank_idea['video_thumbnail'] = GetVimeoThumbnail($video_dict['video_id']);
                    break;
                }
            }

            $result_cache[$idea->guid] = $blank_idea;
        } else if (!$get_blank) {
            return;
        }

        return $blank_idea;
    }

    public static function GetIdeaPotentialContributorsGUIDList($idea_guid) {
        $idea_dict = MiigleIdea::GetIdea($idea_guid);
        $owner_guid = $idea_dict['owner_guid'];

        $FilterIdeaOwner = function($guid) use ($owner_guid) {
            return intval($guid) !== intval($owner_guid);
        };

        $interest_search_list = array($idea_dict['category_int']);
        $expertise_search_list = array_merge(
                array($idea_dict['question_community_int']), 
                $idea_dict['needed_expertise_list']);

        $interest_match_users_guids = SearchMiigleUsersByInterests(
                $interest_search_list);
        $expertise_match_users_guids = SearchMiigleUsersByExpertise(
                $expertise_search_list);

        //
        $potential_contributors_list = MiigleUser::GetMatchingUsers(
                $interest_search_list, $expertise_search_list, 
                array($owner_guid));

        $SortPotentialContributors = function($user_a, $user_b) use ($idea_dict)
        {
            $needed_expertise_list = $idea_dict['needed_expertise_list'];
            $user_a_useful_expertise = __::intersection($needed_expertise_list,
                    $user_a['expertise_list']);
            $user_b_useful_expertise = __::intersection($needed_expertise_list,
                    $user_b['expertise_list']);

            return (sizeof($user_b_useful_expertise) -
                    sizeof($user_a_useful_expertise));
        };

        usort($potential_contributors_list, $SortPotentialContributors);
        return __::pluck($potential_contributors_list, 'guid');
        //
        //
        foreach ($potential_contributors_guid_list as $guid) {
            $user_dict = MiigleUser::GetUserFromGUID($guid);
            json_log($user_dict['username']);
        }
        //json_log($potential_contributors_list);
        //__::map($potential_contributors_guid_list, 'json_log');


        $interest_match_users_guids = array_filter(
                $interest_match_users_guids, $FilterIdeaOwner);
        $expertise_match_users_guids = array_filter(
                $expertise_match_users_guids, $FilterIdeaOwner);

        $interest_and_expertise_match_users_guids =  __::intersection(
                $interest_match_users_guids, 
                $expertise_match_users_guids);

        $only_interest_match_users_guids = __::difference( 
                $interest_match_users_guids, 
                $expertise_match_users_guids);

        $interest_and_expertise_match_users = array_map(
                'MiigleUser::GetUserFromGUID', 
                $interest_and_expertise_match_users_guids);
        $only_interest_match_users = array_map(
                'MiigleUser::GetUserFromGUID', 
                $only_interest_match_users_guids);

        json_log(sizeof($interest_and_expertise_match_users));
        json_log(sizeof($only_interest_match_users));

        usort($interest_and_expertise_match_users, 
                function($user_a, $user_b) use ($idea_dict) {
                    return sizeof(array_intersect(
                            $user_a['work_info_dict']['expertise_list'], 
                            $idea_dict['needed_expertise_list']))
                         < sizeof(array_intersect(
                            $user_b['work_info_dict']['expertise_list'], 
                            $idea_dict['needed_expertise_list']));
                });




        return array_map('GetObjectGUID', 
                array_merge($interest_and_expertise_match_users, 
                            $only_interest_match_users));
    }

    public static function GetIdeaPotentialContributors($idea_guid) {
        return array_map('MiigleUser::GetUserFromGUID', 
                MiigleIdea::GetIdeaPotentialContributorsGUIDList($idea_guid));
    }

    //kind of yucky, but whatever. Everything should be cached.
    public function toDict() {
        return MiigleIdea::GetIdea($this->getGUID());
    }

    public static function DeleteIdea($idea_guid) {
        MiigleIdea::_authorize();

        $idea = get_entity($idea_guid);
        if ($idea) {
            return $idea->delete();
        } 
        return false;
    }

    public static function DeleteIdeaImage($idea_guid, $image_guid) {
        MiigleIdea::_authorize();

        $idea = get_entity($idea_guid);
        if (!$idea) {
            return false;
        }

        $idea_image_guid_list = $idea->getImageGUIDs();

        //if this image is one of the images owned by this idea, delete the fucker
        if (in_array(intval($image_guid), $idea_image_guid_list)) {
            $image = get_entity($image_guid);
            if ($image) {
                return $image->delete();
            }
        }

        return false;
    }

    public static function DeleteIdeaStakeholder($idea_guid, $stakeholder_id) {
        MiigleIdea::_authorize();

        $return_value = true;
        $idea = get_entity($idea_guid);

        if (!$idea) {
            return false;
        }

        //since it's a list we have to run through it to find our match
        //this could be made better if need be. But for now. eh.
        $stakeholders_list = $idea->_getStakeholders();
        foreach ($stakeholders_list as $stakeholder) {
            if ($stakeholder->get('stakeholder_id') == $stakeholder_id) {
                $return_value &= $stakeholder->delete();
            }
        }

        return $return_value;
    }

    public function __construct($guid=null) {
        parent::__construct($guid);

        if (!$guid) { //if making a new MiigleIdea
            $this->set('subtype', 'idea');
            $this->set('owner_guid', get_loggedin_userid());
            $this->set('access_id', ACCESS_PUBLIC);
        }
    }

    public function getIconGUID() {
        $icon = $this->_getIcon();
        if ($icon !== false) {
            return intval($icon->getGUID());
        } else {
            return 0;
        }
    }

    private function _getIcon() {
        $idea_icons = elgg_get_entities_from_relationship(array(
            'relationship'         => 'idea_icon',
            'relationship_guid'    => $this->getGUID(),
            'inverse_relationship' => true,
        ));
        if ($idea_icons) {
            return $idea_icons[0];
        } else {
            return false;
        }
    }

    public function deleteIcon() {
        $icon = $this->_getIcon();
        if ($icon) {
            return $icon->delete();
        }
        return true;
    }

    private function _getImages() {
        $idea_image_list = elgg_get_entities_from_relationship(array(
            'relationship'         => 'idea_image',
            'relationship_guid'    => $this->getGUID(),
            'inverse_relationship' => true,
            'limit' => 999999,
        ));

        if (!$idea_image_list) {
            return array();
        } else {
            return $idea_image_list;
        }
    }

    public function getImageGUIDs() {
        return array_map(function($idea_image) {
            return intval($idea_image->guid);
        }, $this->_getImages());
    }

    private function _getVideos() {
        $video_list = elgg_get_entities_from_relationship(array(
            'relationship'         => 'idea_video',
            'relationship_guid'    => $this->getGUID(),
            'inverse_relationship' => true,
            'limit' => 999999,
        ));

        if (!$video_list) {
            return array();
        } else {
            return $video_list;
        }
    }

    public function getVideoDicts() {
        return array_map(function($idea_video) {
            return $idea_video->toDict();
        }, $this->_getVideos());
    }

    private function _getStakeholders() {
        $stakeholders_list = elgg_get_entities_from_relationship(array(
            'relationship'         => 'idea_stakeholder',
            'relationship_guid'    => $this->getGUID(),
            'inverse_relationship' => true,
            'limit' => 999999,
        ));

        if (!$stakeholders_list) {
            return array();
        } else {
            return $stakeholders_list;
        }
    }

    public function getStakeholdersDict() {
        $stakeholders_list = $this->_getStakeholders();
        $stakeholders_dict = array();

        //we don't want a list, we want a dict keyed on stakeholder_id
        //so, make it.
        foreach ($stakeholders_list as $stakeholder) {
            $stakeholder_dict = $stakeholder->toDict();

            $stakeholder_id = $stakeholder_dict['stakeholder_id'];
            unset($stakeholder_dict['stakeholder_id']);

            $stakeholders_dict[$stakeholder_id] = $stakeholder_dict;
        }

        if ($stakeholders_dict == array()) {
            return new stdClass();
        } else {
            return $stakeholders_dict;
        }
    }

    public function delete() {
        $this->deleteIcon();

        //delete idea images
        array_map(function($image) {
            if ($image) {
                $image->delete();
            }
        }, $this->_getImages());

        //deleteing stakeholders
        array_map(function($stakeholder) {
            if ($stakeholder) {
                $stakeholder->delete();
            }
        }, $this->_getStakeholders());

        return parent::delete();
    }

    //This is a monster. Oh god. It needs a refactor already.
    /**
    */
    public static function EditIdea($idea_guid,
                                    $idea_title, 
                                    $idea_type_int, 
                                    $idea_website, 
                                    $idea_category_int, 
                                    $idea_pitch, 
                                    $idea_tags_string, 
                                    $idea_description,
                                    $idea_question_community_int, 
                                    $idea_question, 
                                    $idea_foster_url,
                                    $idea_videos_list,
                                    $location_dict,
                                    $stakeholders_dict,
                                    $image_files_dict,
                                    $needed_expertise_list,
                                    $women_founders,
                                    $image_guids_list,
                                    $icon_guid) {
        MiigleIdea::_authorize();

        /* INPUT VALIDATION STARTING */
        //basically what this is doing, is removing any stakeholder_dict that doesn't have a value for
        //each of the required keys defined two lines down.
        $stakeholders_dict = array_filter($stakeholders_dict, function($stakeholder_dict) {
            $REQUIRED_STAKEHOLDER_KEYS = array('first_name', 'last_name', 'role');
            foreach ($REQUIRED_STAKEHOLDER_KEYS as $key) {
                if (!isset($stakeholder_dict[$key]) || strlen($stakeholder_dict[$key]) == 0) {
                    return false;
                }
            }
            return true;
        });

        //what this is doing is ripping out linkedin_url if it is blank
        $stakeholders_dict = array_map(function($stakeholder_dict) {
            if (isset($stakeholder_dict['linkedin_url']) && strlen($stakeholder_dict['linkedin_url']) == 0) {
                unset($stakeholder_dict['linkedin_url']);
            }
            return $stakeholder_dict;
        }, $stakeholders_dict);

        //If the idea_question_community_int supplied isn't valid, set it as 0. 
        //0 is the "null" or "unset" value.
        if (!array_key_exists($idea_question_community_int, GetIdeaCategories())) {
            $idea_question_community_int = 0;
        }

        //If they didn't send us a community, then they don't get to ask a question!
        if ($idea_question_community_int == 0) {
            $idea_question = '';
        }

        //error checking location_dict
        $location_dict = json_decode($location_dict, true); //remember, this comes in as a json string
        if (!array_key_exists('fqcn', $location_dict)) { //if we don't have a fqcn gtfo
            error_log('invalid location dict');
            return false;
        }

        //let's just re-look-this-up. it should already be cached,
        //and prevents the user messing around with their location client side
        $new_location_dict = MiigleLocationLookup($location_dict['fqcn']);
        if (!$new_location_dict) {
            throw new Exception("Invalid location: {$location_dict}");
        } else {
            $location_dict = $new_location_dict;
        }

        if (json_decode($needed_expertise_list) === null) {
            $needed_expertise_list = '[]';
        }
        /* INPUT VALIDATION ENDING */

        $creating_idea = false;
        $changed_icon = false;
        $added_image = false;
        $added_video = false;
        $added_stakeholder = false;

        $added_images_list = array();
        $added_videos_list = array();
        $added_stakeholders_list = array();

        $idea = new MiigleIdea($idea_guid);

        if ($idea->guid) {
            $creating_idea = false;
        } else {
            $creating_idea = true;

            $idea->cheer_count = 0;
            $idea->rating_count = 0;
            $idea->total_rating_points = 0;
            if(!$idea->save()) { //save it now because we need a guid
                throw new Exception('First idea save failed. This should never happen');
            }
        }

        //copying over a large batch of info
        $idea->title = strval($idea_title);
        $idea->description = strval($idea_description);
        $idea->type_int = intval($idea_type_int);
        $idea->website = strval($idea_website);
        $idea->category_int = intval($idea_category_int);
        $idea->category_string = strval(GetIdeaCategories()[intval($idea_category_int)]); //used for sphinx search
        $idea->pitch = strval($idea_pitch);
        //$idea_tags_string is a json list string, e.g. - "["http://blah", "http://blah2"]"
        $idea->tags_list = strval($idea_tags_string); 
        $idea->question_community_int = intval($idea_question_community_int);
        $idea->question = strval($idea_question);
        $idea->foster_url = strval($idea_foster_url);
        $idea->location_dict = json_encode($location_dict); //json object
        $idea->cia_map_reference = strval($location_dict['cia_map_reference']); //copying this data here for easy metadata search
        $idea->needed_expertise_list = $needed_expertise_list;
        $idea->women_founders = json_encode(($women_founders) ? true : false);

        $current_idea_videos_dict_list = $idea->getVideoDicts();
        $incoming_idea_videos_dict_list = json_decode($idea_videos_list, true);

        $videos_to_remove = array_diff($current_idea_videos_dict_list, $incoming_idea_videos_dict_list);
        $videos_to_add = array_diff($incoming_idea_videos_dict_list, $current_idea_videos_dict_list);

        //deleting
        $guids_to_remove = array_map('GetObjectGUID', $videos_to_remove);
        $entities_to_remove = array_map('get_entity', $guids_to_remove);
        array_map('delete_entity', $guids_to_remove);

        //adding
        foreach ($videos_to_add as $idea_video_dict) {
            $new_video = IdeaVideo::create($idea_video_dict['video_id'], $idea_video_dict['video_type']);
            $new_video_guid = $new_video->save();

            if ($new_video_guid) {
                $new_video->addRelationship($idea->guid, 'idea_video'); 
                $added_videos_list[] = $new_video->guid;
                $added_video = true;
            }
        }

        if ($icon_guid != 0) {
            $idea->deleteIcon();
            add_entity_relationship($icon_guid, 'idea_icon', $idea->guid);
            $changed_icon = true;
        }

        if ($image_files_dict['idea_icon']) {
            $icon_contents = file_get_contents($image_files_dict['idea_icon']);
            $icon_guid = CreateMiigleImage($icon_contents);

            if ($icon_guid !== false) {
                $idea->deleteIcon();
                add_entity_relationship($icon_guid, 'idea_icon', $idea->guid);

                $changed_icon = true;
            } else {
                //uh...? I hate these situations...
            }
        }

        foreach ($image_files_dict['idea_images_list'] as $file_name) {
            $image_contents = file_get_contents($file_name);
            $image_guid = CreateMiigleImage($image_contents);

            if ($image_guid !== false) {
                $image = get_entity($image_guid);

                $image->addRelationship($idea->guid, 'idea_image'); //$image is idea_image of $idea
                $added_images_list[] = $image_guid;
                $added_image = true;
            }
        }

        $image_guids_list = json_decode($image_guids_list);
        if (sizeof($image_guids_list) > 0) {
            $image_entities = array_map('get_entity', $image_guids_list);
            foreach ($image_entities as $image) {
                $image->addRelationship($idea->guid, 'idea_image');
                $added_images_list[] = $image->guid;
                $added_image = true;
            }
        }

        foreach ($image_files_dict['stakeholder_icons_dict'] as $stakeholder_id=>$file_name) {
            //ugly ugly ugly
            $stakeholder_dict = $stakeholders_dict[$stakeholder_id];
            if (isset($stakeholder_dict['linkedin_url'])) {
                $stakeholder_url = $stakeholder_dict['linkedin_url'];
            } else {
                $stakeholder_url = '';
            }

            $new_stakeholder = IdeaStakeholder::CreateNonUserStakeholder($stakeholder_id,  
                                                                         $stakeholder_dict['first_name'],
                                                                         $stakeholder_dict['last_name'],
                                                                         $stakeholder_dict['role'],
                                                                         $url=$stakeholder_url,
                                                                         file_get_contents($file_name));
            if ($new_stakeholder) {
                $added_stakeholder = true;
                $new_stakeholder->addRelationship($idea->getGUID(), 'idea_stakeholder');
                $added_stakeholders_list[] = $new_stakeholder->getGUID();
                unset($stakeholders_dict[$stakeholder_id]);
            }
        }
        //done with all that shit.
        //now let's put in some feed entries.

        $subject_guid = intval(get_loggedin_userid());
        $object_guid = $idea->guid;

        if ($creating_idea) {
            $action = 'user:create::idea';
            $action_payload = json_encode(array($idea->getGUID()));
            CreateFeedItem($subject_guid, $action, $action_payload, $object_guid);
        } else {
            if ($added_stakeholder) {
                $action = 'user:add:stakeholder:idea';
                $action_payload = json_encode($added_stakeholders_list);
                CreateFeedItem($subject_guid, $action, $action_payload, $object_guid);
            }
            if ($added_video) {
                $action = 'user:add:video:idea';
                $action_payload = json_encode($added_videos_list);
                CreateFeedItem($subject_guid, $action, $action_payload, $object_guid);
            }
            if ($added_image) {
                $action = 'user:add:image:idea';
                $action_payload = json_encode($added_images_list);
                CreateFeedItem($subject_guid, $action, $action_payload, $object_guid);
            }
            if ($changed_icon) {
                $action = 'user:update:icon:idea';
                $action_payload = json_encode(array($icon_guid));
                CreateFeedItem($subject_guid, $action, $action_payload, $object_guid);
            }
            if (!$added_stakeholder && !$added_video && !$added_image && !$changed_icon) {
                //updated something like the description
                $action = 'user:update::idea';
                $action_payload = json_encode(array($idea->getGUID()));
                CreateFeedItem($subject_guid, $action, $action_payload, $object_guid);
            }
        }

        //do we need this?
        $idea->save();

        //we return the idea's URL so the edit page can redirect to it
        return array(
            'guid' => $idea->getGUID(),
            'idea_dict' => MiigleIdea::GetIdea($idea->getGUID()),
        );
    }
}

//dude...this is so bad
function EditIdeaProxy($idea_guid=0, 
                       $idea_title, 
                       $idea_type_int, 
                       $idea_website, 
                       $idea_category_int, 
                       $idea_pitch, 
                       $idea_tags_string, 
                       $idea_description,
                       $idea_question_community_int=0, 
                       $idea_question='', 
                       $foster_url='',
                       $idea_videos_list='[]',
                       $location_dict,
                       $needed_expertise_list='[]',
                       $women_founders,
                       $image_guids_list='[]',
                       $icon_guid=0) {

    $women_founders = $_POST['women_founders']; //idk what is happening why is this not working
    //I blame elgg

    $stakeholders_dict = json_decode($_POST['stakeholders_dict'], true);
    $image_files_dict = array(
        'idea_icon' => '',
        'idea_images_list' => array(),
        'stakeholder_icons_dict' => array(),
    );

    //tmp_name is something like "/tmp/10298408djflaksjdf" or some such garbage.
    //It's where the $_POST files get written to disk initially.

    //This is fairly arbitrary. This limit is also "mirrored" if you will in miigle_ideas.js
    //when setting the max file size upload. If we change it, change it together. 
    //Or, should we link the two so that we have one single source of the truth?
    $MAX_IMAGE_SIZE = 5000000; //5MB. Actually I think it's 4.768


    if (isset($_FILES['idea_icon']) && !empty($_FILES['idea_icon']) && 
        $_FILES['idea_icon']['error'] == 0 && $_FILES['idea_icon']['size'] < $MAX_IMAGE_SIZE) {
        $image_files_dict['idea_icon'] = $_FILES['idea_icon']['tmp_name'];
    }

    if (isset($_FILES['idea_images']) && !empty($_FILES['idea_images'])) {
        $idea_images = $_FILES['idea_images'];
        foreach($idea_images['tmp_name'] as $i=>$tmp_name) {
            if ($idea_images['error'][$i] == 0 && $idea_images['size'][$i] < $MAX_IMAGE_SIZE) {
                $image_files_dict['idea_images_list'][] = $tmp_name;
            }
        }
    }

    if (isset($_FILES['stakeholder_icons']) && !empty($_FILES['stakeholder_icons'])) {
        $stakeholder_icons = $_FILES['stakeholder_icons'];

        //Look in the above if block for the explanation of tmp_name
        foreach($stakeholder_icons['tmp_name'] as $stakeholder_id=>$tmp_name) {
            if ($stakeholder_icons['error'][$stakeholder_id] == 0 && 
                $stakeholder_icons['size'][$stakeholder_id] < $MAX_IMAGE_SIZE &&
                isset($stakeholders_dict[$stakeholder_id])) { //make sure they're sending us an image to a stakeholder that exists
                $image_files_dict['stakeholder_icons_dict'][$stakeholder_id] = $tmp_name;
            }
        }
    }

    return MiigleIdea::EditIdea($idea_guid,
                                $idea_title, 
                                $idea_type_int, 
                                $idea_website, 
                                $idea_category_int, 
                                $idea_pitch, 
                                $idea_tags_string, 
                                $idea_description,
                                $idea_question_community_int,
                                $idea_question,
                                $foster_url,                                
                                $idea_videos_list,
                                $location_dict,
                                $stakeholders_dict,
                                $image_files_dict,
                                $needed_expertise_list,
                                $women_founders,
                                $image_guids_list,
                                $icon_guid);
}

function CheerIdeaProxy($idea_guid) {
    $user_guid = get_loggedin_userid();
    return MiigleIdea::CheerIdea($user_guid, $idea_guid);
}
function UncheerIdeaProxy($idea_guid) {
    $user_guid = get_loggedin_userid();
    return MiigleIdea::UncheerIdea($user_guid, $idea_guid);
}
function RateIdeaProxy($idea_guid, $rating_dict_string) {
    $user_guid = get_loggedin_userid();
    return MiigleIdeaRating::RateIdea($user_guid, $idea_guid, json_decode($rating_dict_string, true));
}
function UnrateIdeaProxy($idea_guid) {
    $user_guid = get_loggedin_userid();
    return MiigleIdeaRating::UnrateIdea($user_guid, $idea_guid);
}
function GetUserIdeaRatingProxy($idea_guid) {
    $user_guid = get_loggedin_userid();
    $rating = MiigleIdeaRating::GetUserIdeaRatingDict($user_guid, $idea_guid);

    if (!$rating) {
        return 'false';
    } else {
        return $rating;
    }
}

function GetIdeaProxy($idea_guid=0) {
    $get_blank = ($idea_guid === 0);
    return MiigleIdea::GetIdea($idea_guid, $get_blank);
}

//expose the stuff we need over REST
expose_function('ideas.get', 'GetIdeaProxy', 
    array(
        'idea_guid' => array('type'=>'int', 'required'=>false, 'default'=>0),
    ),
    'Get an idea',
    'GET',
    false,
    false
);
expose_function('ideas.get_potential_contributors', 'MiigleIdea::GetIdeaPotentialContributors', 
    array(
        'idea_guid' => array('type'=>'int', 'required'=>false, 'default'=>0),
    ),
    'Get an idea\'s potential contributors',
    'GET',
    false,
    false
);
expose_function('ideas.delete', 'MiigleIdea::DeleteIdea',
    array(
        'idea_guid' => array('type'=>'int', 'required'=>true),
    ),
    'Delete an idea',
    'POST',
    false,
    false
);
expose_function('ideas.delete.image', 'MiigleIdea::DeleteIdeaImage',
    array(
        'idea_guid'  => array('type'=>'int', 'required'=>true),
        'image_guid' => array('type'=>'int', 'required'=>true),
    ),
    'Delete an idea image',
    'POST',
    false,
    false
);
expose_function('ideas.delete.stakeholder', 'MiigleIdea::DeleteIdeaStakeholder',
    array(
        'idea_guid'      => array('type'=>'int', 'required'=>true),
        'stakeholder_id' => array('type'=>'int', 'required'=>true),
    ),
    'Delete an idea stakeholder',
    'POST',
    false,
    false
);
expose_function('ideas.edit', 'EditIdeaProxy',
    array(
        'guid'                   => array('type'=>'int',    'required'=>false, 'default'=>0),
        'title'                  => array('type'=>'string', 'required'=>true),
        'type_int'               => array('type'=>'int',    'required'=>true),
        'website'                => array('type'=>'string', 'required'=>false, 'default'=>''),
        'category_int'           => array('type'=>'int',    'required'=>true),
        'pitch'                  => array('type'=>'string', 'required'=>true),
        'tags_list'              => array('type'=>'string', 'required'=>true), //json string list
        'description'            => array('type'=>'string', 'required'=>true),
        'question_community_int' => array('type'=>'int',    'required'=>false, 'default'=>0),
        'question'               => array('type'=>'string', 'required'=>false, 'default'=>''),
        'foster_url'             => array('type'=>'string', 'required'=>false, 'default'=>''),
        'videos_list'            => array('type'=>'string', 'required'=>false, 'default'=>'[]'), //json string list
        'location_dict'          => array('type'=>'string', 'required'=>true), //json string object
        'needed_expertise_list'  => array('type'=>'string', 'required'=>false, 'default'=>'[]'),
        'women_founders'         => array('type'=>'bool',   'required'=>true),
        'image_guids_list'       => array('type'=>'string', 'required'=>false, 'default'=>'[]'),
        'icon_guid'              => array('type'=>'int',    'required'=>false, 'default'=>0),
    ),
    'Edit or create an idea',
    'POST',
    false,
    false
);
expose_function('ideas.cheer', 'CheerIdeaProxy',
    array(
        'idea_guid' => array('type'=>'int', 'required'=>true)
    ),
    'Cheer an idea',
    'POST',
    false,
    false
);
expose_function('ideas.uncheer', 'UncheerIdeaProxy',
    array(
        'idea_guid' => array('type'=>'int', 'required'=>true)
    ),
    'Uncheer an idea',
    'POST',
    false,
    false
);
expose_function('ideas.rate', 'RateIdeaProxy',
    array(
        'idea_guid' => array('type'=>'int', 'required'=>true),
        'rating_dict_string' =>array('type'=>'string', 'required'=>true),
    ),
    'Rate an idea',
    'POST',
    false,
    false
);
expose_function('ideas.unrate', 'UnrateIdeaProxy',
    array(
        'idea_guid' => array('type'=>'int', 'required'=>true),
    ),
    'Unrate an idea',
    'POST',
    false,
    false
);
expose_function('ideas.get_user_rating', 'GetUserIdeaRatingProxy',
    array(
        'idea_guid' => array('type'=>'int', 'required'=>true),
    ),
    'Get a user rating for an idea',
    'GET',
    false,
    false
);


//we shouldn't use this
function CreateImageURL($image_guid) {
    return '/pg/image/' . $image_guid;
}

function GetPathBetweenVerticies($graph, $start, $end) {
    $paths = array();
    $d = new BreadthFirst($start);

    try {
        $w = $d->getWalkTo($end);
    } catch (Exception $error) {
        return array();
    }

    foreach ($w->getEdges() as $edge) {
        $path = array(
            'from' => $edge->getVerticesStart()->getVertexFirst()->getId(),
            'to'   => $edge->getVerticesTarget()->getVertexFirst()->getId(),
        );

        $paths[] = $path;
    }

    return $paths;
}


function GetUserFollowGraphMatrix() {
    $users = elgg_get_entities(array(
        'type' => 'user',
        'limit' => 99999,
    ));

    $users = array_filter($users, function($user) {
        return $user->last_login > strtotime('January 2014');
    });

    $user_dicts = array();
    $user_guids = array_map('GetObjectGUID', $users);

    foreach ($user_guids as $user_guid) {
        $user_followers = MiigleUser::GetUserFollowerGUIDs($user_guid);
        $user_following = MiigleUser::GetUserFollowingGUIDs($user_guid);

        $user_dicts[] = array(
            'guid'           => $user_guid,
            'followers_list' => $user_followers,
            'following_list' => $user_following,
        );
    }

    return array_values($user_dicts);
}

function BuildUserGraph($user_dicts) {
    $graph = new Graph();
    $guid_to_vertex = array();

    foreach ($user_dicts as $user_dict) {
        $vertex = $graph->createVertex($user_dict['guid']);
        $guid_to_vertex[$user_dict['guid']] = $vertex;
    }

    foreach ($user_dicts as $user_dict) {
        $vertex = $guid_to_vertex[$user_dict['guid']];

        foreach ($user_dict['followers_list'] as $follower_guid) {
            if (isset($guid_to_vertex[$follower_guid])) {
                $other_vertex = $guid_to_vertex[$follower_guid];
                $other_vertex->createEdgeTo($vertex);
            }
        }
        foreach ($user_dict['following_list'] as $following_guid) {
            if (isset($guid_to_vertex[$following_guid])) {
                $other_vertex = $guid_to_vertex[$following_guid];
                $vertex->createEdgeTo($vertex);
            }
        }
    }

    return $graph;
}

function BuildMiigleUserGraph() {
    static $graph_cache;

    if (!$graph_cache) {
        $all_users = GetUserFollowGraphMatrix();
        $graph_cache = BuildUserGraph($all_users);
    }

    return $graph_cache;
}

function GetIdeaPotentialContributorsSocialGraph($idea_guid) {
    $idea = get_entity($idea_guid);
    $owner_guid = $idea->owner_guid;
    $potential_contributors_guid_list = MiigleIdea::GetIdeaPotentialContributorsGUIDList($idea_guid);

    $graph = BuildMiigleUserGraph();
    $idea_owner_vertex = $graph->getVertex($owner_guid);

    $pc_guid_to_path = array();

    foreach ($potential_contributors_guid_list as $guid) {
        $pc_vertex = $graph->getVertex($guid);
        $pc_guid_to_path[$guid] = GetPathBetweenVerticies($graph, $idea_owner_vertex, $pc_vertex);
    }

    return $pc_guid_to_path;
}

expose_function(
    'ideas.get_potential_contributors_graph', 
    'GetIdeaPotentialContributorsSocialGraph',
    array(
        'idea_guid' => array('type'=>'int', 'required'=>true),
    ),
    '',
    'GET',
    false,
    false
);

class VimeoAPIKeyValueStore extends NamespacedKeyValueStore {
    public function __construct() {
        parent::__construct('vimeo_api', new ElggDatabaseKeyValueStore());
    }
}

function GetVimeoThumbnail($video_id) {
    $kv_store = new VimeoAPIKeyValueStore();
    $json_string = $kv_store->get($video_id);

    if (!$json_string) {
        $json_string = file_get_contents(sprintf(
                'http://vimeo.com/api/v2/video/%d.json', $video_id));
        $kv_store->set($video_id, $json_string);
    }

    $json_dict = json_decode($json_string, true)[0];

    return $json_dict['thumbnail_large'];
}

class DisqusAPIHelpers {
    private static function DisqusAPICall($url) {
        global $CONFIG;

        $api_url = sprintf('%s&api_key=%s', $url, 
                                            $CONFIG->DISQUS_MIIGLE_API_KEY);
        $disqus_response = file_get_contents($api_url);
        $json_response = json_decode($disqus_response, true);

        if ($json_response['code'] !== 0) {
            throw new Exception("Error in Disqus API call: $disqus_response");
        } 

        return $json_response;
    }

    public static function GetCommentDetails($comment_id) {
        $json_response = DisqusAPIHelpers::DisqusAPICall(sprintf(
                'https://disqus.com/api/3.0/posts/details.json?&post=%s', 
                $comment_id));

        return array(
            'username'   => $json_response['response']['author']['name'],
            'message'    => $json_response['response']['raw_message'],
            'message_id' => $json_response['response']['id'],
            'thread_id'  => $json_response['response']['thread'],
            'parent'     => $json_response['response']['parent'],
        );
    }

    public static function GetThreadPosts($cursor_url) {
        $json_response = DisqusAPIHelpers::DisqusAPICall($cursor_url);
        $posts = array();

        foreach ($json_response['response'] as $post) {
            $posts[] = array(
                'id'        => $post['id'],
                'parent'    => $post['parent'],
                'author'    => $post['author']['name'],
                'createdAt' => $post['createdAt'],
                'message'   => $post['raw_message'],
            );
        }

        return array(
            'posts'   => $posts,
            'hasNext' => $json_response['cursor']['hasNext'],
            'next'    => $json_response['cursor']['next'],
        );
    }

    public static function GetThreadDetails($thread_id) {
        $disqus_url = sprintf(
                'https://disqus.com/api/3.0/threads/listPosts.json?thread=%s&limit=2',
                $thread_id);

        $posts = array();
        $cursor_url = $disqus_url;
        do {
            $posts_dict = DisqusAPIHelpers::GetThreadPosts($cursor_url);
            $posts = array_merge($posts, $posts_dict['posts']);
            $cursor_url = sprintf('%s&cursor=%s', $disqus_url, 
                    $posts_dict['next']);
        } while ($posts_dict['hasNext']);

        usort($posts, function($post_one, $post_two) {
            return $post_one['createdAt'] < $post_two['createdAt'];
        });

        //$posts[0] is the newest post
        return $posts;
    }
}

function json_log($x) {
    error_log(json_encode($x));
}

class MiigleObject extends ElggObject {
    public static function create($dict) {
        $object = new MiigleObject();

        $keys = array_keys($dict);

        $object->set('subtype', 'miigle_object');
        $object->set('owner_guid', get_loggedin_userid());
        $object->set('access_id', ACCESS_PUBLIC);
        $object->set('keys', json_encode($keys));

        foreach ($dict as $key=>$value) {
            $object->set('key:'.json_encode($key), json_encode($value));
        }

        $object->save();

        return $object;
    }

    public function toDict() {
        $keys = json_decode($this->get('keys'), true);
        $dict = array(
            'guid' => $this->get('guid'),
        );

        foreach ($keys as $key) {
            $namespace_key = 'key:'.json_encode($key);
            $dict[$key] = json_decode($this->get($namespace_key), true);
        }

        return $dict;
    }
}

function MakeDisqusComment($idea_guid, $comment_id) {
    global $CONFIG;
    $user_guid = get_loggedin_userid();
    if (!$user_guid) {
      return false;
    }

    $idea = get_entity($idea_guid);
    $idea_owner = get_entity($idea->owner_guid);
    $owner_username = $idea_owner->username;

    if (!$idea) {
        throw new Exception("Invalid idea_guid: $idea_guid");
    }
    if (!$idea_owner) {
        throw new Exception("Invalid user: {$idea->owner_guid}");
    }

    $comment = DisqusAPIHelpers::GetCommentDetails($comment_id);
    $all_comments = DisqusAPIHelpers::GetThreadDetails(
            $comment['thread_id']);
    $subject_username = $comment['username'];
    $subject = get_user_by_username($subject_username);

    $all_usernames = array_values(array_unique(array_map(function($comment) {
        return $comment['author'];
    }, $all_comments)));

    if (!in_array($owner_username, $all_usernames)) {
        //send special owner note
        MiigleNotification::CreateNotification($idea_owner->guid, 
                                               $subject->guid,
                                               $idea_guid, 'comment', 
                                               json_encode($comment['message']));
    } else if ($owner_username == $subject_username) {
        $all_usernames = array_values(
                array_diff($all_usernames, array($owner_username)));
    }

    if ($comment['parent'] != null) {
        $parent_comment = DisqusAPIHelpers::GetCommentDetails(
                $comment['parent']);
        $parent_username = $parent_comment['username'];
        $all_usernames = array_values(
                array_diff($all_usernames, array($parent_username)));

        //send special reply note
        $parent_user = get_user_by_username($parent_username);
        MiigleNotification::CreateNotification($parent_user->guid, 
                                               $subject->guid,
                                               $idea_guid, 'comment_reply', 
                                               json_encode($comment['message']));
    }

    //send all notes
    foreach ($all_usernames as $username) {
        $user = get_user_by_username($username);
        MiigleNotification::CreateNotification($user->guid, 
                                               $subject->guid,
                                               $idea_guid, 'comment', 
                                               json_encode($comment['message']));
    }

    $comment_object = MiigleObject::create($comment);
    $action_payload = json_encode(array($comment_object->guid));
    CreateFeedItem($subject->guid, 'user:comment::idea', $action_payload,
            $idea_guid);

    return true;
}
expose_function('ideas.disqus_comment', 'MakeDisqusComment',
    array(
        'idea_guid'    => array('type'=>'int',    'required'=>true),
        'comment_id' =>   array('type'=>'string', 'required'=>true),
    ),
    '',
    'POST',
    false,
    false
);
