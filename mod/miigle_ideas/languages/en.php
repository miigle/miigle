<?php

    $english = array(
    
        /**
         * Menu items and titles
         */
    
            'idea' => "Idea",
            'ideas' => "Ideas",
            'idea:user' => "%s's idea",
            'idea:user:friends' => "%s's friends' ideas",
            'idea:your' => "Your idea",
            'idea:ideatitle' => "%s's idea: %s",
            'idea:addphotos' => "Choose Photo",
            'idea:addvideos' => "YouTube Video URL",
            'idea:adddocs' => "Choose Document",
            'idea:friends' => "Friends' ideas",
            'idea:yourfriends' => "Your friends' latest ideas",
            'idea:everyone' => "All site ideas",
            'idea:newidea' => "New idea",
            'idea:via' => "via idea",
            'idea:read' => "Read idea",    
            'idea:addidea' => "Submit New Idea",
            'idea:editidea' => "Edit idea",
            'ideas:settings:idea_categories' => 'Idea Categories',
    
            'idea:text' => "Full Idea Description",
    
            'idea:strapline' => "%s",
            
            'item:object:idea' => 'posted ideas',
    
            'idea:never' => 'never',
            'idea:preview' => 'Preview',
    
            'idea:draft:save' => 'Save draft',
            'idea:draft:saved' => 'Draft last saved',
            'idea:comments:allow' => 'Allow comments',
            'idea:conversation' => 'Conversation',
    
            'idea:preview:description' => 'This is an unsaved preview of your idea.',
            'idea:preview:description:link' => 'To continue editing or save your idea, click here.',
    
            'groups:enableidea' => 'Enable group idea',
            'idea:group' => 'Group idea',
            'idea:nogroup' => 'This group does not have any idea posts yet',
            'idea:more' => 'More idea posts',
    
            'idea:read_more' => 'Read full idea',

        /**
         * idea widget
         */
        'idea:widget:description' => 'This widget displays your latest idea entries.',
        'idea:moreideas' => 'More idea posts',
        'idea:numbertodisplay' => 'Number of idea posts to display',
        
         /**
         * idea river
         **/
            
            //generic terms to use
            'idea:river:created' => "%s wrote",
            'idea:river:updated' => "%s updated",
            'idea:river:posted' => "%s posted",
            
            //these get inserted into the river links to take the user to the entity
            'idea:river:create' => "a new idea post titled",
            'idea:river:update' => "a idea post titled",
            'idea:river:annotate' => "a comment on this idea post",
            
    
        /**
         * Status messages
         */
    
            'idea:posted' => "Your idea was successfully posted.",
            'idea:deleted' => "Your idea was successfully deleted.",
    
        /**
         * Error messages
         */
    
            'idea:error' => 'Something went wrong. Please try again.',
            'idea:save:failure' => "Your idea could not be saved. Please try again.",
            'idea:blank' => "Sorry; you need to fill in both the title and body before you can make a post.",
            'idea:notfound' => "Sorry; we could not find the specified idea.",
            'idea:notdeleted' => "Sorry; we could not delete this idea.",
    
    );
                    
    add_translation("en",$english);

?>