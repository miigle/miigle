<?php
class Scraper {
    /*
    Kickstarter and AngelList scrapes share common keys: title, pitch, 
        image_urls, and foster_url

    Possible added keys are tags, location, logo_url, video_urls, url, and
        description
    */

    /*
    returns an array with keys: title, pitch, image_urls, pitch, tags, 
        foster_url, location.
    Possibly adds key logo_url
    */
    public static function ScrapeKickstarterURL($url) {
        $script_path = dirname(__FILE__) . '/kickstarter.js';
        $process_output = exec("phantomjs $script_path $url");
        if ($process_output === 'null') {
            throw new Exception("Unknown error with url: $url");
        }
        $json_dict = json_decode($process_output, true);
        $project_data = $json_dict['data'];

        $project_dict = array(
            'title' => $project_data['name'],
            'pitch' =>$project_data['blurb'],
            'image_urls' => array($project_data['photo']['full']),
            'location' => $project_data['location']['displayable_name'],
            'tags'     => array($project_data['category']['name']),
            'foster_url' => $project_data['urls']['web']['project'],
        );

        /*
        if (sizeof($project_data['video']) > 0) {
            if (isset($project_data['video']['high'])) {
                $project_dict['video_url'] = $project_data['video']['high'];
            } else if (isset($project_data['video']['base'])) {
                $project_dict['video_url'] = $project_data['video']['base'];
            }
        }
        */

        if (sizeof($project_data['creator']) > 0) {
            $project_dict['logo_url'] = $project_data['creator']['avatar'][
                    'medium'];
        }

        return $project_dict;
    }

    private static function GetJSON($url) {
        return json_decode(file_get_contents($url), true);
    }

    /*
    returns an array with keys: title, pitch, image_urls, logo_url, url, 
        foster_url, and description.
    Possibly adds keys tags, location, video_url, and image_urls

    */
    public static function ScrapeAngelListURL($url) {
        $url = trim($url, '/'); //stripping off trailing /
        $url = 'https' . preg_replace('/^http(s)?/', '', 
                $url); //making sure we have https
        $url_parts = explode('/', $url);
        $startup_name = array_pop($url_parts);
        $json_response = Scraper::GetJSON(
                "http://api.angel.co/1/search?query=$startup_name&type=Startup");

        foreach ($json_response as $json_result) {
            //make this check better
            if ($json_result['url'] == $url) {
                $id = $json_result['id'];
                break;
            }
        }

        if (!isset($id)) {
            throw new Exception("Unable to find $startup_name");
        }
        
        $startup_json = Scraper::GetJSON(
                "http://api.angel.co/1/startups/$id");
        $roles_json =   Scraper::GetJSON(
                "http://api.angel.co/1/startups/$id/roles");

        $startup_roles = $roles_json['startup_roles'];

        foreach($startup_roles as $stakeholder) {
            continue;
            $stakeholder_id = $stakeholder['id'];
            file_get_contents("https://api.angel.co/1/users/$stakeholder_id");
        }

        $startup_dict = array(
            'title'       => $startup_json['name'],
            'pitch'       => $startup_json['high_concept'],
            'image_urls'  => $startup_json['screenshots'],
            'logo_url'    => $startup_json['logo_url'],
            'url'         => $startup_json['company_url'],
            'description' => $startup_json['product_desc'],
            'foster_url'  => $url,
        );

        $tags = array_map(function($market) {
            return $market['name'];
        }, $startup_json['markets']);

        if (sizeof($tags) > 0) {
            $startup_dict['tags'] = $tags;
        }

        if (sizeof($startup_json['locations']) > 0) {
            $location_dict = array_shift($startup_json['locations']);
            $startup_dict['location'] = $location_dict['display_name'];
        }

        if (sizeof($startup_json['video_url']) > 1) {
            $startup_dict['video_url'] = $startup_json['video_url'];
        }

        if (sizeof($startup_json['screenshots']) > 0) {
          $screenshot_urls = array_map(function($screenshot) {
              return $screenshot['original'];
          }, $startup_json['screenshots']);
          $startup_dict['image_urls'] = $screenshot_urls;
        }

        return $startup_dict;
    }

    public static function ScrapeCrunchbaseURL($url) {
        global $CONFIG;

        $url = trim($url, '/');
        $url_parts = explode('/', $url);
        $entity_name = array_pop($url_parts);
        $entity_type = array_pop($url_parts);

        if (!in_array($entity_type, array('company', 'product'))) {
            throw new Exception("Invalid url: $url");
        }

        $json_response = Scraper::GetJSON(sprintf(
                'http://api.crunchbase.com/v/1/%s/%s.js?api_key=%s', 
                    $entity_type, $entity_name, $CONFIG->CRUNCHBASE_API_KEY));

        $idea_dict = array(
            'title' => $json_response['name'],
            'description' => strip_tags($json_response['overview']),
            'website' => $json_response['homepage_url'],
            'foster_url' => $json_response['crunchbase_url'],
            'type_int' => ($entity_type == 'company') ? 1 : 2,
        );

        if (!empty($json_response['image'])) {
            $logo_url = 'http://crunchbase.com/' . array_pop(array_pop(
                    $json_response['image']['available_sizes']));
            $idea_dict['logo_url'] = $logo_url;
        }

        if (!empty($json_response['screenshots'])) {
            $image_urls = array_map(function($screenshot_dict) {
                return 'http://crunchbase.com/' . array_pop(array_pop(
                        $screenshot_dict['available_sizes']));
            }, $json_response['screenshots']);
            $idea_dict['image_urls'] = $image_urls;
        }

        if ($json_response['tag_list']) {
            $idea_dict['tags_list'] = explode(',', $json_response['tag_list']);
        }

        if (!empty($json_response['offices'])) {
            $office = array_shift($json_response['offices']);
            $idea_dict['location'] = $office['city'];
        }

        foreach ($idea_dict as $key=>$value) {
            if (boolval($value) == false) {
                unset($idea_dict[$key]);
            }
        }

        return $idea_dict;
    }
}

function ScrapeExternalIdea($url) {
    $url = trim($url);
    if (preg_match('/^.*angel.co.*$/', $url)) {
        $idea_dict = Scraper::ScrapeAngelListURL($url);
        $idea_dict['type_int'] = 1;
    /*
    } else if (preg_match('/^.*kickstarter.com.*$/', $url)) {
        $idea_dict = Scraper::ScrapeKickstarterURL($url);
        $idea_dict['type_int'] = 2;
    */
    } else if (preg_match('/^.*crunchbase.com.*$/', $url)) {
        $idea_dict = Scraper::ScrapeCrunchbaseURL($url);
    } else {
        throw new Exception("Invalid url: $url");
    }

    if (!isset($idea_dict['tags'])) {
        $idea_dict['tags'] = array($idea_dict['title']);
    }
    if (!isset($idea_dict['description']) && isset($idea_dict['pitch'])) {
        $idea_dict['description'] = $idea_dict['pitch'];
    }
    if (!isset($idea_dict['url'])) {
        $idea_dict['url'] = $idea_dict['foster_url'];
    }
    if (!isset($idea_dict['video_urls'])) {
        $idea_dict['video_urls'] = array();
    }

    $image_guids = array();
    if (isset($idea_dict['image_urls']) && !empty($idea_dict['image_urls'])) {
        $image_urls = $idea_dict['image_urls'];
        $image_contents = array_map('file_get_contents', $image_urls);
        $image_guids = array_map('CreateMiigleImage', $image_contents);

    }
    unset($idea_dict['image_urls']);
    $idea_dict['image_guids'] = $image_guids;

    if (isset($idea_dict['logo_url'])) {
        $logo_url = $idea_dict['logo_url'];
        $icon_contents = file_get_contents($logo_url);
        $icon_guid = CreateMiigleImage($icon_contents);

        unset($idea_dict['logo_url']);
        $idea_dict['icon_guid'] = $icon_guid;
    }

    return $idea_dict;
}


expose_function('ideas.scrape_url', 'ScrapeExternalIdea',
    array(
        'url' => array('type'=>'string', 'required'=>true),
    ),
    '',
    'POST',
    false,
    false
);
