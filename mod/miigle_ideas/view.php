<?php

    /**
     * Elgg view idea details page
     * 
     * @package ElggIdeas
     */

    // Load Elgg engine
    require_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
    
    //gatekeeper();

    // Get the specified idea post
    $idea_guid = (int) get_input('idea_guid', 0);
    
    //echo '$idea_guid='.$idea_guid;
    $idea = get_entity($idea_guid);

    // If we can get out the idea ...
    if ($idea){
        $idea_is_locked = (isset($idea->is_locked)?$idea->is_locked:"No")=="Yes"?true:false;
        if($idea_is_locked){
            register_error("This idea is locked, to view you must login and send request to the owner.");
            forward();
        }
        // Set the page owner
        if ($idea->owner_guid) {
            set_page_owner($idea->owner_guid);
        } else {
            set_page_owner($idea->container_guid);
        }

        $page_owner = page_owner_entity();

        //get the comments form
        $comments_form = elgg_view("ideas/forms/comment", array(
                                        'entity' => $idea,
                                        'entity_owner' => $page_owner
                                        ));
                                        
        //get the comments thread
        $comments_thread = elgg_view("ideas/comments-listing", array(
                                        'entity' => $idea,
                                        'entity_owner' => $page_owner
                                        ));
                                        
        // Display it
        $area2 = elgg_view("ideas/view", array(
                                        'entity' => $idea,
                                        'entity_owner' => $page_owner,
                                        'comments_form' => $comments_form,
                                        'comments_thread' => $comments_thread
                                        ));

        // Set the title appropriately
        $title = $idea->title;
                

        // Display through the correct canvas area
        $body = elgg_view_layout("one_column", $area2);
    }
    else{
        // If we're not allowed to see the idea
        
        // Display the 'post not found' page instead
        $body = elgg_view("ideas/notfound");
        $title = elgg_echo("idea:notfound");
    }
        
    // Display page
    page_draw($title,$body);
?>