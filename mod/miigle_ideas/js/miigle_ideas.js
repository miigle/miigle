/* john.pavlick@miigle.com write this file. Email me for questions */

/* Snatched from http://stackoverflow.com/questions/5830387/how-to-find-all-youtube-video-ids-in-a-string-using-a-regex
             and http://stackoverflow.com/questions/2916544/parsing-a-vimeo-id-using-javascript */
function ExtractYouTubeVideoID(url) {
    var re = /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*[^\w\s-])([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig;
    return url.replace(re, '$1');
}
function ExtractVimeoVideoID(url) {
    var match = /vimeo.*\/(\d+)/i.exec(url);
    if (match) {
        return match[1];
    }
}
function IsVideoURLValid(url) {
    return (ExtractYouTubeVideoID(url) != url ||
            ExtractVimeoVideoID(url) !== undefined);
}


function CalculateGeographicCoordinatesDistance(longitude_one, latitude_one, longitude_two, latitude_two) {
    var ConvertDegreesToRadians = function(degrees) {
        return degrees * Math.PI / 180;
    };
    var EARTH_RADIUS_MILES = 3961;
    var d_longitude;
    var d_latitude;
    var a;
    var i;

    for(i in arguments) {
        arguments[i] = ConvertDegreesToRadians(arguments[i]);
    }

    d_longitude = longitude_one - longitude_two;
    d_latitude = latitude_one - latitude_two;

    a = Math.pow(Math.sin(d_latitude / 2), 2) + (Math.cos(latitude_one) * 
                                                 Math.cos(latitude_two) * 
                                                 Math.pow(Math.sin(d_longitude / 2), 2));

    return 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)) * EARTH_RADIUS_MILES;
}

function GenerateVideoDictFromVideoURL(video_url) {
    var video_dict;
    var vimeo_video_id;
    var youtube_video_id;

    vimeo_video_id = ExtractVimeoVideoID(video_url);
    youtube_video_id = ExtractYouTubeVideoID(video_url);

    if (vimeo_video_id === undefined && youtube_video_id !== video_url) {
        return {video_type: 'youtube', video_id: youtube_video_id};
    } else if (vimeo_video_id !== undefined && youtube_video_id === video_url) {
        return {video_type: 'vimeo', video_id: vimeo_video_id};
    } else {
        //oh god wut
        console.error('oh god');
        return false;
    }
}

angular.module('miigle.idea.view', ['miigle.video', 'miigle.share.email', 'miigle.share.twitter', 'ngSanitize', 'miigle.feed',
                                    'miigle_star_rating', 'miigle.profile.employer', 'miigle.rpc'])
    .directive('sharebutton', function() {
        return {
            restrict: 'E',
            replace: true,
            template: '<a><span class="glyphicon glyphicon-share"></span> Share</a>',
        };
    })
    .filter('newlines', function() {
        return function(text) {
            return text.replace(/\n/g, '<br>');
        };
    })
    .controller('IdeaPotentialContributorsSocialGraphViewController', function($scope, $controller,
                                                                               IdeaPotentialContributorsCacheFactory) {
        $controller('IdeaPotentialContributorsController', {$scope: $scope});

        var idea_owner = miigle.getEntity($scope.idea.owner_guid);
        var idea_owner_name = idea_owner.first_name + ' ' + idea_owner.last_name;
        var full_name_to_icon_url = {};
        var full_name_to_user = {};
        var social_graph = {};

        $scope.social_graph = {};
        $scope.potential_contributors_graph_loaded = false;

        IdeaPotentialContributorsCacheFactory.getPotentialContributorsGraph($scope.idea.guid)
            .then(function(abc) {
                $scope.potential_contributors_graph_loaded = true;
                $scope.social_graph = abc;

                $.each($scope.social_graph, function(key, value) {
                    key = Number(key);

                    if (value.length == 0) {
                        value = [{to: key, from: key}];
                    }

                    social_graph[key] = value;
                });
                InitGraph();
            });

        var InitGraph = function() {
            var nodes = {};
            var links = [];
            var filtered_contributors_guids = $scope.filtered_contributors
                .map(function(user_dict) {
                    return user_dict.guid;
                });

            $('#putsvghere').html('');

            Object.keys(social_graph)
                .map(Number)
                .filter(function(key) {
                    return key == idea_owner.guid || filtered_contributors_guids.indexOf(key) !== -1;
                })
                .map(String)
                .map(function(key) {
                    return social_graph[key];
                })
                .forEach(function(value) {
                    links.push.apply(links, value);
                });

            links = links
                .map(function(link) {
                    var source = miigle.getEntity(link.from);
                    var target = miigle.getEntity(link.to);

                    var source_name = source.first_name + ' ' + source.last_name;
                    var target_name = target.first_name + ' ' + target.last_name;

                    link.source = source_name;
                    link.target = target_name;

                    full_name_to_icon_url[source_name] = '/pg/image/' + source.icon_guid + '?w=100&h=100';
                    full_name_to_icon_url[target_name] = '/pg/image/' + target.icon_guid + '?w=100&h=100';

                    full_name_to_user[source_name] = source;
                    full_name_to_user[target_name] = target;

                    return link;
                });

            // Compute the distinct nodes from the links.
            links.forEach(function(link) {
              link.source = nodes[link.source] || (nodes[link.source] = {name: link.source});
              link.target = nodes[link.target] || (nodes[link.target] = {name: link.target});
            });

            var width = $('#putsvghere').parent().width(),
                height = $(window).height() - $('#putsvghere').offset().top;

            var force = d3.layout.force()
                .nodes(d3.values(nodes))
                .links(links)
                .size([width, height])
                .linkDistance(120)
                .charge(-1000)
                .on('tick', tick)
                .start();

            var svg = d3.select('#putsvghere').append('svg')
                .attr('width', width)
                .attr('height', height);

            // Per-type markers, as they don't inherit styles.
            svg.append('defs').selectAll('marker')
                .data(['suit', 'licensing', 'resolved'])
                .enter().append('marker')
                .attr('id', function(d) { return d; })
                .attr('viewBox', '0 -5 10 10')
                .attr('refX', 15)
                .attr('refY', -1.5)
                .attr('markerWidth', 6)
                .attr('markerHeight', 6)
                .attr('orient', 'auto')
                .append('path')
                .attr('d', 'M0,-5L10,0L0,5');

            var path = svg.append('g').selectAll('path')
                .data(force.links())
                .enter().append('path')
                .attr('class', function(d) { 
                  //console.log('applying class to ', d); 
                  return 'link ' + d.type; 
                })
                ;

            function GetNodeXY(node) {
                if (node.name === idea_owner_name) {
                    return -50;
                } else {
                    return -25;
                }
            }

            function GetNodeHeightWidth(node) {
                if (node.name === idea_owner_name) {
                    return 100;
                } else {
                    return 50;
                }
            }


            var node = svg.selectAll('.node')
                .data(d3.values(nodes))
                .enter().append('g')
                .attr('class', 'node')
                .call(force.drag);

            node.append('rect')
              .attr('x', GetNodeXY)
              .attr('y', GetNodeXY)
              .attr('height', GetNodeHeightWidth)
              .attr('width', GetNodeHeightWidth)
              .style('stroke', '#ffe000')
              .style('fill', 'none')
              .style('stroke-width', function(node) {
                var user_guid = full_name_to_user[node.name].guid;
                return (filtered_contributors_guids.indexOf(user_guid) === -1) ?
                    '0px' : '5px';
              });

            var mouse_x = 0;
            var mouse_y = 0;

            node.append('image')
                .attr('xlink:href', function(n) {
                  return full_name_to_icon_url[n.name];
                })
                .attr('x', GetNodeXY)
                .attr('y', GetNodeXY)
                .attr('width', GetNodeHeightWidth)
                .attr('height', GetNodeHeightWidth)
                .style('cursor', 'pointer')
                .on('mouseover', function(event) {
                    mouse_x = event.x;
                    mouse_y = event.y;
                })
                .on('mouseleave', function(event) {
                })
                .on('click', function(event) {
                    var user = full_name_to_user[event.name];
                    
                    if (event.x == mouse_x && event.y == mouse_y) {
                        window.location.href = '#/user/' + user.username;
                    }
                })
                ;

            var text = svg.append('g').selectAll('text')
                .data(force.nodes())
                .enter().append('text')
                .attr('x', function(n) {
                    if (n.name === idea_owner_name) {
                        return '-30px';
                    } else {
                        return '-25px';
                    }
                })
                .attr('y', function(n) {
                    if (n.name === idea_owner_name) {
                        return '60px';
                    } else {
                        return '35px';
                    }
                })
                .text(function(d) { return d.name; });

            // Use elliptical arc path segments to doubly-encode directionality.
            function tick() {
              path.attr('d', linkArc);
              text.attr('transform', transform);
              node.attr('transform', transform);
            }

            function linkArc(d) {
              return 'M' + d.source.x + ' ' + d.source.y + ' ' + d.target.x + ' ' + d.target.y;
            }

            function transform(d) {
              if (d.name === idea_owner_name) {
                var x = width / 2 + 50;
                var y = height / 2;
                d.x = x;
                d.y = y;
                return 'translate(' +  x + ',' + y + ')';
              } else {
                if (d.x < 30) {
                    //d.x = 25;
                    d.x += 1;
                } else if (d.x > width - 35) {
                    //d.x = width - 45;
                    d.x -= 1;
                }
                if (d.y < 30) {
                    //d.y = 25;
                    d.y += 1;
                } else if (d.y > height - 35) {
                    //d.y = height - 45;
                    d.y -= 1;
                }

                return 'translate(' + d.x + ',' + d.y + ')';
              }
            }
        };

        $scope.$watch('filtered_contributors', InitGraph);
        $scope.$watch('potential_contributors_list', InitGraph);
    })
    .controller('IdeaPotentialContributorsViewController', function($scope, $controller) {
        $controller('IdeaPotentialContributorsController', {$scope: $scope});
    })
    .factory('IdeaPotentialContributorsCacheFactory', function($cacheFactory, $q, $http) {
        var idea_potential_contributors_list_cache  = $cacheFactory('pc_list');
        var idea_potential_contributors_graph_cache = $cacheFactory('pc_graph');

        var get = function(idea_guid, pc_type) {
            var value;
            var deferred = $q.defer();
            var method;
            var cache;

            switch (pc_type) {
            case 'list':
                cache = idea_potential_contributors_list_cache;
                method = 'ideas.get_potential_contributors';
                break;

            case 'graph':
                cache = idea_potential_contributors_graph_cache;
                method = 'ideas.get_potential_contributors_graph';
                break;

            default: //oh god
                throw new Error('Unknown pc_type', pc_type);
            }

            value = cache.get(String(idea_guid));

            if (value === undefined) {
                $http({
                    method: 'GET',
                    url: '/services/api/rest/json',
                    params: {
                        method: method,
                        idea_guid: Number(idea_guid),
                    },
                    cache: false,
                    responseType: 'json',
                })
                .success(function(data, status, headers, config) {
                    value = data.result;
                    cache.put(String(idea_guid), value);
                    deferred.resolve(value);
                });
            } else {
                deferred.resolve(value);
            }

            return deferred.promise;
        };

        return {
            getPotentialContributorsList: function(idea_guid) {
                return get(idea_guid, 'list');
            },
            getPotentialContributorsGraph: function(idea_guid) {
                return get(idea_guid, 'graph');
            },
        };
    })
    .controller('IdeaPotentialContributorsController', function($scope, $controller) {
        var init;
        var filterContributors;

        $controller('IdeaViewController', {$scope: $scope});

        init = function() {
            $(window).on('resize', function() {
                $('.conditional-affix').each(function(i, element) {
                    var $element = $(element);
                    if ($element.height() + $element.offset().top < $(window).height()) {
                        $element.width($element.parent().width());
                        $element.affix();
                    }
                });
            });

            $scope.resetLocation();
            $scope.contributor_location = $scope.idea.location_dict;

            $scope.persona_filters = [
               {persona: 'Entrepreneur', checked: true},
               {persona: 'Investor', checked: true},
               {persona: 'Mentor', checked: true},
               {persona: 'Professional', checked: true},
               {persona: 'Beta User', checked: true},
            ];

            $scope.expertise_filters = Object.keys($scope.expertise_dict)
                .map(function(expertise_int) {
                    expertise_int = Number(expertise_int); //Object.keys give syou strings always?
                    return {
                        expertise_int: expertise_int, 
                        expertise: $scope.expertise_dict[expertise_int], 
                        checked: ($scope.idea.needed_expertise_list.length > 0) ? 
                                  $scope.idea.needed_expertise_list.indexOf(expertise_int) !== -1 :
                                  true,
                    };
                });

            $scope.$watch('persona_filters', filterContributors, true);
            $scope.$watch('expertise_filters', filterContributors, true);
            $scope.$watch('contributor_radius', filterContributors, true);
            $scope.$watch('contributor_location', filterContributors, true);

            $scope.$watch('idea', function() {
                InitLocationDropdown($('#contributor-location-input'), {location_dict: {}}, function(location) {
                    $scope.$apply(function() {
                        $scope.contributor_location = location;
                    });
                });
                $('#contributor-location-input').val($scope.idea.location_dict.fqcn);
            });
        };

        $scope.resetLocation = function() {
            $('#contributor-location-input').val('');
            $scope.contributor_radius = 0;
            $scope.contributor_location = undefined;
        };

        filterContributors = function() {
            var personas = $scope.persona_filters
                .filter(function(persona_dict) {
                    return persona_dict.checked;
                })
                .map(function(persona_dict) {
                    return persona_dict.persona;
                });

            var expertise_ints = $scope.expertise_filters
                .map(function(expertise_dict) {
                    if (expertise_dict.checked) {
                        return expertise_dict.expertise_int;
                    }
                })
                .filter(function(expertise_int) {
                    return expertise_int !== undefined;
                });

            $scope.filtered_contributors = $scope.potential_contributors_list
                .filter(function(potential_contributor) { //filtering on persona
                    return potential_contributor.personas_list
                        .some(function(persona) {
                            return personas.indexOf(persona) !== -1;
                        });
                })
                .filter(function(potential_contributor) { //filtering on expertise
                    return potential_contributor.work_info_dict.expertise_list
                        .some(function(expertise_int) {
                            return expertise_ints.indexOf(expertise_int) !== -1;
                        });
                })
                .filter(function(potential_contributor) { //filtering on distance
                    var args;
                    var distance;

                    if (Number($scope.contributor_radius) === 0 || $scope.contributor_location === undefined) {
                        return true;
                    } else if (!potential_contributor.location_dict || 
                               !potential_contributor.location_dict.latitude || 
                               !potential_contributor.location_dict.longitude) {
                        return false;
                    } else {
                        args = [
                            potential_contributor.location_dict.latitude,
                            potential_contributor.location_dict.longitude,
                            $scope.contributor_location.latitude,
                            $scope.contributor_location.longitude,
                        ];

                        distance = CalculateGeographicCoordinatesDistance.apply(null, args);
                        return ($scope.contributor_radius > distance);
                    }
                })
                ;
        };

        $scope.$on('potential_contributors_list_loaded', filterContributors);

        init();
    })
    .controller('IdeaViewController', function($scope, $sce, $controller, FeedFactory,
                                               IdeaPotentialContributorsCacheFactory) {
        var i, stakeholder_id, num_stakeholders = 0;
        var current_video_block = [];
        var current_image_block = [];

        var PartitionBlocks;

        $controller('IdeaController', {$scope: $scope});

        $scope.potential_contributors_list_loaded = false;
        $scope.potential_contributors_list = [];

        IdeaPotentialContributorsCacheFactory.getPotentialContributorsList($scope.idea.guid)
            .then(function(abc) {
                $scope.potential_contributors_list_loaded = true;
                $scope.potential_contributors_list = abc;
                $scope.$broadcast('potential_contributors_list_loaded', true);
            });

        var init; //ideapotentialcontributorsviewcontroller also needs this

        $scope.toggleCheer = function(method_name) { 
            if (method_name === 'ideas.uncheer') {
                delete $scope.cheering_users_dict[$scope.loggedin_user_dict.guid];
            } else {
                $scope.cheering_users_dict[$scope.loggedin_user_dict.guid] = $scope.loggedin_user_dict;
            }

            $scope.$broadcast('cheer', method_name);
        };

        $scope.updateIdea = function() {
            $scope.$apply(function() {
                $scope.idea.cheer_count = miigle.getEntity($scope.idea.guid).cheer_count;
            });
            FeedFactory.BreakFeedCache();
            FeedFactory.WarmCache();
        };

        $scope.ViewSpecificInit = function() {
            InitDisqus($scope.idea.title);

            $scope.num_cheering = 0;
            //fuck you angular
            $scope.$watchCollection('cheering_users_dict', function() {
                $scope.num_cheering = Object.keys($scope.cheering_users_dict).length;
            });

            $scope.idea_rating = {
                impact: 1,
                originality: 1,
                monetizable: 1,
                green: 1, 
            };
            $scope.has_user_rated_idea = false;

            $.ajax({
                type: 'GET',
                url: '/services/api/rest/json/?method=ideas.get_user_rating',
                dataType: 'json',
                data: { idea_guid: $scope.idea.guid },
                complete: function(jqxhr, text_status) {
                    var response = JSON.parse(jqxhr.responseText);
                    var rating_dict;

                    if (response.status == 0 && response.result !== 'false') {
                        $scope.has_user_rated_idea = true;
                        rating_dict = response.result.rating_dict;
                        $.each(rating_dict, function(key, value) {
                            rating_dict[key] = value.toString();
                        });

                        $scope.$apply(function() {
                            $scope.idea_rating = rating_dict;
                        });
                    }
                },
            });
        };

        init = function() {
            $scope.idea.type = miigle_ideas.idea_types[$scope.idea.type_int];
            $scope.idea_owner = miigle.getEntity($scope.idea.owner_guid);
            $scope.idea.time_created *= 1000; //seconds to milliseconds
            num_stakeholders = Object.keys($scope.idea.stakeholders_dict).length;
            $scope.idea.has_stakeholders = (num_stakeholders > 0);

            $scope.idea.image_guid_blocks = PartitionBlocks($scope.idea.image_guids_list, 3);

            $scope.viewing_user_is_owner = (miigle.getLoggedInUser().guid == $scope.idea.owner_guid);

            $scope.cheering_users_dict = {};

            /*
            $scope.potential_contributors_list = $scope.potential_contributors_guid_list
                .map(function(guid) {
                    return miigle.getEntity(guid);
                });
            */

            $scope.idea.cheering_guids_list.forEach(function(follower_guid) {
                $scope.cheering_users_dict[follower_guid] = miigle.getEntity(follower_guid);
            });

            //does this do anything?
            $('#idea-rating-impact-input').tooltip({
                animation: false,
                title: function() {
                    return $scope.idea_rating.impact;
                },
            });

            $scope.idea_question_community = miigle_ideas.idea_question_communities[$scope.idea.question_community_int];
            $scope.idea_question_community_class = window.GetExpertiseIntClass($scope.idea.question_community_int);
        };

        //turns [a, b, c, d, e, f, g, h, i] into (with a blocksize of 3)
        //[[a, b, c], [d, e, f], [g, h, i]]
        PartitionBlocks = function(list, block_size) {
            var current_block = [];
            var blocks = [];
            
            $.each(list, function(i, list_item) {
                current_block.push(list_item);

                if (current_block.length == block_size) {
                    blocks.push(current_block);
                    current_block = [];
                }
            });
            if (current_block.length > 0) {
                blocks.push(current_block);
            }

            return blocks;
        };

        $scope.deleteIdea = function($event) {
            $event.preventDefault();

            var post_data = Object.keys(miigle.getActionTokenDict()).map(function(key) {
                    return {name: key, value: miigle.getActionTokenDict()[key]};
                })
                .concat({
                    name: 'idea_guid',
                    value: $scope.idea.guid,
                });

            if (confirm('Are you sure?')) {
                $.ajax({
                    type: 'POST',
                    url: '/services/api/rest/json/?method=ideas.delete',
                    dataType: 'json',
                    data: post_data,
                    complete: function(jqxhr, text_status) {
                        var response_dict = JSON.parse(jqxhr.responseText);

                        if (response_dict.status == 0) {
                            FeedFactory.BreakFeedCache();
                            FeedFactory.WarmCache();
                            window.location.href = '/pg/miigle';
                        } else {
                            alert(response_dict.message);
                        }
                    },
                });
            }
        };

        $scope.rateIdea = function() {
            if (miigle.getLoggedInUser().guid == 0) {
                alert('You must login to rate an idea!');
                return;
            } else {
                $('#rating-modal').modal('show');
            }
        };

        $scope.submitRating = function() {
            $.each($scope.idea_rating, function(key, value) {
                $scope.idea_rating[key] = parseInt(value, 10);
            });

            var post_data = Object.keys(miigle.getActionTokenDict()).map(function(key) {
                    return {name: key, value: miigle.getActionTokenDict()[key]};
                })
                .concat({
                    name: 'idea_guid',
                    value: $scope.idea.guid,
                })
                .concat({
                    name: 'rating_dict_string',
                    value: JSON.stringify($scope.idea_rating),
                });

            $.ajax({
                type: 'POST',
                url: '/services/api/rest/json/?method=ideas.rate',
                dataType: 'json',
                data: post_data,
                complete: function(jqxhr, text_status) {
                    var response = JSON.parse(jqxhr.responseText);
                    $scope.$apply(function() {
                        $scope.idea = response.result;
                        miigle.setEntity($scope.idea.guid, $scope.idea);
                    });
                },
            });
        };

        $scope.GenerateEmailBody = function(child_scope) {
            return encodeURI(
             'Hi ' + child_scope.first_name + '\n\n' +
             'Check out ' + $scope.idea.title + '\n' +
             miigle.getLoggedInUser().first_name);
        };

        $scope.GenerateEmailSubject = function(child_scope) {
            return encodeURI(miigle.getLoggedInUser().first_name + ' wants to show you ' + $scope.idea.title);
        };

        $scope.slideIt = function($event, ass) {
            var $parent = $($event.target).parent();
            $(ass).carousel($parent.data().slide);
        };

        //when a user clicks in the video iframe, pause the carousel
        $(window).blur(function() {
            $('#carousel-video').carousel('pause');
        });

        init();
    })
    ;

//this is a disgusting monster
angular.module('miigle.idea.edit', ['blueimp.fileupload', 'miigle.video', 'miigle.image_upload'])
    //This is dumb. This is so we can "share" a stakeholder icon between controllers
    .factory('StakeholderIconFactory', function() {
        var stakeholder_icon;

        //check out this sexy closure
        //stakeholder_icon is now a "private" variable.
        return {
            get: function() {
                var temp = stakeholder_icon;
                stakeholder_icon = undefined;
                return temp;
            },

            set: function(new_icon) {
                stakeholder_icon = new_icon;
            },
        };
    })
    .controller('IconController', function(StakeholderIconFactory, $scope, $element, $controller) {
        $controller('ImageUploadController', {
            $scope: $scope, 
            $element: $element,
        });

        $scope.PostAppendHook = function($element, thumbnail) {
            //if they're setting a stakholder icon, put it in the factory
            //so CardController can grab it later. This is what I was talking
            //about a couple lines up with my big paragraph comment.
            if ($element.hasClass('stakeholder-fileupload')) {
                StakeholderIconFactory.set(thumbnail);
            }
        };
    })
    //pull in the icon from the factory, and put it on the card
    .controller('CardController', function(StakeholderIconFactory, $scope, $element) {
        var thumbnail = StakeholderIconFactory.get();

        if (thumbnail) {
            $element.empty();
            $element.append(thumbnail);
        }
    })
    //oh god this is a monster!
    .controller('IdeaEditController', ['$scope', '$controller', '$timeout', 
        'RPCService',
        function($scope, $controller, $timeout, RPCService) {
        var key;
        var category_int;
        var num_stakeholders = 0;
        var init;
        var post_url_base = '/services/api/rest/json/';
        var $validator;
        $scope.current_step = 0;
        $scope.stakeholder_nonce = 0;
        $scope.description_max_length = 5000;
        $scope.question_max_length = 5000;

        //copying over some variables that edit_view.php jammed
        //into the global scope with a <script> tag
        $scope.idea_types = miigle_ideas.idea_types;
        $scope.idea_categories = miigle_ideas.idea_categories;

        $scope.scraping = false;
        $scope.scrape_button_text = 'Autofill';

        $controller('IdeaController', {$scope: $scope});

        $scope.scrape = function() {
            $scope.scraping = true;
            $scope.scrape_button_text = 'Give me a minute...';

            RPCService.RPC({
                method: 'ideas.scrape_url',
                url: $scope.scrape_url, 
            })
            .then(function(idea_dict) {
                var $tags_list = $('.tm-input');

                $scope.scraping = false;
                $scope.scrape_button_text = 'Hooray!';

                idea_dict.image_guids_list = idea_dict.image_guids;
                idea_dict.website = idea_dict.url;

                delete idea_dict.url;
                delete idea_dict.image_guids;
                delete idea_dict.video_urls;

                $tags_list.tagsManager('empty');
                idea_dict.tags.forEach(function(tag) {
                    $tags_list.tagsManager('pushTag', tag);
                });
                delete idea_dict.tags;

                if (idea_dict.location !== undefined) {
                    $('.typeahead')
                        .typeahead('setQuery', idea_dict.location)
                        .focus();
                    delete idea_dict.location;
                }

                Object.keys(idea_dict)
                    .forEach(function(key) {
                        $scope.idea[key] = idea_dict[key];
                    });

            }, function(error) {
                $scope.scraping = false;
                $scope.scrape_button_text = 'Uh oh...';
                alert(error);
            });
        };

        //if your editor supports it, collapse this function. It's horribly long
        init = function() {
            //this is like $(document).ready, we do a bunch of boring
            $(document).ready(function() {
                //Setting up the tag input
                
                //idk why I have to do this inside of $timeout
                //fuck you, Angular
                $timeout(function() {
                    $('.tm-input').tagsManager({
                        CapitalizeFirstLetter: true,
                        prefilled: $scope.idea.tags_list,
                        output: '#hidden-tags-list',
                    });

                    //dumb hack to make sure the tags-input always is green/red
                    //when it is supposed to. so many edge cases...oh god this was
                    //a nightmare...
                    $('#hidden-tags-list').on('change', function() {
                        $(this).valid();
                    });
                });

                //omg why?
                $scope.$watch('idea', function() {
                    InitLocationDropdown($('#idea-location-input'), $scope.idea);
                });

                //setting up custom methods
                $.validator.addMethod('idea_tags', function(value, element) {
                    return $('#hidden-tags-list').val().length > 0;
                }, 'Please specify at least one tag.');

                $.validator.addMethod('idea_type', function(value, element) {
                    return value !== undefined;
                }, 'Please select an idea type.');

                $.validator.addMethod('video_url', function(value, element) {
                    return (ExtractYouTubeVideoID(value) != value ||
                            ExtractVimeoVideoID(value) !== undefined ||
                            value === '');
                }, 'Please enter a valid YouTube or Vimeo video URL.');

                //setting up form validation
                $validator = $('#idea-edit-form').validate({
                    debug: true,
                    validClass: 'has-success',
                    errorClass: 'error has-error',

                    rules: {
                        idea_title: {
                            required: true,
                            minlength: 2, //?
                            maxlength: 50,
                        },
                        idea_url: {
                            required: false,
                            url: true,
                        },
                        idea_location: {
                            required: true,
                            location: true,
                        },
                        idea_category: {
                            required: true,
                        },
                        idea_pitch: {
                            required: true,
                            minlength: 10, //?
                            maxlength: 140,
                        },
                        idea_tags: {
                            idea_tags: true,
                        },
                        idea_description: {
                            required: true,
                            minlength: 10, //?
                            maxlength: 5000, //?
                        },
                        idea_question: {
                            //again ?
                            minlength: 10,
                            maxlength: 5000,
                            required: {
                                depends: function(element) {
                                    return $('#idea-question-community').val();
                                },
                            }
                        },
                        idea_foster_url: {
                            required: false,
                            url: true,
                        },
                        add_video_url_input: {
                            required: false,
                            url: true,
                            video_url: true,

                        },
                        stakeholder_first_name_input: {
                            required: true,
                        },
                        stakeholder_last_name_input: {
                            required: true,
                        },
                        stakeholder_role_input: {
                            required: true,
                        },
                        stakeholder_linkedin_input: {
                            required: false,
                            url: true,
                        },
                    },

                    errorPlacement: function(error, element) {
                        if(element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    highlight: function(element, errorClass, validClass) {
                        $(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        //for linkedin url and omg why
                        if (element.value.length > 0 && element.id != 'idea-tags') {
                            $(element).closest('.form-group').addClass(validClass);
                        } else {
                            $(element).closest('.form-group').removeClass(validClass);
                        }
                    },
                });

                //again why the fuck,angular?
                $timeout(function() {
                    $('.idea-type').rules('add', {
                        idea_type: true,
                    });
                });
            });

            //yeah this is disgusting. it's used for keeping track of which
            //stakeholder icon div should be visible, and which ones
            //to keep hidden.
            $scope.stakeholder_icon_div_ids = [];

            $scope.temp = {
                video_url: '',
            }; //used for holding temporary stakeholder inputs

            //if we have an idea guid, then we're editing an idea. 
            //so the button should say "save"
            if ($scope.idea.guid != 0) {
                $scope.submit_button_text = 'Submit';
            } else {
                $scope.submit_button_text = 'Save';
            }

            //These need to be turned into strings because otherwise they get screwed
            //up in Angular and the DOM because <option> values are strings. I can't really
            //remember exactly, but you're entering a world of pain if you remove these. (I think)
            $scope.idea.category_int = $scope.idea.category_int.toString();
            $scope.idea.question_community_int = $scope.idea.question_community_int.toString();

            //Oh yeah this is ridiculous. The ng-options in edit_view.php is horrible.
            //you're not allowed to iterate over a list and use index, value. 
            //You iterate over a list of objects, and use the object's k/v pairs as index, value
            //so $scope.ng_options_list comes out to be something like 
            //[{category_int: 1, category_string: 'Business'}, {category_int: 2, category_string: 'Arts'}, etc]
            //absolutely disgusting.
            $scope.ng_category_options_list = [];
            $scope.ng_community_options_list = [];
            $.each($scope.idea_categories, function(key, value) {
                $scope.ng_category_options_list.push({
                    category_int: key,
                    category_string: value
                });
            });
            $.each(miigle_ideas.idea_question_communities, function(key, value) {
                $scope.ng_community_options_list.push({
                    category_int: key,
                    category_string: value,
                });
            });


            //If we're editing an idea, we need to look through all the stakeholders, find the max
            //stakeholder_id, and set our nonce as it+1. This way, we never overwrite stakeholders.
            $.each($scope.idea.stakeholders_dict, function(stakeholder_id, stakeholder_dict) {
                num_stakeholders += 1;
                stakeholder_id = Number(stakeholder_id);

                if (stakeholder_id > $scope.stakeholder_nonce) {
                    $scope.stakeholder_nonce = stakeholder_id;
                }
            });

            $scope.stakeholder_nonce += 1;
            $scope.stakeholder_icon_div_ids.push($scope.stakeholder_nonce);
            //push it to the stakeholder id list so angular displays it for us

            $scope.preview_video_dict = {
                show_video_preview: false,
            };
        };

        $scope.updateVideoPreview = function() {
            var $url_input = $('#add-video-url-input');
            var video_dict = GenerateVideoDictFromVideoURL($url_input.val());

            $scope.preview_video_dict.show_video_preview = !!video_dict; //like saying boolval(video_dict)
            $scope.preview_video_dict.video_id = video_dict.video_id;
            $scope.preview_video_dict.video_type = video_dict.video_type;
        };

        $scope.addVideoURL = function() {
            var new_url = $scope.temp.video_url;
            var $url_input = $('#add-video-url-input');
            var video_dict;
            var video_type;
            var video_id;
            var duplicate_video;

            //make sure this url is valid
            if ($url_input.valid()) {
                video_dict = GenerateVideoDictFromVideoURL(new_url);

                //angular puts crap in our objects since we're using them in an ng-repeat
                //specifically, it puts a $$hashKey k/v pair in there.
                //So we have to roll our own Array.indexOf() here by just comparing the k/v
                //pairs we care about. (not comparing $$hashKey)
                //so what I'm doing here is looking through $scope.idea.video_list to see if
                //we have any matches to video_dict
                duplicate_video = $scope.idea.videos_list.some(function(element, index, array) {
                    //you can't compare objects with == or === as I found out the hard way...
                    return (element.video_type === video_dict.video_type &&
                            element.video_id   === video_dict.video_id);
                });

                //make sure we're not adding a duplicate video.
                if (duplicate_video) {
                    return;
                }

                $scope.preview_video_dict.show_video_preview = false;

                //it would be nice if we didn't use a list here. It would be nice if we used
                //an ordered set or something, but for now this is fine.
                $scope.idea.videos_list.push(video_dict);
                $url_input.val('');
            }
        };

        //the input here is really just the list index
        $scope.removeVideoURL = function(video_url_id) {
            $scope.idea.videos_list.splice(video_url_id, 1); //rip it out of the list
            //don't do "delete some_list[some_index]" JavaScript will surprise you with the result!
        };

        //is this crap readable?
        //or should I have just typed $.ajax twice?
        (function() {
            var GetBasePostData;
            var CreateAjax;

            //these $scope functions are what you care about.
            //They are called when a user clicks on the X next to an image
            $scope.deleteImage = function(image_guid) {
                var image_list_index = $scope.idea.image_guids_list.indexOf(image_guid);
                $scope.idea.image_guids_list.splice(image_list_index, 1);
                CreateAjax('image_guid', image_guid, 'ideas.delete.image');
            };

            $scope.deleteStakeholder = function(stakeholder_id) {
                //you can "delete" a k/v out of an object, but don't do it on a list as I talked about earlier
                delete $scope.idea.stakeholders_dict[stakeholder_id]; 
                CreateAjax('stakeholder_id', stakeholder_id, 'ideas.delete.stakeholder');
            };

            GetBasePostData = function() { 
                //this returns an array of the following form
                /*
                [{name: '__elgg_token', value: 'some weird token'},
                 {name: '__elgg_ts', value: 'some timestamp'},
                 {name: 'idea_guid', value: 'the idea we are editing's guid'}]
                */
                return Object.keys(miigle.getActionTokenDict()).map(function(key) {
                        return {name: key, value: miigle.getActionTokenDict()[key]};
                    })
                    .concat({
                        name: 'idea_guid',
                        value: $scope.idea.guid,
                    });
            };

            CreateAjax = function(extra_param_name, extra_param_value, method_name) {
                //if we're creating a new idea, we don't need to ajax to the server to tell it to delete
                //stuff, because the server has no stuff at this point. Make sense?
                if (!$scope.idea.guid) { return; } 

                //fire the ajax call
                //if this fails...it fails silently. bad.
                $.ajax({
                    type: 'POST',
                    url: post_url_base + '?method=' + method_name,
                    dataType: 'json',
                    data: GetBasePostData().concat({name: extra_param_name, value: extra_param_value}),
                    success: function() {
                        //FeedFactory.BreakFeedCache();
                        //FeedFactory.WarmCache();
                    },
                });
            }
        })();

        $scope.addStakeholder = function(hide_popup) { //defaults to false
            var stakeholder_inputs_valid = true;
            var icon_file_list;

            $('.stakeholder-input').each(function(index, element) {
                stakeholder_inputs_valid &= $(element).valid();
            });

            //only add a stakeholder if all of the fields are valid
            if (!stakeholder_inputs_valid) {
                return;
            }

            icon_file_list = $('input[name="stakeholder_icons[' + $scope.stakeholder_nonce + ']"]')[0].files;

            if (icon_file_list.length == 0) {
                if (!hide_popup) {
                    alert('Please add a stakeholder icon');
                }

                return;
            }

            //copying in from $scope.temp
            $scope.idea.stakeholders_dict[$scope.stakeholder_nonce] = {
                first_name: $scope.temp.stakeholder_first_name,
                last_name: $scope.temp.stakeholder_last_name,
                role: $scope.temp.stakeholder_role,
                linkedin_url: $scope.temp.stakeholder_linkedin_url,
                icon_url: '/pg/image/0', //this gets us the default, grey user icon
            };

            //clear temp
            $scope.temp = {};

            //unhighlight and clear each input. kinda of nasty
            $('.stakeholder-input').each(function(index, element) {
                $validator.settings.unhighlight.call( 
                    $validator, 
                    $(element).val('')[0],
                    $validator.settings.errorClass, 
                    $validator.settings.validClass
                );
            });

            //This crap...yeah I've talked about it before...
            $scope.stakeholder_nonce += 1;
            $scope.stakeholder_icon_div_ids.push($scope.stakeholder_nonce);
        };

        $scope.nextStep = function() {
            $scope.slideTo($scope.current_step + 1);
        };

        $scope.prevStep = function() {
            $scope.slideTo($scope.current_step - 1);
        };

        //BUG: trying to slide from step 1 to step 3 goes through without checking step 2 inputs
        //TODO: fix it
        $scope.slideTo = function(step_number) {
            var $current_step_page = $('#steps-body > .item.active');
            var $inputs_to_validate = $current_step_page.find('.form-control,.idea-type,#idea-tags,#hidden-tags-list');
            var all_inputs_valid = true;
            var $steps_carousel = $('#steps-carousel');
            var $uploads_carousel = $('#uploads-carousel');

            //only check if we're moving up a step
            //e.g. don't check if we go from step 2 to step 1
            if ($scope.current_step < step_number) {
                $inputs_to_validate.each(function(index, element) {
                    all_inputs_valid &= $(element).valid();
                });
            }

            if (all_inputs_valid) {
                $scope.current_step = Number(step_number);
                var new_header_title = $($('#steps-body > .item')[step_number]).children('.step').attr('data-header-text');

                $('#steps-header-title').text(new_header_title);
                $('html, body').animate({scrollTop: 0}, 'fast');

                //these raise an error from bootstrap.js
                //Cannot call method 'to' of null
                //But doesn't seem to be a problem...
                $steps_carousel.carousel(step_number);
                $uploads_carousel.carousel(step_number);
            };
        };

        $scope.submitIdea = function() {
            var $form;
            var all_file_inputs;
            var key, value;
            var file_list = [], param_name_list = [];
            var form_data = [];

            if ($scope.temp.stakeholder_first_name) {
                $scope.addStakeholder(true);
            }

            //if it's a CSV string, make it a list
            //I think it's always a CSV string coming in from the tag manager plugin
            if (!$.isArray($scope.idea.tags_list)) { 
                $scope.idea.tags_list = $scope.idea.tags_list.split(',');
            }

            $form = $('#idea-edit-form'); //the entire form that wraps everything

            //filling in elggts and elggtoken
            $.each(miigle.getActionTokenDict(), function(name, value) {
                form_data.push({
                    name: name,
                    value: value,
                });
            });

            $scope.idea.needed_expertise_list = Object.items($scope.idea_expertise_dict)
                .filter(function(tuple) { return tuple[1]; })
                .map(function(tuple) { return parseInt(tuple[0]); });

            //we need form_data to look like 
            //[{name: 'blah1', value: 'blah1val'}, {name: 'blah2', value: 'blah2val'}, ...] 
            //we did this once before in this file...maybe we should make this into a function?
            $.each($scope.idea, function(key, value) {
                if ($.isArray(value) || $.isPlainObject(value)) {
                    //have to use this instead of JSON.parse because angular puts 
                    //shit in your $scope objects. like $$hashKey and stuff.
                    //angular.toJson removes that stuff.
                    value = angular.toJson(value); 
                }

                form_data.push({
                    name: key,
                    value: value,
                });
            });

            //initting the fileupload that will actually do uploading    
            $form.fileupload({
                url: $form.attr('action'),
                type: 'POST',
                replaceFileInput: false,
                singleFileUploads: false,
                formData: form_data,
                dataType: 'json',
                add: function(e, data) {
                    //jQuery File Upload has (what I consider) a bug if you
                    //try to upload 0 files. So we just append it a dummy file here
                    if (data.files.length == 0) {
                        data.files.push('');
                    }

                    data.submit()
                        .complete(function(jqxhr, text_status) {
                            var response = JSON.parse(jqxhr.responseText);
                            var new_idea_dict = {};

                            if (response.status == 0) {
                                new_idea_dict = response.result.idea_dict;
                                miigle.setEntity(new_idea_dict.guid, new_idea_dict);
                                window.location.href = '#/idea/' + new_idea_dict.guid;
                            } else {
                                /*
                                What should we do here? This can occur if you POST too much data.

                                Each image is restricted to <= 5MB, (we set this in our jQuery file upload settings)
                                but we aren't limiting how many images the user can POST. So, the user can end up POSTing a lot of
                                data. We can up the limit in php.ini (or whatever it's called) to like 100MB,
                                or, we can check here in miigle_ideas.js that the sum total of image file sizes
                                doesn't exceed a certain value. We could create a REST endpoint that responds with
                                the php.ini max POST setting, and then set the limit in here dynamically using that.
                                So, we have some options.
                                */
                                console.log('oh no...');
                                console.log(JSON.parse(jqxhr.responseText));
                                alert('An error has occured. Maybe you added too many images? Error: ' + jqxhr.responseText);
                            }
                        });
                },

                //TODO: test the loading bar

                //start loading bar
                submit: function(e, data) {
                    $('#progress-bar').removeClass('hidden');
                    console.log(data);
                    return true;
                },

                //update loading bar
                progress: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    var $progress_bar = $('#progress-bar');

                    $progress_bar.css('width', progress + '%');
                    $progress_bar.attr('aria-valuenow', progress); //wtf is this
                },

                //turn off loading bar
                always: function(e, data) {
                    $('#progress-bar').addClass('hidden');
                },
            });

            //joining together all our file inputs
            //one for the icon, N for the images, and K for the stakeholder icons
            all_file_inputs = $('input[type="file"]');

            //for each file input
            //return does the for loop equivelant of "continue" for a $.each
            //loop. I can't remember why I do this.
            $.each(all_file_inputs, function(i, file_input) {
                var stakeholder_id;

                if (file_input.dataset &&
                    file_input.dataset.stakeholderId !== undefined) {
                    stakeholder_id = Number(file_input.dataset.stakeholderId);
                    if (!(stakeholder_id in $scope.idea.stakeholders_dict)) {
                       return;
                    }
                }

                //for each file in the file input
                $.each(file_input.files, function(j, file) {
                    file_list.push(file);
                    param_name_list.push(file_input.name);
                });
            });

            //this fires the add method on the fileupload
            $form.fileupload('add', {
                files: file_list,
                paramName: param_name_list,
            });
        };

        init();
    }
]);

angular.module('miigle.idea', ['ngRoute', 'miigle.idea.view', 'miigle.idea.edit'])
    .controller('CheerButtonController', ['$scope', '$rootScope',
        function($scope, $rootScope) {
            $scope.idea = miigle.getEntity($scope.ideaGuid);
            $scope.is_user_cheering_idea = ($scope.idea.cheering_guids_list.indexOf(miigle.getLoggedInUser().guid) != -1);
            $scope.custom_style = {};

            if ($scope.is_user_cheering_idea) {
                $scope.custom_style['background-color'] = '#ebebeb';
            }

            $scope.$on('cheer', function(event, data) {
                if (data === 'ideas.cheer') {
                    $scope.is_user_cheering_idea = true;
                } else {
                    $scope.is_user_cheering_idea = false;
                }
            });

            $scope.toggleCheer = function() {
                var method_name;

                if ($rootScope.loggedin_user_dict.guid == 0) {
                    alert('You must login to cheer an idea!');
                    return;
                }

                if ($scope.is_user_cheering_idea) {
                    method_name = 'ideas.uncheer';
                } else {
                    method_name = 'ideas.cheer';
                }

                $scope.is_user_cheering_idea = !$scope.is_user_cheering_idea; 

                if ($scope.initialCallback()) {
                    $scope.initialCallback()(method_name);
                }

                var post_data = Object.keys(miigle.getActionTokenDict()).map(function(key) {
                        return {name: key, value: miigle.getActionTokenDict()[key]};
                    })
                    .concat({
                        name: 'idea_guid',
                        value: $scope.ideaGuid,
                    });

                $.ajax({
                    type: 'POST',
                    url: '/services/api/rest/json/?method=' + method_name,
                    dataType: 'json',
                    data: post_data,
                    complete: function(jqxhr, text_status) {
                        var new_idea = JSON.parse(jqxhr.responseText).result;
                        miigle.setEntity(new_idea.guid, new_idea);
                        if ($scope.successCallback()) {
                            $scope.successCallback()();
                        }
                    },
                });
            };
        }
    ])
    .directive('cheerbutton', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_ideas/templates/cheer_button.html', 
            scope: {
                ideaGuid: '=',
                initialCallback: '&',
                successCallback: '&',
                cheeredText: '=',
                cheerItText: '=',
            },
            controller: 'CheerButtonController',
        };
    })
    .controller('IdeaController', function($controller, $scope, $routeParams) {
        $(window).scrollTop(0);

        $scope.expertise_dict = miigle.getExpertiseDict();

        var GetBlankIdea = function() {
            var idea_dict;

            $.ajax({
                url: '/services/api/rest/json/?method=ideas.get&guid=0',
                dataType: 'json',
                async: false,
                success: function(data) {
                    idea_dict = data.result;
                },
            });

            return idea_dict;
        };

        if ($routeParams.idea_guid === undefined) {
            $scope.idea = GetBlankIdea();
        } else {
            $scope.idea = $.extend(true, {}, miigle.getEntity($routeParams.idea_guid));
            miigle.getEntity($scope.idea.owner_guid);
        }

        $scope.idea_expertise_dict = Object.create(null);
        $.each($scope.expertise_dict, function(expertise_int, expertise) {
            expertise_int = parseInt(expertise_int);
            $scope.idea_expertise_dict[expertise_int] = ($scope.idea.needed_expertise_list.indexOf(expertise_int) !== -1);
        });
    })
    .config(function($routeProvider) {
        $routeProvider
            .when('/idea/new', {
                controller: 'IdeaEditController',
                templateUrl: '/mod/miigle_ideas/templates/edit_idea.html',
            })
            .when('/idea/:idea_guid/edit', {
                controller: 'IdeaEditController',
                templateUrl: '/mod/miigle_ideas/templates/edit_idea.html',
            })
            .when('/idea/:idea_guid', {
                controller: 'IdeaViewController',
                templateUrl: '/mod/miigle_ideas/templates/view_idea.html',
            })
            .when('/idea/:idea_guid/potential_contributors/graph', {
                controller: 'IdeaPotentialContributorsSocialGraphViewController',
                templateUrl: '/mod/miigle_ideas/templates/view_idea_potential_contributors_social_graph.html',
            })
            .when('/idea/:idea_guid/potential_contributors', {
                controller: 'IdeaPotentialContributorsViewController',
                templateUrl: '/mod/miigle_ideas/templates/view_idea_potential_contributors.html',
            })
            ;
    })
    ;
