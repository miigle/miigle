(function() {
  var page = require('webpage').create();
  var system = require('system');
  var url = system.args[1];

  page.open(url, function(status) {
    var current_project;

    if (status !== 'success') {
      console.log('Error');
    } else {
      current_project = page.evaluate(function() {
        return window.current_project;
      });

      console.log(JSON.stringify(current_project));
      phantom.exit();
    }
  });
})();
