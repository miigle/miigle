<?php

    /**
     * Elgg ideas CSS extender
     * 
     * @package ElggIdeas
     */

?>
/*********** quickidea box *************/
#quickidea-box{
    background:none;
    width:495px;
    height:39px;
}
#quickidea-box input[type="text"]{
    background:transparent;
    border:0;
    width:428px;
    height:29px;
    padding:5px 10px;
}
#quickidea-box input[type="button"]{
    width:40px;
    height:39px;
    padding:0;
    margin:0;
    background:transparent;
    border:0;
}
#quickidea-box input#btn-quick-idea-share{
    background:url('/mod/miigle_theme/graphics/share-idea-bar.png') no-repeat scroll left top transparent;
    width:497px;
    height:41px;
    padding:0;
    margin:0;
    border:0;
}
#quickidea-box #quickideatitle{
    font-size:14px;
}
#ideaDetails #quickideatitle.show-hint{
    color:#999999;
}

#ideaDetails{
    /*display:none;*/
    background-color:#fafafa;
    border:1px solid #ddd;
    border-radius:0 0 4px 4px;
    border-top:0;
    padding:10px 20px;
    margin:0 0 0 0;
}

#ideaDetails p{
    padding:6px 0;
    margin:0;
    text-align:left;
}

#ideaDetails textarea#ideabody{
    height:100px;
    width:500px;
}

#ideaDetails input[type="text"], #ideaDetails select, #ideaDetails  textarea{
    border:1px solid #b4b4b4;
    font-size:12px;
    width:443px;
}


/************************************/
#ideas .pagination {
    margin:5px 10px 0 10px;
    padding:5px;
    display:block;
}
#ips-tab1 #two_column_left_sidebar_maincontent {
    padding-bottom:50px;
    min-height:100px;
}

#two_column_left_sidebar_maincontent{
    width:481px;
    margin:0 auto;
}

.buttons_bar{
    display:block;
    width:auto;
    height:auto;
    padding:5px 5px 5px 378px;
    background-color:#E0E0E0;
}

.buttons_bar input{
    margin:0;
}

.idea-del-btn{
    cursor:pointer;
}

.idea-del-btn:hover{
    background-color:#BBB;
}

#form_container{
    padding:0 10px;
}

#form_container p{
    margin: 20px 0 0 0;
}

#form_container input[type="text"], #form_container textarea{
    width:445px;
    border-radius:5px;
}

#form_container input[type="text"], 
#form_container textarea, 
#form_container select, #form_container input[type="file"]{
    font-size:12px;
    font-weight:normal;
    color:#222;
}

#form_container input.idea_vids{
    width:390px;
    margin-bottom:5px;
}

#form_container input.idea_pics{
    margin-bottom:5px;
}

#form_container label{
    font-weight:bold;
}

label span.normal-font{
    font-weight:normal;
}

#dummy_box{
    display:block;
    border:1px solid #B4BBCD;
    padding:2px;
    width:475px;
    border-radius:0;
    height:50px;
    color:#777777;
}

#ips-tab1 input[type="file"].idea_pics{
    margin-bottom:15px;
}

#ips-tab1 textarea.idea_vids{
    margin-bottom:15px;
    width:80%;
}

.idea-box{
    margin-bottom:30px;
    border-bottom:1px dotted #000;
}

.idea-desc-box{
    width:99%;
    border:1px solid #777777;
    height:150px;
    overflow-y:scroll;
}

.idea-header{
    margin-bottom:20px;
}

.pics-n-vids-container{
    width:99%;
    margin: 10px auto;
    padding:10px 0;
}

.idea-top-pic{
    float:left;
    margin-left:10px;
}

.idea-top-pic{
    float:right;
    margin-right:10px;
}

.idea-comments-box{
    margin:20px 0 0 0;
    padding:20px 0;
    width:495px;
    background-color:#f3f3f3;
    border:1px solid #cfcfcf;
}

.idea-comments-box .comments_form{
    margin:0 auto;
    width:475px;
}

.idea-comments-box .comments-listing{
    width:475px;
    margin:0 10px;
}

.idea-comments-box .comments-listing h3{
    color:#242424;
    font-size:12px;
    font-weight:normal;
    padding:0;
    height:17px;
}

.idea-comments-box .comments-listing h3 label{
    display:block;
    float:left;
    background:url('/mod/miigle_theme/graphics/comments-count-bg.png') no-repeat scroll left top transparent;
    width:25px;
    height:25px;
    padding:4px 4px 2px 4px;
    color:#F9D401;
    text-align:center;
    font-weight:bold;
    font-size:11px;
}

.comments-listing .single-comment{
    margin: 20px 0 0 0;
    padding: 0 0 25px 0;
    border-bottom:1px solid #cfcfcf;
}

.comments-listing .single-comment.last{
    border:0 none;
}

.comment-annotations .commenter{
    float:left;
    padding:4px 4px 4px 0;
}

.comment-annotations .commenter a{
    text-decoration:none;
    text-transform:none;
    font-size:12px;
    font-weight:normal;
}

.commenter div.usericon{
    float:left;
}

.commenter label{
    display:block;
    float:left;
    font-weight:normal;
    margin:3px;
}

.comment-stamp{
    font-size:10px;
    font-weight:bold;
}

.comment-reply-container{
    background:url('/mod/miigle_theme/graphics/icon-reply.png') no-repeat scroll left 10px transparent;
    width:430px;
    padding:15px 5px 5px 40px;
    border-bottom:1px dashed #cfcfcf;
    font-size:11px;
    font-weight:normal;
}
.comment-reply-container.last{
    border:0 none;
}
a.comment-reply{
    display:block;
    background:url('/mod/miigle_theme/graphics/icon-comment-reply.png') no-repeat scroll 5px 5px transparent;
    float:left;
    margin-left:25px;
    padding:5px;
    width:60px;
    height:22px;
}

/*
a.comment-reply:hover{
    border:1px solid #cfcfcf;
    border-radius:3px;
    -webkit-border-radius:3px;
    padding:5px;
}
*/

div.thumbs-up-btn{
    float:left;
    margin-left:25px;
    padding:5px;
}

div.thumbs-up-btn label{
    padding-top:5px;
    float:right;
    font-weight:bold;
    width:20px;
    text-align:left;
}

div.thumbs-up-btn a.thumbs-up{
    display:block;
    float:left;
    background:url('/mod/miigle_theme/graphics/icon-thumbs-up.png') no-repeat scroll 0 2px transparent;
    width:32px;
    height: 21px;
}

a.mentor-request{
    display: block;
    background: url('/mod/miigle_theme/graphics/icon-mentor-request.png') no-repeat scroll 5px 5px transparent;
    float:left;
    margin-left:25px;
    padding:5px;
    width: 134px;
    height: 22px;
}

.comment-text{
    margin-top:10px;
    font-size:13px;
    font-weight:normal;
}



div.idea-photo{
    width:100%;
    padding-bottom:10px;
    margin-right:10px;
    margin-bottom:10px;
    border-bottom:1px solid #c9c9c9;
}

div.idea-video-container{
    padding:10px 5px 20px 5px;
    margin-bottom:30px;
    border-bottom:1px solid #c9c9c9;
}

div.idea-video-container div.idea-video-menu input{
    margin:10px 0 0 0;
    padding:0;
}

div.idea-video-menu{
    
}

div.photos-videos{
    background-color:#d8d8d8;
    padding:5px;
}

input.photos-button{
    float:left;
}

input.photos-button-big{
    float:left;
}

input.photos-button-big{
    float:left;
}

input.videos-button{
    float:right;
}


div.idea-video-hidden{
    /*display:none;*/
    text-align:center;
    margin:0px auto;
    padding:0;
    width:640px;
}

div#idea-main-media iframe{
    position:relative; 
    z-index:0;
}

.sectionContainer{
    background-color: #F5F5F5;
    border: 1px solid #DBDBDB;
    border-radius: 2px 2px 2px 2px;
    height: auto;
    margin: 20px 0 0;
    padding: 10px;
    width: 475px;
}

.sectionHeader h2{
    float: left;
}

.sectionHeader h3{
    float: left;
}

.sectionHeader input.section-toggle-btn{
    float:right;
}

.sectionHeader input.expand{
    background: url("/mod/miigle_theme/graphics/icon-expand.png") no-repeat scroll left top transparent;
    border: 0 none;
    cursor: pointer;
    height: 25px;
    margin: 0;
    padding: 0;
    width: 25px;
}

.sectionHeader input.collapse{
    background: url("/mod/miigle_theme/graphics/icon-collapse.png") no-repeat scroll left top transparent;
    border: 0 none;
    cursor: pointer;
    height: 25px;
    margin: 0;
    padding: 0;
    width: 25px;
}

.sectionContent{
    /*display:none;*/
    margin-top:10px;
}

.sectionContent input[type="checkbox"]{
    margin:0 5px 0 0;
}

#funding-options{
    /*display:none;*/
    margin:10px 0 0 10px;
}

.options-info-box{
    /*display:none;*/
    margin:10px 0 10px 20px;
}

.funding-options-details{
    background-color:#e5e5e5;
    margin:4px 0;
    padding:5px;
    border-radius:5px;
}

#saveBtnBox{
    width:200px;
    margin:20px auto;
}

#saveBtnBox input[type='submit']{
    font-size:14px;
    font-weight:bold;
    width:190px;
    padding:5px;
    height:30px;
}
/****************************** View Idea Styles ********************************/
    #startup p.description {
      font-size:14px;
    }
    .idea-view-first-col{
        float:left;
        width:300px;
        margin:0 25px 0 0;
        padding:0;
    }
    
    .idea-view-first-col .idea-post-info{
        font-size: 11px;
        font-weight: normal;
        margin: 5px 0 0;
    }
    
    .idea-tags{
        margin:10px 0 0 0;
        color:#f9d401;
        font-weight:normal;
        font-size:11px;
    }
    .idea-tags label{
        color:#242424;
        font-weight:bold;
        margin-right:5px;
    }
    
    .idea-full-desc-text{
        margin:10px 0 0 0;
        font-size:12px;
        text-align:justify;
    }
    
    .idea-description{
        margin:10px 0 0 0;
        font-size:12px;
        text-align:justify;
    }
    
    .idea-view-second-col{
        float:right;
        width:165px;
        margin:0;
        padding:0;
    }
    
    .idea-view-second-col img.invite{
        width:19px;
        height:20px;
        margin:0 0 0 15px;
        padding:0;        
    }
    
    .idea-view-second-col img.locked{
        width:10px;
        height:14px;
        margin:0 20px 2px 0;
        padding:0;
    }
    
    .idea-view-second-col h3{
        font-size:14px;
        font-weight:bold;
        margin:20px 0 5px 0;
    }
    
    .idea-main-video-image-overlay{
        cursor:pointer;
        position:absolute;
        left:0px;
        top:0px;
        width:165px;
        height:124px;
        background:url("/mod/miigle_theme/graphics/btn-video-play.png") no-repeat scroll 63px 43px transparent;
    }
    
    .idea-main-video-big-image-overlay{
        cursor:pointer;
        position:absolute;
        left:0px;
        top:0px;
        width:200px;
        height:150px;
        background:url("/mod/miigle_theme/graphics/btn-video-play.png") no-repeat scroll 50% 50% transparent;
    }
    
    ul#idea-videos-slider li{
        position:relative;
    }
    
    .idea-video-image-overlay{
        cursor:pointer;
        position:absolute;
        left:0px;
        top:0px;
        width:400px;
        height:300px;
        background:url("/mod/miigle_theme/graphics/btn-video-play.png") no-repeat scroll 180px 130px transparent;
    }
    
    input[type="button"].photos-button{
        background:url("/mod/miigle_theme/graphics/icon-small-idea-photos.png") no-repeat scroll left top transparent;
        width:58px;
        height:16px;
        border:0 none;
        border:0 none;
        border-radius:0;
        -moz-border-radius:0;
        -webkit-border-radius:0;
        cursor:pointer;
    }
    
    input[type="button"].videos-button{
        background:url("/mod/miigle_theme/graphics/icon-small-idea-videos.png") no-repeat scroll left top transparent;        
        width:65px;
        height:15px;
        border:0 none;
        border:0 none;
        border-radius:0;
        -moz-border-radius:0;
        -webkit-border-radius:0;
        margin-left:10px;
        cursor:pointer;
    }

    input[type="button"].photos-button-big{
        background:url("/mod/miigle_theme/graphics/icon-idea-photos.png") no-repeat scroll left top transparent;
        width:75px;
        height:21px;
        border:0 none;
        border:0 none;
        border-radius:0;
        -moz-border-radius:0;
        -webkit-border-radius:0;
        cursor:pointer;
        float:left;
    }
    
    input[type="button"].videos-button-big{
        background:url("/mod/miigle_theme/graphics/icon-idea-videos.png") no-repeat scroll left top transparent;        
        width:84px;
        height:19px;
        border:0 none;
        border:0 none;
        border-radius:0;
        -moz-border-radius:0;
        -webkit-border-radius:0;
        margin-left:10px;
        cursor:pointer;
        float:left;
    }
    
    input.cheer-button[type="button"]{
        background:url("/mod/miigle_theme/graphics/icon-idea-cheer-small.png") no-repeat scroll left top transparent;        
        width:62px;
        height:23px;
        border:0 none;
        border-radius:0;
        -moz-border-radius:0;
        -webkit-border-radius:0;
        margin-left:100px;
        cursor:pointer;
        float:left;
        padding:0 0 0 27px;
    }

    input[type="button"].fund-button{
        background:url("/mod/miigle_theme/graphics/icon-idea-fund.png") no-repeat scroll left top transparent;        
        width:56px;
        height:22px;
        border:0 none;
        border:0 none;
        border-radius:0;
        -moz-border-radius:0;
        -webkit-border-radius:0;
        margin-left:10px;
        cursor:pointer;
        float:left;
    }

    input[type="button"].follow-button{
        background:url("/mod/miigle_theme/graphics/icon-idea-follow.png") no-repeat scroll left top transparent;        
        width:64px;
        height:22px;
        border:0 none;
        border-radius:0;
        -moz-border-radius:0;
        -webkit-border-radius:0;
        margin-left:10px;
        cursor:pointer;
        float:left;
    }

    .idea-photos img:first{
        margin-left:10px;
    }
    .idea-photos img{
        margin-right:10px;
    }
    
    #foster-tab-content table{
        width:100%;
        border:0 none;
        border-collapse:collapse;
        background-color:#d9d9d9;
        margin-top:15px;
    }
    
    #foster-tab-content table tr th{
        padding:5px;
        border:1px solid #e5e5e5;
        text-align:center;
    }
    
    #foster-tab-content table tr td{
        padding:5px;
        border:1px solid #e5e5e5;
        text-align:center;
    }
    
    #pledge_idea_btn{
        background:url("/mod/miigle_theme/graphics/btn-pledge.png") no-repeat scroll left top transparent;
        width:53px;
        height:20px;
        border:0 none;
        border-radius:0;
        -moz-border-radius:0;
        -webkit-border-radius:0;
        padding:0;
        margin:0;
        cursor:pointer;
    }
    #loan_idea_btn{
        background:url("/mod/miigle_theme/graphics/btn-loan.png") no-repeat scroll left top transparent;
        width:53px;
        height:20px;
        border:0 none;
        border-radius:0;
        -moz-border-radius:0;
        -webkit-border-radius:0;
        padding:0;
        margin:0;
        cursor:pointer;
    }
    
    input[type="button"].btn-foster-doc{
        background:url("<?php echo $vars["url"]?>mod/miigle_theme/graphics/btn-foster-doc.png") no-repeat scroll left top transparent;
        width:473px;
        height:34px;
        border:0 none;
        border-radius:0;
        -moz-border-radius:0;
        -webkit-border-radius:0;
        text-align:left;
        padding:0 0 0 40px;
        font-size:13px;
        font-weight:bold;
        color:#8e8e8e;
        margin:20px 0 0 0;
        cursor:pointer;
    }
    
    input#bid-amount-text-box{
        width:105px;
        height:24px;
        padding:10px 5px;
        border-radius:2px;
        border:1px solid #e4e4e4;
    }
    input#btn-place-bid{
        background:url('/mod/miigle_theme/graphics/btn-place-bid.png') scroll no-repeat left top transparent;
        border:0 none;
        border-radius:0;
        -moz-border-radius:0;
        -webkit-border-radius:0;
        width:159px;
        height:44px;
        vertical-align:top;
        margin-left:5px;
    }
    #pledge-container, #loan-container{
        margin:20px 0;
        padding:0;
        border-bottom:2px solid #ddd;
    }
    input#btn-place-pledge{
        background:url('/mod/miigle_theme/graphics/btn-pledge.png') scroll no-repeat left top transparent;
        border:0 none;
        border-radius:0;
        -moz-border-radius:0;
        -webkit-border-radius:0;
        width:159px;
        height:44px;
        vertical-align:top;
        margin-left:5px;
    }
    input#btn-place-loan{
        background:url('/mod/miigle_theme/graphics/btn-loan.png') scroll no-repeat left top transparent;
        border:0 none;
        border-radius:0;
        -moz-border-radius:0;
        -webkit-border-radius:0;
        width:159px;
        height:44px;
        vertical-align:top;
        margin-left:5px;
    }
    
    #foster-tab-content #investors-list{
        margin:20px 0 10px;
    }
    
    #foster-tab-content #investors-list h3{
        font-size:16px;
        font-weight:bold;
        padding:5px 0;
    }
    
    #foster-tab-content #investors-list .investor{
        float:left;
        margin: 0 10px 0;
    }
    
    #foster-tab-content #investors-list .investor.last{
        margin-right:0;
    }
    
    #cheer-tab-content #investors-list{
        margin:20px 0 10px;
    }
    
    #cheer-tab-content #investors-list h3{
        font-size:16px;
        font-weight:bold;
        padding:5px 0;
    }
    
    #cheer-tab-content #investors-list .investor{
        float:left;
        margin: 0 10px 0;
    }
    
    #cheer-tab-content #investors-list .investor.last{
        margin-right:0;
    }
    
    /************* PHOTOS / VIDEOS / DOCS ****************/
    
    #idea-photos-container{
        width:400px;
        margin:15px auto;
    }
    
    #idea-photos-container img{
        margin:0 auto;
        padding:0;
        width:400px;
    }
    
    #idea-photos-container .bx-next{
        right:-32px;
    }
    
    #idea-photos-container .bx-prev{
        left:-32px;
    }
    
    #idea-photos-box{
        padding:10px;
        background-color:#EDEDED;
    }
    
    .idea-photos-header{
        display:block;
        width:455px;
        height:25px;
    }
    
    #idea-photos-box .idea-photos-header h2{
        display:block;
        float:left;
        width:430px;
        text-align:center;
    }
    
    .idea-photos-header input#idea-photos-toggle-btn{
        float:right;
    }
    
    
    #idea-videos-container{
        width:400px;
        margin:15px auto;
    }
    
    #idea-videos-container img{
        margin:0 auto;
        padding:0;
        width:400px;
        height:300px;
    }
    
    #idea-videos-container .bx-next{
        right:-32px;
    }
    
    #idea-videos-container .bx-prev{
        left:-32px;
    }
    
    #idea-videos-box{
        margin-top:20px;
        padding:10px;
        background-color:#EDEDED;
    }
    
    .idea-videos-header{
        display:block;
        width:455px;
        height:25px;
    }
    
    #idea-videos-box .idea-videos-header h2{
        display:block;
        float:left;
        width:430px;
        text-align:center;
    }
    
    .idea-videos-header input#idea-videos-toggle-btn{
        float:right;
    }

    #idea-docs-box{
        margin-top:20px;
        padding:10px;
        background-color:#EDEDED;
    }
    
    #idea-docs-container{
        /*display:none;*/
        width:400px;
        margin:15px auto;
    }
    
    ul#idea-docs-list{
        display:block;
        background:none transparent;
        list-style-type:none;
    }
    
    .idea-docs-header{
        display:block;
        width:455px;
        height:25px;
    }
    
    #idea-docs-box .idea-docs-header h2{
        display:block;
        float:left;
        width:430px;
        text-align:center;
    }
    
    .idea-docs-header input#idea-docs-toggle-btn{
        float:right;
    }
    
    input[type="button"].btn-idea-doc{
        background:url("<?php echo $vars["url"]?>mod/miigle_theme/graphics/btn-idea-doc.png") no-repeat scroll left top transparent;
        width:400px;
        height:34px;
        border:0 none;
        border-radius:0;
        -moz-border-radius:0;
        -webkit-border-radius:0;
        text-align:left;
        padding:0 0 0 40px;
        font-size:13px;
        font-weight:bold;
        color:#8e8e8e;
        margin:0 0 10px 0;
        cursor:pointer;
    }

/********************* STATUS MSG BOX **********************/
#statusmsg-container{
    background:url("/mod/miigle_theme/graphics/bg-input-status.png") no-repeat scroll left top transparent;
    width:495px;
    height:39px;
    padding:0;
    margin:0;
}
#statusmsg-container input#statusmsg{
    float:left;
    width:412px;
    height:29px;
    padding:5px;
    border:0 none;
    color:#999;
    background:transparent;
    font-size:14px;
}
#statusmsg-container input#statusmsg.selected{
    color:#000;
}
#statusmsg-container input#btn-submit-status{
    float:right;
    width:67px;
    height:39px;
    border:0 none;
    border-radius:0;
    -moz-border-radius:0;
    -webkit-border-radius:0;
    background:transparent;
}

.idea-search-view-first-col{
    float:left;
    width:100px;
}

.idea-search-view-second-col{
    width:450px;
    float:right;
    font-size:11px;
    color:#222;
}

.idea-search-view-second-col a{
    color:#222;
    text-decoration:none;
}

#steps-indicators > li {
    list-style: none;
    display: inline;
}

#steps-carousel {
    padding-bottom: 0;
}

#steps-indicators {
    margin-top: 15px;
}

.character-counter {
    position: relative;
    top: 15px;
}

textarea {
    resize: vertical;
}

/* Use this for an input that is loading via Ajax or some such thing */
input.input-loading {
    background: url("/mod/miigle_theme/graphics/ajax-loader.gif") no-repeat right;
}

#startup .carousel-control {
    position: absolute;
    top: 90%;
    color: #777777;
}

.video-container {
    position: relative;
    padding-bottom: 56.25%;
    padding-top: 30px;
    height: 0;
    overflow: hidden;
}

.video-container iframe,  
.video-container object,  
.video-container embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

#beta-sidebar .input-group {
    margin-bottom: 5px;
}

#beta-sidebar label.error {
    display: block;
}

#social-buttons-div > div {
    margin-bottom: 5px;
}

#beta-sidebar .sidebar-header {
    text-align: center;
    font-weight: bold;
    margin-top: 5px;
    margin-bottom: 5px;
}

.orange-community {
    border-color: #E69138;
}
button.orange-community, label.orange-community {
    background-color: #E69138 !important;
}
.green-community {
    border-color: #93C47D;
}
button.green-community, label.green-community {
    background-color: #93C47D !important;
}
.dark-blue-community {
    border-color: #6FA8DC;
}
button.dark-blue-community, label.dark-blue-community {
    background-color: #6FA8DC !important;
}
.light-blue-community {
    border-color: #9FC5E8;
}
button.light-blue-community, label.light-blue-community {
    background-color: #9FC5E8 !important;
}
.pink-community {
    border-color: #D5A6BD;
}
button.pink-community, label.pink-community {
    background-color: #D5A6BD !important;
}
.yellow-community {
    border-color: #F1C232;
}
button.yellow-community, label.yellow-community {
    background-color: #F1C232 !important;
}

span.stars, span.stars span {
    vertical-align: top;
    display: inline-block;
    background: url("/mod/miigle_theme/graphics/stars.png") 0 -16px repeat-x;
    width: 80px;
    height: 16px;
}

span.stars span {
    background-position: 0 0;
}

.idea-image {
    max-width: 200px;
    max-width: 300px;
}
#contributors {
    background:#4CBBA6;
}
#contributors .whitebox-header,
#contributors .whitebox-footer {
  border:none;
}
#contributors .whitebox-header,
#contributors .whitebox-footer a {
    color:white;
}
#contributors .padding-bg {
    margin:10px;
    background:white;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
}
#contributors .whitebox-footer {
    text-align:center;
}
.btn-rate:hover .glyphicon {
    color:gold;
}
.btn-cheer:hover .glyphicon {
    color:red;
}
.btn-foster:hover .fa {
    color:green;
}
#pg-potential-contributors .bg-leftCol li div.active {
  background:white;
  color:#666;
}
#pg-potential-contributors .bg-leftCol li:last-child div {
  -webkit-border-bottom-right-radius: 5px;
  -webkit-border-bottom-left-radius: 5px;
  -moz-border-radius-bottomright: 5px;
  -moz-border-radius-bottomleft: 5px;
  border-bottom-right-radius: 5px;
  border-bottom-left-radius: 5px;
}
#pg-potential-contributors .potential-contributor {
  margin-bottom: 15px; 
  border-bottom: 1px solid #eee;
  padding-bottom: 10px;
}
#pg-potential-contributors .potential-contributor p {
  margin-bottom: 0; 
}
#pg-potential-contributors .potential-contributor a.tooltipped {
  color:#777;
  font-size:11px;
}
#pg-potential-contributors #location {
  color:#666;
}
#pg-potential-contributors #location.bg-leftCol .form-control {
  border-left:1px solid #ccc;
  color:#666;
}
#pg-potential-contributors #location .form-group {
  position:relative;
  margin-top:5px;
}
#pg-potential-contributors #location .clearfix.legend {
  border:1px solid #ccc;
  margin:10px 0;
}
#pg-potential-contributors #location .clearfix.legend:before {
  background:#ccc;
  content:'';
  height:20px;
  width:2px;
  position:absolute;
  left:0;
  margin-top:-10px;
}
#pg-potential-contributors #location .clearfix.legend:after {
  background:#ccc;
  content:'';
  height:20px;
  width:2px;
  position:absolute;
  right:0;
  margin-top:-10px;
}
#idea-edit-form .women-founders input {
  float:left;
}
#idea-edit-form .women-founders label {
  margin-left:20px;
}

/**************Social Graph*********************/

#view-social-graph .link {
    fill: none;
    stroke: #666;
    stroke-width: 1.5px;
}
#view-social-graph #licensing {
    fill: green;
}
#view-social-graph .link.licensing {
    stroke: green;
}
#view-social-graph .link.resolved {
    stroke-dasharray: 0,2 1;
}
#view-social-graph circle {
    fill: #ccc;
    stroke: #333;
    stroke-width: 1.5px;
}

#view-social-graph text {
    font: 10px sans-serif;
    pointer-events: none;
    text-shadow: 0 1px 0 #fff, 1px 0 0 #fff, 0 -1px 0 #fff, -1px 0 0 #fff;
}
#view-social-graph #loading-div {
  margin: 0;
  padding: 0;
}
#view-social-graph #loading-div.container {
  width: 200px;
  height: 200px;
  padding-top: 100px;
  margin: 0 auto;
}
#view-social-graph .ball {
  width: 10px;
  height: 10px;
  margin: 10px auto;
  border-radius: 50px;
  display: inline-block;
}    
#view-social-graph .ball:nth-child(1) {
  background: #ff005d;
  -webkit-animation: ball-right 1s infinite ease-in-out;
  -moz-animation: ball-right 1s infinite ease-in-out;
  animation: ball-right 1s infinite ease-in-out;    
}

#view-social-graph .ball:nth-child(2) {
  background: #35ff99;
  -webkit-animation: ball-left 1.1s infinite ease-in-out;
  -moz-animation: ball-left 1.1s infinite ease-in-out;
  animation: ball-left 1.1s infinite ease-in-out;
}

#view-social-graph .ball:nth-child(3) {
  background: #008597;
  -webkit-animation: ball-right 1.05s infinite ease-in-out;
  -moz-animation: ball-right 1.05s infinite ease-in-out;
  animation: ball-right 1.05s infinite ease-in-out;
}

#view-social-graph .ball:nth-child(4) {
  background: #ffcc00;
  -webkit-animation: ball-left 1.15s infinite ease-in-out;
  -moz-animation: ball-left 1.15s infinite ease-in-out;
  animation: ball-left 1.15s infinite ease-in-out;
 }

#view-social-graph .ball:nth-child(5) {
  background: #2d3443;  
  -webkit-animation: ball-right 1.1s infinite ease-in-out;
  -moz-animation: ball-right 1.1s infinite ease-in-out;
  animation: ball-right 1.1s infinite ease-in-out;
}

#view-social-graph .ball:nth-child(6) {
  background: #ff7c35;  
  -webkit-animation: ball-left 1.05s infinite ease-in-out;
  -moz-animation: ball-left 1.05s infinite ease-in-out;
  animation: ball-left 1.05s infinite ease-in-out;
 }

#view-social-graph .ball:nth-child(7) {
  background: #4d407c;  
  -webkit-animation: ball-right 1s infinite ease-in-out;
  -moz-animation: ball-right 1s infinite ease-in-out;
  animation: ball-right 1s infinite ease-in-out;
}


@-webkit-keyframes ball-right {
  0%   { -webkit-transform: translate(0, -15px);   }
  50%  { -webkit-transform: translate(0, 15px);    }
  100% { -webkit-transform: translate(0, -15px);   }
}

@-webkit-keyframes ball-left {
  0%   { -webkit-transform: translate(0, 15px);    }
  50%  { -webkit-transform: translate(0, -15px);   }
  100% { -webkit-transform: translate(0, 15px);    }
}

@-moz-keyframes ball-right {
  0%   { -moz-transform: translate(0, -15px);   }
  50%  { -moz-transform: translate(0, 15px);    }
  100% { -moz-transform: translate(0, -15px);   }
}

@-moz-keyframes ball-left {
  0%   { -moz-transform: translate(0, 15px);    }
  50%  { -moz-transform: translate(0, -15px);   }
  100% { -moz-transform: translate(0, 15px);    }
}

@keyframes ball-right {
  0%   { transform: translate(0, -15px);  }
  50%  { transform: translate(0, 15px);   }
  100% { transform: translate(0, -15px);  }
}

@keyframes ball-left {
  0%   { transform: translate(0, 15px);   }
  50%  { transform: translate(0, -15px);  }
  100% { transform: translate(0, 15px);   }
}

#view-social-graph #loading-table {
    width:100%;
    display:table;
}
#view-social-graph #loading-cell {
    width:100%;
    height:200px;
    display:table-cell;
    vertical-align:middle;
}
#view-social-graph #loading-balls {
    width:88px;
    margin:0 auto;
}