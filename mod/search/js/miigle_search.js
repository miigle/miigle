var miigle_search = angular.module('miigle.search', ['ngRoute']);

miigle_search.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider
      .when('/search/:searchtype/:category_int/:location/:query', {
        controller: 'SearchFeedController',
        templateUrl: '/mod/miigle_feed/templates/idea_feed.html',
        reloadOnSearch: true,
      });
  }
]);
