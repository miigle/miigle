<?php

//$query = "some search term"
//$search_type = 'all', users', 'ideas'
function MiigleSearch($query, $search_type, $offset=0, $category_int=0, $location='all', $persona='') {
    // @todo there is a bug in get_input that makes variables have slashes sometimes.
    // @todo is there an example query to demonstrate ^
    // XSS protection is more important that searching for HTML.
    $query = stripslashes($query);

    $LIMIT = 20;

    // set up search params
    $params = array(
        'query' => $query,
        'search_type' => $search_type,
        'category_int' => $category_int,
        'location' => $location,
        'persona' => $persona,

        'users_offset' => $offset,
        'ideas_offset' => $offset,
        'limit' => $LIMIT,
        'order' => 'desc',
        'count' => false,
    );

    //this is because...just you don't wanna know
    if ($search_type == 'all' && $offset > 0) {
        $previous_offset = $offset - (2 * $LIMIT);

        if ($previous_offset < 0) {
            $previous_offset = 0;
        }

        $params['count'] = true;
        $params['users_offset'] = $previous_offset;
        $params['ideas_offset'] = $previous_offset;

        $ideas_count = trigger_plugin_hook('search', 'object:idea', $params, NULL);
        $users_count = trigger_plugin_hook('search', 'object:miigle_user', $params, NULL);

        $params['count'] = false;
        $params['users_offset'] = $previous_offset + $users_count;
        $params['ideas_offset'] = $previous_offset + $ideas_count;
    }

    switch ($search_type) {
        case 'all':
            $results = array_merge(
                trigger_plugin_hook('search', 'object:miigle_user', $params, NULL),
                trigger_plugin_hook('search', 'object:idea', $params, NULL)
            );
            break;

        case 'users':
            $results = trigger_plugin_hook('search', 'object:miigle_user', $params, NULL);
            break;

        case 'ideas':
            $results = trigger_plugin_hook('search', 'object:idea', $params, NULL);
            break;

        default: 
            //this should never happen 
            $results = array();
            break;
    }

    if (!$results) {
        return array();
    }

    usort($results, function($a, $b) {
        return $a->time_updated < $b->time_updated;
    });
    $guid_sort_order = array_flip(array_map('GetObjectGUID', $results));

    $entity_dicts_list = array_map('DictifyEntityWithType', $results);
    $entities_dict = BuildDictFromList($entity_dicts_list, 'guid');

    uksort($entities_dict, function($a, $b) use ($guid_sort_order) {
        return $guid_sort_order[$a] > $guid_sort_order[$b];
    });

    return array_values($entities_dict);
}

expose_function('search', 'MiigleSearch',
    array(
        'query'        => array('type'=>'string', 'required'=>true),
        'search_type'  => array('type'=>'string', 'required'=>false, 'default'=>'all'),
        'offset'       => array('type'=>'int',    'required'=>false, 'default'=>0),
        'category'     => array('type'=>'int',    'required'=>false, 'default'=>0),
        'location'     => array('type'=>'string', 'required'=>false, 'default'=>'all'),
        'persona'      => array('type'=>'string', 'required'=>false, 'default'=>''),
    ),
    'Search Miigle',
    'GET',
    false,
    false
);
