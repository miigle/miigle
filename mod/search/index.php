<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/engine/start.php');

gatekeeper();

//get feed body
$body = elgg_view('miigle_search/view');

//get title
$title = elgg_echo('Search');

//draw page
page_draw($title, $body);