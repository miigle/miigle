<?php
/**
 * Index page for MiigleHomeFeed
 * 
 * @package MiigleHomeFeed
 */

// Get the Elgg engine
require_once(dirname(dirname(dirname(__FILE__))) . '/engine/start.php');

gatekeeper();

//get feed body
$body = elgg_view('miigle_feed/view');

//get title
$title = elgg_echo('Idea Feed');

//draw page
page_draw($title, $body);
