<?php
/**
 * Elgg MiigleHomeFeed plugin
 * 
 * @package MiigleHomeFeed
 */

global $CONFIG;
require_once($CONFIG->pluginspath . 'miigle_profile/api.php');
// Get the Sphinx Search API
require_once(dirname(__FILE__) . '/lib/sphinxapi.php');

// Expose API functions
require_once(dirname(__FILE__) . '/api.php');

function MiigleHomeFeedInit() {
    global $CONFIG;
    
    // Extend system CSS with our own styles, which are defined in the idea/css view
    elgg_extend_view('css', 'miigle_feed/css');
    
    //register page handler
    register_page_handler('ideafeed', 'MiigleFeedPageHandler');
    //register_page_handler('idea', 'MiigleFeedPageHandler');
    //register_page_handler('miigle', 'MiigleFeedPageHandler');
    
    // Replace the default index page
    register_elgg_event_handler('login','user','IdeaFeedLoginHandler');

}

//I have a feeling this isn't useful or even functional...
function IdeaFeedLoginHandler($hook, $type, $entity){
    return true;
}

function MiigleFeedPageHandler($page){
    elgg_extend_view('miigle_dependencies/header_js', 'miigle_feed/header_js');
    elgg_extend_view('miigle_dependencies/footer_js', 'miigle_feed/footer_js');
    return require_once('index.php');
}

        
register_elgg_event_handler('init', 'system', 'MiigleHomeFeedInit');

//make sure that we hear about entities being deleted, so we can delete
//any river depdencies if need be
register_elgg_event_handler('delete', 'object', 'DeleteRiverDependency');
