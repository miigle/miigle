<?php
    /**
     * Elgg MiigleHomeFeed CSS extender
     * 
     * @package MiigleHomeFeed
     */
?>
span.icon-action {
    display:block;
    margin-bottom:10px;
}

span.icon-action a {
    font-weight: normal;
    padding: 0 0 0 5px;
    color: #888888;
}
span.icon-action .active {
    font-weight:500;
    color: #000;
}

/*
span.icon-action button.glyphicon {
    font-size:24px;
    padding: 0 5px;
    margin-top: -16px;
    line-height: 2;
    color:#cecece;
}

.well span.icon-action button.glyphicon:hover {
    color:#2a6496;
}
*/

.bg-nopadding.grid-feed {
    background:none;
    border:none;
}
.bg-nopadding.grid-feed .page-header {
    background:white;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    border: 1px solid #cecece;
    border-bottom:none;
}
h1#feed a {
    font-size:12px;
}
#remaining-todos {
    background:#4CBBA6;
}
#remaining-todos .whitebox-header {
    color:white;
}
#remaining-todos .whitebox-header span {
    color:#ffe000;
}
#remaining-todos .completed-todo a {
    color:#999;
}
#remaining-todos i.fa {
    margin-right:5px;
}
#remaining-todos i.fa-circle-o {
    color:#d2322d;
}
#remaining-todos i.fa-check-circle-o {
    color:#4CBBA6;
}

.grid-feed .well {
  border-bottom:5px solid white !important;
}
.grid-row {
  padding-top:20px;
}
.grid-item .thumbnail-header {
  height:64px;
}
.grid-item .btn-default.btn-cheer {
  background-color:transparent;
}
.grid-item img {
  border-radius:5px;
  -webkit-border-radius:5px;
  -moz-border-radius:5px;
  -ms-border-radius:5px;
  -o-border-radius:5px;
}
.grid-item .flip-container {
  width: 100%;
}
.grid-item .flip-container .video-preview {
    width:255px;
    height:133px;
    background-size:100% auto;
    background-position:center center;
    text-align:center;
}
.grid-item .flip-container .back .video-preview {
    -webkit-filter: grayscale(100%) brightness(50%);
  -moz-filter: grayscale(100%) brightness(50%);
  -ms-filter: grayscale(100%) brightness(50%);
  -o-filter: grayscale(100%) brightness(50%);
  filter: grayscale(100%) brightness(50%);
  margin-left:-5px;
  margin-top:-3px;
}
.grid-item .front img.play {
  width:60px;
  margin-top:35px;
}
.grid-item .back .pitch {
  position:absolute;
  width:255px;
  height:133px;  
  top:0;
  left:0;
  padding:10px;
}
.grid-item .back .pitch button {
  position:absolute;
  bottom:10px;
  left:96px;
}

/*************************************
| Flip Cards (http://davidwalsh.name/css-flip)
**************************************/

/* entire container, keeps perspective */
.grid-item .flip-container {
	-webkit-perspective: 1000;
  perspective: 1000;
}
	/* flip the pane when hovered */
	.grid-item .flip-container:hover .flipper, .flip-container.hover .flipper {
		-webkit-transform: rotateY(180deg);
    transform: rotateY(180deg);
	}

.grid-item .front, .grid-item .back {
	width: 100%;
  height:135px;
}

/* flip speed goes here */
.grid-item .flipper {
	transition: 0.6s;
	transform-style: preserve-3d;
  -webkit-transition: 0.6s;
	-webkit-transform-style: preserve-3d;

	position: relative;
}

/* hide back of pane during swap */
.grid-item .front, .back {
	backface-visibility: hidden;
  -webkit-backface-visibility: hidden;

	position: absolute;
	top: 0;
	left: 0;
}

/* front pane, placed above back */
.grid-item .front {
	z-index: 2;
  text-align:center;
}

/* back, initially hidden pane */
.grid-item .back {
  text-align:center;
  position:relative;
  padding:5px;
	transform: rotateY(180deg);
  -webkit-transform: rotateY(180deg);
}
.grid-item .back p {
  color:white;
}
.grid-item .front p {
  padding-top:25px;
}