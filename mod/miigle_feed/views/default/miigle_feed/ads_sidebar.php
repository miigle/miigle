<div class="bg-rightCol" id="sponsored">
  <div class="page-header">
    <h4>Sponsored by</h4>
  </div>
  <div class="padding-bg">
    <img src="images/ad_03.jpg" alt="...">
    <p>Dropbox helps you access your files on any device, anywhere and anytime.<a class="more pull-right" href="#">Learn More</a></p>
  </div>
</div>

<div class="bg-rightCol" id="featured">
  <div class="page-header">
    <h4>Featured Startups</h4>
  </div>
  <div class="padding-ad">
    <img src="images/ad_19.jpg" alt="...">
    <p><a class="more" href="#">Try out Coda</a>Web Development done faster and easier!</p>
  </div>
  <div class="padding-ad">
    <img src="images/ad_29.jpg" alt="...">
    <p><a class="more" href="#">Tera to the Rescue!</a>Learn how to create phone apps in 7 days.</p>
  </div>
</div>

<div class="bg-rightCol" id="interests">
  <div class="page-header">
    <h4>Profile Interests</h4>
  </div>
  <div class="padding-bg">
    <p>Suggested user profiles for your to view.</p>
    <img src="images/avator1.jpg" alt="" class="avator">
    <img src="images/avator1.jpg" alt="" class="avator">
    <img src="images/avator1.jpg" alt="" class="avator">
    <img src="images/avator1.jpg" alt="" class="avator">
    <img src="images/avator1.jpg" alt="" class="avator">
    <img src="images/avator1.jpg" alt="" class="avator">
    <img src="images/avator1.jpg" alt="" class="avator">
    <img src="images/avator1.jpg" alt="" class="avator">
    <img src="images/avator1.jpg" alt="" class="avator">
    <img src="images/avator1.jpg" alt="" class="avator">
    <img src="images/avator1.jpg" alt="" class="avator">
    <img src="images/avator1.jpg" alt="" class="avator">
    <img src="images/avator1.jpg" alt="" class="avator">
    <img src="images/avator1.jpg" alt="" class="avator">
    <img src="images/avator1.jpg" alt="" class="avator">
    <p><a class="more pull-right" href="#">See More</a></p>
  </div>
</div>