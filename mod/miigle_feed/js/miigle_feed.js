angular.module('miigle.feed', ['infinite-scroll', 'miigle.video', 
    'miigle.share.email', 'miigle.share.twitter', 'miigle.profile.status_update', 
    'miigle.idea.view', 'ngSanitize', 'miigle.profile.employer', 
    'miigle_star_rating', 'miigle.rpc'])
    //for new feed functionality, add your own directive here
    .directive('idea', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_feed/templates/idea.html',
        };
    })
    .directive('ideaimage', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_feed/templates/image.html',
        };
    })
    .directive('stakeholder', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_feed/templates/stakeholder.html',
        };
    })
    .directive('ideavideo', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_feed/templates/video.html',
        };
    })
    .directive('feed', function() {
        return {
            restrict: 'E',
            transclude: true,
            replace: true,
            templateUrl: '/mod/miigle_feed/templates/feed.html',
        };
    })
    .directive('feeditem', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_feed/templates/feed_item.html',
            scope: {
                item: '=',
                feedType: '=',
            },
        };
    })
    /* Jesus Josh what in the fuck is this shit? */
    .directive('griditem', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_feed/templates/grid_item.html',
            scope: {
                item: '=',
                feedType: '=',
            },
            controller: function($scope) {
              $('#modal-video').on('hidden.bs.modal', function (e) {
                $('#modal-video .modal-body').html('');
              });
              $scope.toggleModalVideo = function(video_id, video_type) {
                var embed_src;
                switch (video_type) {
                    case 'youtube':
                        embed_src = '//www.youtube.com/embed/' + video_id;
                        break;

                    case 'vimeo':
                        embed_src = '//player.vimeo.com/video/' + video_id;
                        break;

                    default:
                        break;
                }
                var template = '<div class="video-container"><iframe src="'+embed_src+'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
                $('#modal-video .modal-body').html(template);
                $('#modal-video').modal('show');
              };
            },
        };
    })
    .directive('question', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_feed/templates/question.html',
            scope: {
                item: '=',
            },
            link: function($scope) {
                $scope.idea_question_community_class = window.GetExpertiseIntClass($scope.item.idea.question_community_int);
            },
        };
    })
    .directive('comment', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_feed/templates/comment.html',
            scope: {
                item: '=',
            },
        };
    })
    .directive('searchitem', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_feed/templates/search_item.html',
            scope: {
                item: '=',
            },
        };
    })
    .directive('user', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_feed/templates/user.html',
        };
    })
    .directive('status', function() {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            templateUrl: '/mod/miigle_feed/templates/status.html',
        };
    })
    .directive('idearating', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_feed/templates/rating.html',
        };
    })
    .factory('ShowAllCategories', function() {
        var show_all_categories = false;
        return {
            toggle: function() {
                show_all_categories = !show_all_categories;
            },
            get: function() { return show_all_categories; },
        };
    })
    .factory('ProfileInterestsFactory', ['RPCService', 
        function(RPCService) {
            var promise = RPCService.RPC({
                method: 'user.profile_interests',
            });

            return {
                promise: promise,
            };
        }
    ])
    .controller('FeedController', ['$routeParams', '$route', '$scope', 
        'ShowAllCategories', 'FeedFactory', 'ProfileInterestsFactory',
        function($routeParams, $route, $scope, ShowAllCategories, FeedFactory,
                 ProfileInterestsFactory) {
            var key;

            setTimeout(function() {
                var kill_me = $('#hack-me-please').clone().html();
                $('#social-buttons-div').append(kill_me);
                /*
                if (confirm('Is it time to finally kill myself?')) {
                    me.commit_suicide();
                }
                */
            }, 1000);

            $scope.profile_interests = [];
            ProfileInterestsFactory.promise.then(function(data) {
                $scope.profile_interests = data
                    .filter(function(user_dict) {
                        return user_dict.icon_guid !== 0;
                    });
            });

            $scope.showTooltip = function($event) {
                var $event_target = $(event.target);

                if ($event_target.is('div.col-md-3')) {
                    $event_target.find('img').tooltip('show');
                }
            };

            $scope.hideTooltip = function($event) {
                var $event_target = $(event.target);

                if ($event_target.is('div.col-md-3')) {
                    $event_target.find('img').tooltip('hide');
                }
            };

            $scope.category_int_filter = parseInt($routeParams.category_int, 10) || 0;
            $scope.location_filter = $routeParams.location || 'all';

            $scope.show_all_categories = ShowAllCategories.get();

            $scope.idea_location_counts_dict = miigle_feed.idea_location_counts_dict;
            $scope.idea_category_counts_dict = miigle_feed.idea_category_counts_dict;
            $scope.idea_categories_dict = Object.create(miigle_ideas.idea_categories);
            $scope.idea_categories_dict[-1] = 'Women Founders';

            //angular hurts me
            for (key in miigle_ideas.idea_categories) {
                $scope.idea_category_counts_dict[key] = $scope.idea_category_counts_dict[key] || 0;
            }

            $scope.toggleAllCategories = function($event) {
                ShowAllCategories.toggle();
                $scope.show_all_categories = ShowAllCategories.get();
                $event.preventDefault();
            };
        }
    ])
    .controller('SearchFeedController', function($routeParams, $route, $scope, FeedFactory, $controller) {
        $controller('FeedController', {$scope: $scope}); 

        $scope.root_route = 'search';
        $scope.feed_type = $routeParams.feedtype || 'all';
        $scope.search_term = $routeParams.query;

        $scope.feed_type_dict_list = [{route: 'all',   display: 'All',      loading_text: 'Gathering your search results'},
                                      {route: 'users', display: 'Profiles', loading_text: 'Gathering your search results'},
                                      {route: 'ideas', display: 'Ideas',    loading_text: 'Gathering your search results'}];
        $scope.feed_loading_text = $scope.feed_type_dict_list.filter(function(feed_type_dict) {
            return feed_type_dict.route == $scope.feed_type;
        })[0].loading_text;

        $scope.feed = FeedFactory.CreateSearchFeed($scope.feed_type, 
                                                   $scope.category_int_filter, 
                                                   $scope.location_filter,
                                                   $scope.search_term);
    })
    .controller('IdeaFeedController', function($routeParams, $route, $scope, FeedFactory, $controller) {
        $controller('FeedController', {$scope: $scope}); 
        $scope.root_route = $routeParams.root_route;
        $scope.feed_type = $routeParams.feedtype || 'interest';

        $scope.feed_type_dict_list = [
              //{route: 'new',       display: 'All',           loading_text: 'Loading recent feed activity'},
              {route: 'interest',  display: 'By Interest',   loading_text: 'Personalizing feed based on your interests'},
              {route: 'question',  display: 'Questions',     loading_text: 'Gathering questions'},
              {route: 'popular',   display: 'All (by popularity)', loading_text: 'Sorting feed based on popularity'},
        ];
        $scope.feed_loading_text = $scope.feed_type_dict_list.filter(function(feed_type_dict) { //Just kill me
            return feed_type_dict.route == $scope.feed_type;
        })[0].loading_text;

        $scope.feed = Object.create(FeedFactory.CreateIdeaFeed(
            ($scope.feed_type === 'question') ? 'popular' : $scope.feed_type,
            $scope.category_int_filter,
            $scope.location_filter
        ));

        if ($scope.root_route === 'grid' && $scope.feed_type === 'interest') {
          $scope.$watch('feed.items', function() {
            $scope.feed.items = $scope.feed.items.filter(function(feed_item) {
              return feed_item.action_type === 'user:create::idea';
            });
          }, true);
        }

        if ($scope.loggedin_user_dict.guid == 0) { 
            window.location.href = window.location.origin; //preventing non-loggedin scum from seeing the feed
        }

        $scope.todo = {
            create_bio: ($scope.loggedin_user_dict.bio.length > 0),
            select_interests: ($scope.loggedin_user_dict.interests_list.length > 0),
            //select_expertise: ($scope.loggedin_user_dict.work_info_dict.expertise_list.length > 0),
            profile_image: ($scope.loggedin_user_dict.icon_guid !== 0),
            has_personas: ($scope.loggedin_user_dict.personas_list.length > 0),
            post_idea: ($scope.loggedin_user_dict.ideas_list.length > 0),
        };
        $scope.remaining_todos = (Object
            .keys($scope.todo)
            .filter(function(todo) {
                return !$scope.todo[todo];
            })
            .length > 0);

        $scope.statusUpdateCallback = function() {
            $route.reload();
        };

        $(window).on('load', function() {
            $('.conditional-affix').each(function(i, element) {
                var $element = $(element);
                if ($element.height() + $element.offset().top < $(window).height()) {
                    $element.width($element.parent().width());
                    $element.affix();
                }
            });
        });
    })
    .controller('UserFeedController', function($scope, FeedFactory) {
        $scope.init = function(user_guid) {
            $scope.root_route = 'view';
            $scope.feed = FeedFactory.CreateUserFeed(user_guid);
        };
    })
    .factory('FeedFactory', function($http, $sce, $cacheFactory, $timeout) {
        var feed_cache = $cacheFactory('feedcache');
        var is_warming = false;
        var factory_functions;

        var BreakFeedCache = function() {
            feed_cache.removeAll();
            is_warming = false;
        };

        var WarmCache = function(callback) {
            if (!is_warming) {
                is_warming = true;
                //prewarm the cache
                //factory_functions.CreateIdeaFeed('new', 0, 'all');
                factory_functions.CreateIdeaFeed('interest', 0, 'all');
                factory_functions.CreateIdeaFeed('popular', 0, 'all');
            }
        };

        var ExtractURLs = function(string) {
            var regex = /(\b(https?|ftp|file):\/\/[\-A-Z0-9+&@#\/%?=~_|!:,.;]*[\-A-Z0-9+&@#\/%=~_|])/ig;
            return string.match(regex) || [];
        };

        var GetFeed = function(rest_args_dict) {
            var rest_args_dict_string = Object
                .keys(rest_args_dict)
                .sort()
                .map(function(key) { return key + ':' + rest_args_dict[key]; })
                .join(':');
            var feed;

            if (feed_cache.get(rest_args_dict_string) === undefined) {
                feed = new Feed(rest_args_dict);
                feed_cache.put(rest_args_dict_string, feed);
            }

            return feed_cache.get(rest_args_dict_string);
        };

        var Feed = function(rest_args_dict) {
            this.items = [];
            this.busy = false;
            this.stopped = false;
            this.params_dict = {
                offset: 0,
            };
            this.feed_type = (rest_args_dict.query !== undefined) ? 'search' : 'view';

            $.each(rest_args_dict, function(key, value) {
                this.params_dict[key] = value;
            }.bind(this));

            this.nextPage(); //cache first 20 results
        };

        Feed.prototype.transformFeedItem = function(feed_item) {
            var i;
            var key;
            var rating_sum;
            var video_guid, stakeholder_guid, stakeholder;
            var action_type = feed_item.action_type;
            var action_parts;
            var plural;
            var idea_type_string;
            var object_type, subject_type;
            var subject_entity = miigle.getEntity(feed_item.subject_guid);
            var object_entity = miigle.getEntity(feed_item.object_guid);
            var status_urls;

            if (subject_entity === undefined || object_entity === undefined) {
                //oh no we are in trouble
                console.log('oh no');
            }

            action_parts = action_type.split(':');
            subject_type = action_parts[0];
            object_type = action_parts[3];

            //default empty dicts
            feed_item.subject = {
                name: '',
                url: '/',
                icon_url: '/pg/image/0',
            };
            feed_item.object = {
                name: '',
                url: '/',
            };

            //as we put more objects and subjects in the feed,
            //add cases to these switches
            switch (subject_type) {
                case 'user':
                    feed_item.subject.name = subject_entity.first_name + ' ' + subject_entity.last_name;
                    feed_item.subject.url = '#/user/' + subject_entity.username;
                    feed_item.subject.icon_url = '/pg/image/' + subject_entity.icon_guid;
                    break;

                default:
                    break;
            }

            switch (object_type) {
                case 'idea':
                    feed_item.object.name = object_entity.title;
                    feed_item.object.url = '#/idea/' + object_entity.guid;
                    break;  

                case 'user':
                    feed_item.object.name = object_entity.first_name + ' ' + object_entity.last_name;
                    feed_item.object.url = '#/user/' + object_entity.username;
                    feed_item.object.icon_url = '/pg/image/' + object_entity.icon_guid;
                    feed_item.object.work_info_dict = object_entity.work_info_dict;
                    break;

                case 'status':
                    break;

                default:
                    console.error('Unknown object_type', object_type);
                    break;
            }

            //seconds to milliseconds
            feed_item.posted *= 1000;

            if (feed_item.action_payload.length > 1) {
                plural = true;
            } else {
                plural = false;
            }

            //as we get more action types, fill out more cases in this switch
            switch (action_type) {
                case 'user:add:image:idea':
                    feed_item.directive = 'image';
                    if (plural) {
                        feed_item.action_string = 'added images for';
                    } else {
                        feed_item.action_string = 'added an image for';
                    }
                    break;

                case 'user:add:video:idea':
                    feed_item.directive = 'video';
                    if (plural) {
                        feed_item.action_string = 'added videos for';
                    } else {
                        feed_item.action_string = 'added a video for';
                    }

                    //feed_item.video_dicts gets used by the <ideavideo> directive
                    feed_item.video_dicts = feed_item.action_payload.map(function(video_guid) {
                        return miigle.getEntity(video_guid);
                    });
                    break;

                case 'user:add:stakeholder:idea':
                    feed_item.directive = 'stakeholder';
                    if (plural) {
                        feed_item.action_string = 'added stakeholders for';
                    } else {
                        feed_item.action_string = 'added a stakeholder for';
                    }

                    feed_item.stakeholder_dicts = [];
                    $.each(feed_item.action_payload, function(i, stakeholder_guid) {
                        var url;
                        var stakeholder = miigle.getEntity(stakeholder_guid);

                        if (stakeholder.url !== '') {
                            url = stakeholder.url;
                        } else {
                            url = '#/idea/' + feed_item.object_guid;
                        }

                        //feed_item.stakeholder_dicts gets used by the <stakeholder> directive
                        feed_item.stakeholder_dicts.push({
                            name: stakeholder.first_name + ' ' + stakeholder.last_name,
                            role: stakeholder.role,
                            icon_guid: stakeholder.icon_guid,
                            url: url,
                        });
                    }.bind(this));
                    break;

                //feed_item.idea gets used by the <idea> directive
                case 'user:update:icon:idea':
                    feed_item.directive = 'icon';
                    feed_item.action_string = 'updated the icon for';
                    feed_item.idea = {};
                    feed_item.idea.pitch = object_entity.pitch;
                    feed_item.idea.icon_guid = object_entity.icon_guid;
                    break;

                case 'user:update::idea':
                    feed_item.directive = 'idea';
                    feed_item.action_string = 'updated';
                    feed_item.idea = {};
                    feed_item.idea.pitch = object_entity.pitch;
                    feed_item.idea.icon_guid = object_entity.icon_guid;
                    break;

                case 'user:create::idea':
                    idea_type_string = miigle_ideas.idea_types[object_entity.type_int];
                    feed_item.directive = 'idea';
                    feed_item.action_string = 'created';

                    if (feed_item.id > 0) { //for the popular feed we give id=0 for this
                        feed_item.action_string += ' a new ' + idea_type_string.toLowerCase();

                    }

                    feed_item.idea = object_entity;
                    break;

                case 'user:follow::idea':
                    feed_item.directive = 'idea';
                    feed_item.action_string = 'is following';
                    feed_item.idea = {};
                    feed_item.idea.pitch = object_entity.pitch;
                    feed_item.idea.icon_guid = object_entity.icon_guid;
                    break;

                case 'user:follow::user':
                    feed_item.directive = 'user';
                    feed_item.action_string = 'is following';
                    break;

                case 'user:cheer::idea':
                    feed_item.directive = 'idea';
                    feed_item.action_string = 'cheered for';
                    feed_item.idea = {};
                    feed_item.idea.pitch = object_entity.pitch;
                    feed_item.idea.icon_guid = object_entity.icon_guid;
                    break;

                case 'user:rate::idea':
                    feed_item.directive = 'idearating';
                    feed_item.action_string = 'rated';
                    feed_item.idea = {};
                    feed_item.idea.pitch = object_entity.pitch;
                    feed_item.idea.icon_guid = object_entity.icon_guid;
                    feed_item.rating_dict = miigle.getEntity(feed_item.action_payload).rating_dict;
                    //kill me please
                    rating_sum = Object.keys(feed_item.rating_dict).reduce(function(sum, key) {
                        return (sum + feed_item.rating_dict[key]);
                    }, 0);
                    feed_item.num_stars = 5 * rating_sum / (Object.keys(feed_item.rating_dict).length * 10);
                    break;

                case 'user:update::status':
                    feed_item.directive = 'status';
                    feed_item.action_string = 'updated their status';
                    status_urls = ExtractURLs(object_entity.status);
                    feed_item.video_dicts = [];
                    feed_item.status = object_entity.status;

                    status_urls.forEach(function(url) {
                        if (IsVideoURLValid(url)) {
                            feed_item.video_dicts.push(GenerateVideoDictFromVideoURL(url));
                        }

                        feed_item.status = feed_item.status.replace(url, 
                            '<a href="' + url + '">' + url + '</a>');
                    });
                    break;

                case 'user:comment::idea':
                    feed_item.directive = 'comment';
                    feed_item.action_string = 'left a comment on';
                    feed_item.comment = feed_item.action_payload
                        .map(function(guid) {
                            return miigle.getEntity(guid);
                        })
                        .pop()
                        .message;
                    break;

                default:
                    console.log('default');
                    console.log(action_type);
                    break;
            }


            return feed_item;
        };

        Feed.prototype.nextPage = function() {
            if (this.busy || this.stopped) return;
            this.busy = true;

            $http({
                method: 'GET',
                url: '/services/api/rest/json',
                params: this.params_dict,
                cache: false,
                responseType: 'json',
            })
            .success(function(data, status, headers, config) {
                var feed_items;
                var entities;
                
                WarmCache();
                /*
                data.result contains an entities object that maps guid->entity_dict
                and a feed_items list of feed_item_dicts. the feed_item dicts reference
                entity GUIDs that are in result.entities.
                we send that junk off to transformFeedItem to get what want out of the 
                entities_dict into the feed_item for displaying in the template later.
                */
                if (data.status !== 0) {
                    alert('error in getting feed');
                    this.busy = false;
                    this.stopped = true;
                } else {
                    if (this.feed_type === 'view') { 
                        feed_items = data.result.feed_items;
                        entities = data.result.entities;

                        $.each(entities, function(guid, entity) {
                            miigle.setDefaultEntity(guid, entity);
                        });

                        //transform the feed items and store them
                        $.each(feed_items, function(i, feed_item) {
                            feed_item = this.transformFeedItem(feed_item);
                            this.items.push(feed_item);
                        }.bind(this));
                    } else { //search
                        feed_items = data.result; //it's a list here to preserve search order
                        $.each(feed_items, function(i, entity) {
                            var guid = entity.guid;
                            if (entity.type == 'object:idea') {
                                entity.idea_type = miigle_ideas.idea_types[entity.type_int];
                            }
                            miigle.setDefaultEntity(guid, entity);
                            this.items.push(entity);
                        }.bind(this));
                    }

                    this.params_dict.offset = this.items.length;
                    this.busy = false;
                    this.stopped = feed_items.length == 0;
                }
            }.bind(this))
            ;
        };

        factory_functions = {
            CreateUserFeed: function(user_guid) {
                return GetFeed({
                    method: 'feed.get.new',
                    subject_guid: user_guid,
                });
            },
            CreateIdeaFeed: function(feed_type, category_int, location) {
                return GetFeed({
                    method: 'feed.get.' + feed_type,
                    category: category_int,
                    location: location,
                });
            },
            CreateSearchFeed: function(feed_type, category_int, location, query) {
                return GetFeed({
                    query: query,
                    method: 'search',
                    search_type: feed_type,
                    category: category_int,
                    location: location,
                });
            },
            BreakFeedCache: BreakFeedCache,
            WarmCache: WarmCache,
        };

        return factory_functions;
    })
    .config(['$routeProvider',
        function($routeProvider) {
            $routeProvider
                .when('/ideafeed/:root_route/:feedtype/:category_int/:location', {
                    controller: 'IdeaFeedController',
                    templateUrl: '/mod/miigle_feed/templates/idea_feed.html',
                    reloadOnSearch: true,
                })
                .when('/ideafeed/:root_route', {
                    redirectTo: '/ideafeed/view/interest/0/all',
                })
                .when('/ideafeed/search/:feedtype/:category_int/:location/:query', {
                    controller: 'SearchFeedController',
                    templateUrl: '/mod/miigle_feed/templates/idea_feed.html',
                    reloadOnSearch: true,
                })
                .when('/ideafeed/search', {
                    redirectTo: '/ideafeed/search/all/0/all',
                })
                .when('/ideafeed', {
                    redirectTo: '/ideafeed/view/interest/0/all',
                });
        }
    ]);
