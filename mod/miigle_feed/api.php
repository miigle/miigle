<?php

/**
 * Elgg Follow API
 * 
 * @package MiigleHomeFeed
 */

//a lot of this crap was written by john.pavlick@miigle.com - email me for help

function ObjectToArray($object) {
    return (array)$object;
}
 
//get the river item's annotation's action_payload,
//intval() some stuff, remove some stuff we don't need
//and finally get it out of stdClass and into a dict.
function TransformRiverItem($river_item) {
    $annotation = get_annotation($river_item->annotation_id);
    $river_item->action_payload = json_decode($annotation->value, true);

    $river_item->id = intval($river_item->id);
    $river_item->posted = intval($river_item->posted);
    $river_item->subject_guid = intval($river_item->subject_guid);
    $river_item->object_guid = intval($river_item->object_guid);
    unset($river_item->type);
    unset($river_item->subtype);
    unset($river_item->view);
    unset($river_item->access_id);
    unset($river_item->annotation_id);

    return ObjectToArray($river_item);
}

/* returns a list of GUIDs that this feed_item relies on */
function GetFeedItemGUIDs($feed_item_dict) {
    return array_merge(
        $feed_item_dict['action_payload'], 
        array($feed_item_dict['object_guid'], $feed_item_dict['subject_guid'])
    );
}

function DictifyEntityWithType($entity) {
    $dict = array();
    $dict['type'] = sprintf('%s:%s', $entity->getType(), $entity->getSubtype());
    return array_merge($dict, DictifyEntity($entity));
}

//idk where to put this either. We should probably
//have this centralized somewhere, but for now it's OK here
//since it's only used here.
function DictifyEntity($entity) {
    if ($entity->getType() == 'user' && $entity->getSubtype() != 'miigle_user') {
        $user = new MiigleUser($entity);
        $dict = $user->toDict();
        return $dict;
    } else if ($entity->getSubtype() == 'file') {
        return array('guid' => $entity->getGUID());
    } else if ($entity) {
        return $entity->toDict();
    } else {
        return array();
    }
}

//DictifyEntity($your_mom)


function GetFeedItemObjectGuid($feed_item) {
    return $feed_item['object_guid'];
}

function GenerateObjectValueComparator($field_name, $value) {
    return function($object) use ($field_name, $value) {
        return $object->__get($field_name) == $value;
    };
}

function GenerateEntityInArrayPartial($field_name, $array) {
    return function($entity) use ($array, $field_name) {
        return in_array($entity->__get($field_name), $array);
    };
}

function GetObjectGUID($object) {
    switch (gettype($object)) {
        case 'object':
            return intval($object->guid);

        case 'array':
            return intval($object['guid']);

        default:
            return 0;
    }
}

function GenerateFeedItemFilterFunction($valid_object_guids_list) {
    return function($feed_item) use ($valid_object_guids_list) {
        return in_array($feed_item['object_guid'], $valid_object_guids_list);
    };
}

function FlattenArray($array) { //$array= array of arrays. only works for 2d
    return array_reduce($array, 'array_merge', array());
}

function BuildDictFromList($dict_list, $key) {
    return array_reduce(
        $dict_list,
        function($dict, $dict_list_item) use ($key) {
            $dict[$dict_list_item[$key]] = $dict_list_item;
            return $dict;
        },
        array());
}

function MultipleFilterEntities($object_entities, $category_ints_list=array(0), 
                                $locations_list=array('all')) {
    if ($category_ints_list != array(0)) {
        if (in_array(-1, $category_ints_list)) {
            $object_entities = array_filter($object_entities, function($entity) {
                return json_decode($entity->women_founders);
            });
        } else {
            $category_int_filter_function = GenerateEntityInArrayPartial(
                    'category_int', $category_ints_list);
            $object_entities = array_filter($object_entities, 
                    $category_int_filter_function);
        }
    }
    if ($locations_list != array('all')) {
        $location_filter_function = GenerateEntityInArrayPartial(
                'cia_map_reference', $locations_list);
        $object_entities = array_filter($object_entities, 
                $location_filter_function);
    }

    return $object_entities;
}

function FilterEntities($object_entities, $category_int=0, $location='all') {
    return MultipleFilterEntities($object_entities, array($category_int), 
            array($location));
}

function MultipleFilterFeedItems($feed_items_list, $category_ints_list=array(0), 
                                 $locations_list=array('all')) {
    $object_guids_list = array_map('GetFeedItemObjectGuid', $feed_items_list);
    $object_entities = array_map('get_entity', $object_guids_list);

    $object_entities = MultipleFilterEntities($object_entities, 
            $category_ints_list, $locations_list);

    $filtered_guids_list = array_unique(
            array_map('GetObjectGUID', $object_entities));
    $feed_item_filter_function = GenerateFeedItemFilterFunction(
            $filtered_guids_list);
    $filtered_feed_items = array_filter($feed_items_list, 
            $feed_item_filter_function);

    return $filtered_feed_items;
}

function FilterFeedItemsByFollowing($feed_items_list, $following_guids_list) {
    return array_filter($feed_items_list, 
        function($feed_item) use ($following_guids_list) {
            return (in_array($feed_item['subject_guid'], $following_guids_list) || 
                    in_array($feed_item['object_guid'],  $following_guids_list));
        });
}

function FilterFeedItemsByCreation($feed_items_list) {
    return array_filter($feed_items_list, function($feed_item) {
        return $feed_item['action_type'] == 'user:create::idea';
    });
}

function FilterFeedItems($feed_items_list, $category_int=0, $location='all') {
    return MultipleFilterFeedItems($feed_items_list, array($category_int), 
            array($location));
}

function IsMiigleIdea($entity) {
    return $entity instanceof MiigleIdea;
}

function AggregateFeedItemsDependencies($feed_items_list) { //gets all the entities needed for the feed items
    $start_time = microtime(true);
    $guid_lists_list = array_map('GetFeedItemGUIDs', $feed_items_list);
    $guids_list = array_unique(FlattenArray($guid_lists_list));
    $entity_list = array_filter(array_map('get_entity', $guids_list), 'boolval');
    //$idea_entities = array_filter($entity_list, 'IsMiigleIdea');
    $entity_dict_list = array_map('DictifyEntity', $entity_list);
    $entities_dict = BuildDictFromList($entity_dict_list, 'guid');
    $end_time = microtime(true);
    $elapsed_time = $end_time - $start_time;
    //error_log("$elapsed_time in aggregate");

    return $entities_dict;
}

function FilterDuplicateFeedItems($feed_items_list) {
    foreach ($feed_items_list as $i=>$feed_item) {
        if ($i > 0 &&
            $previous_feed_item['action_type'] === $feed_item['action_type'] &&
            $previous_feed_item['object_guid'] === $feed_item['object_guid'] &&
            $previous_feed_item['subject_guid'] === $feed_item['subject_guid']) {
                $feed_items_list[$i] = false;
        }

        $previous_feed_item = $feed_item;
    }

    return array_filter($feed_items_list);
}

//get the river items, transform them, get the entity GUIDs they rely on,
//and make one list and one dict. $feed_items is a list of feed items
//$guids_dict maps entity_guid->$entity->toDict()
//oh this is horrible. oh god this will not scale.
//oh jesus this is horrible!
function GetNewestFeedEntriesMultipleFilters($subject_guid=0, $object_guid=0, 
        $offset=0, $category_ints_list=array(0), $locations_list=array('all'), 
        $following=false, $only_idea_create=false) {

    //the hacks continue...
    $start_time = microtime(true);
    $user = MiigleUser::GetUserFromGUID(get_loggedin_userid());
    $following_list = array_merge($user['following_list'], $user['cheering_list']);
    $limit = 30;
    $river_offset = 0;
    $filtered_feed_items = array();
    do {
        $action_string = $only_idea_create ? 'user:create::idea' : '';
        $river_items = get_river_items($subject_guid, $object_guid, '', '', '',
                $action_string, $limit, $river_offset);
        $guids_dict = array();

        if ($river_items) {
            $feed_items = array_map('TransformRiverItem', $river_items);
            $feed_items_no_duplicates = FilterDuplicateFeedItems($feed_items);

            $temp_filtered_feed_items = MultipleFilterFeedItems(
                    $feed_items_no_duplicates, $category_ints_list, 
                    $locations_list);

            if ($following) {
                $following_feed_items = FilterFeedItemsByFollowing(
                        $feed_items, $following_list);
            } else {
                $following_feed_items = array();
            }

            $following_feed_items_dict = BuildDictFromList(
                    $following_feed_items, 'id');
            $temp_filtered_feed_items_dict = BuildDictFromList(
                    $temp_filtered_feed_items, 'id');
            $all_feed_items_dict = ($following_feed_items_dict + 
                                    $temp_filtered_feed_items_dict);

            $sorted_feed_items = array_values($all_feed_items_dict);
            usort($sorted_feed_items, function($a, $b) {
                return $a['id'] < $b['id'];
            });

            $filtered_feed_items = array_merge($filtered_feed_items, 
                                               $sorted_feed_items);
        }

        $river_offset += sizeof($river_items);
    }
    while ($river_items && sizeof($river_items) > 0 && 
           sizeof($filtered_feed_items) < $offset + $limit);

    $filtered_feed_items = array_splice($filtered_feed_items, $offset);
    $guids_dict = AggregateFeedItemsDependencies($filtered_feed_items);

    $end_time = microtime(true);
    $elapsed = $end_time - $start_time;

    //error_log("$elapsed GetNewestFeedEntriesMultipleFilters");

    return array(
        'feed_items'=> $filtered_feed_items,
        'entities'  => $guids_dict,
    );

}
function GetNewestFeedEntries($subject_guid=0, $object_guid=0, $offset=0, 
                              $category_int=0, $location='all') {
    return GetNewestFeedEntriesMultipleFilters($subject_guid, $object_guid, 
            $offset, array($category_int), array($location));
}
expose_function('feed.get.new', 'GetNewestFeedEntries',
    array(
        'subject_guid' => array('type'=>'int', 'required'=>false, 'default'=>0),
        'object_guid' => array('type'=>'int', 'required'=>false, 'default'=>0),
        'offset' => array('type'=>'int', 'required'=>false, 'default'=>0),
        'category' => array('type'=>'int', 'required'=>false, 'default'=>0),
        'location' => array('type'=>'string', 'required'=>false, 'default'=>'all'),
    ),
    'Get the new feed',
    'GET',
    false,
    false
);

function GetFollowingFeedEntries($subject_guid=0, $object_guid=0, $offset=0, 
                                 $category_int=0, $location='all') {
     return GetNewestFeedEntriesMultipleFilters($subject_guid, $object_guid, 
             $offset, array($category_int), array($location), true);
}
expose_function('feed.get.following', 'GetFollowingFeedEntries',
    array(
        'subject_guid' => array('type'=>'int', 'required'=>false, 'default'=>0),
        'object_guid' => array('type'=>'int', 'required'=>false, 'default'=>0),
        'offset' => array('type'=>'int', 'required'=>false, 'default'=>0),
        'category' => array('type'=>'int', 'required'=>false, 'default'=>0),
        'location' => array('type'=>'string', 'required'=>false, 'default'=>'all'),
    ),
    'Get the new feed',
    'GET',
    false,
    false
);

function GetPopularFeedEntries($offset=0, $category_int=0, $location='all') {
    global $CONFIG;
    $guids_dict = array();
    $filtered_feed_items = array();
    $popular_query = "SELECT owner_guid AS subject_guid,
                             guid_two AS object_guid, 
                             COUNT(guid_two) AS cheer_count,
                             {$CONFIG->dbprefix}entities.time_created AS posted
                             FROM {$CONFIG->dbprefix}entity_relationships AS r
                             INNER JOIN {$CONFIG->dbprefix}entities ON r.guid_two=guid
                             WHERE r.relationship='cheering_idea' 
                             GROUP BY r.guid_two
                             ORDER BY cheer_count DESC
                             LIMIT 20
                             OFFSET $offset;";

    $popular_query = "SELECT e.owner_guid AS subject_guid,
                             e.guid AS object_guid,
                             ms2.string AS cheer_count,
                             e.time_created AS posted
                             FROM {$CONFIG->dbprefix}metastrings ms1 
                             INNER JOIN {$CONFIG->dbprefix}metadata md1 ON ms1.id=md1.name_id
                             INNER JOIN {$CONFIG->dbprefix}metastrings ms2 ON md1.value_id=ms2.id
                             INNER JOIN {$CONFIG->dbprefix}entities e ON md1.entity_guid=e.guid
                             WHERE ms1.string='cheer_count'
                             ORDER BY cheer_count DESC
                             LIMIT 20
                             OFFSET $offset
                             ;";


    $query_results = get_data($popular_query, 'ObjectToArray');

    if ($query_results) {
        $feed_items_dict_list = array_map(
            function($feed_item_dict) {
                foreach ($feed_item_dict as $key=>$value) { //these all come back as strings for some reason
                    $feed_item_dict[$key] = intval($value);
                }
                $feed_item_dict['id'] = 0;
                $feed_item_dict['action_type'] = 'user:create::idea';
                $feed_item_dict['action_payload'] = array();

                return $feed_item_dict;
            }, 
            $query_results);

        $filtered_feed_items = FilterFeedItems($feed_items_dict_list, 
                $category_int, $location);
        $guids_dict = AggregateFeedItemsDependencies($filtered_feed_items);
    }

    return array(
        'feed_items'=> $filtered_feed_items,
        'entities'  => $guids_dict,
    );
}
expose_function('feed.get.popular', 'GetPopularFeedEntries',
    array(
        'offset' => array('type'=>'int', 'required'=>false, 'default'=>0),
        'category' => array('type'=>'int', 'required'=>false, 'default'=>0),
        'location' => array('type'=>'string', 'required'=>false, 'default'=>'all'),
    ),
    'Get the new feed',
    'GET',
    false,
    false
);

function GetInterestFeedEntries($offset=0, $category_ints_list=array(0), 
                                $location='all') {
    $feed_items_and_entities_dict = GetNewestFeedEntriesMultipleFilters(0, 0, 
            $offset, $category_ints_list, array($location), $following=true);
    $feed_items = $feed_items_and_entities_dict['feed_items'];

    /* I think Luc wants this functionality
    $feed_items = array_filter($feed_items_and_entities_dict['feed_items'], 
            function($feed_item) {
                $entity = get_entity($feed_item['object_guid']);
                return get_loggedin_userid() != $entity->owner_guid;
            });
    */

    $feed_items_and_entities_dict['feed_items'] = $feed_items;
    return $feed_items_and_entities_dict;
}
function GetInterestFeedEntriesProxy($offset=0, $location='all', $category=0) {
    $user_guid = get_loggedin_userid();
    $user_dict = MiigleUser::GetUserFromGUID($user_guid);

    if ($category == 0) {
        $category = $user_dict['interests_list'];
    } else {
        $category = array($category);
    }

    return GetInterestFeedEntries($offset, $category, $location);
}
expose_function('feed.get.interest', 'GetInterestFeedEntriesProxy',
    array(
        'offset' => array('type' => 'int', 'required'=>false, 'default'=>0),
        'location' => array('type'=>'string', 'required'=>false, 'default'=>'all'),
        'category' => array('type'=>'int', 'required'=>false, 'default'=>0),
    ),
    'Get the interest feed',
    'GET',
    false,
    false
);

/**
 * @param int    $subject_guid
 * @param string $action
 * @param string $action_payload - has to be a json list of guids
 * @param int    $object_guid
 */
function CreateFeedItem($subject_guid, $action, $action_payload, $object_guid) {
    $annotation_name = 'feed_annotation';
    $annotation_id = create_annotation($object_guid, $annotation_name, 
            $action_payload, 'string', $subject_guid, ACCESS_PUBLIC);

    if ($annotation_id) {
        //first argument doesn't matter as long as it is a valid view. it will not be used.
        $river_id = add_to_river('object/thewire', $action, $subject_guid, 
                $object_guid, '', time(), $annotation_id);        

        $entity_guids = json_decode($action_payload, true); //ElggFiles or something
        foreach ($entity_guids as $entity_guid) {
            InitializeDependentRiverIdTracking($entity_guid);
            AddDependentRiverIdToEntity($river_id, $entity_guid);
        }

        return true;
    } else {
        return false;
    }
}

function InitializeDependentRiverIdTracking($entity_guid) {
    $entity = get_entity($entity_guid);
    $river_ids = $entity->getPrivateSetting('dependent_river_ids');
    if ($river_ids === false) {
        $entity->setPrivateSetting('dependent_river_ids', json_encode(array()));
    }
}

function AddDependentRiverIdToEntity($river_id, $entity_guid) {
    $entity = get_entity($entity_guid);
    $json_list = $entity->getPrivateSetting('dependent_river_ids');

    $river_ids = json_decode($json_list, true);
    $river_ids[] = $river_id;
    $json_list = json_encode($river_ids);

    $entity->setPrivateSetting('dependent_river_ids', $json_list);
}


/*
Here we want to delete $entity. But, we need to make sure $entity
isn't in any feed entries. So, we delete it from any feed entries that it's in.
Loop over the dependent_river_ids, then look in each of their annotations.
The annotation->value is a json list string of entity guids. One of which will
be $entity->guid. Remove it from the list. If the list is empty, that means the 
feed item should be removed. If so, delete the annotation and the river item.
*/
function DeleteRiverDependency($event, $entity_type, $entity) {
    global $CONFIG;


    $dependent_river_ids_string = $entity->getPrivateSetting('dependent_river_ids');

    if ($dependent_river_ids_string !== false) {
        $dependent_river_ids = json_decode($dependent_river_ids_string, true);
        foreach ($dependent_river_ids as $river_id) {
            //fuck you elgg why do I need to do this?!
            if ($river_id) {
                $sql = "SELECT * from {$CONFIG->dbprefix}river where id=$river_id";
                $river_entries = get_data($sql);

                //this should be just one here...but whatever. loop it once
                foreach ($river_entries as $river_entry) {
                    $annotation_id = $river_entry->annotation_id;
                    $annotation = get_annotation($annotation_id);

                    $feed_entity_guids_list = json_decode($annotation->value, true);

                    $index = array_search($entity->guid, $feed_entity_guids_list);
                    if ($index !== false) {
                        array_splice($feed_entity_guids_list, $index, 1);
                        update_annotation($annotation_id, 
                                          $annotation->name, 
                                          json_encode($feed_entity_guids_list), 
                                          'text', 
                                          $annotation->owner_guid, 
                                          $annotation->access_id);
                    }

                    if (sizeof($feed_entity_guids_list) == 0) {
                        delete_annotation($river_entry->annotation_id);
                        remove_from_river_by_annotation($river_entry->annotation_id);
                    }
                }
            }
        }
    }

    return true;
}
