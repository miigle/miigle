<?php

	/**
	 * Elgg Ideas plugin
	 * 
	 * @package ElggIdeas
	 */

	/**
	 * initialisation
	 *
	 * These parameters are required for the event API, but we won't use them:
	 * 
	 * @param unknown_type $event
	 * @param unknown_type $object_type
	 * @param unknown_type $object
	 */

		function ideacheer_init(){
			// Load system configuration
				global $CONFIG;
				
			// Extend system CSS with our own styles, which are defined in the idea/css view
				//elgg_extend_view('css','ideacheer/css');
		}
		
		//Include api functions
		include(dirname(__FILE__) . "/api.php");
		
		/**
		 * 
		 * get idea objects cheered by user
		 *
		 * @params $user
		 * 
		 * @return Array
		 * 
		 */
		function get_ideas_cheered_by_user($user, $limit=0, $offset=0){
			return elgg_get_entities_from_relationship(array(
				'relationship' => 'cheering',
				'relationship_guid' => $user->guid,
				'types' => 'object',
				'subtypes' => 'idea',
				'limit' => $limit,
				'offset' => $offset
			));
		}

		/**
		 * 
		 * get users cheering idea
		 *
		 * @params $idea
		 * 
		 * @return Array
		 * 
		 */
		function get_users_cheering_idea($idea, $limit=0, $offset=0){
			return elgg_get_entities_from_relationship(array(
				'relationship' => 'cheering',
				'relationship_guid' => $idea->guid,
				'types' => 'user',
				'limit' => $limit,
				'offset' => $offset,
				'inverse_relationship'=>true
			));
		}		 
		
		/**
		 * Is idea cheered by user
		 * 
		 * @param $idea_guid
		 * @param $user_guid
		 * 
		 * @return boolean; true if user is cheering the idea else return false
		 */
		function is_idea_cheered($user_guid, $idea_guid){
			if(!check_entity_relationship($user_guid, "cheering", $idea_guid)){
				return false;
			}else{
				return true;
			}
		}
		
		/**
		 * Get idea cheer action url
		 * 
		 * @param $idea_guid
		 */
		function get_idea_cheer_url($idea_guid){
			global $CONFIG;
			$cheer_url = $CONFIG->wwwroot."action/ideacheer/cheer?idea_guid=".$idea_guid;
			return elgg_add_action_tokens_to_url($cheer_url);
		}

		/**
		 * Get idea cheer action url
		 * 
		 * @param $idea_guid
		 */
		function get_idea_uncheer_url($idea_guid){
			global $CONFIG;
			$uncheer_url = $CONFIG->wwwroot."action/ideacheer/uncheer?idea_guid=".$idea_guid;
			return elgg_add_action_tokens_to_url($uncheer_url);
		}
				
		
	// Make sure the ideas plugin initialisation function is called on initialisation
		//register_elgg_event_handler('init','system','ideacheer_init');
		
	// Register actions
		global $CONFIG;
		//register_action("ideacheer/cheer",false,$CONFIG->pluginspath . "ideacheer/actions/cheer.php");
		//register_action("ideacheer/uncheer",false,$CONFIG->pluginspath . "ideacheer/actions/uncheer.php");