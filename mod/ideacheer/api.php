<?php

/**
 * Elgg Ideas plugin
 * 
 * @package ElggIdeas
 */
 
/**
 * Get a ideas cheered by a user
 *
 * @param int $user_guid 
 * @param int $offset
 * @param int $limit
 * @param bool $infinite - whether to return a link to next set of content
 */
function api_get_user_cheered_ideas($user_guid, $order='title', $offset=0, $limit=50, $infinite=false){
	try {
		// Make sure we're logged in (send us to the front page if not)
		gatekeeper();
		
		global $CONFIG;
		$return = array();
		
		//get the followers
		$ideas_q = elgg_get_entities_from_relationship(array(
			'relationship' => 'cheering',
			'relationship_guid' => $user_guid,
			'type' => 'object',
			'subtype' => 'idea',
			'limit' => $limit,
			'offset' => $offset,
			'joins' => array("INNER JOIN {$CONFIG->dbprefix}objects_entity o ON (e.guid = o.guid)"),
			'order_by' => 'o.'.$order
		));
		if($ideas_q){
			foreach($ideas_q as $idea){
				$ideas['guid'] = $idea->guid;
				$ideas['title'] = $idea->title;			
				$ideas['ideatype'] = $idea->ideatype;
				$ideas['pitch'] = $idea->pitch;
				$ideas['icon'] = $idea->getIcon('small');
				$ideas['url'] = $idea->getURL();
				
				$return[] = $ideas;
			}
		}		
		
		return $return;
		
	}
	catch (Exception $e){
		return $e->getFile();
	}
}
expose_function("ideas.get_user_cheered_ideas", 
	"api_get_user_cheered_ideas", 
	array(
		"user_guid" => array('type' => 'string'),
		"order" => array('type' => 'string', 'required'=>false),
		"offset" => array('type' => 'int', 'required'=>false),
		"limit" => array('type' => 'int', 'required'=>false),
		"infinite" => array('type' => 'bool', 'required'=>false)
	),
	'Get a user\'s followers',
	'GET',
	false,
	false
);