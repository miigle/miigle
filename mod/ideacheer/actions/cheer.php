<?php
	/**
	 * 
	 * Cheer an idea
	 * 
	 */

	// Make sure we're logged in (send us to the front page if not)
	gatekeeper();
	action_gatekeeper();
	
	global $CONFIG;
	
	$forward_url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "pg/dashboard";
	
	//get logged in user
	$user = get_loggedin_user();

	//get idea_guid to cheer
	$idea_guid = get_input('idea_guid');
	$idea = get_entity($idea_guid);
	$idea_owner = get_entity($idea->owner_guid);
	//check if user is already cheering the idea
	if(!check_entity_relationship($user->guid, "cheering", $idea_guid)){
		//create relationship between user and idea as cheering
		if(add_entity_relationship($user->guid, 'cheering', $idea_guid)){
			$msg = '<a href="'.$user->getURL().'">'.ucwords($user->name).'</a> cheered the idea <a href="'.$idea->getURL().'">"'.$idea->title.'</a>"';
			if(!miigle_notify_user($idea_owner, $msg)){
				//register_error("notification not sent");
			}
			system_message("You are cheering the idea now.");
			//echo 'success';
		}else{
			register_error("Error cheering idea.");
			//echo 'failure';
		}
	}else{
		system_message("You are cheering the idea now.");
		//echo 'success';
	}
	
	forward($forward_url);
?>