<?php
	/**
	 * 
	 * UnCheer an idea
	 * 
	 */

	// Make sure we're logged in (send us to the front page if not)
	gatekeeper();
	action_gatekeeper();
	
	global $CONFIG;
	
	$forward_url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "pg/dashboard";
	
	//get logged in user
	$user = get_loggedin_user();

	//get idea_guid to uncheer
	$idea_guid = get_input('idea_guid');
	
	//check if user is cheering the idea
	if(check_entity_relationship($user->guid, "cheering", $idea_guid)){
		//remove cheering relationship between user and idea
		if(remove_entity_relationship($user->guid, 'cheering', $idea_guid)){
			system_message("You have Uncheered the idea.");
			//echo 'success';
		}else{
			register_error("Error Uncheering idea.");
			//echo 'failure';
		}
	}else{
		system_message("You have Uncheered the idea.");
		//echo 'success';
	}
	
	forward($forward_url);
?>