<?php
	/**
	 * 
	 * @param $vars['idea_entities'] ;optional, if not passed shared ideas will be listed
	 * @param $vars['un']
	 * @param $vars['page_ctr']
	 * 
	 */

	//get logged in user
	$loggedin_user = get_loggedin_user();
	
	//get user, whose cheered ideas will be listed
	if(isset($vars['un']) && !empty($vars['un'])){
		$user = get_user_by_username($vars['un']);
	}else{
		$user = get_loggedin_user();
	}

	if(isset($vars['idea_entities']) && is_array($vars['idea_entities']) && count($vars['idea_entities'])>0){
		$arrIdeas = $vars['idea_entities'];
	}else{
		//get all cheered ideas by this user
		$arrIdeas = get_ideas_cheered_by_user($user);
	}
	
	if(is_array($arrIdeas) && count($arrIdeas)>0){
		$count_Ideas = count($arrIdeas);
	}
	else{
		$count_Ideas = 0;
	}

	//current page counter
	if(isset($vars["page_ctr"]) && !empty($vars["page_ctr"])){
		$page_ctr = $vars["page_ctr"];
	}else{
		$page_ctr = 1;
	}
	
	//get setting for max ideas to show on a page
	$MAX_IDEAS_PER_PAGE = get_plugin_setting('max_ideas_per_page', 'custom_dashboard');
	
	if($user->guid != $loggedin_user->guid){
		//get setting for max ideas to show on a page
		$MAX_IDEAS_PER_PAGE = get_plugin_setting('max_ideas_per_page', 'custom_dashboard');		
	}else{
		$MAX_IDEAS_PER_PAGE = 1;
	}
	
	//ideas to show on current page
	$limit = min(($count_Ideas - ($page_ctr-1)*$MAX_IDEAS_PER_PAGE), $MAX_IDEAS_PER_PAGE);
	
	//offset index
	$offset = ($page_ctr-1)*$MAX_IDEAS_PER_PAGE;

	if($offset==0){
		$prev_ideas_url = "#";
	}else{
		$prev_ideas_url = ajaxview_get_url('ideacheer', 'list', array(
																		'un'=>$user->username,
																		'page_ctr'=>($page_ctr-1)
																));
	}
	
	if(($offset+$limit) < $count_Ideas){
		$next_ideas_url = ajaxview_get_url('ideacheer', 'list', array(
																		'un'=>$user->username,
																		'page_ctr'=>($page_ctr+1)
																));
	}else{
		$next_ideas_url = "#";
	}
	
?>
	<h3><label style="font-weight:bold;"><?php echo ucwords($user->name);?></label> is cheering <?php echo $count_Ideas;?> Idea<?php echo ($count_Ideas>1?'s':'');?></h3>
<?php 	
	//view for visitors
	//if($loggedin_user->guid != $user->guid)
	echo elgg_view("ideas/listing_view_others", array(
														'context'=>'cheer_tab',
														'idea_entities'=>$arrIdeas,
														'user'=>$user,
														'page_ctr'=>$page_ctr,
														'limit'=>$limit,
														'offset'=>$offset,
														'prev_url'=>$prev_ideas_url,
														'next_url'=>$next_ideas_url
													));
?>