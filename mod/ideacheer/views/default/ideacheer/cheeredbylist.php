<?php
	/**
	 * get list of users cheering this idea
	 */

	//gatekeeper();
	
	$loggedin_user = get_loggedin_user();
	
	$idea_guid = $vars["idea_guid"];
	
	$idea = get_entity($idea_guid);
	
	$arrUsers = get_users_cheering_idea($idea);
	
?>
	<h3 style="float:left;"><?php echo (isset($arrUsers) && !empty($arrUsers)?count($arrUsers):0);?> people are cheering this Idea.</h3>
	<div style="float:right">
		<input type="button" class="cheer-button" value="" onclick="window.location.href='<?php echo get_idea_cheer_url($idea->guid);?>'" />
	</div>
	<div class="clearfloat"></div>
<?php
	
	foreach($arrUsers as $user){
		// get the user's main profile picture
		$icon = elgg_view("profile/icon", array(
													'entity' => $user,
													'size' => 'small',
													'override' => true
												)
						);
		$profile_url = "{$vars['url']}pg/dashboard/{$user->username}";
?>
	<div style="margin:10px;">
		<div style="float:left;"><?php echo $icon;?></div>
		<div style="float:left;margin-left:15px;">
			<a href="<?php echo $profile_url;?>"><?php echo ucwords($user->name)?></a>
		</div>
		<div class="clearfloat"></div>
	</div>
<?php 
	}
	
	//get users mentoring idea
	$arrMentors = get_mentors_for_idea($idea);
	//echo '$arrMentors='.count($arrMentors);
	//print_r($arrMentors);exit;
	if($arrMentors){
		$countMentors = is_array($arrMentors)?count($arrMentors):1;
?>
	<div id="investors-list">
		<h3>Mentors.</h3>
<?php 
		$ctr = 0;
		foreach($arrMentors as $mentor){
			$ctr++;
			$icon = $mentor->getIcon("small");
			$url = $mentor->getURL();
			$name = ucwords($mentor->name);
			$extraclass = ($ctr==$countMentors)?" last":"";
?>
			<div class="investor<?php echo $extraclass;?>"><a title="<?php echo $name;?>" href="<?php echo $url;?>"><img src="<?php echo $icon;?>" /></a></div>
<?php 
		}
?>
		<div class="clearfloat"></div>
	</div>
<?php 
	}else{
		echo 'No Mentors.';
	}
?>