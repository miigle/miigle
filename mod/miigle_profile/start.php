<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/engine/start.php');
require_once(dirname(__FILE__) . '/api.php');
require_once(dirname(__FILE__) . '/router.php');
require_once(dirname(dirname(__FILE__)) . '/search/search_hooks.php');

function MiigleProfileInit() {
    register_entity_type('user', 'miigle_user');
    add_subtype('user', 'miigle_user', 'MiigleUser');
    register_plugin_hook('search', 'object:miigle_user', 'SearchMiigleUsersHook');

    register_entity_type('object', 'miigle_user_status');
    add_subtype('object', 'miigle_user_status', 'MiigleUserStatus');

    // extend the user profile view
    //elgg_extend_view("profile/userdetails", "miigle_profile/profile/userdetails");
    
    // Register a URL handler for users - this means that profile_url()
    // will dictate the URL for all ElggUser objects
    //register_entity_url_handler('GetMiigleUserURL', 'user', 'miigle_user');
    register_entity_url_handler('GetMiigleUserURL', 'user', 'all');

    // Now Register the profile page handler
    //register_page_handler('dashboard', 'MiigleProfilePageHandler'); //does this work?
    register_page_handler('user', 'MiigleProfilePageHandler');
            
    elgg_extend_view('css', 'miigle_profile/css');
    elgg_extend_view('miigle_dependencies/footer_js', 'js/footer_js');
}


function GetMiigleUserURL($user) {
    global $CONFIG;
    return $CONFIG->wwwroot."pg/miigle#/user/{$user->username}";
}

/**
 * fix user submitted url that do not start with "http://" or "https://"
 */
function fix_url($url) {
    if(isset($url) && !empty($url)){
        if (substr($url, 0, 7) == 'http://') { return $url; }
        if (substr($url, 0, 8) == 'https://') { return $url; }
        return 'http://'. $url;
    }else{
        return "";
    }
}

/**
 * get user's investors
 */
function get_users_investors($user, $limit=0, $offset=0){
    if($user instanceof ElggUser){
        return get_entities_from_relationship('investor', $user->guid, true, 'user', '', 0, "", $limit, $offset);
    }else{
        return false;
    }
}

// Expose /action/thewire/add
/*
expose_function("thewire.miigleadd", 
    "api_thewire_miigleadd", 
     null,
     'Saves a wire post and returns it',
     'POST',
     false,
     false
);
function api_thewire_miigleadd(){
    // Make sure we're logged in (send us to the front page if not)
    if (!isloggedin()) forward();
    
    // Get input data
    $body = get_input('note');
    $access_id = (int)get_default_access();
    if ($access_id == ACCESS_PRIVATE) {
        $access_id = ACCESS_LOGGED_IN; // Private wire messages are pointless
    }
    $method = get_input('method');
    $parent = (int)get_input('parent', 0);
    if (!$parent) {
        $parent = 0;
    }
    // Make sure the body isn't blank
    if (empty($body)) {
        register_error(elgg_echo("thewire:blank"));
        forward("mod/thewire/add.php");
    }
    
    global $SESSION;
    
    // Initialise a new ElggObject
    $thewire = new ElggObject();
    
    // Tell the system it's a thewire post
    $thewire->subtype = "thewire";
    
    // Set its owner to the current user
    $thewire->owner_guid = get_loggedin_userid();
    
    // For now, set its access to public (we'll add an access dropdown shortly)
    $thewire->access_id = $access_id;
    
    // Set its description appropriately
    $thewire->description = elgg_substr(strip_tags($body), 0, 160);
    
    // add some metadata
    $thewire->method = $method; //method, e.g. via site, sms etc
    $thewire->parent = $parent; //used if the note is a reply
    $thewire->is_preview = false;
    $preview_link = urldecode(get_input('url'));
    if($preview_link){
        $thewire->preview_link = $preview_link;
        $thewire->preview_title = urldecode(get_input('title'));
        $thewire->preview_description = urldecode(get_input('description'));
        $thewire->preview_thumb = urldecode(get_input('thumbnail_url'));
        $thewire->is_preview = true;
    }
    
    
    //save
    $save = $thewire->save();
    
    if ($save) {
        add_to_river('river/object/thewire/create','create',$SESSION['user']->guid,$thewire->guid);
    
        // tweet
        $params = array(
            'plugin' => 'thewire',
            'message' => $thewire->description
        );
    
        trigger_plugin_hook('tweet', 'twitter_service', $params);
        
        $obj = array(
            'description'=>$thewire->description, 
            'is_preview'=>$thewire->is_preview
        );
        if($thewire->is_preview){
            $obj['preview_title'] = $thewire->preview_title;
            $obj['preview_description'] = $thewire->preview_description;
            $obj['preview_thumb'] = $thewire->preview_thumb;
            $obj['preview_link'] = $thewire->preview_link;
        }
        return $obj;
    } else {
        return elgg_echo("thewire:error");
    }
}
    

// Make sure the profile initialisation function is called on initialisation

*/

register_elgg_event_handler('init', 'system', 'MiigleProfileInit', 0);
