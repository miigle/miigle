<?php

    /**
     * Elgg profile view
     * 
     * @package ElggProfile
     */

    // Get the Elgg engine
    require_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

    // If we're not logged on, forward the user to index page
    if (!isloggedin()) {
        forward();
    }
    
    $loggedin_user = get_loggedin_user();
    
    // Get owner of profile - set in page handler
    $user = page_owner_entity();
    if (!$user) {
        register_error(elgg_echo("profile:notfound"));
        forward();
    }
    
    $loggedin_is_owner = false;
    if($loggedin_user->guid == $user->guid) $loggedin_is_owner = true;
    
    //see if this is the profile owner or someone else
    $can_edit = $user->canEdit();
    
    //get user stats
    $ideas_miigled = elgg_get_entities_from_metadata(array(
        'type' => 'object',
        'subtype' => 'idea',
        'owner_guid' => $user->guid,
        'count' => true
    ));
    $ideas_cheered = elgg_get_entities_from_relationship(array(
        'relationship' => 'cheering',
        'relationship_guid' => $user->guid,
        'types' => 'object',
        'subtypes' => 'idea',
        'count' => true
    ));
    $following = elgg_get_entities_from_relationship(array(
        'relationship' => 'following_person',
        'relationship_guid' => $user->guid,
        'types' => 'user',
        'count' => true
    ));
    $followers = elgg_get_entities_from_relationship(array(
        'relationship' => 'person_follower',
        'relationship_guid' => $user->guid,
        'types' => 'user',
        'count' => true
    ));

    if ($ideas_miigled === false) {
        $ideas_miigled = 0;
    }
    if ($ideas_cheered === false) {
        $ideas_cheered = 0;
    }
    
    //set profile view context
    set_context("profile");
    
    //get profile menu
    $area1 = elgg_view("profile/sidebar",array(
        "entity"=>$user, 
        'can_edit'=>$can_edit, 
        'loggedin_user'=>$loggedin_user,
        'loggedin_is_owner'=>$loggedin_is_owner
    ));;
    // Get profile page
    $area2 = elgg_view("profile/view",array(
        "entity"=>$user, 
        'can_edit'=>$can_edit, 
        'ideas_miigled'=>$ideas_miigled,
        'ideas_cheered'=>$ideas_cheered,
        'following'=>$following,
        'followers'=>$followers,
        'loggedin_user'=>$loggedin_user,
        'loggedin_is_owner'=>$loggedin_is_owner
    ));
        
    // get the required canvas area
    $body = elgg_view_layout("two_column", $area1, $area2);
    
    // Draw the page
    page_draw(elgg_echo("profile:view"),$body);
?>
