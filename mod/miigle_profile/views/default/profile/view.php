<script>
  var miigle_profile = miigle_profile || {};
  miigle_profile.user_guid = <?php echo json_encode($vars['model']['user_dict']['guid']); ?>;
  miigle.setEntity(miigle_profile.user_guid, <?php echo json_encode($vars['model']['user_dict']); ?>);
  <?php foreach ($vars['model']['ideas_list'] as $idea_dict): ?>
  	miigle.setEntity(<?php echo $idea_dict['guid']; ?>, <?php echo json_encode($idea_dict); ?>);
  <?php endforeach; ?>
</script>

<div ng-app="miigle.profile" ng-view></div>
