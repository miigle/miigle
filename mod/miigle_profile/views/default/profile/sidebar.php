<?php

    /**
     * Profile left sidebar
     *
     * @package ElggProfile
     *
     * @uses $vars['model']['entity'] The user entity
     * @uses $vars['model']['can_edit'] Result of $entity->canEdit()
     * @uses $vars['model']['loggedin_user'] 
     * @uses $vars['model']["loggedin_is_owner"]
     */

    require_once(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/miigle_messages/api.php');

    $is_following = IsUserOneFollowingUserTwo($vars['model']['loggedin_user']->guid, $vars['model']['entity']->guid);
    $follows_you = IsUserOneFollowingUserTwo($vars['model']['entity']->guid, $vars['model']['loggedin_user']->guid);
    $mutual_follow = $is_following && $follows_you ? true : false;
    $follow_url = '/services/api/rest/json?method=follow.follow_user';
    $follow_text = 'Follow Me';
    $follow_complete_text = 'Totally followed';
    if($is_following) {
        $follow_url = '/services/api/rest/json?method=follow.unfollow_user';
        $follow_complete_text = "It's not you, it's me :(";
        $follow_text = 'Unfollow Me';
    }

    //Get tokens for ajax functions
    $ts = time();
    $token = generate_action_token($ts);

    //Build the message form
    $message_form_action = "/action/messages/send";
    $message_form_method = "post";
    $message_form = true;
    $message_footer = 'form';
    $message_title = 'Send Message';
    if($mutual_follow) {
        $message_modal = elgg_view('miigle_messages/send_message_view', array('user'=>$vars['model']['entity']));
    } else {
        $message_body = '<img src="/mod/miigle_theme/graphics/sad_kitten.jpg" alt="Sad Kitten" />';
        $message_body .= '<h3>A Pro account is required to message those who aren\'t mutual followers.</h3>';
        $message_body .= '<a 
        data-user_guid="'.$vars['model']['loggedin_user']->guid.'" 
        data-__elgg_token="'.$ts.'"
        data-__elgg_ts="'.$token.'"
        class="upgrade" 
        href="#">
        Be the first to know when Pro accounts are available.
    </a><img class="ajax-loader" src="/mod/miigle_theme/graphics/ajax-loader.gif" alt="loading..." />';
        $message_form = false;
        $message_title = 'Upgrade Required';
        $message_modal = elgg_view( "miigle_theme/modal", array(
        'id' => 'modal_send_message',
        'header' => true,
        'title' => $message_title,
        'body' => $message_body,
        'footer' => true,
        //wut
        'form' => $message_form,
        'form_action' => $message_form_action,
        'form_method' => $message_form_method,
        ));
    }
?>
<div id="profile_sidebar">
blah blah blah
    <div class="row">
        <div class="col-md-12">
            <?php echo elgg_view(
                "profile/icon", array(
                    'entity' => $vars['model']['entity'],
                    'size' => 'master',
                    'class' => '',
                    'override' => true
                    )); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php if($vars['model']['can_edit']): ?>
                        <a href="<?php echo $vars['model']['url']?>pg/profile/<?php echo $vars['model']['entity']->username;?>/edit" class="btn btn-default btn-lg btn-block"><span class="glyphicon glyphicon-pencil"></span> Edit Profile</a>
                    <?php endif; ?>
                    <?php if(!$vars['model']['loggedin_is_owner']): ?>
                        <a id="send-message-button" data-toggle="modal" href="#modal_send_message" href="#" class="btn btn-default btn-lg btn-block">Message Me</a>        
                        <a href="#" 
                        id="follow_user" 
                        class="btn btn-ajax btn-default btn-lg btn-block" 
                        data-url="<?php echo $follow_url; ?>"
                        data-type="POST"
                        data-loading-text="Give me a sec..." 
                        data-complete-text="<?php echo $follow_complete_text; ?>" 
                        data-error-text="My bad. Try refreshing."
                        data-follower_guid="<?php echo $vars['model']['loggedin_user']->guid; ?>"
                        data-following_guid="<?php echo $vars['model']['entity']->guid; ?>"
                        data-__elgg_token="<?php echo $token; ?>"
                        data-__elgg_ts="<?php echo $ts; ?>"><?php echo $follow_text; ?></a>    
                        <?php echo $message_modal; ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 social">
                    <?php if($vars['model']['entity']->twitter): ?>
                        <a class="btn btn-default" href="<?php echo $vars['model']['entity']->twitter; ?>">
                            <i class="fa fa-fw fa-twitter"></i>
                        </a>            
                    <?php endif; ?>
                    <?php if($vars['model']['entity']->facebook): ?>
                        <a class="btn btn-default" href="<?php echo $vars['model']['entity']->facebook; ?>">
                            <i class="fa fa-fw fa-facebook"></i>
                        </a>                    
                    <?php endif; ?>
                    <?php if($vars['model']['entity']->linkedin): ?>
                        <a class="btn btn-default" href="<?php echo $vars['model']['entity']->linkedin; ?>">
                            <i class="fa fa-fw fa-linkedin"></i>
                        </a>                        
                    <?php endif; ?>
                    <?php if($vars['model']['entity']->googleplus): ?>
                        <a class="btn btn-default" href="<?php echo $vars['model']['entity']->googleplus; ?>">
                            <i class="fa fa-fw fa-google-plus"></i>
                        </a>                        
                    <?php endif; ?>        
                </div>
            </div>

            <?php if($vars['model']['entity']->interests): ?>
                <div class="row">
                    <div class="col-md-12 interests">
                        <div class="whitebox">
                            <div class="whitebox-header">
                                <h2>Interests</h2>
                            </div>
                            <div class="whitebox-body">
                                <?php foreach(explode(',', $vars['model']['entity']->interests) as $interest): ?>
                                    <span class="label label-muted"><?php echo $interest; ?></span>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

<!--<div class="row">
    <div class="col-md-12">
        <div class="whitebox">
            <div class="whitebox-header">
                <h2>Social Networks</h2>
            </div>
            <div class="whitebox-body">
                <?php if($vars['model']['entity']->twitter): ?>
                    <a class="btn btn-default" href="<?php echo $vars['model']['entity']->twitter; ?>">
                        <i class="icon-fixed-width icon-twitter"></i>
                    </a>                    
                <?php endif; ?>
                <?php if($vars['model']['entity']->facebook): ?>
                    <a class="btn btn-default" href="<?php echo $vars['model']['entity']->facebook; ?>">
                        <i class="icon-fixed-width icon-facebook"></i>
                    </a>                    
                <?php endif; ?>
                <?php if($vars['model']['entity']->linkedin): ?>
                    <a class="btn btn-default" href="<?php echo $vars['model']['entity']->linkedin; ?>">
                        <i class="icon-fixed-width icon-linkedin"></i>
                    </a>                    
                <?php endif; ?>
                <?php if($vars['model']['entity']->googleplus): ?>
                    <a class="btn btn-default" href="<?php echo $vars['model']['entity']->googleplus; ?>">
                        <i class="icon-fixed-width icon-google-plus"></i>
                    </a>                    
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>-->
</div>
