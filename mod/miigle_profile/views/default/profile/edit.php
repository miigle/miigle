<?php

    /**
     * Miigle custom profile edit form
     * 
     * @package MiigleProfile
     * 
     * @uses $vars['entity'] The user entity
     */

    //get user
    $user = $vars['entity'];
    //get profile icon
    $profile_icon = elgg_view("profile/icon", array(
                    'entity' => $user,
                    'size' => 'medium',
                    'override' => true,
                  ));
                  
    $profileicon_file = elgg_view('input/file', array(
        'internalname' => 'profileicon', 
        'value' => $user->profileicon, 
        'size' => '60', 
        'id'=>'icon',
        'extra_tags'=>'accept="image/*"'));
    $bio = elgg_view('input/longtext', array(
        'id'=>'bio' , 
        'internalname' => 'user[bio]', 
        'value' => $user->bio, 
        'class'=>'form-control',
        'extra_tags'=>'placeholder="i.e. I swore an oath to rid the city of the evil. I spent my youth traveling the world, training myself to intellectual and physical perfection and learning a variety of crime-fighting skills."'));
    $tagline = elgg_view('input/longtext', array(
        'id'=>'tagline',
        'maxlength'=>'160',
        'internalname'=>'user[tagline]',
        'value'=>$user->tagline,
        'class'=>'form-control',
        'extra_tags'=>'placeholder="I am the protector of Gotham City."',
    ));
    $interests = elgg_view('input/tags', array(
        'internalname' => 'user[interests]', 
        'class' => 'form-control tm-tags', 
        'value' => $user->interests, 
        'id'=>'interests',
        'extra_tags'=>'placeholder="i.e. Fast Cars, Cat Women, Real Estate"'));        
    $email = elgg_view('input/email', array(
        'internalname' => 'user[email]', 
        'class' => 'form-control', 
        'value' => $user->email, 
        'id'=>'email',
        'extra_tags'=>'placeholder="i.e. BruceWayne@WayneEnterprises.com"'));
    $phone = elgg_view('input/text', array(
        'internalname' => 'user[phone]', 
        'class' => 'form-control', 
        'value' => $user->phone, 
        'id'=>'phone',
        'extra_tags'=>'placeholder="i.e. 15738675309" minlength="4" maxlength="11" digits="true"'));
    $twitter = elgg_view('input/url', array(
        'internalname' => 'user[twitter]', 
        'class' => 'form-control', 
        'value' => $user->twitter, 
        'id'=>'twitter',
        'extra_tags'=>'placeholder="i.e. http://twitter.com/BruceWayne"'));
    $facebook = elgg_view('input/url', array(
        'internalname' => 'user[facebook]', 
        'class' => 'form-control', 
        'value' => $user->facebook, 
        'id'=>'facebook',
        'extra_tags'=>'placeholder="i.e. http://www.facebook.com/BruceWayne"'));
    $googleplus = elgg_view('input/url', array(
        'internalname' => 'user[googleplus]', 
        'class' => 'form-control', 
        'value' => $user->googleplus, 
        'id'=>'googleplus',
        'extra_tags'=>'placeholder="i.e. http://plus.google.com/102246385446910031933"'));
    $linkedin = elgg_view('input/url', array(
        'internalname' => 'user[linkedin]', 
        'class' => 'form-control', 
        'value' => $user->linkedin, 
        'id'=>'linkedin',
        'extra_tags'=>'placeholder="i.e. http://www.linkedin.com/in/BruceWayne"'));
    $current_title = elgg_view('input/text', array(
        'internalname' => 'user[current_title]', 
        'class' => 'form-control', 
        'value' => $user->current_title, 
        'id'=>'current_title',
        'extra_tags'=>'placeholder="i.e. VP of Knockin\' Boots"'));
    $current_employer = elgg_view('input/text', array(
        'internalname' => 'user[current_employer]', 
        'class' => 'form-control', 
        'value' => $user->current_employer, 
        'id'=>'current_employer',
        'extra_tags'=>'placeholder="i.e. Wayne Enterprises"'));
    $employer_website = elgg_view('input/url', array(
        'internalname' => 'user[employer_website]', 
        'class' => 'form-control', 
        'value' => $user->employer_website, 
        'id'=>'employer_website',
        'extra_tags'=>'placeholder="i.e. http://www.wayneenterprises.com"'));
    $skills = elgg_view('input/tags', array(
        'internalname' => 'user[skills]', 
        'class' => 'form-control tm-tags', 
        'value' => $user->skills, 
        'id'=>'skills',
        'extra_tags'=>'placeholder="i.e. Fighting Evil, Forensics, STRIKING FEAR IN HEARTS"'));
    $school = elgg_view('input/text', array(
        'internalname' => 'user[school]', 
        'class' => 'form-control', 
        'value' => $user->school, 
        'id'=>'school',
        'extra_tags'=>'placeholder="i.e. Cambridge"'));
    $studied = elgg_view('input/text', array(
        'internalname' => 'user[studied]', 
        'class' => 'form-control', 
        'value' => $user->studied, 
        'id'=>'studied',
        'extra_tags'=>'placeholder="i.e. Criminology"'));
    $degree = elgg_view('input/text', array(
        'internalname' => 'user[degree]', 
        'class' => 'form-control', 
        'value' => $user->degree, 
        'id'=>'degree',
        'extra_tags'=>'placeholder="i.e. MBA"'));
    $graduation = elgg_view('input/text', array(
        'internalname' => 'user[graduation]', 
        'class' => 'form-control', 
        'value' => $user->graduation, 
        'id'=>'graduation',
        'extra_tags'=>'placeholder="i.e. 2012" minlength="4" maxlength="4" digits="true"'));
?>

