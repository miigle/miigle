<?php
    //print_r($vars[entity]);exit;
    $user = $vars[entity];
    $user_url = $user->getUrl();
    $icon = elgg_view("profile/icon", array(
                                                'entity' => $user,
                                                'size' => 'small',
                                                'override' => false
                                            ));
    $title = ucwords($user->name);

    //Show the current or latest position at work
    $arr_employers = json_decode($user->employers);
    $latest_emp = array();
    $present_job = false;
    foreach($arr_employers as $employer_json){
        $arr_employer = json_decode($employer_json,true);
        if(count($arr_employer['is_current_job'])>0){
            $present_job = true;
            //this is the latest job so skip everything else
            $latest_emp = $arr_employer;
            break;
        }
        if(count($latest_emp)>0){
            if($latest_emp["job_end_year"] == $arr_employer["job_end_year"]){
                //year matched, check month
                if($latest_emp["job_end_month"] < $arr_employer["job_end_month"]){
                    //update latest job
                    $latest_emp = $arr_employer;
                }
            }elseif($latest_emp["job_end_year"] < $arr_employer["job_end_year"]){
                //update latest job
                $latest_emp = $arr_employer;
            }else{
                //nothing to do
            }
        }else{
            $latest_emp = $arr_employer;
        }
    }
    
    //if we have a latest job to show
    if(count($latest_emp)>0){
        $position = "";
        if(isset($latest_emp["job_position"]) && !empty($latest_emp["job_position"])){
            $position .= ucwords($latest_emp["job_position"]);
        }
        if(isset($latest_emp["employer_name"]) && !empty($latest_emp["employer_name"])){
            $employer_name = " @ " . $latest_emp["employer_name"];
        }
        /***** For job location, but we dont want this ***********
        if(isset($latest_emp["job_city"]) && !empty($latest_emp["job_city"])){
            $job_city = " in " . ucwords($latest_emp["job_city"]);
        }
        */
        if(!(isset($latest_emp["job_position"]) && !empty($latest_emp["job_position"])) 
            && !(isset($latest_emp["employer_name"]) && !empty($latest_emp["employer_name"])) 
            && !(isset($latest_emp["job_city"]) && !empty($latest_emp["job_city"]))){
            $position = "";
        }
    }
    //get current location
    if(isset($user->current_city) && !empty($user->current_city)){
        $current_location = ucwords($user->current_city);
    }else{
        $current_location = "";
    }
    
    $description = "";
    if(!empty($position)){
        $description .= $position;
    }
    if(!empty($current_location)){
        if(empty($description)){
            $description .= "$current_location";
        }else{
            $description .= " | $current_location";
        }
    }
    if(empty($description)){
        $description = '<span style="font-weight:normal;font-style:italic;color:#666;">No info available</span>';
    }
    
    $bio = $user->bio;
    if(empty($bio)){
        $bio = '<span style="font-weight:normal;font-style:italic;color:#666;">No info available</span>';
    }
?> 
    <div class="search_listing">
        <div class="search_listing_icon"><a href="<?php echo $user_url;?>"><?php echo $icon; ?></a></div>
        <div class="search_listing_info">
            <p class="search_item_title"><a href="<?php echo $user_url;?>"><?php echo $title; ?></a></p>
            <p class="search_item_description"><?php echo $description; ?></p>
            <p class="search_item_bio"><?php echo $bio; ?></p>
        </div>
        <div class="clearfloat"></div>
    </div>
