<?php

    /**
     * Miigle Custom Profile Styles
     * 
     * @package Miigle Profile
     */
     
?>

/************** Common Styles ********************/


/************** profile picture upload n crop page ********************/
#avatar-preview-box{
    float:right;
}
#avatar-preview-box img{
    border:1px #cecece dashed;
}
#user_avatar_preview_title{
    margin:0px;
}
#profile_picture_form {
    height:145px;
    float:left;
}
#current_user_avatar {
    float:left;
    width:160px;
    height:130px;
    border-right:1px solid #cccccc;
    margin:0 20px 0 0;
}
#profile_picture_croppingtool {
    border-top: 1px solid #cccccc;
    margin:20px 0 0 0;
    padding:10px 0 0 0;
}
#profile_picture_croppingtool #user_avatar {
    float: left;
    margin-right: 20px;
    max-width:350px;
}
#profile_picture_croppingtool #applycropping {

}
#profile_picture_croppingtool #user_avatar_preview {
    float: left;
    position: relative;
    overflow: hidden;
    width: 100px;
    height: 100px;
    border:1px dashed #666;
}
/************** Edit Profile Page Styles ******************/
div.edit_profile_section{
    font-size:13px;
    margin-top:20px;
    margin-bottom:20px;
}
div.edit_profile_section p{
    font-size:13px;
    padding:5px 0;
}
div.edit_profile_section label{
    font-size:13px;
    font-weight:bold;
    margin:5px 0;
}

div.edit_profile_section input[type='text'], div.edit_profile_section textarea{
    width:250px;
    font-size:13px;
}

div.edit_profile_section textarea{
    height:100px;
}

div.edit_profile_section textarea#biotext{
    width:475px;
    margin-right:0;
    height:60px;
    padding:10px;
}

div.edit_profile_section input[type='text']{
    /*height:21px;*/
}

div.edit_profile_section input, div.edit_profile_section textarea, div.edit_profile_section select{
    color:#333;
    margin-right:10px;
}

div.edit_profile_section select{
    vertical-align:top;
    max-width:150px;
    margin-right:0px;
}
div.edit_profile_section select.input-access{
    float:right;
}
div.edit_profile_section select option{
    padding-right:5px;
}

div.edit_profile_section input.hint{
    color:#777;
}

div.edit_profile_section h4{
    float:left;
    margin:0;
    padding:5px 0;
}

ul.property-list{
    color:#333;
    padding:0;
    margin:0 0 25px 0;
}

ul.property-list li{
    padding:4px;
    margin:10px 10px 10px 20px;
    background-color: #F7F7F7;
    border:1px solid #cecece;
    border-radius: 8px;
    -moz-border-radius: 8px;
    -webkit-border-radius: 8px;
}

.property-list form .form-template-table input[type="text"]{
    width:250px;
}

ul.property-list li span{
    font-weight:bold;
    font-size:12px;
    margin-right:10px;
}

ul.property-list li a{
    background-color: #DDDDDD;
    border: 1px solid #DDDDDD;
    border-radius: 4px 4px 4px 4px;
    color: #999999;
    display: block;
    float: right;
    font-size: 11px;
    font-weight: bold;
    padding: 0 5px;
    text-decoration: none;
    cursor:pointer;
}

div.employer-form, div.school-form, div.college-form, div.interest-form{
    background-color:#F7F7F7;
    padding:10px;
    width:394px;
}

div.employer-form-template, div.school-form-template, div.college-form-template, div.interest-form-template{
    display:none;
}

div.employer-form h3, div.school-form h3, div.college-form h3, div.interest-form h3{
    display:block;
    float:left;
    padding:0;
    margin:0;
}

div.btns_container{
    float:right;
}

div.btns_container a.edit_btn, div.btns_container a.remove_btn{
    float:left;
    display:none;
}

a.remove_btn{
    margin-left:10px;
}

a.edit_btn:hover, a.remove_btn:hover{
    background-color:#cccccc;
    border-color:#cccccc;
}

input[name="cancel"]{
    cursor:pointer;
}

div.employer-form hr, div.school-form hr, div.college-form hr, div.interest-form hr{
    color:#c3c3c3;
    background-color:#c3c3c3;
    height:1px;
    border:0 none;
}

table.form-template-table tr th{
    text-align:right;
    padding:0 5px 10px 0;
    vertical-align:middle;
}

table.form-template-table tr td{
    padding-bottom:10px;
}

table.form-template-table form input.input-text{
    width:250px;
}

table.form-template-table td input[type='submit']{
    text-align:center;
}

ul.employer-list li, ul.school-list li, ul.college-list li, ul.interest-list li{
    list-style-type:none;
    width:411px;
}

table.form-template-table form input.input-checkboxes{
    padding:0;
    margin:3px 4px 6px 0;
}
.form-group.hide-profile {
    margin:20px 0 0 0;
}

/************** Edit Profile Menu ********************/
div.menu-edit-profile-box{
    border-bottom: 1px solid #CFCFCF;
    margin: 0;
    padding: 10px 5px 20px 0;
}

div.menu-edit-profile-box ul{
    padding:0;
    margin:0;
}

div.menu-edit-profile-box ul li{
    list-style-type:none;
    padding:0;
    margin:4px 4px 4px 0;
}

div.menu-edit-profile-box ul li a{
    display:block;
    margin:0;
    padding:4px 4px 4px 10px;
    color:#333333;
    font-size:12px;
    font-weight:normal;
    cursor:pointer;
    text-decoration:none;
}

div.menu-edit-profile-box ul li a:hover{
    background-color:#F7F7F7;
}

div.menu-edit-profile-box ul li a.selected{
    background-color:#CCCCCC;
    font-weight:bold;
}

/************************ View Profile Styles ******************************/

#profile_details{
    margin-top:20px;
}

.profile-section-box{
    padding:5px;
}

div.section-heading{
    background-color:#F2F2F2;
    border:0 none;
    border-top:1px solid #e2e2e2;
    padding:0;
    margin:0;
}

div.section-heading h3{
    padding:5px;
    margin:0
}

div.sub-section-box{
    width:100%;
    padding:15px 0;
    border:0 none;
    border-top:1px solid #D9D9D9;
}

div.sub-section-box.first{
    border-top-width:0;
}

div.sub-section-box div.sub-section-title{
    float:left;
    width:30%;
    color:#999999;
    font-size:12px;
    font-weight:bold;
    vertical-align:top;
}

div.sub-section-box div.sub-section-info{
    float:left;
    width:70%;
}

div.sub-section-info ul li{
    display:block;
    list-style-type:none;
    padding:0;
    margin:0 0 10px 0;
    border-top:1px solid #e9e9e9;
}

div.sub-section-info ul li.first{
    border-top-width:0;
}

div.sub-section-info ul li h3{
    padding:0;
    margin:5px 0;
    color:#696969;
}

div.profile-info-tag{
    color:gray;
    font-size:12px;
    font-weight:bold;
    vertical-align:top;
}

div.profile-info-tag span{
    font-weight:normal;
    font-size:11px;
    margin-top:2px;
}

div.phone-box{
    margin-top:10px;
    width:395px;
}

div.phone-box input{
    float:left;
    margin-left:10px;
}

a#add_phone_box, a#add_email_box {
    color:blue;
}

div.phone-box select{
    float:left;
}

div.phone-box input{
    float:left;
}

div.email-box{
    margin-top:10px;
    width:300px;
}

div.email-box input{
    float:left;
}

div.email-box select{
    float:left;
}

#contactinfo input[type="text"]{
    margin-bottom:10px;
}

#social-links-container input[type="text"]{
    width:400px;
}

div.email-box a.remove-email{
    background-color: #DDDDDD;
    border: 1px solid #DDDDDD;
    border-radius: 4px 4px 4px 4px;
    color: #999999;
    cursor: pointer;
    display: block;
    float: right;
    font-size: 11px;
    font-weight: bold;
    margin-left: 10px;
    margin-top: 7px;
    padding-bottom: 1px;
    padding-left: 5px;
    padding-right: 5px;
    text-decoration: none;
    width: 6px;
}

div.email-box a.remove-email:hover{
    background-color:#cccccc;
    border-color:#cccccc;    
    text-decoration:none;
}

div.phone-box a.remove-phone{
    background-color: #DDDDDD;
    border: 1px solid #DDDDDD;
    border-radius: 4px 4px 4px 4px;
    color: #999999;
    cursor: pointer;
    display: block;
    float: right;
    font-size: 11px;
    font-weight: bold;
    margin-left: 10px;
    margin-top: 7px;
    padding-bottom: 1px;
    padding-left: 5px;
    padding-right: 5px;
    text-decoration: none;
    width: 6px;
}

div.phone-box a.remove-phone:hover{
    background-color:#cccccc;
    border-color:#cccccc;
    text-decoration:none;
}

/************************************ RIVER STYLES ***************************************/
.river_content_display{
    padding:0;
    margin:0;
    font-size:13px;
}