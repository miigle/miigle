<?php

    /**
     * Miigle custom edit profile menu
     * 
     * @Description outputs a menu for editing user's profile
     * 
     */
    
    $user = $vars["entity"];
    $base_url = $vars["url"]."pg/profile/".$user->username."/edit";
    $ps = get_input("ps");
    if(!empty($ps) && !is_null($ps)){
        //
    }
    else{
        $ps = 'basicinfo';
    }
?>

<div class="menu-edit-profile-box">
    <ul>
        <li><a href="<?php echo $base_url.'?ps=basicinfo';?>" id="basicinfo" <?php echo ($ps=='basicinfo'?'class="selected"':'');?>>Basic Information</a></li>
        <li><a href="<?php echo $base_url.'?ps=bio';?>" id="bio" <?php echo ($ps=='bio'?'class="selected"':'');?>>Bio</a></li>
        <li><a href="<?php echo $base_url.'?ps=profilepic';?>" id="profilepic" <?php echo ($ps=='profilepic'?'class="selected"':'');?>>Profile Picture</a></li>
        <li><a href="<?php echo $base_url.'?ps=eduwork';?>" id="eduwork" <?php echo ($ps=='eduwork'?'class="selected"':'');?>>Education and Work</a></li>
        <li><a href="<?php echo $base_url.'?ps=interests';?>" id="interests" <?php echo ($ps=='interests'?'class="selected"':'');?>>Interests</a></li>
        <li><a href="<?php echo $base_url.'?ps=contactinfo';?>" id="contactinfo" <?php echo ($ps=='contactinfo'?'class="selected"':'');?>>Contact Information</a></li>
    </ul>
</div>
