<?php

/**
 * Elgg profile index
 * 
 * @package ElggProfile
 */

// If we're not logged on, forward the user to index page
if (!isloggedin()) {
    //forward();
}

$username = get_input('username');
$user_dict = MiigleUser::GetUserFromUsername($username);

$ideas_dict_list = array_map('MiigleIdea::GetIdea', (array)$user_dict['ideas_list']);

$model = array(
    'user_dict' => $user_dict,
    'ideas_list' => $ideas_dict_list,
);

global $CONFIG;
require_once($CONFIG->pluginspath . 'miigle_profile/controllers/view_controller.php');
