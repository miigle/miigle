<?php

//set profile view context
set_context("profile");

$area2 = elgg_view("profile/view",array(
    'model' => $model
));

// Draw the page
page_draw(elgg_echo("profile:view"), $area2);
