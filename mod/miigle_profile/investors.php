<?php
    /**
     * list all investors of a user
     */

    //check access
    gatekeeper();
    
    //get username
    $username = get_input("username", "");
    //get display mode
    $mode = get_input("mode", "");
    //get viewtype    
    $viewtype = get_input("viewtype", "full");
    //get user
    $user = get_user_by_username($username);
    
    if(!$user){
        $msg = "User not found.";
        if($mode==="widget"){
            die($msg);
        }else{
            register_error($msg);
            forward(REFERER);
        }
    }
    
    //everything seems ok, get list of all followers
    $arrInvestors = get_users_investors($user);

    $title = count($arrInvestors) . " people have Invested in " . ucwords($user->name) . "'s Ideas.";

    $baseurl = "{$vars['url']}pg/profile/{$user->username}/investors/$viewtype/$mode";
    
    $body = elgg_view("users/list", array("entities" => $arrInvestors, 
                                            "baseurl" => $baseurl,
                                            "title" => $title, 
                                            "viewtype" => $viewtype));
    
    if($mode === "widget"){
        die($body);
    }else{
        $body = elgg_view_layout("three_column", "", $body);
        $title = "Investors of ".ucwords($user->name);
        page_draw($title, $body);
    }
?>