
angular.module('miigle.profile.employer', ['miigle.button_checkbox'])
  .directive('employer', function() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/mod/miigle_profile/templates/employer_span.html',
      scope: {
        workInfoDict: '=',
      },
    };
  });

var miigle_profile_module = angular.module('miigle.profile', ['ngRoute', 
    'miigle.image_upload', 'miigle.feed', 
    'miigle.idea.view', 'miigle.messages.send_message', 
    'miigle.profile.status_update', 'ngSanitize', 'miigle.profile.employer',
    'miigle.feed']);

miigle_profile_module.filter('ExpertiseIntToString', function() {
  return function(expertise_int) {
    return miigle.getExpertiseDict()[expertise_int];
  };
});

miigle_profile_module.controller('ProfileController', ['$routeParams', '$scope',
  function($routeParams, $scope) {
    'use strict';

    var user_guid = miigle.getUsernameGUID($routeParams.username);

    $scope.GetExpertiseIntClass = window.GetExpertiseIntClass;

    $scope.getUserDict = function() {
      return miigle.getEntity(user_guid);
    };
    $scope.setUserDict = function(new_user_dict) {
      Object.keys(new_user_dict)
      .map(function(key) {
        $scope.user_dict[key] = new_user_dict[key];
      });

      return miigle.setEntity(new_user_dict.guid, new_user_dict);
    };

    $scope.user_dict = $scope.getUserDict();
  }
]);

miigle_profile_module.controller('ProfileViewController', ['$scope', '$route', 
  '$routeParams', '$sce', '$controller', 'FeedFactory', 
  function($scope, $route, $routeParams, $sce, $controller, FeedFactory) {
    'use strict';

    var init;
    var FindUserRole;
    var SetIdeaUserRole;

    $controller('ProfileController', {$scope: $scope});

    $(window).scrollTop(0);

    init = function() {
      $scope.idea_categories = miigle_ideas.idea_categories;
      $scope.viewing_user_dict = miigle.getLoggedInUser();
      $scope.show_education = ($scope.user_dict.education_info_dict.degree ||
                               $scope.user_dict.education_info_dict.studied ||
                               $scope.user_dict.education_info_dict.school ||
                               $scope.user_dict.education_info_dict.graduated);

      $scope.is_viewing_user_following_user = ($scope.viewing_user_dict.following_list.indexOf($scope.user_dict.guid) != -1);
      $scope.is_user_following_viewing_user = ($scope.user_dict.following_list.indexOf($scope.viewing_user_dict.guid) != -1);
      $scope.mutual_followers = $scope.is_viewing_user_following_user && $scope.is_user_following_viewing_user;

      $scope.$watch('is_viewing_user_following_user', function() {
        $scope.mutual_followers = $scope.is_viewing_user_following_user && $scope.is_user_following_viewing_user;
        $scope.follow_button_text = (($scope.is_viewing_user_following_user) ? 'Unfollow' : 'Follow') + ' ' + $scope.user_dict.first_name;
      });


      $scope.showSendMessageModal = function() {
        $scope.$broadcast('message:show-modal');
      };

              //functional neckbeard program growing...
              $scope.has_social_networks = Object
              .keys($scope.user_dict.contact_info_dict)
              .filter(function(social_network) {
                return social_network !== 'email_address' && social_network !== 'phone_number';
              })
              .some(function(social_network) {
                return $scope.user_dict.contact_info_dict[social_network].length > 0;
              });


              $scope.user_stakeholding_list = $scope.user_dict.ideas_list
              .map(miigle.getEntity)
              .map(SetIdeaUserRole)
              ;

              $scope.user_cheering_idea_list = $scope.user_dict.cheering_list
              .map(miigle.getEntity)
              ;

              $scope.user_followers_list = $scope.user_dict.followers_list
              .map(miigle.getEntity)
              .sort(function(a, b) {
                return a.followers_list.length < b.followers_list.length;
              })
              ;

              $scope.user_following_list = $scope.user_dict.following_list
              .map(miigle.getEntity)
              .sort(function(a, b) {
                return a.followers_list.length < b.followers_list.length;
              })
              ;

              //append to this later with the things they are stakeholders in

              $('#details_accordion').click(function(event) {
                if (event.target.localName !== 'a') {
                  event.preventDefault();

                  if (miigle.getLoggedInUser().guid === 0) {
                    alert('You register and login to view user info.');
                  } else {
                    $('#details_panel').collapse('toggle');
                  }
                }
              });

              if (miigle.getLoggedInUser().guid === 0) {
                $('#details_panel').collapse();
              }

              $('#details_accordion').on('shown.bs.collapse', function() {
                var $a = $(this).find('.panel-heading a');
                $a.find('.text').html($a.data('shown'));
                $a.find('.glyphicon').removeClass('glyphicon-arrow-down');
                $a.find('.glyphicon').addClass('glyphicon-arrow-up');
              });
              $('#details_accordion').on('hidden.bs.collapse', function() {
                var $a = $(this).find('.panel-heading a');
                $a.find('.text').html($a.data('hidden'));
                $a.find('.glyphicon').removeClass('glyphicon-arrow-up');
                $a.find('.glyphicon').addClass('glyphicon-arrow-down');
              });
    };

    $scope.toggleFollow = function() {
      var method_name;

      if ($scope.is_viewing_user_following_user) {
        method_name = 'unfollow';
        //oh god kill me now this is horrible.
        //I just want to array.remove(value);
        $scope.user_dict.followers_list.splice(
            $scope.user_dict.followers_list.indexOf(
                $scope.viewing_user_dict.guid), 
            1);
        $scope.viewing_user_dict.following_list.splice(
          $scope.viewing_user_dict.following_list.indexOf(
                $scope.user_dict.guid),
            1);
      } else {
        method_name = 'follow';
        $scope.user_dict.followers_list.push($scope.viewing_user_dict.guid);
        $scope.viewing_user_dict.following_list.push($scope.user_dict.guid);
      }

      $scope.viewing_user_dict = miigle.setEntity($scope.viewing_user_dict.guid, $scope.viewing_user_dict);
      $scope.is_viewing_user_following_user = !$scope.is_viewing_user_following_user;

      $.ajax({
        type: 'POST',
        url: '/services/api/rest/json/?method=users.' + method_name,
        dataType: 'json',
        data: {
          __elgg_ts: miigle.getActionTokenDict().__elgg_ts,
          __elgg_token: miigle.getActionTokenDict().__elgg_token,
          method: 'users.' + method_name,
          user_guid: $scope.user_dict.guid,
        },
        complete: function(jqxhr, text_status) {
          FeedFactory.BreakFeedCache();
          FeedFactory.WarmCache();
        },
      });
    };

    $scope.toggleShare = function() {
    };

    $scope.statusUpdateCallback = function() {
      $route.reload();
    };

    $scope.linkFromModal = function($event) {
      var $parent_modal; 

      if ($event.target.localName === 'a') {
        $parent_modal = $($event.target).parents('.modal');

        if ($parent_modal.is(':visible')) {
          $parent_modal.on('hidden.bs.modal', function (event) {
            $event.target.click();
          });
          $parent_modal.modal('hide');
          $event.preventDefault();
        }
      }
    };

    FindUserRole = function(idea_dict) {
      var stakeholders_dict = idea_dict['stakeholders_dict'];
      var stakeholders_list = Object
        .keys(stakeholders_dict)
        .map(function(stakeholder_id) {
          return stakeholders_dict[stakeholder_id];
        });

      //should be a list of 1
      var user_list = stakeholders_list.filter(function(stakeholder) {
        return (stakeholder.first_name == $scope.user_dict.first_name &&
          stakeholder.last_name  == $scope.user_dict.last_name);
      });

      if (user_list.length) {
        return user_list[0]['role'];
      } else {
        return undefined;
      }
    };

    SetIdeaUserRole = function(idea_dict) {
      idea_dict['user_role'] = FindUserRole(idea_dict);
      return idea_dict;
    };

    init();
  }
]);

miigle_profile_module.controller('ProfileEditController', ['$scope', 
  '$controller',
  function($scope, $controller) {
    'use strict';

    var init;

    $controller('ProfileController', {$scope: $scope});

    init = function() {
      $scope.expertise_dict = miigle.getExpertiseDict();
      $scope.idea_categories = miigle_ideas.idea_categories;
      $scope.save_button_text = 'Save';
      $scope.next_tab_href  = '#contact';
      $scope.viewing_user_dict = miigle.getLoggedInUser();
      $scope.persona_dict = {
        'Entrepreneur': false,
        'Investor': false, 
        'Mentor': false,
        'Professional': false,
        'Beta User': false,
      };
      $scope.user_dict.personas_list.
        forEach(function(persona) {
          $scope.persona_dict[persona] = true;
        });

      $scope.user_interests_dict = Object.create(null);
      Object.keys($scope.idea_categories)
        .forEach(function(category_int) {
          category_int = parseInt(category_int, 10);
          $scope.user_interests_dict[category_int] = (
              $scope.user_dict.interests_list.indexOf(category_int) !== -1);
        });

      $scope.user_expertise_dict = Object.create(null);
      Object.keys($scope.expertise_dict)
        .forEach(function(expertise_int) {
           expertise_int = parseInt(expertise_int, 10);
           $scope.user_expertise_dict[expertise_int] = (
               $scope.user_dict.work_info_dict.expertise_list.indexOf(
               expertise_int) !== -1);
      });

      angular.element(document).ready(function() {
          //why do we have to do this?
          $scope.$watch('user_dict.location_dict', function() {
            InitLocationDropdown($('#user-location-input'), $scope.user_dict);
          });

          $('#profile-edit-form').validate({
            debug: true,
            validClass: 'has-success',
            errorClass: 'error has-error',
            rules: {
              graduated: {
                required: false,
                number: true,
                max: 2020, //I guess we can allow future graduation years
                min: 1900,
              },
              user_location: {
                required: false,
                location: true,
              },
              tagline: {
                required: false,
                maxlength: 140,
              },
              password2: {
                required: false,
                equalTo: '#password1-input',
              },
            },
            errorPlacement: function(error, element) {
              if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
              } else {
                error.insertAfter(element);
              }
            },
            highlight: function(element, errorClass, validClass) {
              $(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function(element, errorClass, validClass) {
              $(element).closest('.form-group').addClass(validClass).removeClass(errorClass);
            },
          });
      });
    };

    $scope.changePassword = function() {
      var $form = $('#profile-edit-form');
      var inputs_valid = ['password', 'password1', 'password2']
          .reduce(function(all_valid, input_name) {
            return all_valid && $('input[name="' + input_name + '"]').valid();
          }, true);

      if (!inputs_valid) return;

      //this will pull in a bunch of other crap, but on the sever
      //we only look for the password fields so it's fine
      //still bothers me
      var form_data = $form.serializeArray().concat({
        name: '__elgg_ts',
        value: miigle.getActionTokenDict().__elgg_ts,
      }).concat({
        name: '__elgg_token',
        value: miigle.getActionTokenDict().__elgg_token,
      });

      $.ajax({
        type: 'POST',
        url: '/action/miigle_user_settings/edit',
        data: form_data,
        complete: function(jqxhr, text_status) {
          //excuse me while I vomit
          if (jqxhr.responseText == 'success') {
            $('#change-password-button').css('background-color', 'green');
          } else {
            $('#change-password-button').css('background-color', 'red');
            alert(jqxhr.responseText);
          }
        },
      });
    };

    $scope.transitionTabs = function(event) {
      $scope.save_button_text = 'Save';
      $scope.next_tab_href = $(event.currentTarget).next().find('a').attr('href');
      event.preventDefault();
    };

    $scope.continue = function() {
      var $next_tab_link = $('#edit-profile-tabs').find('a[href="' + $scope.next_tab_href + '"]');
      var $next_next_tab_link = $next_tab_link.parent().next().find('a');

      $scope.next_tab_href = $next_next_tab_link.attr('href');
      $next_tab_link.tab('show');
      $(window).scrollTop(0);
    };

    $scope.submitProfile = function() {
      var form_data_list = [];
      var $form = $('#profile-edit-form');
      var twitter_handle;

      //filling in elggts and elggtoken
      $.each(miigle.getActionTokenDict(), function(name, value) {
        form_data_list.push({
          name: name,
          value: value,
        });
      });

      //prep $scope.user_dict
      $scope.user_dict.gender_int = parseInt($scope.user_dict.gender_int, 10);
      $scope.user_dict.interests_list = Object
        .items($scope.user_interests_dict)
        .filter(function(tuple) { return tuple[1]; })
        .map(function(tuple) { return parseInt(tuple[0]); });
      $scope.user_dict.work_info_dict.expertise_list = Object
        .items($scope.user_expertise_dict)
        .filter(function(tuple) { return tuple[1]; })
        .map(function(tuple) { return parseInt(tuple[0]); });

      $scope.user_dict.personas_list = Object
        .keys($scope.persona_dict)
        .filter(function(persona) {
          return $scope.persona_dict[persona];
        });

      twitter_handle = $scope.user_dict.contact_info_dict.twitter_handle;
      if (twitter_handle && twitter_handle[0] !== '@') {
        twitter_handle = '@' + twitter_handle;
        $scope.user_dict.contact_info_dict.twitter_handle = twitter_handle;
      }

      form_data_list.push({
        name: 'user_dict_string',
        value: JSON.stringify($scope.user_dict),
      });

      //initting the fileupload that will actually do uploading    
      $form.fileupload({
        url: '/services/api/rest/json?method=users.edit',
        type: 'POST',
        replaceFileInput: false,
        singleFileUploads: false,
        formData: form_data_list,
        dataType: 'json',
        pasteZone: null,
        dropZone: null,

        add: function(e, data) {
          //jQuery File Upload has (what I consider) a bug if you
          //try to upload 0 files. So we just append it a dummy file here
          if (data.files.length == 0) {
            data.files.push('');
          }

          data.submit().complete(function(jqxhr, text_status) {
            var response_dict = JSON.parse(jqxhr.responseText);

            if (response_dict.status == 0) {
              $scope.$apply(function() {
                $scope.user_dict.icon_guid = response_dict.result.icon_guid;
                $scope.setUserDict(response_dict.result);
              });
            } else {
              /*
              What should we do here? This can occur if you POST too much data.

              Each image is restricted to <= 5MB, (we set this in our jQuery file upload settings)
              but we aren't limiting how many images the user can POST. So, the user can end up POSTing a lot of
              data. We can up the limit in php.ini (or whatever it's called) to like 100MB,
              or, we can check here in miigle_ideas.js that the sum total of image file sizes
              doesn't exceed a certain value. We could create a REST endpoint that responds with
              the php.ini max POST setting, and then set the limit in here dynamically using that.
              So, we have some options.
              */
              alert(jqxhr.responseText);
              throw new Error('oh no');
            }

            $scope.$apply(function() {
              $scope.save_button_text = 'Saved';
            });
            $('#ajax-spinner').addClass('hidden');
            $form.fileupload('destroy');
          });
        },

        submit: function(e, data) {
          $scope.save_button_text = 'Saving';
          $('#ajax-spinner').removeClass('hidden');
          return true;
        },
      });

      $form.fileupload('add', {
        files: $('#user-icon-input')[0].files,
      });
    };

    init();
  }
]);

miigle_profile_module.controller('ProfileInterestsController', ['$scope', 
  'ProfileInterestsFactory', 
  function($scope, ProfileInterestsFactory) {
    'use strict';
    
    $scope.profile_interests = [];
    ProfileInterestsFactory.promise.then(function(data) {
      $scope.profile_interests = data
        .map(function(user_dict) {
          user_dict.type = 'user:miigle_user';
          return user_dict;
        });
    });
  }
]);

miigle_profile_module.config(['$routeProvider', 
  function($routeProvider) {
    'use strict';

    $routeProvider
      .when('/user/:username', {
        controller: 'ProfileViewController',
        templateUrl: '/mod/miigle_profile/templates/view_profile.html',
      })
      .when('/user/:username/edit', {
        controller: 'ProfileEditController',
        templateUrl: '/mod/miigle_profile/templates/edit_profile.html',
      })
      .when('/profile-interests', {
        controller: 'ProfileInterestsController',
        templateUrl: '/mod/miigle_profile/templates/profile_interests.html',
      });
  }
]);
