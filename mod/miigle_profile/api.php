<?php
require_once(dirname(dirname(__FILE__)) . '/miigle_locations/api.php');
class MiigleUser extends ElggUser {
	public static function _authorize() {
		gatekeeper();
		if (!validate_action_token(false)) {
			throw new APIException('unauthorized');
		}
	}

	public static function GetBlankUser() {
		return array(
			'guid' => 0,
			'time_created' => 0,
			'icon_guid' => 0,

			'username' => '',
			'first_name' => '',
			'last_name' => '',
			'gender_int' => 0, //0 is male, 1 is female

			'location_dict' => GetBlankMiigleLocation(),

			'contact_info_dict' => array(
				'email_address' => '',
				'phone_number' => '',
				'twitter_handle' => '',
				'facebook_url' => '',
				'googleplus_url' => '',
				'linkedin_url' => '',
			),

			'work_info_dict' => array(
				'current_title' => '',
				'current_employer' => '',
				'current_employer_website' => '',
				'expertise_list' => array(),
			),

			'education_info_dict' => array(
				'school' => '',
				'degree' => '',
				'studied' => '',
				'graduated' => 0
			),

			'tag_line' => '',
			'bio' => '',
			'interests_list' => array(),
			'followers_list' => array(),
			'following_list' => array(),
			'cheering_list' => array(),
			'ideas_list' => array(),
			'personas_list' => array(),

			'hide_profile' => true,
		);
	}

	public static function BootstrapElggUser($user) {
        global $CONFIG;

        $subtype_int = intval(get_subtype_id('user', 'miigle_user'));
		$blank_user = MiigleUser::GetBlankUser();

        //because elgg sucks
        update_data("UPDATE {$CONFIG->dbprefix}entities set subtype=$subtype_int where guid={$user->getGUID()}");

        foreach ($blank_user as $key=>$value) {
            if ($user->get($key) == null) {

                if (gettype($value) == 'array') {
                    $user->set($key, json_encode($value));
                } else if ($key != 'first_name' && $key != 'last_name') {
                    $user->set($key, $value);
                }

                if (!isset($user->fname) || !isset($user->lname)) {
                    $name_parts = explode(' ', $user->name);
                    $user->set('fname', $name_parts[0]);
                    $user->set('lname', $name_parts[1]);
                }
            }
        }
        if ($user->get('cia_map_reference') == null) {
            $user->set('cia_map_reference', json_decode($user->get('location_dict'), true)['cia_map_reference']);
        }

        try {
            $user->save();
        }
        catch (Exception $e) {}
    }

	private static function _GetUser($user, $get_blank=true) {
		static $result_cache;
        global $CONFIG;

		if (!$result_cache) {
			$result_cache = array();
		} else if (isset($result_cache[$user->guid])) {
			return $result_cache[$user->guid];
		}

	    $blank_user = MiigleUser::GetBlankUser();

	    if ($user) {
	        $blank_user['guid'] = intval($user->guid);
	        $blank_user['is_admin'] = ($user->admin) ? true : false;
	        $blank_user['icon_guid'] = intval($user->icon_guid);
	        $blank_user['time_created'] = intval($user->time_created);
	        $blank_user['username'] = strval($user->username);
	        $blank_user['first_name'] = strval($user->fname);
	        $blank_user['last_name'] = strval($user->lname);
	        $blank_user['gender_int'] = intval($user->gender_int);
	        $blank_user['tag_line'] = strval($user->tag_line);
            $blank_user['bio'] = strval($user->bio);
            $blank_user['interests_list'] = json_decode($user->interests_list, true);
            $blank_user['location_dict'] = json_decode($user->location_dict, true);
            $blank_user['contact_info_dict'] = json_decode($user->contact_info_dict, true);

            if (!$user->hide_profile || 
            	AreUsersMutualFollowers(get_loggedin_userid(), $user->guid) || 
                $user->guid == get_loggedin_userid() ||
                $user->isAdmin()) {
		        $blank_user['contact_info_dict']['email_address'] = strval($user->email);
            } else {
                $blank_user['contact_info_dict']['phone_number'] = '';
            }

            $blank_user['education_info_dict'] = json_decode($user->education_info_dict, true);
            $blank_user['hide_profile'] = ($user->hide_profile) ? true : false;
            $blank_user['work_info_dict'] = json_decode($user->work_info_dict, true);

            $current_employer = $blank_user['work_info_dict']['current_employer'];

            /* Here I figure out if their current employer is on Miigle. If so, we replace the URL */
            if (sizeof($current_employer)) {
                $ideas = array_filter(MiigleSearch($current_employer, 'ideas'), function($idea) use ($current_employer) {
                    return trim($idea['title']) === trim($current_employer);
                });

                if (sizeof($ideas) > 0) {
                    $idea = array_pop($ideas);
                    $blank_user['work_info_dict']['current_employer_website'] = sprintf('%spg/miigle#/idea/%d', $CONFIG->wwwroot, $idea['guid']);
                }
            }

            $blank_user['followers_list'] = $user->getFollowerGUIDs();
            $blank_user['following_list'] = $user->getFollowingGUIDs();
            $blank_user['cheering_list'] = $user->getCheeringGUIDs();
            $blank_user['ideas_list'] = $user->getIdeaGUIDs();
            $blank_user['personas_list'] = json_decode($user->personas_list, true);


            $result_cache[$user->guid] = $blank_user;
	    } else if (!$get_blank) {
	        return;
	    }

	    return $blank_user;
	}

	public static function GetUserFromGUID($user_guid, $get_blank=true) {
	    $user = get_entity($user_guid);
	    return MiigleUser::_GetUser($user, $get_blank);
	}

	public static function GetUserFromUsername($username) {
		$user = get_user_by_username($username);
		return MiigleUser::_GetUser($user, false);
	}

	public static function EditUser($user_dict) {
		/*
		TODO: input validation
		*/

        //error checking location_dict
        //let's just re-look-this-up. it should already be cached,
        //and prevents the user messing around with their location client side
        $location_dict = MiigleLocationLookup($user_dict['location_dict']['fqcn']);
        if (!$location_dict) {
        	$location_dict = GetBlankMiigleLocation();
        }

		$user_guid = intval($user_dict['guid']);
		if ($user_guid != get_loggedin_userid()) {
			throw new APIException('unauthorized');
		}

		$user_entity = get_entity($user_guid);

		$user_entity->hide_profile = (bool)$user_dict['hide_profile'];
		$user_entity->fname = strval($user_dict['first_name']);
		$user_entity->lname = strval($user_dict['last_name']);
		$user_entity->tag_line = strval($user_dict['tag_line']);
		$user_entity->gender_int = intval($user_dict['gender_int']);
		$user_entity->bio = strval($user_dict['bio']);

		//$user_entity->username = strval($user_dict['username']); should we allow this?
		//$user_entity->email = strval($user_dict['contact_info_dict']['email_address']); resend email confirmation?
		unset($user_dict['contact_info_dict']['email_address']);

		$user_entity->contact_info_dict = json_encode($user_dict['contact_info_dict']);
		$user_entity->work_info_dict = json_encode($user_dict['work_info_dict']);
		$user_entity->education_info_dict = json_encode($user_dict['education_info_dict']);
		$user_entity->location_dict = json_encode($location_dict);
		$user_entity->cia_map_reference = strval($location_dict['cia_map_reference']);
		$user_entity->interests_list = json_encode($user_dict['interests_list']);
		$user_entity->personas_list = json_encode($user_dict['personas_list']);

		$user_entity->save();

		if (isset($user_dict['user_icon_filename'])) {
			$current_icon = get_entity($user_entity->icon_guid);
			if ($current_icon) {
				$current_icon->delete();
			}

			$user_entity->icon_guid = CreateMiigleImage(file_get_contents($user_dict['user_icon_filename']));
		}

		return MiigleUser::GetUserFromGUID($user_guid);
	}

	private function _getIdeas() {
		$ideas_list = elgg_get_entities(array(
			'type' => 'object',
			'subtype' => 'idea',
			'owner_guid' => $this->getGUID(),
		));

		if (!$ideas_list) {
			return array();
		} else {
			return $ideas_list;
		}
	}

	private function _getFollowers() {
        $followers_list = elgg_get_entities_from_relationship(array(
            'relationship'         => 'following_user',
            'relationship_guid'    => $this->getGUID(),
            'inverse_relationship' => true,
            'limit' => 999999,
        ));

        if (!$followers_list) {
            return array();
        } else {
            return $followers_list;
        }
	}

	private function _getFollowing() {
        $following_list = elgg_get_entities_from_relationship(array(
            'relationship'         => 'following_user',
            'relationship_guid'    => $this->getGUID(),
            'inverse_relationship' => false,
            'limit' => 999999,
        ));

        if (!$following_list) {
            return array();
        } else {
            return $following_list;
        }
	}

	private function _getCheering() {
        $cheering_list = elgg_get_entities_from_relationship(array(
            'relationship'         => 'cheering_idea',
            'relationship_guid'    => $this->getGUID(),
            'inverse_relationship' => false,
            'limit' => 999999,
        ));

        if (!$cheering_list) {
        	return array();
        } else {
	        return $cheering_list;
        }
	}

	public function getCheeringGUIDs() {
		return array_map('GetObjectGUID', $this->_getCheering());
	}

	public function getFollowerGUIDs() {
		return array_map('GetObjectGUID', $this->_getFollowers());
	}

	public function getFollowingGUIDs() {
		return array_map('GetObjectGUID', $this->_getFollowing());
	}

    public static function GetUserFollowerGUIDs($user_guid) {
        $result = get_data("SELECT * FROM miigle_entity_relationships WHERE relationship='following_user' AND guid_one=$user_guid");
        if (!$result) {
            return array();
        } else {
            return array_map(function($result_object) {
                return $result_object->guid_two;
            }, $result);
        }
    }
    public static function GetUserFollowingGUIDs($user_guid) {
        $result = get_data("SELECT * FROM miigle_entity_relationships WHERE relationship='following_user' AND guid_two=$user_guid");
        if (!$result) {
            return array();
        } else {
            return array_map(function($result_object) {
                return $result_object->guid_one;
            }, $result);
        }
    }

	public function getIdeaGUIDs() {
		return array_map('GetObjectGUID', $this->_getIdeas());
	}

	public function toDict() {
		return MiigleUser::_GetUser($this, false);
	}

	/*
	public function isCheeringIdea($idea_guid) {
		return (check_entity_relationship($this->getGUID(), 'cheering_idea', $idea_guid) != false); //ghetto boolval()
	}
	*/

	//follower follows the leader
	public static function FollowUser($follower_guid, $leader_guid) {
		if ($follower_guid == $leader_guid) {
			return false;
		}

		$follower = get_entity($follower_guid);
		$leader = get_entity($leader_guid);

		if (!$follower || !$leader) {
			return false;
		}

        if (check_entity_relationship($follower_guid, 'following_user', $leader_guid) == false) {
            $follower->addRelationship($leader_guid, 'following_user'); 
            CreateFeedItem($follower_guid, 'user:follow::user', '[]', $leader_guid);

            MiigleNotification::CreateNotification($leader_guid, $follower_guid, 
                                                   $leader_guid, 'follow');
        }

        return true;
	}
	public static function UnfollowUser($follower_guid, $leader_guid) {
		if ($follower_guid == $leader_guid) {
			return false;
		}

		$follower = get_entity($follower_guid);
		$leader = get_entity($leader_guid);

		if (!$follower || !$leader) {
			return false;
		}

        remove_entity_relationship($follower_guid, 'following_user', $leader_guid);
        return true;
	}

	public static function UpdateStatus($user_guid, $new_status) {
		$user = get_entity($user_guid);
		if (!$user || sizeof($new_status) == 0) {
			return false;
		}

		$new_status_object = new MiigleUserStatus();
        $new_status_object->set('title', $new_status);
        if (!$new_status_object->save()) {
        	return false;
        }

        return CreateFeedItem($user_guid, 'user:update::status', '[]', $new_status_object->getGUID());
	}

    public static function GetMatchingUsers($interests_list, $expertise_list,
                                            $filter_guids_list) {
        $matching_guids_list = SearchMiigleUsersByInterestsAndExpertise(
                    $interests_list, $expertise_list);
        $guids_list = __::difference($matching_guids_list, $filter_guids_list);

        $GetUserExpertiseAndInterests = function($user_guid) {
            $user_dict = MiigleUser::GetUserFromGUID($user_guid);
            $interests_list = $user_dict['interests_list'];
            $expertise_list = $user_dict['work_info_dict']['expertise_list'];

            return array(
                'guid' => $user_guid,
                'interests_list' => $interests_list,
                'expertise_list' => $expertise_list,
            );
        };

        return __::map($guids_list, $GetUserExpertiseAndInterests);
    }

    public static function GetProfileInterests($user_guid) {
        if ($user_guid == 0) {
            return array();
        }

        $user_dict = MiigleUser::GetUserFromGUID($user_guid);

        $interests_list = $user_dict['interests_list'];
        $expertise_list = $user_dict['work_info_dict']['expertise_list'];

        $filter_user_guids = array_merge(array($user_guid), 
                                         $user_dict['following_list']);

        $users = MiigleUser::GetMatchingUsers($interests_list, $expertise_list,
                $filter_user_guids);

        $SortProfileInterests = function($user_a, $user_b) use ($interests_list,
                $expertise_list) {
            $user_a_shared_expertise = __::intersection($expertise_list,
                    $user_a['expertise_list']);
            $user_b_shared_expertise = __::intersection($expertise_list,
                    $user_b['expertise_list']);

            $user_a_shared_interests = __::intersection($interests_list,
                    $user_a['interests_list']);
            $user_b_shared_interests = __::intersection($interests_list,
                    $user_b['interests_list']);

            $user_a_score = (sizeof($user_a_shared_expertise) + 
                             sizeof($user_a_shared_interests));
            $user_b_score = (sizeof($user_b_shared_expertise) + 
                             sizeof($user_b_shared_interests));

            return $user_b_score - $user_a_score;
        };

        usort($users, $SortProfileInterests);

        $guids = __::pluck($users, 'guid');
        return __::map($guids, 'MiigleUser::GetUserFromGUID');
    }
}

class MiigleUserStatus extends ElggObject {
	public function __construct($guid=null) {
		parent::__construct($guid);

        if (!$guid) {
        	$this->set('subtype', 'miigle_user_status');
        	$this->set('owner_guid', get_loggedin_userid());
        	$this->set('access_id', ACCESS_PUBLIC);
        }
    }

    public function toDict() {
        return array(
            'guid'   => $this->get('guid'),
            'status' => $this->get('title'),
        );
    }
}

function UpdateStatusProxy($new_status) {
	MiigleUser::_authorize();
	return MiigleUser::UpdateStatus(get_loggedin_userid(), $new_status);
}

function FollowUserProxy($other_user_guid) {
	MiigleUser::_authorize();
	return MiigleUser::FollowUser(get_loggedin_userid(), $other_user_guid);
}
function UnfollowUserProxy($other_user_guid) {
	MiigleUser::_authorize();
	return MiigleUser::UnfollowUser(get_loggedin_userid(), $other_user_guid);
}

function EditUserProxy($user_dict_string) {
	MiigleUser::_authorize();
	$user_json_dict = json_decode($user_dict_string, true);

	if (isset($_FILES['user_icon']) && $_FILES['user_icon']['error'] == 0 && $_FILES['user_icon']['size'] < 5000000) {
		$user_json_dict['user_icon_filename'] = $_FILES['user_icon']['tmp_name'];
	}

	return MiigleUser::EditUser($user_json_dict);
}
expose_function('users.edit', 'EditUserProxy',
	array(
		'user_dict_string' => array('type'=>'string', 'required'=>true),
	),
	'Edit a user',
	'POST',
	false,
	false
);
expose_function('users.get.guid', 'MiigleUser::GetUserFromGUID', 
    array(
        'user_guid' => array('type'=>'int', 'required'=>true),
    ),
    'Get a user',
    'GET',
    false,
    false
);
expose_function('users.get.username', 'MiigleUser::GetUserFromUsername', 
    array(
    	'username'  => array('type'=>'string', 'required'=>true),
    ),
    'Get a user',
    'GET',
    false,
    false
);
expose_function('users.follow', 'FollowUserProxy',
    array(
        'user_guid' => array('type'=>'int', 'required'=>true),
    ),
    'Follow a user',
    'POST',
    false,
    false
);
expose_function('users.unfollow', 'UnfollowUserProxy',
    array(
        'user_guid' => array('type'=>'int', 'required'=>true),
    ),
    'Unfollow a user',
    'POST',
    false,
    false
);
expose_function('user.update_status', 'UpdateStatusProxy',
    array(
        'status' => array('type'=>'string', 'required'=>true),
    ),
    'Post a new status',
    'POST',
    false,
    false
);

function GetUserDisqusRemoteAuthString($user, $secret_key) {
	if (!$user) {
		return false;
	}

	$host = $_SERVER['HTTP_HOST'];

	$message_dict = array(
		'id' => sprintf('%s-%s', $host, strval($user->guid)),
		'username' => $user->username,
		'email' => $user->email,
		'avatar' => sprintf('http://%s/pg/image/%d', $host, $user->icon_guid),
		'url' => sprintf('http://%s/pg/miigle#/user/%s', $host, $user->username),
	);
	$message_string = base64_encode(json_encode($message_dict));
	$timestamp = time();
	$full_message_string = sprintf('%s %d', $message_string, $timestamp);
	$hmac_message = hash_hmac('sha1', $full_message_string, $secret_key);

	return sprintf('%s %s %s', $message_string, $hmac_message, $timestamp);
}

function GetProfileInterestsProxy() {
    return MiigleUser::GetProfileInterests(get_loggedin_userid());
}

expose_function('user.profile_interests', 'GetProfileInterestsProxy',
    array(
    ),
    '',
    'POST',
    false,
    false
);
