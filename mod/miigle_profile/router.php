<?php
/**
 * Profile page handler
 *
 * @param array $page Array of page elements, forwarded by the page handling mechanism
 * @return true|false Depending on success
 */
function MiigleProfilePageHandler($page) {
    global $CONFIG;
    
    if (isset($page[0])) {
        $username = $page[0];
        forward('pg/miigle#/user/' . $username);

    } else {
    	forward();
    }

    include(dirname(__FILE__) . '/routes/view_route.php');
}