<?php

/**
 * Elgg thewire: add shout action
 *
 * @package Elggthewire
 */

// Make sure we're logged in (send us to the front page if not)
if (!isloggedin()) forward();

// Get input data
$body = get_input('note');
$access_id = (int)get_default_access();
if ($access_id == ACCESS_PRIVATE) {
    $access_id = ACCESS_LOGGED_IN; // Private wire messages are pointless
}
$method = get_input('method');
$parent = (int)get_input('parent', 0);
if (!$parent) {
    $parent = 0;
}
// Make sure the body isn't blank
if (empty($body)) {
    register_error(elgg_echo("thewire:blank"));
    forward("mod/thewire/add.php");
}

global $SESSION;

// Initialise a new ElggObject
$thewire = new ElggObject();

// Tell the system it's a thewire post
$thewire->subtype = "thewire";

// Set its owner to the current user
$thewire->owner_guid = get_loggedin_userid();

// For now, set its access to public (we'll add an access dropdown shortly)
$thewire->access_id = $access_id;

// Set its description appropriately
$thewire->description = elgg_substr(strip_tags($body), 0, 160);

// add some metadata
$thewire->method = $method; //method, e.g. via site, sms etc
$thewire->parent = $parent; //used if the note is a reply
$thewire->is_preview = false;
$preview_link = urldecode(get_input('url'));
if($preview_link){
    $thewire->preview_link = $preview_link;
    $thewire->preview_title = urldecode(get_input('title'));
    $thewire->preview_description = urldecode(get_input('description'));
    $thewire->preview_thumb = urldecode(get_input('thumbnail_url'));
    $thewire->is_preview = true;
}


//save
$save = $thewire->save();

if ($save) {
    add_to_river('river/object/thewire/create','create',$SESSION['user']->guid,$thewire->guid);

    // tweet
    $params = array(
        'plugin' => 'thewire',
        'message' => $thewire->description
    );

    trigger_plugin_hook('tweet', 'twitter_service', $params);
    
    $obj = array('wire'=>$thewire);
    $obj['wire']['is_preview'] = $thewire->is_preview;
    if($thewire->is_preview){
        $obj['wire']['preview_title'] = $thewire->preview_title;
        $obj['wire']['preview_description'] = $thewire->preview_description;
        $obj['wire']['preview_thumb'] = $thewire->preview_thumb;
        $obj['wire']['preview_link'] = $thewire->preview_link;
    }
    return json_encode($obj);
} else {
    return json_encode(elgg_echo("thewire:error"));
}

?>