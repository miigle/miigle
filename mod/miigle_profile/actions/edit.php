<?php

    /**
     * Miigle profile plugin edit action
     *
     * @package MiigleProfile
     */

    // Load configuration
    global $CONFIG;
    
    //check access
    gatekeeper();
        
    //get logged in user and form input
    $user = get_loggedin_user();    
    $postuser = get_input('user');
    
    //set user fields    
    foreach($postuser as $key => $value){
        $user->$key = $value;
    }
    
    //make sure there's a user
    if (!$user) {
        register_error('profile:noaccess');
        system_message('profile:noaccess');
    } else {
        //make sure user has permission to edit
        if ($user->canEdit()){
                
            // Save user
            $user->save();

            // Now see if we have a file icon
            try {
                if ((isset($_FILES['profileicon'])) && (substr_count($_FILES['profileicon']['type'],'image/'))) {
                    
                    $topbar = get_resized_image_from_uploaded_file('profileicon',16,16, true, true);    
                    $tiny = get_resized_image_from_uploaded_file('profileicon',25,25, true, true);
                    $small = get_resized_image_from_uploaded_file('profileicon',40,40, true, true);
                    $medium = get_resized_image_from_uploaded_file('profileicon',100,100, true, true);
                    $large = get_resized_image_from_uploaded_file('profileicon',200,200);
                    $master = get_resized_image_from_uploaded_file('profileicon',300,300);
                    
                    if ($small !== false
                        && $medium !== false
                        && $large !== false
                        && $tiny !== false) {
                        $filehandler = new ElggFile();
                        $filehandler->owner_guid = $user->getGUID();
                        $filehandler->setFilename("profile/" . $user->guid . "large.jpg");
                        $filehandler->open("write");
                        $filehandler->write($large);
                        $filehandler->close();
                        $filehandler->setFilename("profile/" . $user->guid . "medium.jpg");
                        $filehandler->open("write");
                        $filehandler->write($medium);
                        $filehandler->close();
                        $filehandler->setFilename("profile/" . $user->guid . "small.jpg");
                        $filehandler->open("write");
                        $filehandler->write($small);
                        $filehandler->close();
                        $filehandler->setFilename("profile/" . $user->guid . "tiny.jpg");
                        $filehandler->open("write");
                        $filehandler->write($tiny);
                        $filehandler->close();
                        $filehandler->setFilename("profile/" . $user->guid . "topbar.jpg");
                        $filehandler->open("write");
                        $filehandler->write($topbar);
                        $filehandler->close();
                        $filehandler->setFilename("profile/" . $user->guid . "master.jpg");
                        $filehandler->open("write");
                        $filehandler->write($master);
                        $filehandler->close();
                        
                        $user->icontime = time();
                    }
                }
            } catch(Exception $e) {
                system_message(elgg_echo("profile:noaccess"));
            }
            
            // Notify of profile update
            trigger_elgg_event('profileupdate',$user->type,$user);
            
            //add to river
            add_to_river('river/user/default/profileupdate','update',$user->guid,$user->guid);

            system_message(elgg_echo("profile:saved"));
            
            // Forward to the profile edit page
            forward($vars["url"]."pg/profile/".$user->username."/edit");
        }
        else {
            // If we can't, display an error
            system_message(elgg_echo("profile:noaccess"));
        }
    }
    
?>
