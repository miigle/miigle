<?php

    /*** custom_dashboard for Elgg
    *    
    *    (c) Rahul Ranjan(gx.rahul@gmail.com)
    *    CUSTOM DASHBOARD INDEX PAGE
    */ 

    // Get the Elgg engine
    require_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

    //gatekeeper();
    
    //set page owner entity
    $page_owner = page_owner_entity();
    if ($page_owner === false || is_null($page_owner)) {
        $page_owner = $_SESSION['user'];
        set_page_owner($_SESSION['guid']);
    }
    
    set_context('dashboard');
    $title="Welcome to Dashboard";
    $body="";
    $body = elgg_view_layout('dashboard_three_column', $title , $body);

    page_draw($title,    $body);
?>