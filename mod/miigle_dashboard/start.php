<?php
    
    /**
    * Miigle Dashboard for Elgg
    * (c) Rahul Ranjan (gx.rahul@gmail.com)
    *
    * START PAGE
    */

    //plugin init
    function custom_dashboard_init(){
        return;
        register_page_handler('dashboard', 'custom_dashboard_index');
        
        // Replace the default index page
        register_plugin_hook('index','system','custom_dashboard_handle_index');
        
        elgg_extend_view('metatags', 'custom_dashboard/js');
        elgg_extend_view('css','custom_dashboard/css');
        elgg_extend_view('canvas/layouts', 'canvas/layouts/dashboard_three_column');
    }

    function custom_dashboard_index($page){
        if(isset($page[0]) && !empty($page[0])){
            if(is_numeric($page[0])){
                set_input("page_ctr", intval($page[0]));
            }else{
                set_input("username", $page[0]);
            }
        }
        if (!include_once(dirname(__FILE__) . "/index.php")){
            return false;
        }
        return true;
    }

    function custom_dashboard_handle_index($hook, $type, $return, $params){
        if(isloggedin()){
            forward("pg/miigle");
        }
        return true;
    }
    
    //register_elgg_event_handler('init','system','custom_dashboard_init');
?>
