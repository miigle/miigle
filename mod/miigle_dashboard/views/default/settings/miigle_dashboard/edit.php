<?php
    /**
     * @author Rahul Ranjan
     * @copyright Rahul Ranjan (gx.rahul@gmail.com)
     * 
     * custom_dashboard plugin settings page
     * 
     */
?>
<?php 
    $param_val = (isset($vars['entity']->max_ideas_per_page) && !empty($vars['entity']->max_ideas_per_page))?$vars['entity']->max_ideas_per_page:'3';
?>
<p>
  <?php echo elgg_echo('custom_dashboard:settings:max_ideas_per_page'); ?>
  <input type="text" name="params[max_ideas_per_page]" value="<?php echo $param_val?>" />
</p>