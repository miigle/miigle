<?php
/**
 *    Three column layout for custom_dashboard
 *    @author Rahul Ranjan
 *    (c)20011 Rahul Ranjan (gx.rahul@gmail.com)
 */

    $loggedin_userid = get_loggedin_userid();
    $loggedin_user = get_loggedin_user();
    $visit_user = get_input("username");
    
    //dashboard guest preview
    $preview_mode = get_input("preview", "");

    if(isset($visit_user) && !empty($visit_user)){
        $user = get_user_by_username($visit_user);
    }else{
        $user = get_loggedin_user();
    }

     $icon = elgg_view("profile/icon", array(
                                    'entity' => $user,
                                    'size' => 'large',
                                    'override' => true
                          ));

?>
<!-- page specific styles -->
<style type="text/css">
    #page_wrapper{
        /*width:960px;*/
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $("#recent-ideas-toggle-btn").click(function(){
            $("#recent-ideas-container").slideToggle('medium');
            if($(this).hasClass('expand')){
                $(this).removeClass('expand');
                $(this).addClass('collapse');
            }else{
                $(this).removeClass('collapse');
                $(this).addClass('expand');
            }
        });

        $("#idea-stats-toggle-btn").click(function(){
            $(".stats-row").slideToggle('medium');
            if($(this).hasClass('expand')){
                $(this).removeClass('expand');
                $(this).addClass('collapse');
            }else{
                $(this).removeClass('collapse');
                $(this).addClass('expand');
            }
        });

        $("#Share-Tab-Btn").click(function(){
            $('#tabs li.selected').attr('class','');
            $(this).parent().addClass('selected');
            //show content
            $(".tab-content").hide();
            $("#share-tab-content").show();
            return false;
        });

        $("#Cheer-Tab-Btn").click(function(){
            $('#tabs li.selected').attr('class','');
            $(this).parent().addClass('selected');
            //show content
            $(".tab-content").hide();
            $("#cheer-tab-content").show();
            return false;
        });

        $("#Foster-Tab-Btn").click(function(){
            $('#tabs li.selected').attr('class','');
            $(this).parent().addClass('selected');
            //show content
            $(".tab-content").hide();
            $("#foster-tab-content").show();
            return false;
        });

        //initialise
        $("#recent-ideas-toggle-btn").click();
        $("#idea-stats-toggle-btn").click();
    });
</script>
<!-- main content -->
<div id="three_column">
    <div id="first-col">
        <div id="welcome-box">
            <div id="profile-icon-box">
                <label class="view-profile-hint"><a title="View <?php echo $user->name;?>'s Profile" href="<?php echo $vars['url']?>pg/profile/<?php echo $user->username;?>/view">Click to view profile</a></label>            
                <a title="Go to <?php echo $user->name;?>'s Dashboard" id="profile_picture" href="<?php echo $vars['url']?>pg/dashboard/<?php echo $user->username;?>">
                    <?php echo $icon;?>
                </a>
            </div>
            <div id="welcome-links">
            <?php 
                if($user->guid == $loggedin_user->guid && $preview_mode!="guest"){
            ?>
                <a href="<?php echo $vars['url']?>pg/profile/<?php echo $user->username;?>/edit">EDIT PROFILE</a>
            <?php 
                }else{
                    echo elgg_view("input/miigle_msg_me_btn",array());
                }
            ?>
            </div>
        </div>
        <div id="bio-box">
            <h2>Bio.</h2>
            <div class="bio-text"><?php echo $user->bio;?></div>
            <br />
            <?php if($user->guid != $loggedin_userid || $preview_mode=="guest"){
                if(!is_user_mentor_for_visitor($user->guid) && !(is_mentor_request_sent($user->guid))){
            ?>
                <input type="image" width="180px" src="<?php echo $vars['url']?>mod/miigle_theme/graphics/btn-request-mentor.png" onclick="window.location.href='<?php echo get_mentor_request_url(0, $user->guid)?>'" />
            <?php 
                }
            }
            ?>
        </div>
        <div id="social-box">
            <h2 class="popout">Social Networks.</h2>
            <div class="social-content">
                <?php 
                    $social_links = json_decode($user->social_links,true);
                    $social_links_str = "";
                    
                    foreach($social_links as $social_link_key=>$social_link_url){
                        if(!empty($social_link_url)){
                            $social_links_str .= '<li><a href="'.$social_link_url.'" target="_blank">'.ucwords($social_link_key).'</a></li>';
                        }
                    }
                    
                    if(!empty($social_links_str)){
                        echo '<ul>'.$social_links_str.'</ul>';
                    }else{
                        echo '<span style="font-style:italic;font-weight:normal;color:#666;">None</span>';
                    }
                ?>
            </div>
        </div>
        <div id="interest-box">
            <h2>Interests.</h2>
            <div class="interest-content">
                <?php 
                    $str_interests = "";
                    $interests = json_decode($user->interests,true);
                    foreach($interests as $interest){
                        $interest_arr = json_decode($interest, true);
                        $str_interests .= '<li>'.ucfirst($interest_arr['interest_title']).'</li>';
                    }
                    
                    if(!empty($str_interests)){
                        echo '<ul>'.$str_interests.'</ul>';
                    }else{
                        echo '<span style="font-style:italic;font-weight:normal;color:#666;">None</span>';
                    }
                ?>
            </div>
        </div>
        <div id="investors-box">
            <h2>Investors.</h2>
            <div class="investors-list">
                <?php 
                    $arrInvestors = get_users_investors($user);
                    $viewtype = "compact";
                    $baseurl = "{$vars['url']}pg/profile/{$user->username}/investors/$viewtype";
                    echo elgg_view("users/list", array("entities" => $arrInvestors, 
                                                    "baseurl" => $baseurl,
                                                    "type" => $viewtype));
                ?>
            </div>
        </div>
        <div id="mentors-box">
            <h2>Mentors.</h2>
            <div class="mentors-list">
            <?php 
                    //$arrMentors = get_mentors_for_user($user->guid);
                    $arrMentors = array();
                    $viewtype = "compact";
                    $baseurl = "{$vars['url']}pg/mentor/{$user->username}/all/$viewtype";
                    echo elgg_view("users/list", array("entities" => $arrMentors, 
                                                    "baseurl" => $baseurl,
                                                    "type" => $viewtype));
            ?>
            </div>
        </div>
        <div id="followers-box">
            <h2>Followers.</h2>
            <div class="followers-list">
            <?php 
                    $arrFollowers = get_user_followers($user->guid);
                    $viewtype = "compact";
                    $baseurl = "{$vars['url']}pg/follow/followers/{$user->username}/$viewtype";
                    echo elgg_view("users/list", array("entities" => $arrFollowers, 
                                                    "baseurl" => $baseurl,
                                                    "type" => $viewtype));
            ?>
            </div>
        </div>
        <div id="following-box">
            <h2>Following.</h2>
            <div class="following-list">
            <?php 
                    $arrFollowing = get_user_followers($user->guid);
                    $viewtype = "compact";
                    $baseurl = "{$vars['url']}pg/follow/following/{$user->username}/$viewtype";
                    echo elgg_view("users/list", array("entities" => $arrFollowing, 
                                                    "baseurl" => $baseurl,
                                                    "type" => $viewtype));
            ?>
            </div>
        </div>
    </div>
    <div id="right-cols-container">
        <div id="second-col">
            <div class="profile-dashboard-top">
                <div class="col-title" style="float:left;">
                    <h1><?php echo ucwords($user->name);?></h1>
                    <?php /*?><div style="font-size:18px;">Net worth: <span style="font-weight:bold;">$30,000</span></div><?php */?>
                    <?php 
                        if($preview_mode != "guest" && $loggedin_userid == $user->guid){
                    ?>
                    <div><a href="<?php echo $vars['url']?>pg/dashboard?preview=guest" target="_blank">Preview your dashboard.</a></div>
                    <?php 
                        }
                    ?>
                </div>
                <div style="width:50px;height:50px;margin-left:10px;float:right;background:url('<?php echo $vars["url"]?>mod/miigle_theme/graphics/alpha-badge.png') no-repeat scroll left top transparent;">&nbsp;</div>
                <div class="clearfloat"></div>
            </div>
            <div id="profile-byline">
                <?php 
                    //Show the current or latest position at work
                    $arr_employers = json_decode($user->employers);
                    $latest_emp = array();
                    $present_job = false;
                    foreach($arr_employers as $employer_json){
                        $arr_employer = json_decode($employer_json,true);
                        if(count($arr_employer['is_current_job'])>0){
                            $present_job = true;
                            //this is the latest job so skip everything else
                            $latest_emp = $arr_employer;
                            break;
                        }
                        if(count($latest_emp)>0){
                            if($latest_emp["job_end_year"] == $arr_employer["job_end_year"]){
                                //year matched, check month
                                if($latest_emp["job_end_month"] < $arr_employer["job_end_month"]){
                                    //update latest job
                                    $latest_emp = $arr_employer;
                                }
                            }elseif($latest_emp["job_end_year"] < $arr_employer["job_end_year"]){
                                //update latest job
                                $latest_emp = $arr_employer;
                            }else{
                                //nothing to do
                            }
                        }else{
                            $latest_emp = $arr_employer;
                        }
                    }
                    
                    //if we have a latest job to show
                    if(count($latest_emp)>0){
                        $position = "";
                        if(isset($latest_emp["job_position"]) && !empty($latest_emp["job_position"])){
                            $position .= ucwords($latest_emp["job_position"]);
                        }
                        if(isset($latest_emp["employer_name"]) && !empty($latest_emp["employer_name"])){
                            $employer_name = " @ " . $latest_emp["employer_name"];
                        }
                        /***** For job location, but we dont want this ***********
                        if(isset($latest_emp["job_city"]) && !empty($latest_emp["job_city"])){
                            $job_city = " in " . ucwords($latest_emp["job_city"]);
                        }
                        */
                        if(!(isset($latest_emp["job_position"]) && !empty($latest_emp["job_position"])) 
                            && !(isset($latest_emp["employer_name"]) && !empty($latest_emp["employer_name"])) 
                            && !(isset($latest_emp["job_city"]) && !empty($latest_emp["job_city"]))){
                            $position = "";
                        }
                    }
                    
                    //get the latest education
                    //first get the college, if nothing found then school
                    $arr_colleges = json_decode($user->colleges, true);
                    $latest_edu = array();
                    foreach($arr_colleges as $college_json){
                        $arr_college = json_decode($college_json,true);
                        if(count($latest_edu)>0){
                            if($latest_edu["college_end_year"] == $arr_college["college_end_year"]){
                                //year matched, check month
                                if($latest_edu["college_end_month"] < $arr_college["college_end_month"]){
                                    //update latest education
                                    $latest_edu = $arr_college;
                                }
                            }elseif($latest_edu["college_end_year"] < $arr_college["college_end_year"]){
                                //update latest education
                                $latest_edu = $arr_college;
                            }else{
                                //nothing to do
                            }
                        }else{
                            $latest_edu = $arr_college;
                        }
                    }
                    
                    //if we have a latest education to show
                    if(count($latest_edu)>0){
                        $edu_institute_name = $latest_edu["college_name"];
                    }else{
                        //get the schools
                        $arr_schools = json_decode($user->schools, true);
                        $latest_edu = array();
                        foreach($arr_schools as $school_json){
                            $arr_school = json_decode($school_json,true);
                            if(count($latest_edu)>0){
                                if($latest_edu["school_end_year"] == $arr_school["school_end_year"]){
                                    //year matched, check month
                                    if($latest_edu["school_end_month"] < $arr_school["school_end_month"]){
                                        //update latest education
                                        $latest_edu = $arr_school;
                                    }
                                }elseif($latest_edu["school_end_year"] < $arr_school["school_end_year"]){
                                    //update latest education
                                    $latest_edu = $arr_school;
                                }else{
                                    //nothing to do
                                }
                            }else{
                                $latest_edu = $arr_school;
                            }
                        }
                        
                        //check if we got something now or not
                        if(count($latest_edu)>0){
                            $edu_institute_name = $latest_edu["college_name"];
                        }
                    }
                    
                    //get current location
                    if(isset($user->current_city) && !empty($user->current_city)){
                        $current_location = "Lives in ".ucwords($user->current_city);
                    }else{
                        $current_location = "";
                    }
                    
                    //get users networks
                    //$user_networks = get_user_networks_csv($user);
                    $user_networks = array();
                    
                    //status message
                    if(isset($user->statusmsg) && !empty($user->statusmsg)){
                        $post_time = $user->laststatusupdate_time;
                        $cur_time = time();
                        $timeago = $cur_time - $post_time;
                        $minsago = $timeago/60;
                        $hoursago = $timeago/3600;
                        $daysago = $timeago/(24*3600);
                        if($daysago>=1){
                            $timeago = floor($daysago)." days";
                        }else if($hoursago>=1){
                            $timeago = floor($hoursago)." hours";
                        }else if($minsago>=1){
                            $timeago = floor($minsago)." mins";
                        }else{
                            $timeago = "0 mins";
                        }
                        $statusmsg = $user->statusmsg ." - $timeago ago";
                    }else{
                        $statusmsg = "No updates...";
                    }
                ?>
                <?php if(!empty($position)){?>
                    <strong><?php echo $position;?></strong> <?php echo $employer_name;?><br/>
                <?php } if(!empty($current_location)){
                    echo $current_location.'<br/>';
                }?>
                <?php if(!empty($edu_institute_name)){?>
                    <strong>Education: </strong><?php echo $edu_institute_name;?><br/>
                <?php }?>
                <?php if(!empty($user_networks)){?>
                <strong>Networks: </strong><?php echo $user_networks;?><br/>
                <?php }?>
                <strong>MIIGLER Since: </strong><?php echo date("F j, Y", $user->time_created);?><br/>
                <strong>Updates: </strong><?php echo $statusmsg;?>
            </div>
            <div id="idea-social-box">
            <?php 
                $arrfollowers = get_user_followers($user->guid);
                //print_r($arrfollowers);exit;
                if(is_array($arrfollowers)){
                    $count_followers = count($arrfollowers);
                }else{
                    $count_followers = 0;
                }
            ?>
                <div class="followers">
                    <input type="button" class="followers-btn" value="" onclick="window.location.href='<?php echo get_user_follow_url($user->guid)?>'"/>
                    <div style="display:table-row;height:25px;">
                        <div class="profile-social-button-label"><strong><?php echo $count_followers;?></strong> followers</div>
                    </div>
                    <div class="clearfloat"></div>
                </div>
                <div class="invite">
                    <div class='st_sharethis_custom1' st_url="<?php echo $user->getURL();?>">Share This</div>
                </div>
                <div class="likes">
                    <?php echo elgg_view("miigle_like/miigle_like_btn", array("entity" => $user));?>
                </div>
                <div class="clearfloat"></div>
            </div>
            <?php 
                if($loggedin_userid == $user->guid && $preview_mode!="guest"){
            ?>
            <div id="new-status-box">
                <?php echo elgg_view("ideas/forms/statuspost", $vars);?>
            </div>
            <?php 
                }
            ?>
            <?php 
                if($loggedin_userid == $user->guid && $preview_mode!="guest"){
            ?>
            <div style="margin-top:20px;">
                <?php echo elgg_view("ideas/forms/_new");?>
            </div>
            <?php /*?><a style="color:#333;text-decoration:none;" href="<?php echo $vars['url'];?>pg/ideas/newidea"><div id="share-idea-bar">SHARE A NEW IDEA</div></a><?php */?>
            <?php }?>
            <?php 
                $owner = page_owner_entity();
                $loggedin_user = get_loggedin_user();
                
                if($loggedin_user->guid == $owner->guid && $preview_mode!="guest"){
                    $limit = get_input("limit", 5);
                    $offset = get_input("offset", 0);
                    $results = get_matching_ideas($owner, $limit, $offset);
                    
                    $params = array(
                                    "entities"=>$results['entities'],
                                    "offset"=>$offset,
                                    "limit"=>$limit,
                                    "count"=>$results["count"],
                                    "baseurl"=>$_SERVER['REQUEST_URI'],
                                    "pagination"=>true
                                );
            ?>
            <div id="recent-ideas-box">
                <?php echo elgg_view("ideas/interest_matching", $params);?>
            </div>
            <div id="idea-stats-box">
                <div class="header">
                    <h2>Idea Stats.<span style="padding:0 0 2px 10px;font-size:12px;font-weight:normal;">(Coming Soon)</span></h2>
                    <input id="idea-stats-toggle-btn" class="expand" type="button" />
                </div>
                <div class="stats-row">
                    <div class="stats-col" style="margin-right:17px;">
                        <h4>Estimated Idea Values($)</h4>
                        <img src="<?php echo $vars["url"]?>mod/miigle_theme/graphics/idea-graph-icon2.png" />
                        <div class="clearfloat"></div>
                    </div>
                    <div class="stats-col" style="margin-right:17px;">
                        <h4>Idea Reviews</h4>
                        <img src="<?php echo $vars["url"]?>mod/miigle_theme/graphics/idea-graph-icon1.png" />
                        <div class="clearfloat"></div>
                    </div>
                    <div class="stats-col">
                        <h4>Idea Views</h4>
                        <img src="<?php echo $vars["url"]?>mod/miigle_theme/graphics/idea-graph-icon.png" />
                    </div>
                    <div class="clearfloat"></div>
                </div>
            </div>
            <?php 
                }
            ?>
            <div id="idea-scf-box">
                <ul id="tabs">
                    <li class="selected"><a id="Share-Tab-Btn" href="#" title="Share">Share.</a></li>
                    <li><a id="Cheer-Tab-Btn" href="#" title="Cheer">Cheer.</a></li>
                    <li><a id="Foster-Tab-Btn" href="#" title="Foster">Foster.</a></li>
                </ul>
            </div>
            <div id="share-tab-content" class="tab-content">
                <?php 
                    echo elgg_view("ideas/listing_view", array('un'=>$user->username));
                ?>
            </div>
            <div id="cheer-tab-content" class="tab-content">                
                <?php 
                    echo elgg_view("ideacheer/list", array('un'=>$user->username));
                ?>
            </div>
            <div id="foster-tab-content" class="tab-content">
                <?php 
                    echo elgg_view("ideas/fostered_ideas_list", array('un'=>$user->username));
                ?>
            </div>
            <div style="margin:80px 0 0 0;">&nbsp;<br/></div>
            
            <?php /*?><div id="idea-product-service-box">
                <div class="ips-links">
                    <ul>
                        <li><a class="selected first" href="#">Idea</a></li>
                        <li><a href="#">Product</a></li>
                        <li><a href="#">Service</a></li>
                    </ul>
                    <br style="clear:both;">
                </div>
                <div class="ips-tabs">
                    <div id="ips-tab1">
                        <?php echo $vars['area4'];?>
                    </div>
                </div>
            </div><?php */?>
            <?php echo $vars['area5'];?>
        </div>
        <div id="third-col">
            <div id="latest-activities-box" style="padding-bottom:25px;margin-bottom:20px;">
                <h2>Beta UI Demo.</h2>
                <a style="display:block;margin-top:10px;width:235px;height:140px;" href="http://player.vimeo.com/video/41591740?title=0&byline=0&portrait=0&autoplay=1" class="pop-image">
                    <img style="width:235px;height:140px;" src="<?php echo $vars["url"]?>mod/miigle_theme/graphics/MIIGLE-Beta-UI-Demo.png" alt="" />
                </a>
            </div>
            <div id="latest-activities-box">
                <h2>Latest Activities.</h2>
                <?php 
                    echo elgg_view_river_items(0, 0, '', '', '', '', 4, 0, 0, true);
                ?>
            </div>
            <div id="top-ideas-box">
                <h2>Top Rated Ideas.</h2>
                <?php 
                    echo elgg_view("miigle_ratings/topideas", array("pagination"=>true, "limit"=>4));
                ?>
            </div>
            <?php /*?>
            <div style="width:265px;margin:20px 0 0 0;">
                <div style="text-align:center;color:#ddd;">SPONSORED ADS</div>
                <div style="width:265px;height:177px;margin-top:10px;background-image:url('<?php echo $vars["url"]?>mod/miigle_theme/graphics/ad-placeholder.png');"></div>
                <div style="width:265px;height:177px;margin-top:10px;background-image:url('<?php echo $vars["url"]?>mod/miigle_theme/graphics/ad-placeholder.png');"></div>
                <div style="width:265px;height:177px;margin-top:10px;background-image:url('<?php echo $vars["url"]?>mod/miigle_theme/graphics/ad-placeholder.png');"></div>
            </div>
            <?php */?>
            <?php echo $vars['area3'];?>
        </div>
        <!-- clear float -->
        <br style="clear:both" />
    </div>
</div><!--/ three_column -->
