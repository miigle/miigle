<?php

    /*** Seashells Theme language file*/
    
    $english = array(
        'messages:delete'=>'Delete',
        'messages:markread'=>'Mark Read',
        'messages:selectall'=>'Select All',
        'messages:unselectall'=>'Unselect All',
        'user:password:lost' => 'Reset your password on MIIGLE',
        'miigle:passwordagain' => "Confirm Password",
        'miigle:register1' => "Join MIIGLE today! Forever free.",
        'miigle:register2' => "Share your creativity with the world.",
        'miigle:email' => "Email",
        'seashells:myprofile' => "My Profile",
        'seashells:dashboard' => "News Feed",
        'seashells:addpost' => "Write a post",
        'seashells:addfile' => "Upload a file",
        'seashells:addbookmarks' => "Bookmark a site",
        'seashells:addgroups' => "Create a group",
        'seashells:addpages' => "Author a page",
        'seashells:addphotos' => "Upload photos",
        'seashells:addvideos' => "Add a video",
        'seashells:addpolls' => "Create a poll",
        'seashells:addevent' => "Promote an event",
        'seashells:addquestion' => "Ask a question",
        'seashells:onlineusers' => "Online now",
        'seashells:noonlineusers' => "There are no online users at the moment",
        'seashells:upcomingevents' => "Events",
        'seashells:index:blogs' => "Latest blog posts",
        'seashells:index:files' => "Recently uploaded files",
        'seashells:index:bookmarks' => "Recently bookmarked sites",
        'seashells:index:members' => "Newest members of our community",
        'seashells:index:groups' => "Newest groups",
        'seashells:publish' => "Publish",
        'seashells:new'=> "new",
        'seashells:emptyinbox' => "There are no messages in your inbox",
        'seashells:status' => "My status",
        'seashells:noevents' => "You do not have any upcoming events"
    );

add_translation("en",$english);
?>
