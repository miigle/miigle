<?php
    /**
     * List User Entities
     * 
     * @uses array $vars['entities']
     * @uses array $vars['title']
     * @uses boolean $vars['pagination']
     * @uses integer $vars['limit']
     * @uses integer $vars['offset']
     * @uses integer $vars['count']
     * @uses string $vars['baseurl']
     * @uses string $vars['type'] full or compact listing
     */

    if(isset($vars['entities']) && is_array($vars['entities']) && count($vars['entities'])>0){
        $arrUsers = $vars['entities'];
        $count_users = count($arrUsers);
    }else{
        echo '<span style="font-style:italic;color:#666;">None</span>';
    }
    
    if(isset($vars['type']) && !empty($vars['type'])){
        $viewtype = $vars['type'];
    }else{
        $viewtype = "full";
    }
    
    if(isset($vars['baseurl']) && !empty($vars['baseurl'])){
        $baseurl = $vars['baseurl'];
    }else{
        $baseurl = "#";
    }

    switch($viewtype){
        case 'full':
?>
            <div style="padding:20px 10px;background-color:#FFF;">
                <?php echo elgg_view_title($vars['title']);?>
<?php 
            foreach ($arrUsers as $user){
?>
                <div style="padding:10px 0;border-bottom:1px solid #EEE;">
<?php 
                    echo elgg_view("search/user/entity", array("entity"=>$user));
?>
                </div>    
<?php 
            }
?>
            </div>
<?php 
            break;
        case "compact":
?>
            <ul>
<?php 
                foreach($arrUsers as $user){
                    //get small profile icon of the investor
                    $icon = elgg_view("profile/icon", array(
                                                'entity' => $user,
                                                'align' => "left",
                                                'size' => 'small',
                                                'override' => true
                                        ));
                    $profile_url = $user->getUrl();
?>
                    <li><a href="<?php echo $profile_url;?>"><?php echo $icon;?></a></li>
<?php 
                }
?>
            </ul>
            <div class="clearfloat"></div>
<?php 
            if($count_users>1){
?>
            <a href="<?php echo "$baseurl/widget";?>" rel="content-580-400" class="pirobox">See All</a>
<?php 
            }
            break;
    }
?>