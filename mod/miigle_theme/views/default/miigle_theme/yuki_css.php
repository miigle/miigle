/*----------------------------------------------------------------
 * 
 *  Global
 * 
 -----------------------------------------------------------------*/
.bg {
    padding: 9px 14px;
    margin-bottom: 20px;
    background-color: #ffffff;
    border: 1px solid #cecece;
    border-radius: 6px;
}

.bg-nopadding {
    padding: 0;
    margin-bottom: 20px;
    background-color: #ffffff;
    border: 1px solid #cecece;
    border-radius: 6px;
}

.bg-leftCol,
.bg-rightCol {
    margin-bottom: 20px;
    background-color: #ffffff;
    border: 1px solid #cecece;
    border-radius: 6px;
}

.bg-leftCol .page-header h4,
.bg-rightCol .page-header h4 {
    margin-right:14px;
    margin-left:14px;
    font-size:14px;
}

.bg-leftCol .page-header,
.bg-rightCol .page-header {
    padding:0;
}

.bg-rightCol .page-header {
    border:none;
    margin:0;
}

.padding-bg {
    padding: 10px;
    margin: 0;
    border: none;
}

.page-header {
    margin-top:10px;
}

.page-header h1, h3 {
    font-size:18px;
}

.page-header h4 {
    font-size:16px;
}

.page-header h3 {
    margin-top:0;
}

.page-header.padding-bg {
    margin:0;
}

.page-footer {
    padding-top: 20px;
    margin-top: 0px;
    margin-bottom: 20px;
    border-top: 1px solid #eee;
}

.icon-startup {
    margin-right: 15px;
}

p {
    font-size:12px;
    color:#888888;
}

img.avator {
    border-radius: 6px;
}

section .container {
    padding-top: 20px;
    padding-bottom: 20px;
}

.center-block {
    display: block;
    margin-left: auto;
    margin-right: auto;
}

/*
.element {
  .center-block();
}
*/

span.button-action {
    display:block;
    vertical-align:top;
    margin-bottom:10px;
}
span.button-action span {
    -moz-background-clip:border;
    -moz-background-inline-policy:continuous;
    -moz-background-origin:padding;
    display:inline-block;
    height:15px;
}
span.button-action span.rate {
    background:transparent url('../images/icon_action.png') no-repeat scroll 0 -25px;   
}
span.button-action span.cheer {
    background:transparent url('../images/icon_action.png') no-repeat scroll -25px -25px;
}
span.button-action span.foster {
    background:transparent url('../images/icon_action.png') no-repeat scroll -50px -25px;
}
span.button-action button.btn,
span.button-action a.btn {
    font-size: 14px;
    background-color: #f8f8f8;
}
span.button-action button.btn:hover,
span.button-action a.btn:hover {
    background-color: #ebebeb;
    background-image:none;
}


/* tags */
.tags button {
    font-size:10px;
    color:#888888;
    background-color:#f3f3f3;
    margin:0 5px;
}


/* comments */
#comments {
    margin:20px 0;
}

@media (min-width: 768px) {
    #comments .row {
        padding-bottom:20px;
    }
}

#comments .commentTxt {
    margin-left:64px;
}


#qtocommunity .page-header button.btn {
    font-size: 12px;
    color: white;
    margin-top:-5px;
    background-color: #f8f8f8;
}
#qtocommunity .page-header button.btn:hover {
    background-color: #ebebeb;
    background-image:none;
}

/* carousel */
.carousel {
    padding-bottom:40px;
}
.carousel-control.left,
.carousel-control.right {
    background-image:none;
}
.carousel-indicators {
    bottom: -10px;
}
.carousel-indicators li {
    background-color:#cecece;
}
.carousel-indicators .active {
    background-color:#888888;
}
.carousel table {
    margin-bottom:0;
}
.bg-leftCol .carousel {
    padding-bottom:30px;
    margin-bottom: 10px;
}

/* button */
.btn-default.btn-round {
    border-radius: 16px;
    color:#ccc;
}
.btn-default.btn-miigle.btn-round {
    border-radius: 16px;
    color:#333;
    border-color: #333;
}


/*----------------------------------------------------------------
 * 
 *  Startup Detail Page
 * 
 -----------------------------------------------------------------*/
#startup h1 {
    margin-top:0;
    margin-bottom: 4px;
}

#startup .page-header p {
    margin-bottom: 4px;
}

#startup h1,
#startup .page-header p {
    margin-left:76px;
}

#startup .videoBox,
#startup .imageBox {
    margin: 20px 0 20px;
}

#startup .videoBox h3,
#startup .imageBox h3 {
    margin-bottom: 20px;
}

#startup .videoBox .video-thumb table td {
    border:none;
    padding:0 0 0 10px;
}
#startup .videoBox .video-thumb table tr.even td {
    padding-top:10px;
}

#startup .imageBox table td {
    border:none;
    padding:0;
}

#startup .imageBox table td+td,
#startup .imageBox table td+td+td
#startup .imageBox table td+td+td+td {
    border:none;
    padding:0 0 0 10px;
}

@media (max-width: 768px) {
    #startup .table-responsive {
    overflow-x:hidden;
    border:none;
    }
}

@media (min-width: 768px) {
    #stakeholders .row {
        padding-bottom:20px;
    }
}

#stakeholders p {
    font-size:14px;
    color:#333333;
    padding-left:64px;
}
#stakeholders p > strong {
    display:block;
}
#followers img {
    padding-bottom:20px;
}


/*----------------------------------------------------------------
 * 
 *  Feed and Search Result Page
 * 
 -----------------------------------------------------------------*/
 /* Left Col Nav */
form.catsearch input,
form.locationsearch input {
    font-size:14px;
}

form.catsearch,
form.locationsearch {
    border-bottom: 1px solid #ccc;
    padding:10px;
}

.dropdown-search {
    padding: 0;
    margin: 0;
    list-style: none;
}

.dropdown-search .dropdown-list {
    display: block;
    line-height: 1.428571429;
    border-bottom: 1px solid #eee;
    background-color:#f7f7f7;
    margin-top:1px;
    color: #888;
}

.dropdown-search .dropdown-header {
    padding:0;
}

.dropdown-search .dropdown-list a {
    display:block;
    padding: 10px 20px;
    color: #888;
}
.dropdown-search .dropdown-list a span.glyphicon {
    display:none;
}

.dropdown-search>.dropdown-list:last-child {
    border-radius: 6px;
    border-bottom: none;
} 
 
.dropdown-search .dropdown-list:hover,
.dropdown-search .dropdown-list.active {
    background-color:#fff;
    color: #000;
}

.dropdown-search .dropdown-list a:hover,
.dropdown-search .dropdown-list.active a {
    text-decoration:none;
    color: #000;
} 

.dropdown-search .dropdown-list:hover a span.glyphicon,
.dropdown-search .dropdown-list.active a span.glyphicon {
    display:block;
}

.bg-leftCol .input-group-addon {
    background: white;
    border-right: none;
    padding:5px 8px;
}

.bg-leftCol .form-control {
    border-left: none;
}

.bg-leftCol .input-group a {
    white-space: nowrap;
    vertical-align: middle;
    display: table-cell;
    padding:0 0 0 10px;
}

.bg-leftCol span.glyphicon.collapse {
    font-size:18px;
    color:#bcbcbc;
}

@media (max-width:768px) { 
    .bg-leftCol #cat, 
    .bg-leftCol #loc,
    .bg-leftCol #carousel-feature {
        display:none;
    }  
}


/* Main */
.page-header h1#feed.feed-icon {
    background: transparent url("/mod/miigle_theme/graphics/feed-icon.jpg") no-repeat scroll 0 0;
    padding-left: 35px;
    padding:5px 0 5px 35px;
}
.page-header h1#feed {
    padding:5px 0 5px 5px;
    margin: 5px 0;
}

.page-header h1#feed span.result {
    color:#888;
    font-size:14px;
    font-weight:normal;
}

.page-header h1#search {
    padding:0;
    margin: 5px 0;
}

.bg-nopadding .well {
    border-radius:0;
    margin:0;
    padding: 12px 19px 3px 19px;
}

h3.media-heading {
    font-size:14px;
}

img.media-object {
    border:1px solid #cecece;
}

.media.padding-bg {
    border-bottom:1px solid #cecece;
}

.media.padding-bg:last-child {
    border-bottom:none;
}

.media h5 {
    font-size:12px;
    font-weight:normal;
}

.bg-rightCol a.more {
    display:block;
    font-weight:500;
}

#sponsored.bg-rightCol img,
#featured.bg-rightCol img {
    margin-bottom:20px;
}

#featured.bg-rightCol .padding-ad {
    text-align: center;
    padding: 14px;
    border-bottom:1px solid #cecece;
}

#featured.bg-rightCol .padding-ad:last-child {
    border:none;
}

#interests.bg-rightCol img.avator {
    padding:3px;

}

/*----------------------------------------------------------------
 * 
 *  Startup Edit Page
 * 
 -----------------------------------------------------------------*/
.page-header h3 > span.steps {
    font-size:14px;
} 
.page-header h3 span.glyphicon.sm {
    font-size:8px;
    color:#ececec;
}

#questionnaire .edit-group {
    padding:14px 0;
}

#questionnaire .level2 .edit-group {
    padding:28px 0 14px 0;
}

#questionnaire .pad-right {
    padding-right:14px;
}

#questionnaire .pad-left {
    padding-left:14px;
}

#questionnaire .pad-bottom {
    margin-bottom:14px;
}

#questionnaire label {
    display:block;
    margin-bottom: 15px;
}

#questionnaire label span {
    font-weight:normal;
}
#questionnaire label small {
    font-weight:normal;
    color:#888;
}

#questionnaire .btn.btn-default.btn-type {
    color:#acacac;
    text-align:center;
    font-size:24px;
    width:100%;
    height: 100px;
	margin-bottom:0;
	line-height:85px;
}
#questionnaire .ideatype input {
	visibility:hidden;
	width:1px;
	height:1px;
}
#questionnaire .btn.btn-default.btn-type:hover,
#questionnaire .btn.btn-default.btn-type:focus,
#questionnaire .btn.btn-default.btn-type:active,
#questionnaire .btn.btn-default.btn-type.active,
#questionnaire .btn.btn-default.btn-type.dropdown-toggle,
#questionnaire .btn.btn-default.btn-type.default {
    background-color: #fffadb;
    color:#333;
}

@media (max-width:768px) { 
    #questionnaire .btn.btn-default.btn-type {
        width:100%;
        height:100%;
    }
}


#startup-keyword input {
    border:none;
}
#video-url .tm-tag-base {
    width:100%;
}
#video-url .tm-tag-base a {
    float:right;
}

span.help-block {
    font-size:12px;
}

#questionnaire .startup-role {
    margin:28px 0;
    text-align: center;
}

#questionnaire .startup-role h4 {
    font-weight:normal;
}

#questionnaire .startup-role span.icon-avator img {
    width: 60px;
    height: 60px;
}

#questionnaire .startup-role .padding {
    padding-top:24px;
}


/* File Upload */
.fileupload .fileupload-buttonbar {
    border: 3px dashed #ccc;
    border-radius: 6px;
    text-align: center;
    background-color:#fbfbfb;
    font-size:12px;
    color:#888;
    padding:20px;
}

.fileupload .fileupload-buttonbar span.textline {
    display:block;
}

/*.fileupload .fileupload-buttonbar */.icon-avator {
    display:block;
    margin-bottom:28px;
}

/*.fileupload .fileupload-buttonbar*/ 
.icon-avator img {
    width:100px;
    height:100px;
}

.fileupload table.table {
    margin-top:24px;
}


/*----------------------------------------------------------------
 * 
 *  Footer section
 * 
 -----------------------------------------------------------------*/

section#footer {
    background:#1f1f1f;
    color:#acacac;
    height: auto;
}
section#footer a {
    color:#acacac;
    font-weight:normal;
}
section#footer ul {
    list-style-type:none;
    color:white;
    font-weight:500;
    margin-right:40px;
}
section#footer button {
    margin-right:15px;
}
section#footer .copyright {
    background:#131313;
    text-align: left;
    color: #acacac;
    font-size: 14px;
    line-height: 48px;
    height: auto;
}

section#footer .copyright strong {
    color:white;
}
section#footer .copyright a {
    font-weight:500;
    color:white;
}
@media (min-width:768px) {
    section#footer .copyright {
        margin-top:60px;
    }
}
@media (min-width:768px) { 
    section#footer .container {
        padding-top:60px;
        padding-bottom:60px;
    }
}

/*----------------------------------------------------------------
 * 
 *  Fuel UX Wizard || Customized
 * 
 -----------------------------------------------------------------*/

#FuelUXWizard.wizard:before,
#FuelUXWizard.wizard:after {
  display: table;
  line-height: 0;
  content: "";
}

#FuelUXWizard.wizard:after {
  clear: both;
}

/*
#FuelUXWizard.wizard ul {
  padding: 0;
  margin: 0;
  list-style: none outside none;
}
*/


#FuelUXWizard.wizard .btn[disabled] {/* non clickable */
    opacity: 1;
}



/*----------------------------------------------------------------
 * 
 *  Loading Arrow
 * 
 -----------------------------------------------------------------*/
.load-busy {
    font-size:16px;
    color:#888888;
    padding: 20px 0;
    text-align: center;
}
.load-busy span {
    display:inline-block;
    vertical-align: middle;
}


