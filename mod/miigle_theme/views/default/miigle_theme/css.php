
<?php
/**
 * Miigle Theme
 * core CSS file
 *
 *
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['wwwroot'] The site URL
 */
?>

/*-------------------------------------------------------
|
|    Global Styles
|
|--------------------------------------------------------*/

body {
    padding-top: 80px;
    background:#E9E9E9;
    font-family: 'Roboto', sans-serif;
    font-weight:300;
}
strong {
  font-weight:500;  
}
.label {
  font-weight:500;  
}
label {
  font-weight:500;
}
.dropdown-header {
  font-size:14px;
  font-weight:500;
}
div#content_area_user_title {
    display:none;
}
.modal .close {
    font-size:32px;
}
.btn {
    font-weight:500;
    border: none !important;
}
.btn-default {
  background-color:#F3F3F3;
}
.btn-default:hover {
  background-color:#D0D0D0;
}
.btn-default.btn-miigle {
    background-color:#ffe000;
}
.btn-default.btn-miigle:hover {
    background-color:#F1D200;
}
.btn-default.btn-miigle-light,
.btn-default.btn-miigle-light.active {
    background: #FEEB84;
}
.btn-default.btn-block:hover {
    background-color:#D0D0D0;
}
.btn.active {
  -webkit-box-shadow: none;
  box-shadow: none;
  background-color: #D0D0D0;
}
form label.btn-default {
  background-color:#ebebeb;
}
form label.btn-default:hover {
  background-color:#F3F3F3;
}
.form-control {
  border:none;
  background:#f5f5f5;
}
.input-group .btn, .input-group-addon {
  padding: 7px 12px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}
.input-group-addon {
  border: none !important;
}
img.media-object {
    border: none !important;
}
.well,
.navbar-sub, img.icon, #details_panel, .whitebox,
.padding-bg, .thumbnail, .bg-nopadding,
.bg, .bg-rightCol {
    border: none !important;
}
.onoffswitch {
    position: relative; width: 80px;
    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
}
.onoffswitch-checkbox {
    display: none;
}
.onoffswitch-label {
    display: block; overflow: hidden; cursor: pointer;
    border-radius: 10px;
}
.onoffswitch-inner {
    width: 200%; margin-left: -100%;
    -moz-transition: margin 0.3s ease-in 0s; -webkit-transition: margin 0.3s ease-in 0s;
    -o-transition: margin 0.3s ease-in 0s; transition: margin 0.3s ease-in 0s;
}
.onoffswitch-inner:before, .onoffswitch-inner:after {
    float: left; width: 50%; height: 20px; padding: 0; line-height: 20px;
    font-size: 11px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
    -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box;
}
.onoffswitch-inner:before {
    content: "SHOW";
    padding-left: 10px;
    background-color: #FEEB84; color: #888;
}
.onoffswitch-inner:after {
    content: "HIDE";
    padding-right: 10px;
    background-color: #EBEBEB; color: #888;
    text-align: right;
}
.onoffswitch-switch {
    width: 12px; margin: 4px;
    height:12px;
    background: #FFFFFF;
    border: 2px solid #FFFFFF; border-radius: 10px;
    position: absolute; top: 0; bottom: 0; right: 56px;
    -moz-transition: all 0.3s ease-in 0s; -webkit-transition: all 0.3s ease-in 0s;
    -o-transition: all 0.3s ease-in 0s; transition: all 0.3s ease-in 0s; 
}
.active.onoffswitch-checkbox + .onoffswitch-label .onoffswitch-inner {
    margin-left: 0;
}
.active.onoffswitch-checkbox + .onoffswitch-label .onoffswitch-switch {
    right: 0px; 
}

.bg-leftCol {
    border: none !important;
}
.well {
  -webkit-box-shadow: none;
  box-shadow: none;
}
.dropdown-search li:first-child {
  -webkit-border-top-left-radius: 5px;
  -webkit-border-top-right-radius: 5px;
  -moz-border-radius-topleft: 5px;
  -moz-border-radius-topright: 5px;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
}
.dropdown-search li:last-child {
  -webkit-border-bottom-right-radius: 5px;
  -webkit-border-bottom-left-radius: 5px;
  -moz-border-radius-bottomright: 5px;
  -moz-border-radius-bottomleft: 5px;
  border-bottom-right-radius: 5px;
  border-bottom-left-radius: 5px;
}
.whitebox {    
    background:white;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    border:1px solid #cecece;
}
.whitebox-header h1, h1.whitebox-header {
    font-size:18px;
    margin-top:0;
}
.whitebox-header h2 {
    font-size:15px;
    font-weight:500;
    margin:0;
}
.whitebox-header {
    padding:15px 20px 15px 20px;
    border-bottom:1px solid #cecece;
    margin:0;    
}
.whitebox-subhead {
    padding:5px 20px;
    background:#f7f7f7;
    border-bottom:1px solid #cecece;
}
.whitebox-subhead .btn {
    font-size:12px;
    font-weight:500;
}
.whitebox-subhead .btn.btn-icon {
    font-size:18px;
    font-weight:normal;
}
.whitebox-body {
    padding:20px;
}
.whitebox-body .page-header:first-child {
    margin-top:0;
}
.whitebox-footer {
    padding:5px 20px;
    border-top:1px solid #ececec;
}
.checkbox {
    margin:0;
    padding:0;
    position: relative;
}
.checkbox input[type=checkbox] {
    visibility: hidden;
}
.checkbox label {
    cursor: pointer;
    position: absolute;
    width: 20px;
    height: 20px;
    top: 0;
      left: 0;
    background: white;
    border:1px solid #d3d3d3;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
}
.checkbox label:after {
    opacity: 0;
    content: '';
    position: absolute;
    width: 17px;
    height: 8px;
    background: transparent;
    top: 0px;
    left: 2px;
    border: 4px solid #333;
    border-top: none;
    border-right: none;    

    -webkit-transform: rotate(-45deg);
    -moz-transform: rotate(-45deg);
    -o-transform: rotate(-45deg);
    -ms-transform: rotate(-45deg);
    transform: rotate(-45deg);
}
.checkbox label:hover::after {
    opacity: 0.5;
}
.checkbox input[type=checkbox]:checked + label:after {
    opacity: 1;
}
label.error {
    color: #b94a48;
    font-weight: normal;
    font-style: italic;
    font-size: smaller;
}
.ajax-loader {
    display:none;
}
img.icon {
    max-width:100%;
    border:1px solid #CECECE;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
}
.label-muted {
    background:#f0f0f0;
    border:1px solid #d8d8d8;
    color:#888888;
}
.label-muted:hover {
    background:#d8d8d8;
}
.text-muted {
    color:#888888;
}
.text-muted .glyphicon,
.text-muted i {
    color:#b6b6b6;
}
.btn-lg {
    padding:7px 16px;
}
.btn-icon {
    padding:0;
    color:#b6b6b6;
    background:none;
    border:none;
}
.btn-icon:hover {
    padding:0;
    color:#333;
}
.modal img {
    max-width:100%;
}
.fa-larger {
    font-size:1.666666em;
}
span.button-action span.glyph-text {
    width:auto;
}
.footer {
    margin-top:50px;
}
@media (min-width:768px) {
    body {
        padding-top:140px;
    }
}

/*-------------------------------------------------------
|
|    Header
|
|--------------------------------------------------------*/

body .noUi-handle {
	background:#fedd32;
	margin-top: -13px;
	border-radius: 40px;
	cursor:pointer;
}
body .noUi-background {
	background:#333;
}
body .noUi-origin {
	background:#DDD;
}
body .noUi-base {
	height:20px;
	margin-top:10px;
}
#modal-request h3 {
    font-size:26px;
    text-align:center;
}
#modal-request h4 {
    font-size:16px;
    text-align:center;
}
.container.content {
    min-height:80%;
    padding-bottom:60px;
}
.navbar .glyphicon {
    margin-right:5px;
}
.navbar-main {
    margin-bottom:0;
    font-weight:300;
    min-height:66px;
}
#header_join {
    margin-left:10px;
}
.navbar-main.navbar-inverse {
    color:#999;
}
.navbar-main.navbar-inverse .navbar-nav>li>a {
    color:white;
}
.navbar-main.navbar-inverse .navbar-nav>li:hover {
    background:#333;
}
.navbar.navbar-main>.container .navbar-brand {
    margin-left:30px;
    padding:10px 0px;
}
.navbar-main .navbar-brand {
    margin-right:15px;
}
.navbar-main .navbar-brand img {
    height:43px;
}
.navbar-main .navbar-btn {
    margin-top:18px;
}
.navbar-main.navbar-inverse .navbar-toggle {
    border:none;
}
.navbar-main .nav-mobile .navbar-toggle {
    float:none;
    margin-right:0;
}
.navbar-main .collapse-mobile .divider {
    height: 1px;
    margin: 9px 0;
    overflow: hidden;
    background-color: #e5e5e5;
}
.navbar-main .collapse-mobile .navbar-form {
    border-top:none;
    margin-top:-1px;
}
.navbar-main .nav-mobile .navbar-btn {
    margin-top:15px;
    margin-right:3px;
}
.navbar-main .nav-mobile a {
    color:#999;    
} 
.navbar-main .nav-mobile a:hover {
    color:white;
    background-color: #333;
} 
.navbar-inverse .navbar-nav>li>a.btn-miigle,
.navbar-nav>li>a.btn-miigle {
    padding-top:5px;
    padding-bottom:5px;
    background-color:#ffe000;
    color:#333333;
    border:1px solid #999;
}
.navbar-inverse .navbar-nav>li>a.btn-miigle:hover,
.navbar-nav>li>a.btn-miigle:hover {
    background-color:#F1D200;
    color:black;
    border-color:#666;
}
.navbar-main #post_idea {    
    font-weight:500;
    padding-left:15px;
    padding-right:15px;
}
.navbar-main .navbar-right {
    font-size:18px;
    margin-right:-15px;
}
.navbar-main a.active {
    color:white;
}
.navbar-sub.navbar-default {
    background:white;
}
.navbar-main a img {
    height:22px;
    margin-top:-4px;
    border-radius:5px;
}
.navbar-main li.analytics i {
    margin-top:2px;
}
.navbar-main .tooltip.right .tooltip-arrow {
  top: 50%;
  left: 0;
  margin-top: -5px;
  border-right-color: #d2322d;
  border-width: 5px 5px 5px 0;
}
.navbar-main .tooltip-inner {
  background: #d2322d;
  border: solid 1px #ac2925;
  width:160px;
}

ul.notifications li {
    padding:5px 20px;
}
.nav.notifications>li>a,
.dropdown-menu.notifications>li>a {
    display:inline-block;
    margin:0;
    padding:0;
}
.navbar-sub {
    top:64px;
    border:none;
    border-bottom:1px solid #d3d3d3;
    font-size:14px;
    font-weight:300;
}
.navbar-sub .logged-out ul {
    width:100%;
}
.navbar-sub .logged-out li {
    width:100%;
    text-align:center;
    line-height:50px;
}
.navbar-sub.navbar-default .navbar-nav>.active>a {
    color:#333;
    background:none;
    border-bottom:2px solid #333;
    padding-bottom:13px;
}
.navbar-sub .input-group {
    width:255px;
    margin-top:2px;
}
.navbar-sub .form-control {
    border-right:none;
}
.navbar-sub .input-group-addon {
    background:white;
    border-left:none;
}
.navbar-sub .btn {
  background:#f5f5f5;
}
#upgrade_dialog {
    text-align:center;
}
@media (min-width:768px) {
    .navbar-main .navbar-brand {
        margin-right:45px;
    }
    .navbar-main .nav>li>a {
        padding:22px 15px;
    }
    .navbar-main .navbar-collapse,
    .navbar-sub .navbar-collapse    {
        padding-left:0;
        padding-right:0;
    }
    .navbar.navbar-main>.container .navbar-brand {
        margin-left:0;
    }
    .navbar-main .nav:first-child>li:first-child>a,
    .navbar-sub .nav:first-child>li:first-child>a    {
        padding-left:0;
    }
    .navbar-sub .navbar-form {
        padding-right:0;
    }
    .dropdown-menu.notifications li {
        white-space: nowrap;
    }
    .dropdown-menu.notifications li:last-child {
        margin-top:15px;
        text-align:right;
    }
    .dropdown-menu.notifications>li>a {
        color:#428bca;
    }
    .dropdown-menu.notifications>li>a:hover {
        color:#2a6496;
        background:none;
    }
}

/*-------------------------------------------------------
|
|    /pg/miigle_user_settings/edit
|
|--------------------------------------------------------*/

#usersettings-edit .whitebox-header {
    padding:0;
}
#usersettings-edit .whitebox-header .nav-tabs li a {
    color:#333;
    font-weight:500;
}
#usersettings-edit .whitebox-header .nav-tabs {
    background:#fafafa;
    border-bottom:none;
    border-radius:5px;
}
#usersettings-edit .whitebox-header .nav-tabs li.active a {
    border-top:1px solid white;
    background:white;
}
#usersettings-edit .whitebox-header .nav-tabs>li>a {
    border-radius:0;
}
#usersettings-edit .whitebox-header .nav-tabs>li:first-child>a {
    border-left:none;
    -webkit-border-top-left-radius: 5px;
    -moz-border-radius-topleft: 5px;
    border-top-left-radius: 5px;
}
#usersettings-edit .whitebox-header .nav-tabs>li:last-child>a {
    border-right:none;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topright: 5px;
    border-top-right-radius: 5px;
}
#usersettings-edit .whitebox-header .nav-tabs>li {
    width:20%;
    text-align:center;
}

/*-------------------------------------------------------
|
|    /pg/idea/new
|
|--------------------------------------------------------*/

#ideas_newidea .ideatype.radio {
    padding-left:0;
}
#ideas_newidea .ideatype label {
    width:48%;
    height:158px;
    line-height:158px;
    text-align:center;
    border:1px solid #ccc;
    color:#999;
    font-weight:500;
    display:block;
    float:left;
    border-radius: 4px;
}
#ideas_newidea .ideatype label:first-child {
    margin-right:10px;
}
#ideas_newidea .ideatype label:hover,
#ideas_newidea .ideatype label.active {
    background:#fffadb;
    color:#333;
}
#ideas_newidea .ideatype br {
    display:none;
}
#ideas_newidea .ideatype label input[type=radio] {
    visibility:hidden;
}
.profileicon .form-control {
    height:auto;
    border:3px dashed #ccc;
    text-align:center;
}
.profileicon.has-error .form-control {
    border-color:#b94a48;
}
#ideas_newidea .profileicon input {
    margin: 15px 0 5px 100px;
}
.profileicon span.fileexists {
    display:none;
}
#ideas_newidea textarea#ideapitch {
    height:70px;
}

/*-------------------------------------------------------
|
|    /pg/profile/:username/edit
|
|--------------------------------------------------------*/

#usersettings-edit .whitebox-body .nav-tabs {
    margin-bottom:25px;
    padding-bottom:15px;
}
#usersettings-edit .whitebox-body .nav-tabs>li.active>a,
#usersettings-edit .whitebox-body .nav-tabs>li>a {
    border:none;
    border-right:1px solid #ddd;
    background:none;
}
#usersettings-edit .whitebox-body .nav-tabs>li:last-child>a {
    border-right:none;
}
#usersettings-edit .whitebox-body .nav-tabs>li.active>a {
    font-weight:500;
}
#usersettings-edit .whitebox-body .nav>li>a {
    padding:0px 15px;
}
#usersettings-edit .profileicon .form-control {
    height:205px;
    padding-top:25px;
}
#usersettings-edit .profileicon input {
    margin-top:25px;
}
#usersettings-edit .form-group {
    margin-bottom:24px;
}
#usersettings-edit .form-group.has-error {
    margin-bottom:0;
}

/*-------------------------------------------------------
|
|    /pg/profile/:username/view
|
|--------------------------------------------------------*/

#profile_sidebar .row {
    margin-bottom:20px;
}
#profile_sidebar .icon {
    width:100%;
}
#profile_sidebar .social {
    text-align:center;
}
#profile_sidebar .btn {
    font-size:13px;
}
#profile_view h1 {
    margin-top:0;
    font-size:25px;
    font-weight:500;
}
#profile_view h1 small {
    font-size:13px;
    line-height:30px;
    font-weight:500;
}
#profile_view .stats {    
    font-size:13px;
}
#profile_view .stats_border {    
    border-top:1px solid #ccc;
    border-bottom:1px solid #ccc;
}
#profile_view .stats a {
    color:#333;
}
#profile_view .stats a:hover {
    text-decoration:none;
    background:#f0f0f0;
    border-bottom:1px solid #ffe000;
    padding-bottom:7px;
}
#profile_view a.stat {
    cursor: pointer;
    text-align:center;
    width:100%;
    display:block;
    padding-top:8px;
    padding-bottom:8px;
}
#profile_view a.stat p {
    margin-bottom:3px;
    font-size:15px;
}
#profile_view a.stat p.number {
    font-size:18px;
}
#profile_view #details_accordion .panel {
    background:none;
    border:none;
}
#profile_view #details_accordion .panel-heading {
    background:none;
    border:none;
    text-align:center;
    color:#999;
}
#profile_view #details_accordion .panel-heading a:hover {
    text-decoration:none;
    color:#333;
}
#profile_view #details_accordion .panel-heading a span:first-child {
    margin-right:5px;
}
#profile_view #details_accordion .panel-heading a span:last-child {
    margin-left:5px;
}
#profile_view #details_accordion .panel-body {
    background:white;
    border-radius:5px;
}
#profile_view #details_accordion .panel-body h2 {
    font-size:13px;
    font-weight:500;
    color:#333;
}
#profile_view #details_accordion .panel-body h2:first-child {
    margin-top:0px;
}
#profile_view #details_accordion .panel-body .label {
    margin-right:5px;
    margin-bottom:5px;
}
#profile_view .details {
    margin-bottom:15px;
}
#profile_view .wire_add .whitebox-body {
    padding:12px;
}
#profile_view .wire_add {
    margin-bottom:16px;
}
#profile_view .form-horizontal .form-group {
    margin-bottom:0;
}
#profile_view .wire_add .selector-wrapper {
    display:none;
}
#profile_view .view_river_items .whitebox-header h2 {
    font-size:18px;
    line-height:22px;
}
#profile_view .view_river_items .whitebox-subhead .btn-icon {
    margin-left:8px;
}
#profile_view .view_river_items .whitebox-header h2 .glyphicon {
    margin-right:8px;
}
#profile_view .river_items_list > .row {
    padding:15px 0;
    border-bottom:1px solid #DFDFDF;
}
#profile_view .river_items_list > .row:first-child {
    padding-top:0;
}
#profile_view .river_items_list .well h4 {
    font-weight:500;
}
#profile_view .river_items_list h4,
#profile_view .river_items_list h3 {
    font-size:13px;
    margin-top:0;
    margin-bottom:5px;
}
#profile_view .river_items_list .well {
    margin-bottom:0;
    font-size:13px;
}
#profile_view .river_items_list .well img {
    max-width:40px;
}
@media (min-width:480px) {
    #profile_view .stats > .col-sm-3:before {
        background:#ccc;
        content:'';
        position:absolute;
        left:0;
        height:38px;
        width:1px;
        margin-top:9px;
    }
    #profile_view .stats > .col-sm-3:first-child:before {
        background:none;
        border:none;
    }
}

/*-------------------------------------------------------
|
|    /pg/ideafeed
|
|--------------------------------------------------------*/

#ideafeed .media .preview img {
    max-height:50px;
    margin-right:10px;
}
#ideafeed .media .preview h4 {
    font-size:14px;
}
#.item {
    margin:0;
    padding:0;
    border:none;
}
#ideafeed .page-header h1#feed {
}
    
@media (max-width:768px) {
    body {
        padding-bottom: 0px;
        padding-top: 70px;
    }

    .whitebox-header {
        padding-top: 0px;
    }

    .whitebox {
        margin: 0px;
    }

    .whitebox-subhead {
        padding-right: 0px;
        padding-left: 0px;
    }

    .col-md-12 {
        padding: 0px;
    }

    #rows-select-dropdown {
        display: none;
    }
}

/*https://github.com/jharding/typeahead.js-bootstrap.css*/
.twitter-typeahead .tt-query,
.twitter-typeahead .tt-hint {
  margin-bottom: 0;
}

.tt-dropdown-menu {
  min-width: 160px;
  margin-top: 2px;
  padding: 5px 0;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0,0,0,.2);
  *border-right-width: 2px;
  *border-bottom-width: 2px;
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
     -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
          box-shadow: 0 5px 10px rgba(0,0,0,.2);
  -webkit-background-clip: padding-box;
     -moz-background-clip: padding;
          background-clip: padding-box;
}

.tt-suggestion {
  display: block;
  padding: 3px 20px;
}

.tt-suggestion.tt-is-under-cursor {
  color: #fff;
  background-color: #0081c2;
  background-image: -moz-linear-gradient(top, #0088cc, #0077b3);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#0088cc), to(#0077b3));
  background-image: -webkit-linear-gradient(top, #0088cc, #0077b3);
  background-image: -o-linear-gradient(top, #0088cc, #0077b3);
  background-image: linear-gradient(to bottom, #0088cc, #0077b3);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0077b3', GradientType=0)
}

.tt-suggestion.tt-is-under-cursor a {
  color: #fff;
}

.tt-suggestion p {
  margin: 0;
}

/* Why do I have to hack like this? */
span.twitter-typeahead {
    display: block !important;
}

#feed-icon {
    height: 17px;
    width: 17px;
    vertical-align: text-top;
}
