<?php
    /**
     * @description Shows a Bootstrap modal dialog box
     * 
     * @uses vars["id"]
     * @uses vars["class"]
     * @uses vars["header"]
     * @uses vars["title"]
     * @uses vars["body"]
     * @uses vars["footer"]
     * 
     */
?>

<div class="modal fade <?php echo $vars['class']; ?>" id="<?php echo $vars['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $vars['id']; ?>Label" aria-hidden="true">
    <div class="modal-dialog">
        <?php if($vars['form'] == true): ?>
            <form action="<?php echo $vars['form_action']; ?>" method="<?php echo $vars['form_method']; ?>" class="form-validate <?php echo $vars['form_class']; ?>">
        <?php endif; ?>
        <div class="modal-content">
            <?php if($vars['header'] == true): ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><?php echo $vars['title']; ?></h4>
                </div>
            <?php else: echo $vars['header']; ?>
            <?php endif; ?>
            <div class="modal-body">
                <?php echo $vars['body']; ?>
            </div>
            <?php if($vars['form'] == true): ?>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-default btn-miigle">Submit</button>
                </div>
            <?php elseif($vars['footer'] == true): ?>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            <?php else: echo $vars['footer']; ?>
            <?php endif; ?>
        </div><!-- /.modal-content -->
        <?php if($vars['form'] == true): ?>
            </form>
        <?php endif; ?>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->