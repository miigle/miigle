<?php
/**
 * Miigle Theme
 * core JS file
 *
 *
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['wwwroot'] The site URL
 */
?>

<script src="/mod/miigle_theme/miigle_theme.js"></script>
