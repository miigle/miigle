  <!-- footer -->
    <section id="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <ul class="pull-left">
              <li>Company</li>
              <!--<li>
                <a href="/about.php">About</a>
              </li>-->
              <li>
                <a href="http://miiglers.tumblr.com/">Blog</a>
              </li>
            </ul>
            <ul class="pull-left">
              <li>Stay in Touch</li>
              <li>
                <a target="_blank" href="http://www.linkedin.com/company/miigle">LinkedIn</a>
              </li>
              <li>
                <a target="_blank" href="https://www.facebook.com/miiglers">Facebook</a>
              </li>
              <li>
                <a target="_blank" href="https://twitter.com/miiglers">Twitter</a>
              </li>
              <li>
                <a target="_blank" href="https://plus.google.com/111652768429793362697/posts">Google+</a>
              </li>
              <li>
                <a target="_blank" href="http://vimeo.com/miiglers">Vimeo</a>
              </li>
              <li>
                <a target="_blank" href="http://www.youtube.com/user/miigleTV">Youtube</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              &copy; 2013 <strong>Miigle</strong>. All rights reserved.
              <span class="pull-right">
                <a href="/terms.html">Terms of Service</a>
                &amp; <a href="/privacy.html">Privacy Policy</a>
              </span>
            </div>
          </div>
        </div>
      </div>        
    </section>
  </body>
<?php echo elgg_view('miigle_dependencies/footer_js'); ?>
</html>
