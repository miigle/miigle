<?php
    //Upgrade modals
    //Get tokens for ajax functions
    $user = get_loggedin_user();
    $ts = time();
    $token = generate_action_token($ts);
    $upgrade_modal_body = '<img src="/mod/miigle_theme/graphics/sad_puppy.jpg" alt="Sad Kitten" />';
    $upgrade_modal_body .= '<h3>Sorry Sparky, your treat is coming soon!</h3>';
    $upgrade_modal_body .= '<a 
    data-user_guid="'.$user->guid.'" 
    data-__elgg_token="'.$ts.'"
    data-__elgg_ts="'.$token.'"
    class="upgrade" 
    href="#">
    Be the first to know when the Marketplace and Miigle Stats are available.
</a><img class="ajax-loader" src="/mod/miigle_theme/graphics/ajax-loader.gif" alt="loading..." />';
$upgrade_modal = elgg_view(
    "miigle_theme/modal", array(
        'id' => 'upgrade_dialog',
        'title' => 'Awesomeness Coming Soon',
        'body' => $upgrade_modal_body,
        'header' => true,
        'footer' => true
        ));    

    //globals to pass to javascript
$loggedin_user_dict = MiigleUser::GetUserFromGUID(get_loggedin_userid());
$timestamp = time();
$action_token = generate_action_token($timestamp);

$action_token_dict = array(
    '__elgg_ts' => $timestamp,
    '__elgg_token' => $action_token,
);

$disqus_auth_string = GetUserDisqusRemoteAuthString(get_loggedin_user(), $CONFIG->DISQUS_MIIGLE_API_SECRET);
?>

<script>
    window.disqus_config = function() {
        var CommentCallback = function(comment) {
          var idea_guid = parseInt(angular.element('body').injector()
              .get('$routeParams').idea_guid, 10);

          $.ajax({
                url: '/services/api/rest/json/',
                dataType: 'json',
                type: 'POST',
                data: {
                    method: 'ideas.disqus_comment',
                    comment_id: comment.id,
                    idea_guid: idea_guid,
                },
            });
        };

        this.callbacks.onNewComment = [CommentCallback];

        <?php if (isloggedin()):  ?>
          this.page.remote_auth_s3 = '<?php echo $disqus_auth_string; ?>';
          this.page.api_key = '<?php echo $CONFIG->DISQUS_MIIGLE_API_KEY; ?>';


          this.sso = {
            name:    'Miigle',
            //button:  'http://example.com/images/samplenews.gif',
            //icon:    'http://example.com/favicon.png',
            url:     window.location.origin + '/pg/login',
            logout:  window.location.origin + '/action/logout',
            width:   '800',
            height:  '400'
          };
        <?php endif ?>
    };
</script>

<script>
var miigle = (function(loggedin_user_dict, action_token_dict, expertise_dict) {
    var entity_cache = Object.create(null);
    var username_cache = Object.create(null);
    var action_token_dict;
    var getEntity;
    var setEntity;
    var removeEntity;
    var getLoggedInUser;
    var getActionTokenDict;
    var getUsernameGUID;
    var getExpertiseDict;
    var hasEntity;
    var setDefaultEntity;

    var loggedin_user_guid = loggedin_user_dict.guid;
    var action_token_dict = action_token_dict;

    getUsernameGUID = function(username) {
        if (username_cache[username] === undefined) {
            $.ajax({
                url: '/services/api/rest/json/?method=users.get.username&username=' + username,
                dataType: 'json',
                async: false,
                success: function(data) {
                    if (data.status === 0) {
                        username_cache[username] = data.result.guid;
                        setEntity(data.result.guid, data.result);
                    }
                },
            });
        }

        return username_cache[username];
    };

    getEntity = function(entity_guid) {
        entity_guid = parseInt(entity_guid);
        var entity = entity_cache[entity_guid];
        var loggedin_user_ideas;

        if (entity === undefined) {
            $.ajax({
                url: '/services/api/rest/json/?method=get_entity&guid=' + entity_guid,
                dataType: 'json',
                async: false,
                success: function(data) {
                    entity = data.result;
                    entity_cache[entity_guid] = entity;
                },
            });
        }

        return entity;
    };

    hasEntity = function(entity_guid) {
        entity_guid = parseInt(entity_guid);
        var entity = entity_cache[entity_guid];

        return (entity !== undefined);
    };

    removeEntity = function(entity_guid) {
        entity_guid = parseInt(entity_guid);
        delete entity_cache[entity_guid];
    };

    setEntity = function(entity_guid, entity) {
        entity_guid = parseInt(entity_guid);
        entity_cache[entity_guid] = entity;
        return entity;
    };

    setDefaultEntity = function(entity_guid, entity) {
        entity_guid = parseInt(entity_guid);
        if (!hasEntity(entity_guid)) {
            setEntity(entity_guid, entity);
        }
        return entity;
    }

    getLoggedInUser = function() {
        return getEntity(loggedin_user_guid);
    };

    getActionTokenDict = function() {
        return action_token_dict;
    };

    getExpertiseDict = function() {
        return expertise_dict;
    };

    setEntity(loggedin_user_guid, loggedin_user_dict);

    return {
        getUsernameGUID: getUsernameGUID,
        hasEntity: hasEntity,
        getEntity: getEntity,
        setEntity: setEntity,
        getLoggedInUser: getLoggedInUser,
        getActionTokenDict: getActionTokenDict,
        getExpertiseDict: getExpertiseDict,
        setDefaultEntity: setDefaultEntity,
    };
})(
<?php echo json_encode($loggedin_user_dict); ?>,
<?php echo json_encode($action_token_dict); ?>,
<?php echo json_encode(GetIdeaQuestionCommunities()); ?>
);

</script>


<nav class="hidden-xs navbar navbar-default navbar-fixed-top navbar-sub" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-sub-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div ng-if="logged_in" class="collapse navbar-collapse navbar-sub-collapse">
      <ul class="nav navbar-nav">
        <li>
          <a href="/pg/miigle#/ideafeed"><span class="glyphicon glyphicon-align-justify"></span> Feed</a>
        </li>
        <li>
          <a href="/pg/miigle#/user/{{loggedin_user_dict.username}}"><span class="glyphicon glyphicon-user"></span> Profile</a>
        </li>
        <!--
        <li>
          <a data-toggle="modal" href="#upgrade_dialog"><span class="glyphicon glyphicon-tag"></span> Marketplace</a>
        </li>
        -->
      </ul>
      <form class="navbar-form navbar-right search-form" style="padding-right: 15px">
        <div class="input-group">          
          <input name="q" type="text" class="form-control" placeholder="Search">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-default">
              <i class="fa fa-search"></i>
            </button>
          </span>
        </div>
      </form>
    </div><!-- /.navbar-collapse -->
    <div ng-if="!logged_in" class="collapse navbar-collapse navbar-sub-collapse logged-out">
      <ul class="nav navbar-nav">
        <li>
          <p style="font-size: 14px; margin-bottom: 0;">Miigle helps anyone share, discover, and contribute to amazing startups and projects taking flight all over the world. <b>Join us!</b></p>
        </li>
      </ul>
    </div>
  </div><!-- /.container -->
</nav><!-- /.navbar -->


<nav class="navbar navbar-inverse navbar-fixed-top navbar-main" role="navigation">
  <div class="container">        
    <!-- logo and mobile menu -->
    <div class="navbar-header">    
      <a class="hidden-xs navbar-brand" href="/pg/miigle#/ideafeed">
        <img src="/mod/miigle_theme/graphics/miigle-logo1.png" alt="Miigle" />
      </a>
      <div ng-if="logged_in" class="nav-mobile visible-xs pull-left">
        <a href="/pg/miigle#/ideafeed" class="btn navbar-btn">
          <i class="fa fa-larger fa-home"></i>
        </a>
        <a href="/pg/miigle#/user/{{loggedin_user_dict.username}}" class="btn navbar-btn">
          <i class="fa fa-larger fa-user"></i>
        </a>                                
        <a class="btn navbar-btn navbar-toggle" data-toggle="collapse" data-target=".navbar-mail-collapse">
          <i class="fa fa-larger fa-envelope"></i>
          <span ng-if="store.messages.unread_count() > 0" class="unread-count-display">{{store.messages.unread_count()}}</span>
        </a>
        <a class="btn navbar-btn navbar-toggle" data-toggle="collapse" data-target=".navbar-notifications-collapse">
          <i class="fa fa-larger fa-bell"></i>
          <span ng-if="unread_notification_count() > 0" class="unread-count-display">{{unread_notification_count()}}</span>
        </a>
      </div>            
      <button type="button" class="navbar-toggle navbar-btn" data-toggle="collapse" data-target=".navbar-main-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>   
    </div>
    <div ng-if="!logged_in" class="navbar-collapse collapse">
      <!--<button id="header_join" data-toggle="modal" href="#modal-request" type="button" class="pull-right btn btn-danger navbar-btn">Join The Beta</button>-->
      <a id="header_join" href="/index.php" class="pull-right btn btn-danger navbar-btn">Join The Beta</a>
      <a href="/pg/login" class="pull-right btn btn-default btn-inverse navbar-btn">Log in</a>
    </div><!--/.nav-collapse -->

    <div ng-if="logged_in" class="visible-xs collapse-mobile">
      <div class="collapse navbar-collapse navbar-mail-collapse">
        <ul class="nav navbar-nav">
          <li ng-if="store.messages.conversations_view_list('inbox').length > 0" ng-repeat="conversation in store.messages.conversations_view_list('inbox')">
            <a style="padding: 5px 5px 5px 5px;" href="/pg/miigle#/messages/inbox/{{conversation.other_user_dict.username}}" 
               ng-class="{'unread': conversation.unread_count > 0}">
              <img style="margin-top: 0px; padding-right: 5px; border-radius: 0px; height: auto;" class="pull-left" height="50" width="50" 
                   src="/pg/image/{{conversation.other_user_dict.icon_guid}}?h=50&w=50">
              <div class="pull-right message-date">
                <small>{{conversation.latest_message.message_timestamp | date:'medium'}}</small>
              </div>
              <span>
                <b>{{conversation.other_user_dict.first_name}} {{conversation.other_user_dict.last_name}}
                  <span ng-if="conversation.unread_count > 0">({{conversation.unread_count}})</span>
                </b>
              </span>
              <br>
              <span>
                <span ng-if="conversation.latest_message.is_reply" class="glyphicon glyphicon-share-alt"></span>
                {{conversation.latest_message.message_text | shortMessage}}
              </span>
            </a>
          </li>
          <li ng-if="store.messages.conversations_view_list('inbox').length == 0" style="text-align: center; padding-bottom: 5px;">Empty</li>
          <li style="margin-top: 2px; margin-bottom: 2px;" class="divider"></li>
          <li>
            <a href="/pg/miigle#/messages/inbox">
              See all messages  &raquo;
            </a>
          </li>
        </ul>
      </div>
      <div class="collapse navbar-collapse navbar-notifications-collapse">
        <ul class="nav navbar-nav notifications">
            <li ng-repeat="notification in notifications_view_list">
              <a href="#/user/{{notification.subject.username}}">{{notification.subject.first_name}} {{notification.subject.last_name}}</a> 
              {{notification.verb}} 
              <span ng-if="notification.verb == 'followed'">you</span> 
              <span ng-if="notification.verb != 'followed'">
                <a href="#/idea/{{notification.object.guid}}">{{notification.object.title}}</a>
              </span> 
            </li>
            <li ng-if="notifications_view_list.length > 0"><a href="/pg/miigle#/notifications">See All Notifications  &raquo;</a></li>
            <li ng-if="notifications_view_list.length == 0"><a href="/pg/miigle#/notifications" class="pull-left">No Notifications</a></li>
        </ul>
      </div>
      <div class="collapse navbar-collapse navbar-main-collapse">
        <form class="navbar-form navbar-right search-form" style="padding-right: 15px">
          <div class="input-group">          
            <input name="q" type="text" class="form-control" placeholder="Search">
            <span class="input-group-btn">
              <button type="submit" class="btn btn-default">
                <i class="fa fa-search"></i>
              </button>
            </span>
          </div>
        </form>
        <ul class="nav navbar-nav">
          <li ng-if="logged_in"><a href="#/user/{{loggedin_user_dict.username}}/edit">Edit Profile</a></li>
          <li ng-repeat="idea in loggedin_user_ideas">
            <a href="#/idea/{{idea.guid}}">{{idea.title}}</a>
          </li>
          <li class="divider"></li>
          <li><a href="/action/logout">Logout</a></li>
        </ul>
          </div>
        </div>
        <div ng-if="!logged_in" class="visible-xs collapse-mobile">
          <!--Josh fix both of these. They show up as like "normal" buttons and not the ones in miigle.com/-->
          <!--fuck CSS I don't want to do it -->
          <a href="/pg/login" class="pull-right btn btn-default btn-inverse navbar-btn">Log in</a>
          <button id="header_join" data-toggle="modal" href="#modal-request" type="button" class="pull-right btn btn-danger navbar-btn">Join The Beta</button>
        </div>
        
        <div ng-if="logged_in" class="collapse navbar-collapse">
          <a id="post_idea" class="btn btn-default btn-miigle navbar-btn" href="/pg/miigle#/idea/new">
          <span class="glyphicon glyphicon-plus-sign"></span> Post a Startup</a>

          <ul class="nav navbar-nav navbar-right">    
            <!--<li class="analytics"><a data-toggle="modal" href="#upgrade_dialog"><i class="hidden-xs fa fa-bar-chart-o"></i></a></li>-->
            <li class="dropdown">                        
              <a class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-envelope"></i>
                <span ng-if="store.messages.unread_count() > 0" class="unread-count-display">{{store.messages.unread_count()}}</span>
              </a>
              <ul id="message_dropdown" class="dropdown-menu user-dropdown-menu" role="menu" style="padding-bottom: 0px;">
                <li style="padding-bottom: 5px;">
                  <span style="padding-left: 5px;">
                    <a class="text-muted" href="/pg/miigle#/messages/inbox">
                      Inbox ({{store.messages.unread_count()}})
                    </a>
                    |
                    <a class="text-muted" href="/pg/miigle#/messages/trash">Trash</a>
                  </span>
                </li>

                <li ng-if="store.messages.conversations_view_list('inbox').length > 0" ng-repeat="conversation in store.messages.conversations_view_list('inbox')">
                  <a style="padding: 5px 5px 5px 5px; min-height: 55px;" href="/pg/miigle#/messages/inbox/{{conversation.other_user_dict.username}}" 
                     ng-class="{'unread': conversation.unread_count > 0}">
                    <img style="margin-top: 0px; padding-right: 5px; border-radius: 0px; height: auto;" class="pull-left" height="50" width="50" 
                         src="/pg/image/{{conversation.other_user_dict.icon_guid}}?h=50&w=50">
                    <div class="pull-right message-date">
                      <small>{{conversation.latest_message.message_timestamp | date:'medium'}}</small>
                    </div>
                    <span>
                      <b>{{conversation.other_user_dict.first_name}} {{conversation.other_user_dict.last_name}}
                        <span ng-if="conversation.unread_count > 0">({{conversation.unread_count}})</span>
                      </b>
                    </span>
                    <br>
                    <span>
                      <span ng-if="conversation.latest_message.is_reply" class="glyphicon glyphicon-share-alt"></span>
                      {{conversation.latest_message.message_text | shortMessage}}
                    </span>
                  </a>
                </li>
                <li ng-if="store.messages.conversations_view_list('inbox').length == 0" style="text-align: center; padding-bottom: 5px;">Empty</li>
              </ul>
            </li>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell"></i>
                <span ng-if="unread_notification_count() > 0" class="unread-count-display">{{unread_notification_count()}}</span>
              </a>
              <ul class="dropdown-menu notifications">
                <li ng-repeat="notification in notifications_view_list">
                  <a href="#/user/{{notification.subject.username}}">{{notification.subject.first_name}} {{notification.subject.last_name}}</a> 
                  {{notification.verb}} 
                  <span ng-if="notification.verb == 'followed'">you</span> 
                  <span ng-if="notification.verb != 'followed'">
                    <a href="#/idea/{{notification.object.guid}}">{{notification.object.title}}</a>
                  </span> 
                </li>
                <li ng-if="notifications_view_list.length > 0"><a href="/pg/miigle#/notifications">See All Notifications  &raquo;</a></li>
                <li ng-if="notifications_view_list.length == 0"><a href="/pg/miigle#/notifications" class="pull-left">No Notifications</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown">
                <img src="/pg/image/{{loggedin_user_dict.icon_guid}}?w=16&h=16" alt="{{loggedin_user_dict.username}}" />
              </a>
              <ul class="dropdown-menu user-dropdown-menu">
                <li ng-if="loggedin_user_dict.is_admin">
                  <a href="/pg/admin">Administration</a>
                </li>
                <li ng-if="loggedin_user_dict.is_admin">
                  <a href="/pg/uservalidationbyemail/admin">Registration Requests</a>
                </li>
                <li>
                  <a href="/pg/miigle#/user/{{loggedin_user_dict.username}}/edit">Edit Profile</a>
                </li>
                <li>
                  <a href="/pg/miigle#/messages/inbox">Inbox ({{store.messages.unread_count()}})</a>
                </li>
                <li ng-repeat="idea in loggedin_user_ideas">
                  <a href="#/idea/{{idea.guid}}">{{idea.title}}</a>
                </li>
                <li role="presentation" class="divider"></li>
                <li>
                  <a href="/action/logout">Log out</a>
                </li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->

    <div class="hidden" id="hack-me-please">
      <div class="addthis_toolbox addthis_32x32_style addthis_default_style">
        <a class="addthis_button_facebook_follow" addthis:userid="miiglers"></a>
        <a class="addthis_button_twitter_follow" addthis:userid="miiglers" href="//twitter.com/miiglers"></a>
        <a class="addthis_button_linkedin_follow" addthis:userid="miigle" addthis:usertype="company"></a>
        <a class="addthis_button_tumblr_follow" addthis:userid="miiglers"></a>
      </div>
    </div>

    <?php echo $upgrade_modal; ?>

    <div id="modal-request" class="modal fade">
      <div class="modal-dialog">
        <form class="form-validate" id="request_form" action="/mod/miigle_theme/request_invite.php" method="post" enctype="multipart/form-data">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h3 class="modal-title">Join Miigle!</h3>
              <h4 class="text-muted">Promote your startup and ideas to the world.</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="fname">First Name</label>
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-user text-muted"></span></span>
                      <input required type="text" class="form-control" name="fname" placeholder="i.e. Bruce">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="lname">Last Name</label>
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-user text-muted"></span></span>
                      <input required type="text" class="form-control" name="lname" placeholder="i.e. Wayne">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <div class="input-group">
                  <span class="input-group-addon"><span class="glyphicon glyphicon-envelope text-muted"></span></span>
                  <input required type="email" class="form-control" name="email" placeholder="i.e. Bruce.Wayne@WayneEnterprises.com">
                </div>
              </div>
              <div class="form-group">
                <label for="url">Website or Project URL e.g. AngelList, Kickstarter, IndieGogo, etc.<small class="text-muted">(optional)</small></label>
                <div class="input-group">
                  <span class="input-group-addon"><span class="glyphicon glyphicon-link text-muted"></span></span>
                  <input type="url" class="form-control" name="url" placeholder="i.e. http://angel.co/batmobile">
                </div>
              </div>
              <div class="form-group">
                <label for="comment">Comments <small class="text-muted">(optional)</small></label>
                <textarea rows="3" name="comment" type="text" class="form-control" placeholder="i.e. I would like to introduce to the world a revolutionary vehicle that will change the way superheroes travel and fight crime. I call it the Bat Mobile."></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <div class="row">
                <div class="col-md-12">
                  <div id="error-request_form"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">                              
                  <h4 class="text-muted">Slide right to submit:</h4>
                </div>
                <div class="col-md-6">
                  <img class="pull-right ajax-loader" src="/mod/miigle_theme/graphics/ajax-loader.gif" alt="ajax loader" />
                  <div class="noUiSlider"></div>
                </div>
              </div>
            </div>                  
          </div><!-- /.modal-content -->
          <div class="hidden success">
            <h4>Thank you!</h4>
            <p><span class="glyphicon glyphicon-ok"></span></p>
            <p class="text-muted">We've received your submission. Stay tuned.</p>
          </div>
          <input type="hidden" name="human" class="human" value="0">
        </form>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
