<?php
/**
 * Elgg pageshell
 * The standard HTML header that displays across the site
 *
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['config'] The site configuration settings, imported
 * @uses $vars['title'] The page title
 * @uses $vars['body'] The main content of the page
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php 
        $context = get_context();
    
        $page_owner = page_owner_entity();
        if(!$page_owner){
            $page_owner = get_loggedin_user();
        }
    ?>
    
    <meta name="Description" content="Miigle is the online community and marketplace 
        for entrepreneurs and startups. We help innovators worldwide share their ideas 
        and connect with people interested to help." />
    <meta name="Keywords" content="miigle, miiglers, social network, social innovation, 
        crowdsourcing, crowdfunding, josh fester, luc berlin, foster ideas, startup, 
        entrepreneurship, entrepreneur community" />
    
    <title>Miigle - The Social Network for Innovators</title>
    
    <?php echo elgg_view('miigle_dependencies/css'); ?>
    <!-- include the default css file -->
    <link rel="stylesheet" href="/_css/css.css?lastcache=<?php echo $vars['config']->lastcache; ?>&viewtype=default">

    <!--headerjs-->
    <?php echo elgg_view('miigle_dependencies/header_js'); ?>

    <!--js-->
    <?php echo elgg_view('js'); ?>
</head>
<body ng-app="miigle" ng-controller="ShellController">
