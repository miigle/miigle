<?php
/**
 * Elgg comments add form
 *
 * @package Elgg
 *
 * @uses $vars['entity']
 */

if (isset($vars['entity']) && isloggedin()) {
    $form_body = "<div class=\"contentWrapper\">";
    $form_body .= "<p class='longtext_editarea'>";
    $form_body .= "<label>".elgg_echo("generic_comments:text")."<br />";
    $form_body .= elgg_view('input/longtext',array('internalname' => 'generic_comment'));
    $form_body .= "</label></p>";
    $form_body .= "<p>" . elgg_view('input/hidden', array('internalname' => 'entity_guid', 'value' => $vars['entity']->getGUID()));
    $form_body .= elgg_view('input/submit', array('value' => elgg_echo("post"))) . "</p></div>";

    echo elgg_view('input/form', array('body' => $form_body, 'action' => "{$vars['url']}action/comments/add"));

}