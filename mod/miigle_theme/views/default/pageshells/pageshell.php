<?php

    /*
    *
    * Elgg pageshell
    * The standard HTML page shell that everything else fits into
    *
    * @package Elgg
    * @subpackage Core
    * @author Rahul Ranjan
    * @link http://elgg.org/
    *
    * @uses    $vars['config'] The site configuration settings, imported
    * @uses    $vars['title'] The page title
    * @uses    $vars['body'] The main content of the page
    * @uses    $vars['messages'] A 2d array of various message registers, passed from system_messages()
    *
    * MODIFIED FOR MIIGLE THEME
    */
    
// Set the content type 
header("Content-type: text/html; charset=UTF-8");

// Set title
if (empty($vars['title'])) {
        $title = $vars['config']->sitename;
} else if (empty($vars['config']->sitename)) {
        $title = $vars['title'];
} else {
        $title = $vars['config']->sitename . ": " .    $vars['title'];
}
?>

<?php echo elgg_view('page_elements/header', $vars);  ?>

<!-- display horizontal menu-->
<?php //echo elgg_view('page_elements/elgg_topbar', $vars); ?>

<!-- Miigle Theme: display header with logo and right-side menu -->
<?php echo elgg_view('page_elements/header_contents', $vars); ?>

<!-- main contents -->
<!-- canvas -->
<div class="container content"> 
    <!-- display any system messages -->
    <?php echo elgg_view('messages/list', array('object' =>    $vars['sysmessages']));  ?>
    <?php echo    $vars['body'];  ?>
</div><!-- /#layout_canvas -->


<!-- footer -->
<?php echo elgg_view('page_elements/footer', $vars); ?>