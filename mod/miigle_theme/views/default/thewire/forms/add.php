<?php

    /**
     * Elgg thewire edit/add page
     * 
     * @package ElggTheWire
     * 
     */

        $wire_user = get_input('wire_username');
        if (!empty($wire_user)) { $msg = '@' . $wire_user . ' '; } else { $msg = ''; }

?>


<div id="thewire_forms_add">
    <form action="<?php echo $vars['url'].'services/api/rest/json?method=thewire.miigleadd'; ?>" class="form-horizontal" method="post" name="noteForm" id="noteForm">
            <div class="form-group">
                <div class="col-md-10">
                    <?php
                        echo "<textarea maxlength=\"140\" id=\"wire_note\" rows=\"1\" class=\"form-control\" name=\"note\" placeholder=\"Status Update\">{$msg}</textarea>";
                        echo elgg_view('input/securitytoken');
                    ?>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-default btn-miigle btn-large btn-block">Post</button>
                    <img src="/mod/miigle_theme/graphics/ajax-loader.gif" class="ajax-loader" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="selector-wrapper"></div>
                </div>
            </div>
    </form>
</div>