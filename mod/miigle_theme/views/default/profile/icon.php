<?php

    /**
     * Elgg profile icon
     * 
     * @package ElggProfile
     * 
     * @uses $vars['entity'] The user entity. If none specified, the current user is assumed.
     * @uses $vars['size'] The size - small, medium or large. If none specified, medium is assumed. 
     */

    // Get entity
        if (empty($vars['entity']))
            $vars['entity'] = $vars['user'];

        if ($vars['entity'] instanceof ElggUser) {
            
        $name = htmlentities($vars['entity']->name, ENT_QUOTES, 'UTF-8');
        $username = $vars['entity']->username;
        
        if ($icontime = $vars['entity']->icontime) {
            $icontime = "{$icontime}";
        } else {
            $icontime = "default";
        }
            
    // Get size
        if (!in_array($vars['size'],array('small','medium','large','tiny','master','topbar')))
            $vars['size'] = "medium";
            
    // Get any align and js
        if (!empty($vars['align'])) {
            $align = " align=\"{$vars['align']}\" ";
        } else {
            $align = "";
        }
        
    //should we override the link
    if(isset($vars['override'])){
        $override = $vars['override'];
    }else{
        $override = false;
    }
?>
<?php if(!$override): ?>
<a href="/pg/profile/<?php echo $vars['entity']->username; ?>/view" class="icon" >
<?php endif; ?>
    <img src="<?php echo elgg_format_url($vars['entity']->getIcon($vars['size'])); ?>" 
        alt="<?php echo htmlentities($vars['entity']->name, ENT_QUOTES, 'UTF-8'); ?>" 
        title="<?php echo htmlentities($vars['entity']->name, ENT_QUOTES, 'UTF-8'); ?>" 
        class="icon <?php echo $vars['class']; ?>" 
        id="<?php echo $vars['id']; ?>"
        <?php echo $vars['extra_tags']; ?>
    />
<?php if(!$override): ?>
</a>
<?php endif; ?>
<?php } ?>