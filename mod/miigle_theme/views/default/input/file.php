<?php
/**
 * Elgg file input
 * Displays a file input field
 *
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['js'] Any Javascript to enter into the input tag
 * @uses $vars['internalname'] The name of the input field
 * @uses $vars['internalid'] The id of the input field
 * @uses $vars['class'] CSS class
 * @uses $vars['disabled'] Is the input field disabled?
 * @uses $vars['value'] The current value if any
 *
 */

if (!empty($vars['value'])) {
    echo '<span class="fileexists">'.elgg_echo('fileexists') . "</span>";
}

$class = "input-file";
if (isset($vars['class'])) {
    $class = $vars['class'];
}

$disabled = false;
if (isset($vars['disabled'])) {
    $disabled = $vars['disabled'];
}

?>
<input type="file" 
    size="<?php echo $vars['size']; ?> " 
    <?php echo $vars['js']; ?> 
    name="<?php echo $vars['internalname']; ?>" 
    <?php if (isset($vars['internalid'])) echo "id=\"{$vars['internalid']}\""; ?> 
    <?php if ($disabled) echo ' disabled="yes" '; ?> 
    class="<?php echo $class; ?>"
    <?php echo $vars['extra_tags']; ?> />