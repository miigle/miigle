<?php
/**
 * Elgg password input
 * Displays a password input field
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * mod by Rahul Ranjan:
 * added support for specifying extra tags like autocomplete
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['value'] The current value, if any
 * @uses $vars['js'] Any Javascript to enter into the input tag
 * @uses $vars['internalname'] The name of the input field
 *
 */

$class = $vars['class'];
if (!$class) {
    $class = "input-password";
}

if (isset($vars['id'])){
    $id = $vars['id'];
}

$extra_tags = "";
if(isset($vars["extra_tags"])){
    $extra_tags = $vars["extra_tags"];
}
?>

<input type="password" id="<?php echo $id;?>" <?php if ($vars['disabled']) echo ' disabled="yes" '; ?> <?php echo $vars['js']; ?> name="<?php echo $vars['internalname']; ?>" <?php if (isset($vars['internalid'])) echo "id=\"{$vars['internalid']}\""; ?> value="<?php echo htmlentities($vars['value'], ENT_QUOTES, 'UTF-8'); ?>" class="<?php echo $class; ?>" <?php echo $extra_tags?> />