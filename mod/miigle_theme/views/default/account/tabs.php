<?php

    /*
    *
    * Edit Tabs
    * Navigation tabs for editing a user account/profile/advertising/billing/reporting
    *
    * @package MiigleTheme
    *
    * @uses    $vars['active'] The active tab
    */
    
    $tabs = array('Profile' =>'/pg/miigle_profile/edit', 
        'Account' =>'/pg/miigle_user_settings/edit', 
        'Advertising' =>'/pg/miigle_advertising/edit', 
        'Reporting' =>'/pg/miigle_reporting/edit', 
        'Billing' =>'/pg/miigle_billing/edit');
?>

<ul class="nav nav-tabs">
    <?php foreach($tabs as $tab => $link): ?>
          <li<?php if($tab == $vars['active']): ?> class="active"<?php endif; ?>><a href="<?php echo $link; ?>"><?php echo $tab; ?></a></li>
      <?php endforeach; ?>
</ul>
