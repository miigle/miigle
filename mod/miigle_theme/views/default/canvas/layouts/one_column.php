<?php
/**
 * Elgg one-column layout
 *
 * @package Elgg
 * @subpackage Core
 */
?>

<!-- main content -->
<div class="row">
    <div class="col-md-12 col-lg-10 col-lg-offset-1">

        <?php echo $vars['area1']; ?>

    </div>
</div><!-- /row -->