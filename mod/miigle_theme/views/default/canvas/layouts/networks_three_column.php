<?php
/**
 *    Three column layout for miigle
 *    @author Rahul Ranjan
 *    (c)20011 Rahul Ranjan (gx.rahul@gmail.com)
 */

    $loggedin_userid = get_loggedin_userid();
    $loggedin_user = get_loggedin_user();

    if(page_owner_entity() instanceof ElggGroup){
        $group = get_entity(page_owner());

        $group_owner = get_user($group->owner_guid);
    
        $icon = elgg_view(
                    "groups/icon", array(
                                    'entity' => $group,
                                    'size' => 'medium'
                                    ));
    }elseif(page_owner_entity() instanceof ElggUser){
        $user = page_owner_entity();
    }
    
?>

<!-- main content -->
<div id="three_column">
    <div id="first-col">
        <?php 
            if(isset($vars["area1"]) && !empty($vars["area1"])){
        ?>
        <div class="profile-menu">
            <?php echo $vars["area1"];?>
        </div>
        <?php 
            }
            else{
        ?>
        <div id="welcome-box">
            <div id="profile-icon-box">
                <?php echo $icon;?>
            </div>
            <div id="welcome-links">
            <?php 
                if($group_owner->guid == $loggedin_user->guid){
            ?>
                <a href="<?php echo $vars['url']?>pg/groups/edit/<?php echo $group->guid;?>">EDIT NETWORK</a>
            <?php 
                }
            ?>
            </div>
        </div>
        <div id="bio-box" style="padding-top:0;">
            <h2>About Us.</h2>
            <div class="bio-text">
                <?php echo $group->description;?>
            </div>
        </div>
        <div id="group-website-box">
            <h2>Website.</h2>
            <div class="group-website-link">
                <?php echo elgg_view("output/url",array('value' => $group->website, 'target'=>'_blank'));?>
            </div>
        </div>
        <div id="interest-box">
            <h2>Interests.</h2>
            <div class="interest-content">
                <?php 
                    $str_interests = "";
                    $interests = $group->interests;
                    foreach($interests as $interest){
                        $str_interests .= '<li>'.ucfirst($interest).'</li>';
                    }
                    
                    if(!empty($str_interests)){
                        echo '<ul>'.$str_interests.'</ul>';
                    }else{
                        echo '<span style="font-style:italic;font-weight:normal;color:#666;">None</span>';
                    }
                ?>
            </div>
        </div>
        <div id="group-members-box">
            <h2>Network Members.</h2>
            <div class="group-members-list">
                <?php
                    $members = $group->getMembers(12);
                    $memberlist_url = "{$vars['url']}pg/groups/memberlist/{$group->guid}/";
                    foreach($members as $mem){
                ?>
                     <div class="member_icon">
                         <a href="<?php echo $mem->getURL();?>">
                             <?php echo elgg_view("profile/icon",array('entity' => $mem, 'size' => 'small', 'override' => 'true'))?>
                         </a>
                     </div>
                <?php
                    }
                ?>
                    <div class="clearfloat"></div>
                    <div id="groups_memberlist_link"><a href="<?php echo $memberlist_url;?>">View All</a></div>
                <?php 
                ?>
                <div class="clearfloat"></div>
            </div>
        </div>
        <div id="followers-box">
            <h2>Network Followers.</h2>
            <div class="followers-list">
            <?php 
                $arrfollowers = get_group_followers($group_owner->guid);
                if(is_array($arrfollowers)){
                    $count_followers = count($arrfollowers);
                }else{
                    $count_followers = 0;
                }
                if($count_followers>0){?>
                    <ul>
                    <?php foreach($arrfollowers as $follower){?>
                        <li><a title="<?php echo $follower->name;?>" href="<?php echo $follower->getUrl();?>"><image src="<?php echo $follower->getIcon('small');?>" /></a></li>
                    <?php }?>
                    </ul>
                <div class="clearfloat"></div>
            <?php }else{
                echo 'None';
            }?>
            </div>
        </div>
        <?php }?>
    </div>
    <div id="right-cols-container">
        <div id="second-col">
            <?php echo $vars['area2']?>
        </div>
        <div id="third-col">
            <div id="latest-activities-box" style="padding-bottom:25px;margin-bottom:20px;">
                <h2>Beta UI Demo.</h2>
                <a style="display:block;margin-top:10px;width:235px;height:140px;" href="http://player.vimeo.com/video/41591740?title=0&byline=0&portrait=0&autoplay=1" class="pop-image">
                    <img style="width:235px;height:140px;" src="<?php echo $vars["url"]?>mod/miigle_theme/graphics/MIIGLE-Beta-UI-Demo.png" alt="" />
                </a>
            </div>
            <div id="latest-activities-box">
                <h2>Latest Activities.</h2>
                <?php 
                    echo elgg_view_river_items(0, 0, '', '', '', '', 4, 0, 0, true);
                ?>
            </div>
            <div id="top-ideas-box">
                <h2>Top Rated Ideas.</h2>
                <?php 
                    echo elgg_view("miigle_ratings/topideas", array("pagination"=>true, "limit"=>4));
                ?>
            </div>
            <?php /*?>
            <div style="width:265px;margin:20px 0 0 0;">
                <div style="text-align:center;color:#ddd;">SPONSORED ADS</div>
                <div style="width:265px;height:177px;margin-top:10px;background-image:url('<?php echo $vars["url"]?>mod/miigle_theme/graphics/ad-placeholder.png');"></div>
                <div style="width:265px;height:177px;margin-top:10px;background-image:url('<?php echo $vars["url"]?>mod/miigle_theme/graphics/ad-placeholder.png');"></div>
                <div style="width:265px;height:177px;margin-top:10px;background-image:url('<?php echo $vars["url"]?>mod/miigle_theme/graphics/ad-placeholder.png');"></div>
            </div>
            <?php */?>
        </div>
        <!-- clearfloat -->
        <div class="clearfloat"></div>
    </div>
</div><!--/ three_column -->