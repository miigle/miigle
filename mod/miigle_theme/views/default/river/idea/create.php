<?php

    $performed_by = get_entity($vars['item']->subject_guid); // $statement->getSubject();
    $idea = get_entity($vars['item']->object_guid);
?>

<h3>
    <a href="<?php echo $performed_by->getURL(); ?>"><?php echo $performed_by->fname; ?></a> 
    created a new <?php echo strtolower($idea->ideatype); ?> 
    <a href="<?php echo $idea->getURL(); ?>"><?php echo $idea->title; ?></a>
</h3>
<h4 class="text-muted"><?php echo elgg_view_friendly_time($vars['item']->posted); ?></h4>
<div class="well">
<div class="media">
    <a class="pull-left" href="<?php echo $idea->getURL(); ?>">
        <img class="media-object" src="<?php echo $idea->getIcon('small'); ?>" alt="<?php echo $idea->title; ?>">
    </a>
    <div class="media-body">
        <?php echo $idea->pitch; ?>
    </div>
</div>

</div>