<?php
    /**
     * generic view for a river item
     * 
     * @param $item, a river item object as returned from get_river_items
     */

    $item = $vars['item'];
    if($item){
        $object = get_entity($item->object_guid);
        $subject = get_entity($item->subject_guid);
        if(!$object || !$subject) {
            // probably means an entity is disabled
            return false;
        }else{
            $body = '<';
        }
        
        echo elgg_view('river/item/wrapper',array(
            'item' => $item,
            'body' => $body
        ));
    }
?>