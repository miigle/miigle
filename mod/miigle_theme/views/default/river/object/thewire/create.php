<?php

    $performed_by = get_entity($vars['item']->subject_guid);
    $object = get_entity($vars['item']->object_guid);
    $url = $object->getURL();

    $string = "<h3><a href=\"{$performed_by->getURL()}\">{$performed_by->name}</a> made a post</h3>";
    $desc .= $object->description;
    $desc = preg_replace('/\@([A-Za-z0-9\_\.\-]*)/i','@<a href="' . $vars['url'] . 'pg/thewire/owner/$1">$1</a>',$desc);
    $string .= '<h4 class="text-muted">'.elgg_view_friendly_time($vars['item']->posted).'</h4>';
    $string .= parse_urls($desc);

    //$string .= " <span class=\"river_item_time\"><a href=\"{$vars['url']}pg/thewire/reply/{$object->getOwnerEntity()->username}\" class=\"reply\">" . elgg_echo('thewire:reply') . "</a></span>";
?>

<?php echo $string; ?>
<?php if($object->is_preview): ?>
    <div class="well">
        <div class="media">
            <a class="pull-left" href="<?php echo $object->preview_link; ?>">
                <img class="media-object" src="<?php echo $object->preview_thumb; ?>" alt="<?php echo $object->preview_title; ?>">
            </a>
            <div class="media-body">
                <h4 class="media-heading"><a href="<?php echo $object->preview_link; ?>"><?php echo $object->preview_title; ?></a></h4>
                <?php echo $object->preview_description; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
