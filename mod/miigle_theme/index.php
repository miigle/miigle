<?php
    // Get the Elgg engine
    require_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");

    if(isloggedin()){
        //forward to user dashboard
        forward("pg/miigle");
    }
    
    set_context('search');
    $offset = (int)get_input('offset', 0);

    set_context('main');
    global $autofeed;
    $autofeed = FALSE;
            
    $body = elgg_view_layout('miigle_home_layout', "" , "");

    page_draw(null, $body);
?>
