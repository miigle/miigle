var miigle_shell = angular.module('miigle.shell', ['ngRoute', 'miigle', 
    //'miigle.messages', 
    'miigle.rpc',
    'miigle.messages.send_message', 'miigle.profile.employer']);


miigle_shell.filter('shortMessage', function() {
  return function(text) {
    if (text.length > 40) {
      return text.slice(0, 40) + '...';
    }
    return text;
  };
});

miigle_shell.factory('NotificationsFactory', ['$http', '$rootScope',
  'RPCService',
  function($http, $rootScope, RPCService) {
    'use strict';

    var init;
    var promise;
    var TransformNotification;

    init = function() {
      $rootScope.notifications_view_list = []; 
      promise = RPCService.RPC({
        method: 'notifications.get',
      })
      .then(function(notifications_list) {
        $rootScope.all_notifications_view_list = notifications_list
          .filter(function(notification_dict) {
            return (notification_dict.notification_type !== 'message');
          })
          .map(TransformNotification);

        $rootScope.notifications_view_list = $rootScope
          .all_notifications_view_list
          .filter(function(notification_dict) {
            return !notification_dict.read;
          });
      });

      $rootScope.unread_notification_count = function() {
        return $rootScope.notifications_view_list
          .filter(function(notification_dict) {
            return !notification_dict.read;
          })
          .length;
      };
    };

    TransformNotification = function(notification_dict) {
      var verb;
      var subject = miigle.getEntity(notification_dict.subject_guid);
      var object = miigle.getEntity(notification_dict.object_guid);
      var object_url;
      var subject_url = '/pg/miigle#/user/' + subject.username;

      switch (notification_dict.notification_type) {
        case 'follow':
          verb = 'followed';
          object_url = '/pg/miigle#/user/' + object.username;
          break;

        case 'rate':
          verb = 'rated';
          object_url = '/pg/miigle#/idea/' + object.guid;
          break;

        case 'cheer':
          verb = 'cheered for';
          object_url = '/pg/miigle#/idea/' + object.guid;
          break;

        case 'comment':
        case 'comment_reply':
        case 'comment_owner':
          verb = 'left a comment on';
          object_url = '/pg/miigle#/idea/' + object.guid;
          break;

        default:
          throw new Error('Unknown notification_type: ' + 
              notification_dict.notification_type);
      }

      return {
        guid: notification_dict.guid,
        object: object,
        object_url: object_url,
        subject: subject,
        subject_url: subject_url,
        time_created: 1000 * notification_dict.time_created,
        read: notification_dict.read,
        verb: verb,
        notification_type: notification_dict.notification_type,
        notification_payload: notification_dict.notification_payload,
      };
    };

    init();

    return {
      getNotifications: function() {
        return promise;
      },
      setNotificationsRead: function() {
        //setting as read and getting guids
        var notification_guids = $rootScope.notifications_view_list
          .map(function(notification_dict) { 
            notification_dict.read = true;
            return notification_dict.guid;
          });

        return RPCService.RPC({
          method: 'notifications.set_read',
          guids_list: JSON.stringify(notification_guids),
        });
      },
    };
  }
]);

miigle_shell.factory('MessagesFactory', ['$http', '$rootScope', 'RPCService', 
  function($http, $rootScope, RPCService) {
    'use strict';

    var init;
    var store = Object.create(null);
    var conversation_promise;
    var CacheConversations;

    init = function() {
      store.conversations = {
        inbox: {},
        trash: {},
      };
      store.messages = {
        inbox: {},
        trash: {},
      };
      store.conversations_view_list = function(box_type) {
        return Object
          .keys($rootScope.store.messages.conversations[box_type])
          .map(function(user_guid) {
            return $rootScope.store.messages.conversations[box_type][user_guid];
          })
          .sort(function(convo_one, convo_two) {
            return (convo_one.latest_message.message_timestamp < 
                    convo_two.latest_message.message_timestamp);
          });
      };
      store.unread_count = function() {
        return Object.keys($rootScope.store.messages.conversations.inbox)
          .map(function(key) {
            return $rootScope.store.messages.conversations.inbox[key];
          })
          .reduce(function(unread_count, conversation_dict) {
            return unread_count + conversation_dict.unread_count;
          }, 0);
      };

      $rootScope.store.messages = store;
      CacheConversations();
    };

    CacheConversations = function() {
      ['inbox', 'trash'].forEach(function(box_type) {
        RPCService.RPC({
          method: 'messages.get_conversations',
          trash: (box_type === 'trash'),
          limit: 9999,
        })
        .then(function(conversations_list) {
          conversations_list.map(function(conversation_dict) {
            conversation_dict.other_user_dict = miigle.getEntity(
                conversation_dict.other_user_guid);
            conversation_dict.latest_message.message_timestamp *= 1000;

            return conversation_dict;
          })
          .forEach(function(conversation_dict) {
            store.conversations[box_type][conversation_dict.other_user_guid] = (
                conversation_dict);
          });
        });
      });
    };

    init();

    return {
      getConversation: function(other_user_guid, box_type) {
        if (store.messages[box_type][other_user_guid] === undefined) {
          conversation_promise = RPCService.RPC({
            method: 'messages.get_conversation',
            trash: (box_type === 'trash'),
            limit: 99999,
            other_user_guid: other_user_guid,
          })
          .then(function(conversation_list) {
            store.messages[box_type][other_user_guid] = conversation_list
              .map(function(conversation) {
                conversation.message_timestamp *= 1000;
                conversation.author_dict = miigle.getEntity(
                    conversation.author_guid);
                return conversation;
              });
          });
        }

        return conversation_promise;
      },
      setConversationAsRead: function(other_user_guid, box_type) {
        var messages = store.messages[box_type][other_user_guid];
        var do_update = messages
          .some(function(message) {
            return !message.is_read;
          });
        var message_guids = messages
          .filter(function(message) {
            return !message.is_read;
          })
          .map(function(message) {
            message.is_read = true;
            return message.message_guid;
          });
          
        if (store.conversations[box_type][other_user_guid]) {
          store.conversations[box_type][other_user_guid].unread_count = 0;
        }

        if (do_update) {
          return this.updateMessages(message_guids, 'messages', 'mark-read', 
              box_type);
        }
      },
      sendMessage: function(other_user_guid, message_body) {
        var that = this;

        return RPCService.RPC({
          method: 'messages.send',
          to_user_guid: other_user_guid,
          message: message_body,
        })
        .then(function(message) {
          if (store.messages.inbox[other_user_guid] === undefined ||
              store.conversations.inbox[other_user_guid] === undefined) {
            that.getConversation(other_user_guid, 'inbox');
            CacheConversations();
          } else {
            message.author_dict = miigle.getEntity(message.author_guid);
            message.message_timestamp *= 1000;
            store.messages.inbox[other_user_guid].unshift(message);
            store.conversations.inbox[other_user_guid].latest_message = message;
          }
        });
      },
      updateMessages: function(guids_list, guids_type, action, box_type) {
        var other_box = (box_type === 'inbox') ? 'trash' : 'inbox';
        var that = this;
        var conversations_to_refresh = [];

        if (guids_type === 'conversations') {
          switch (action) {
            case 'mark-read':
            case 'mark-unread':
              guids_list.forEach(function(guid) {
                var conversation = $rootScope.store.messages.conversations[
                    box_type][guid];
                var messages = $rootScope.store.messages.messages[
                    box_type][guid];

                conversation.unread_count = (action === 'mark-read') ? 
                    0 : conversation.total_count;

                if (messages !== undefined) {
                  messages.forEach(function(message) {
                    message.is_read = (action === 'mark-read');
                  });
                }
              });
              break;

            case 'delete':
              guids_list.forEach(function(guid) {
                delete $rootScope.store.messages.conversations[box_type][guid];
                delete $rootScope.store.messages.messages[box_type][guid];
              });
              break;

            case 'move-trash':
            case 'move-inbox':
              guids_list.forEach(function(guid) {
                var conversations = $rootScope.store.messages.conversations;
                var messages = $rootScope.store.messages.messages;

                var other_box_conversation = conversations[other_box][guid];
                var this_box_conversation = conversations[box_type][guid];
                var other_box_messages = messages[other_box][guid];
                var this_box_messages = messages[box_type][guid];

                delete conversations[box_type][guid];
                delete messages[box_type][guid];

                if (other_box_conversation === undefined) {
                  conversations[other_box][guid] = this_box_conversation;
                } else {
                  other_box_conversation.total_count += (
                      this_box_conversation.total_count);
                  other_box_conversation.unread_count += (
                      this_box_conversation.unread_count);

                  if (this_box_conversation.latest_message.message_timestamp >
                     other_box_conversation.latest_message.message_timestamp) {
                    other_box_conversation.latest_message = (
                        this_box_conversation.latest_message);
                  }
                }

                if (other_box_messages === undefined) {
                  messages[other_box][guid] = this_box_messages;
                } else {
                  messages[other_box][guid] = messages[other_box][guid]
                   .concat(this_box_messages)
                   .sort(function(message_one, message_two) {
                      return (message_one.message_timestamp < 
                              message_two.message_timestamp);
                    });
                 }
              });
              break;

            default:
              throw new Error('Unknown action: ', action);
          }
        } else if (guids_type === 'messages') {
          guids_list.forEach(function(guid) {
            var conversations = $rootScope.store.messages.conversations;
            var messages = $rootScope.store.messages.messages;
            var message = Object
              .keys(messages[box_type])
              .reduce(function(all_messages, other_user_guid) {
                return all_messages.concat(
                    messages[box_type][other_user_guid]);
              },  [])
              .filter(function(message) {
                return message.message_guid === guid;
              })
              .pop();
            var message_index = messages[box_type][message.other_user_guid]
              .indexOf(message);

            if (['move-trash', 'move-inbox', 'delete'].indexOf(action) !== -1) {
              messages[box_type][message.other_user_guid].splice(
                  message_index, 1);
            }

            conversations_to_refresh.push(message.other_user_guid);
          });
        } else {
          throw new Error('Unknown guids_type: ', guids_type);
        }

        return RPCService.RPC({
          method: 'messages.update',
          action: action,
          box_type: box_type,
          guids_list: JSON.stringify(guids_list), //because elgg sucks
        })
        .then(function(response) {
          conversations_to_refresh
            .forEach(function(other_user_guid) {
              that.getConversation(other_user_guid, 'inbox');
              that.getConversation(other_user_guid, 'trash');
            });
          CacheConversations();
        });
      },
    };
  }
]);

miigle_shell.controller('ShellController', ['$scope', '$route', '$routeParams',
  'MessagesFactory', 'NotificationsFactory',
  function($scope, $route, $routeParams) {
    'use strict';

    var init;

    init = function() {
      $scope.logged_in = ($scope.loggedin_user_dict.guid !== 0);

      if ($scope.logged_in) {
        $scope.loggedin_user_ideas = ( 
          $scope.loggedin_user_dict.ideas_list.map(miigle.getEntity));
      }

      $scope.$on('$routeChangeStart', function($event, next, current) {
        //do analytics here
      });
    };

    init();
  }
]);

miigle_shell.controller('NotificationsController', ['$scope', 
  'NotificationsFactory',
  function($scope, NotificationsFactory) {
    'use strict';

    NotificationsFactory.setNotificationsRead();
  }
]);

miigle_shell.controller('MessagesShellController', ['$scope', '$routeParams',
  'MessagesFactory',
  function($scope, $routeParams, MessagesFactory) {
    'use strict';

    var init;

    init = function() {
      $scope.box_type = $routeParams.box_type;
      $scope.view_type = ($routeParams.username === undefined) ? (
          'conversations') : ('messages');
      $scope.checked_items = Object.create(null);

      $scope.CheckAll = function(check) {
        Object
          .keys($scope.checked_items)
          .forEach(function(key) {
            $scope.checked_items[key] = check;
          });
      };

      $scope.scrollToTop = function() {
        $('html, body').animate({ scrollTop: 0 }, 'medium');
      };
    };

    $scope.select = function(selector) {
      switch (selector) {
        case 'none':
          $scope.CheckAll(false);
          break;

        case 'all':
          $scope.CheckAll(true);
          break;

        default:
          $scope.CheckAll(false);
          break;
      }

      $scope.$broadcast('select', selector);
    };

    $scope.action = function(action) {
      var checked_guids = Object.keys($scope.checked_items)
        .filter(function(guid) {
          return $scope.checked_items[guid];
        })
        .map(function(guid) {
          return parseInt(guid, 10);
        });

      MessagesFactory.updateMessages(checked_guids, $scope.view_type, action, 
          $scope.box_type);
    };

    $scope.showSendMessageModal = function() {
      $scope.$broadcast('message:show-modal');
    };

    init();
  }
]);

miigle_shell.controller('ConversationsController', ['$scope', '$routeParams', 
  'MessagesFactory',
  function($scope, $routeParams, MessagesFactory) {
    'use strict';

    var init;

    init = function() {
      $scope.box_type = $routeParams.box_type;

      $scope.$watch(
        function() {
          return $scope.store.messages.conversations_view_list($scope.box_type);
        }, 
        function() {
          $scope.store.messages.conversations_view_list($scope.box_type)
            .forEach(function(conversation_dict) {
              $scope.checked_items[conversation_dict.other_user_guid] = false;
            });
        }, 
        true);
    };

    $scope.$on('select', function(event, selector) {
      switch (selector) {
        case 'none':
        case 'all':
          break;

        case 'unread':
          $scope.store.messages.conversations_view_list($scope.box_type)
            .filter(function(conversation_dict) {
              return conversation_dict.unread_count > 0;
            })
            .map(function(conversation_dict) {
              return conversation_dict.other_user_guid;
            })
            .forEach(function(key) {
              $scope.checked_items[key] = true;
            });
          break;

        case 'read':
          $scope.store.messages.conversations_view_list($scope.box_type)
            .filter(function(conversation_dict) {
              return conversation_dict.unread_count == 0;
            })
            .map(function(conversation_dict) {
              return conversation_dict.other_user_guid;
            })
            .forEach(function(key) {
              $scope.checked_items[key] = true;
            });
          break;

        default:
          throw new Error('Unknown selector:', selector);
      }
    });

    $scope.SimulateLink = function($event) {
      var $td = $($event.target).closest('td:not(.message-select)');
      var href;

      if ($td.length) {
        href = $($event.target).closest('tr').attr('href');
        window.location.hash = href;
      }
    };

    init();
  }
]);

miigle_shell.controller('MessagesController', ['$scope', '$routeParams',
  'MessagesFactory', 
  function($scope, $routeParams, MessagesFactory) {
    'use strict';

    var init;

    init = function() {
      $scope.other_user_username = $routeParams.username;
      $scope.other_user_guid = miigle.getUsernameGUID($routeParams.username);
      $scope.show_checkboxes = false;

      MessagesFactory.getConversation($scope.other_user_guid, $scope.box_type)
        .then(function() {
          $scope.show_progress_spinner = false;
          MessagesFactory.setConversationAsRead($scope.other_user_guid, 
                                                $scope.box_type);
        });
      $scope.show_progress_spinner = true;

      $scope.$watch(function() {
          return $scope.store.messages.messages[$scope.box_type][
              $scope.other_user_guid];
        },
        function() {
          var messages = $scope.store.messages.messages[$scope.box_type][
              $scope.other_user_guid] || [];

          messages.forEach(function(message) {
            $scope.checked_items[message.message_guid] = false;
          });
      }, true);

      $scope.$on('select', function(event, selector) {
        $scope.show_checkboxes = true;
        switch (selector) {
          case 'all':
            $scope.CheckAll(true);
            $scope.show_checkboxes = true;
            break;

          case 'none':
            $scope.show_checkboxes = false;
            break;

          case 'some':
            $scope.show_checkboxes = true;
            break;

          default:
            throw new Error('Unknown selector:', selector);
        }
      });

    };

    init();
  }
]);

miigle_shell.directive('conversationlist', function() {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/mod/miigle_messages/templates/conversations_view.html',
    controller: 'ConversationsController',
  };
});

miigle_shell.directive('messagelist', function() {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/mod/miigle_messages/templates/messages_view.html',
    controller: 'MessagesController',
  };
});

miigle_shell.config(['$routeProvider', 
  function($routeProvider) {
    'use strict';

    $routeProvider
      .when('/notifications', {
        controller: 'NotificationsController',
        templateUrl: '/mod/miigle_notifications/templates/view.html',
      })
      .when('/messages/:box_type', { //inbox or trash
        controller: 'MessagesShellController',
        templateUrl: '/mod/miigle_messages/templates/messages_shell.html',
      })
      .when('/messages/:box_type/:username', { //inbox or trash
        controller: 'MessagesShellController',
        templateUrl: '/mod/miigle_messages/templates/messages_shell.html',
      })
      .when('/messages', {
        redirectTo: '/messages/inbox',
      });
  }
]);

window.GetExpertiseIntClass = function(expertise_int) {
  'use strict';

  expertise_int = parseInt(expertise_int, 10);
  switch (expertise_int) {
    case 1:
    case 8:
    return 'orange-community';
    break;

    case 2:
    case 9:
    return 'green-community';
    break;

    case 3:
    case 6:
    return 'dark-blue-community';
    break;

    case 4:
    case 7:
    return 'light-blue-community';
    break;

    case 5:
    case 11:
    return 'yellow-community';
    break;

    case 10:
    case 11:
    return 'pink-community';
    break;

    default:
    console.error('unknown expertise int', expertise_int);
    return '';
    break;
  }
};

//what the fuck is going on in here?
//this shit needs to be thrown away and rewritten
$(document).ready(function() {

  /*--------------------------------
  |
  |   All pages
  |
  ----------------------------------*/
  
  //Show the "post an idea" tooltip
  $('a#post_idea').tooltip();
  
  //Request a pro account
  $('a.upgrade').on('click', function() {
    var $this = $(this);
    var error = function() {
      alert('Error. Please refresh and try again');
    };

    $.ajax({
      url: '/services/api/rest/json/?method=user.request_account_upgrade',
      data: $this.data(),
      dataType: 'json',
      type: 'POST',
      beforeSend: function() {
        $this.next('.ajax-loader').show();
      },
      success: function(data, status, xhr) {
        if (data.status == 0) {
          $this.html('Thanks! We will add you to the list, you eager beaver.');
        } else {
          error();
        }                           
      },
      error: error,
      complete: function(data) {
        $this.next('.ajax-loader').hide();
      }
    });
  });
  
  // Initiate ajax buttons
  if ($('a.btn-ajax').length != 0) {
    $('a.btn-ajax').on('click', function(e) {
      var $this = $(this);
      var ajaxerror = function() {
        $this.button('error');
        $this.addClass('btn-danger');
        $this.removeClass('btn-miigle btn-default btn-primary');
      };
      $.ajax({
        url: $this.data('url'),
        data: $this.data(),
        dataType: 'json',
        type: $this.data('type'),
        beforeSend: function() {
          $this.button('loading');
        },
        success: function(data, status, xhr) {
          if(data.status == 0) {
            $this.button('complete');                   
            $this.addClass('btn-success');  
            $this.removeClass('btn-miigle btn-danger btn-default btn-primary');     
          } else {
            ajaxerror();
          }                           
        },
        error: ajaxerror,
        complete: function(data) {
          $this.attr('disabled', true);
        }
      });
      e.preventDefault();
    });
  }

  // Validate and submit forms
  if ($('.form-validate').length != 0) {    
    $('.form-validate').each(function() {            
      // Validate
      $(this).validate({
        showErrors: function(errorMap, errorList) {
          $.each(errorList, function(key, value){
            $(value.element).parents('.form-group').addClass('has-error');
          });
          this.defaultShowErrors();
        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).parents('.form-group').removeClass('has-error');
        },
        submitHandler: function(form) {
          $(form).find('input[type=submit]').attr('disabled', 'disabled');
          $(form).find('.ajax-loader').show();
          form[0].submit();
        }
      });         
    });
  }
  
  // Transform tag inputs
  if ($('.tm-tags').length != 0) {
    $(".tm-tags").each(function() {
      var val = $(this).val();
      $(this).tagsManager({ prefilled:val.split(',') });
    });
  }

  

  /*--------------------------------
  |
  |   /pg/profile/:username/view
  |
  ----------------------------------*/  
  if ($('div#profile_view').length != 0) {
    // Show and hide the user details
    $('#details_accordion').on('shown.bs.collapse', function() {
      var $a = $(this).find('.panel-heading a');
      $a.find('.text').html($a.data('shown'));
      $a.find('.glyphicon').removeClass('glyphicon-arrow-down');
      $a.find('.glyphicon').addClass('glyphicon-arrow-up');
    });
    $('#details_accordion').on('hidden.bs.collapse', function() {
      var $a = $(this).find('.panel-heading a');
      $a.find('.text').html($a.data('hidden'));
      $a.find('.glyphicon').removeClass('glyphicon-arrow-up');
      $a.find('.glyphicon').addClass('glyphicon-arrow-down');
    });

    //Use Embedly for posting a status
    if ($('#wire_note').length != 0) {
      var $wire_note = $('#wire_note');

        // Set up preview.
      var preview = function() {
        $wire_note.preview({
          key: 'e439091da5c54e958a7def7ebeafb540',
          success:function() {
            $('#thewire_forms_add .selector-wrapper').fadeIn();
            $wire_note.data('preview-success', 'true');
          },
          error:function() {

          }
        });
      };
      
      // Initiate preview
      preview();
      
      $wire_note.on('loading', function(event) {
        $('#thewire_forms_add button').attr('disabled', true);
        $('#thewire_forms_add button').hide();
        $('#thewire_forms_add .ajax-loader').show();
      });
      $wire_note.on('loaded', function(event) {
        $('#thewire_forms_add button').removeAttr('disabled');
        $('#thewire_forms_add button').show();
        $('#thewire_forms_add .ajax-loader').hide();
      });
      
      //get the ajax options
      var ajaxoptions = {
        dataType:'json',
        beforeSerialize:function($form, options){
          if($wire_note.data('preview-success')=='true'){
            $form.addInputs($wire_note.data('preview'));
          }               
        },
        beforeSend:function(){
          $('#thewire_forms_add button').attr('disabled', true);
          $('#thewire_forms_add button').hide();
          $('#thewire_forms_add .ajax-loader').show();
        },
        success:function(response){             
          if(response.status==0){
            var $newrow = $($('#empty_post').html()).clone();
            $newrow.find('.post_body').html(response.result.description);
            if(response.result.preview_link){
              $newrow.find('.well a').attr('href', response.result.preview_link);
              $newrow.find('.well img').attr('src', response.result.preview_thumb);
              $newrow.find('.well img').attr('alt', response.result.preview_title);
              $newrow.find('.well h4 a').html(response.result.preview_title);
              $newrow.find('.well .preview_body').html(response.result.preview_description);
              $newrow.css('display', 'none');
              $('.view_river_items .river_items_list').prepend($newrow);              
              $('form .selector-wrapper').slideUp(400, function(){ 
                $(this).html(''); 
                $wire_note.html('').val(''); 
                $('.view_river_items .river_items_list').find('.row:first-child').slideDown('slow');
              });
            } else {
              $newrow.css('display', 'none');
              $newrow.find('.well').css('display', 'none');
              $wire_note.html('').val(''); 
              $('.view_river_items .river_items_list').prepend($newrow);
              $('.view_river_items .river_items_list').find('.row:first-child').slideDown('slow');
            }                   
          }
        },
        error:function(xhr, status, error){
          console.log(xhr);
          console.log(status);
          console.log(error);
        },
        complete:function(){
          $wire_note.trigger('close');
          $wire_note.html(''); 
          $wire_note.val(''); 
          $('#thewire_forms_add button').removeAttr('disabled');
          $('#thewire_forms_add button').show();
          $('#thewire_forms_add .ajax-loader').hide();
          preview();
        }
      };
      
      //Submit the form when user presses Enter
      $wire_note.on('keypress', function(event) {
        if (event.which == 13) {
          event.preventDefault();
          $("#noteForm").ajaxSubmit(ajaxoptions);
        }
      });
      
      //Submit form via ajax
      $('#noteForm').ajaxForm(ajaxoptions);
    }       
  }

  $('.search-form').submit(function(event) {
    var search_term = $(this).find('input').val();
    event.preventDefault();
    window.history.pushState(null, '', window.location.origin + window.location.pathname);
    window.location.replace('/pg/miigle#/ideafeed/search/all/0/all/' + encodeURIComponent(search_term));
  });
});
