<?php

/**
 * Miigle Theme API
 * 
 * @package MiigleTheme
 */
 

/**
 * Get all data for theme header
 *
 */
function api_get_header(){
    $user = get_loggedin_user();
    $return = array(
        'user'=> array(
            'guid' => $user->guid,
            'fname' => $user->fname,
            'lname' => $user->lname,
            'email' => $user->email,
            'icon' => $user->getIcon('tiny'),
            'url' => $user->getURL()
        )
    );
    
    //Notifications
    $arrLatestNotifications = get_latest_notifications($user, 6);
    foreach($arrLatestNotifications as $latestNotification){
        switch($latestNotification->notification_type){
            case 'message':
                $msg = $latestNotification->description;
            break;
            default:
                $msg = $latestNotification->description;
        }
        $return['notifications'] = array(
            'guid' => $latestNotification->guid,
            'msg' => $msg
        );    
    }
    
    //Messages
    $messages = get_latest_messages($user, 6, 0);
    $return['unviewed_messages'] = get_unviewed_messages_count($user);
    $return['messages_count'] = sizeof($messages);
    
    foreach($messages as $message){        
        $from_guid = $message->fromId;        
        $from_user = get_user($from_guid);
        $return['notifications'] = array(
            'from_guid' => $from_guid,
            'from_profile_url' => $from_user->getURL(),
            'msg' => (strlen($message->title)>50) ? substr($message->title,0,50)."..." : $message->title,
            'url' => $message->getURL(),
            'from_icon' => $from_user->getIcon("small"),
            'from_name' => $from_user->fname . ' ' . $from_user->lname,
        );    
    }
    
    return $return;
}
expose_function("theme.get_header", 
    "api_get_header", 
    array(),
    'Get data for theme header',
    'GET',
    false,
    false
);