<?php
    
    /*** Miigle Theme for Elgg
    *
    * START PAGE
    */


    function miigle_theme_init(){
        elgg_extend_view('css','miigle_theme/css');
        elgg_extend_view('css','miigle_theme/yuki_css');        
        elgg_extend_view('js','miigle_theme/js');
        if(get_context() != 'admin') { //Default Elgg CSS is only useful at the admin page
            elgg_unextend_view('css', 'css'); //Removes default Elgg CSS
        }
    }
    
    //Include the API functions
    require_once(dirname(__FILE__) . "/api.php");

    register_elgg_event_handler('init','system','miigle_theme_init');
    
    
    /**
     * Request an account upgrade
     *
     * @param int $user_guid 
     */
    function api_request_account_upgrade($user_guid){
        // Make sure we're logged in (send us to the front page if not)
        
        try {
            gatekeeper();
            //action_gatekeeper();
            require_once('vendor/swift/swift_required.php');
            
            $user = get_entity($user_guid);
            $user->requested_upgrade = true;
            $user->save();
            
            $subject = 'Pro Account Request';
            $from = array($email =>$name);
            $to = array(
             'ideas@miigle.com'  => 'Miigle'
            );
            $html = "<a href=\"{$user->getURL()}\">{$user->fname} {$user->lname}</a> has requested a Pro Account.";

            $transport = Swift_SmtpTransport::newInstance('smtp.mandrillapp.com', 587);
            $transport->setUsername('ideas@miigle.com');
            $transport->setPassword('ipLqobIRJPkbKzV8RKpHbA');
            $swift = Swift_Mailer::newInstance($transport);

            $message = new Swift_Message($subject);
            $message->setFrom($user->email);
            $message->setBody($html, 'text/html');
            $message->setTo($to);
            $message->addPart($html, 'text/plain');

            if ($recipients = $swift->send($message, $failures))
            {
             return 'success';
            } else {
             return "There was an error:\n".print_r($failures);
            }
        } catch (Exception $e){
            return $e->getMessage();
        }
    }
    expose_function("user.request_account_upgrade", 
        "api_request_account_upgrade", 
        array(
            "user_guid" => array('type' => 'int')
        ),
        'Request an account upgrade',
        'POST',
        false,
        false
    );
