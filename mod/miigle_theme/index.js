$(document).ready(function(){
	
	/*=======================================================
	 * 
	 * Smooth scroll link
	 * 
	 ========================================================*/ 
	 $('a#btn-scroll').click(function(){
	    $('html, body').animate({
	        scrollTop: $( $.attr(this, 'href') ).offset().top-20
	    }, 500);
	    return false;
	});
	
	
	/*=======================================================
	 * 
	 * Setup carousel
	 * 
	 ========================================================*/ 
	$('#carousel-how').on('slide.bs.carousel', function(evt){
		var $this = $(this);
		var current = $this.find('.active').index();
		var next = $this.find(evt.relatedTarget).index();
		$('section#how .custom-caption.slide-'+current).removeClass('active');
		$('section#how .custom-caption.slide-'+next).addClass('active');
	});
	$('section#how .custom-caption').on('click', function(){
		$('#carousel-how').carousel($(this).data('slide'));
	});
	 
	
		
	/*=======================================================
	 * 
	 * Setup forms
	 * 
	 ========================================================*/ 
     var PreventModalHide = function(event) {
         event.preventDefault();
     };
	 $('#request_form').ajaxForm({
	 	beforeSend:function(){
	 		$('#request_form').find('.noUiSlider').hide();
	 		$('#request_form').find('.ajax-loader').show();
            $('#modal-request').on('hide.bs.modal', PreventModalHide);
	 	},
	 	complete:function(xhr){
	 		$('#request_form').find('.ajax-loader').hide();
	 		if(xhr.responseText == 'success'){
	 			$('#request_form').find('.modal-footer, .modal-body').fadeOut('slow', function(){
	 				var html = $('#request_form').find('.success').html();
		 			$('#request_form').addClass('success');
		 			$('#request_form').find('.modal-body').html(html).fadeIn('slow');
					_gaq.push(['_trackEvent', 'Request_Invite_Submit', 'Submit', 'Success' ]);
                    $('#modal-request').off('hide.bs.modal', PreventModalHide);
	 			});
	 			
	 		} else {
	 			alert('Error submitting form. Please refresh and try again.');
				_gaq.push(['_trackEvent', 'Request_Invite_Submit', 'Submit', 'Error' ]);
                console.log(xhr);
	 		}
	 	},
	 	error:function(xhr){
	 		alert('Error submitting form. Please refresh and try again.');
			_gaq.push(['_trackEvent', 'Request_Invite_Submit', 'Submit', 'Error' ]);
            console.log(xhr);
	 	}
	 });
	 $('#contact_form').ajaxForm({
	 	beforeSend:function(){
	 		$('#contact_form').find('.noUiSlider').hide();
	 		$('#contact_form').find('.ajax-loader').show();
	 	},
	 	complete:function(xhr){
	 		$('#contact_form').find('.ajax-loader').hide();
	 		if(xhr.responseText == 'success'){
	 			$('#request_form').fadeOut('slow', function(){
	 				var html = $('#contact_form').find('.success').html();
		 			$('#contact_form').addClass('success');
		 			$('#contact_form').html(html).fadeIn('slow');
	 			});
	 		} else {
	 			alert('Error submitting form. Please refresh and try again.');
                console.log(xhr);
	 		}
	 	},
	 	error:function(xhr){
	 		alert('Error submitting form. Please refresh and try again.');
            console.log(xhr);
	 	}
	 });
	$('.noUiSlider').noUiSlider({
		range: [1,100],
		start: 1,
		handles:1
	}).on('change', function(){
		var $this = $(this);
		if ($this.val() == 100 && $this.parents('form').valid()) {
			$this.parents('form').find('.human').val(1);
			$this.parents('form').submit();
		} else {
			$this.val(0);
		}
	});
	
	
	/*=======================================================
	 * 
	 * Show video player
	 * 
	 ========================================================*/ 
	 $('section#video .btn-play').on('click', function(e){
		show_player();
		e.preventDefault();
	});
	var show_player = function(){
		$section = $('section#video');
		$section.css('background', 'black');
		$section.find('.overlay').fadeOut('slow', function(){
			var video_height = $section.height();
			var video_width = video_height * (16/9);
			if($section.width() < video_width){
				video_width = $section.width();
				video_height = video_width * (9/16);
			}			
			$('section#video').height(video_height);
			$section.find('iframe').height(video_height).width(video_width);
			$section.find('iframe').fadeIn('slow');
		});		
	};
	
	
	/*=======================================================
	 * 
	 * Validate forms
	 * 
	 ========================================================*/ 
	$('.form-validate').each(function(){	
		// Validate
		$(this).validate({
		  showErrors: function(errorMap, errorList) {
		      $.each(errorList, function(key, value){
		      	$(value.element).parents('.form-group').addClass('has-error');
		      });
		      this.defaultShowErrors();
		  },
		  unhighlight:function(element, errorClass, validClass){
		  	$(element).parents('.form-group').removeClass('has-error');
		  }
		});			
	});	
	
	/*=======================================================
	 * 
	 * Track user events
	 * 
	 ========================================================*/ 
	 
	 $('#header_join').on('click', function(){
		_gaq.push(['_trackEvent', 'Request_Invite', 'Click', 'Header_Button' ]);
	 });
	 $('#body_join').on('click', function(){
		_gaq.push(['_trackEvent', 'Request_Invite', 'Click', 'Body_Button' ]);
	 });
	 $('#footer_join').on('click', function(){
		_gaq.push(['_trackEvent', 'Request_Invite', 'Click', 'Footer_Button' ]);
	 });
	
});
