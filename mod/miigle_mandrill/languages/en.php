<?php 

	$english = array(
		'mandrill:api_key' => 'Your Mandrill API key',
		'mandrill:instructions' => 'You need to create a Mandrill account before using this plugin. <a href="http://mandrill.com">
		http://mandrill.com</a>',
		'mandrill:noreply_email' => 'Always use this noreply address [ %s ] when sending emails?',
		'mandrill:template_name' => 'Your Mandrill template name',
	);

	add_translation("en", $english);
