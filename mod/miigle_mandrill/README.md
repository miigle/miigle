elgg_mandrill
=============

Send Elgg transactional emails via the Mandrill service.

This plugin has been tested on Elgg v1.8.2 and up.

## Instructions ##

*	Drop the plugin into your mod directory
* 	Go to <a href="http://mandrill.com">Mandrill.com</a> and create an account
*   Once you have an account, generate an API key
*   Now create a template which will be used for your emails
*   Select Outbound -> templates -> create a new template
*   Paste the whole contents of email-templates/elgg.html into the html edit box and click publish
*   Go to Elgg's plugin settings, locate 'Mandrill Wrapper' and save your key
*   Next enter the name of your template
*   Then select whether or not you would like all emails to be sent using a noreply@yourdomain.com address (it is recommended you select 'yes'). If you select 'no' then users email addresses will be sent as part of the email.

## Why use Mandrill? ##

* Mandrill is designed to send transactional messages from apps. Using a service such as Mandrill will help in ensuring your emails hit the inbox and not the spam folder.
* Mandrill is made by the guys behind MailChimp, it uses the same infrastructure so is rock solid.

## What this is not for ##

* It is not recommended that you use this plugin to sent newletter type emails. For that, try MailChimp.

## Todo ##

* The email template is super rough and only there to help get started, you will want to improve it. Coding email templates is like building for the web in the '90s, tables are stil used to ensure rendering on all the different email clients, particularily outlook. If someone would like to build a better template, it would be most welcome.
