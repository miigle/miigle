<?php 

/**
 * Mandrill API plugin settings
 */

//set the default noreply email address for display - it is based on the site's domain
$email_url = get_site_domain(elgg_get_site_entity()->guid);
$email_url = 'noreply@' . $email_url;

//instructions on setting up Mandrill
$instructions = elgg_echo('mandrill:instructions', array(elgg_get_site_url()));

$mandrill_key_string = elgg_echo('mandrill:api_key');
$mandrill_key_view = elgg_view('input/text', array(
	'name' => 'params[mandrill_api_key]',
	'value' => $vars['entity']->mandrill_api_key,
	'class' => 'elgg-input-thin',
));

$template_name_string = elgg_echo('mandrill:template_name');
$template_name_view = elgg_view('input/text', array(
	'name' => 'params[mandrill_template_name]',
	'value' => $vars['entity']->mandrill_template_name,
	'class' => 'elgg-input-thin',
));

$default_email_string = sprintf(elgg_echo('mandrill:noreply_email'), $email_url);
$default_email_view = elgg_view('input/dropdown', array(
	'name' => 'params[mandrill_noreply_email]',
	'options_values' => array(
		'yes' => elgg_echo('option:yes'),
		'no' => elgg_echo('option:no'),
	),
	'value' => $vars['entity']->mandrill_noreply_email ? $vars['entity']->mandrill_noreply_email : 'yes',
));

$settings = <<<__HTML
<div class="elgg-content-thin mtm"><p>$instructions</p></div>
<div><label>$mandrill_key_string</label><br /> $mandrill_key_view</div>
<div>$default_email_string $default_email_view</div>
__HTML;

echo $settings;