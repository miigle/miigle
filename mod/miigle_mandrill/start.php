<?php
/**
 * Elgg Mandrill Plugin
 *
 * This plugin intercepts Elgg's notification handler for sending email and routes them
 * through Mandrill. 
 *
 * @package ElggMandrill
 */

require_once(dirname(__FILE__) . "/lib/mandrill.php");

register_elgg_event_handler('init', 'system', 'elgg_mandrill_init');

/**
 * Initialize the mandrill plugin.
 *
 */
function elgg_mandrill_init() {
	
	// notification handler for sending email
	register_notification_handler("email", "email_notification_handler");

}

function email_notification_handler($from, $to, $subject, $message, array $params = NULL){

	global $CONFIG;

	if (!$from) {
		$msg = elgg_echo("NotificationException:MissingParameter", array("from"));
		throw new NotificationException($msg);
	}

	if (!$to) {
		$msg = elgg_echo("NotificationException:MissingParameter", array("to"));
		throw new NotificationException($msg);
	}

	if ($to->email == "") {
		$msg = elgg_echo("NotificationException:NoEmailAddress", array($to->guid));
		throw new NotificationException($msg);
	}
    
    if (is_int($from)) {
        $from = get_user($from);
    }
    
    if (is_int($to)) {
        $to = get_user($to);
    }

	//set required variables
	$site = $CONFIG->url;
	$from_name = 'Miigle';
    $from_email = 'ideas@miigle.com';
	$to_name = $to->name;
	$to_email = $to->email;

	$to_user = MiigleUser::GetUserFromGUID($to->guid);
	$icon_url = $CONFIG->url . 'pg/image/' . $to_user['icon_guid'] . '?h=100&w=100';
	$user_url = $CONFIG->url . 'pg/miigle#user/' . $to_user['username'];

	$set_from_avatar_display = sprintf('<a href="%s"><img src="%s" alt="User avatar" /></a>', $user_url, $icon_url);

	//run the message body through autop
	$message = autop($message);

    	//send to mandrill
   	sendToMandrill($from_email, $from_name, $to_email, $message, $subject, $site, $params);

    	//return false so internal elgg notification is not triggered
	return false;

}

/**
 * A function to send the required information to the Mandrill wrapper class.
 * This function calls the static function ::request found in /lib/mandrill.php
 *
 **/
function sendToMandrill($from_email, $from_name, $to_email, $message, $subject, $site_name, array $params=NULL) {
	global $CONFIG;
  
	$mandrill = new Mandrill('nreat53PhD6KYRiVAXneWA');

	$header = (isset($params['header']) ? $params['header'] : 'Miigle');

	//get the Mandrill template name
	$template_name = 'Default-Template';

	$template_content = array (
		array (
			"name" => "header",
			"content" => $header
		),
		array (
			"name" => "main",
			"content" => $message
		),
	);

	$message = array(
		'subject' => $subject,
		'from_email' => $from_email,
		'from_name' => $from_name,
		'to' => array(array('email'=> $to_email))
	);

  	if ($CONFIG->url != 'http://localhost/') {
		$result = $mandrill->messages->sendTemplate($template_name, $template_content, $message);
  	} else {
  		$template_content = json_encode($template_content);
  		$message = json_encode($message);
  		error_log("Mandrill sendTemplate template_name=$template_name template_content=$template_content message=$message");
  	}
}
