<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/engine/start.php');
require_once(dirname(__FILE__) . '/api.php');
require_once(dirname(__FILE__) . '/router.php');

function MiigleImagesInit() {
	register_entity_type('object', 'miigle_image');
	add_subtype('object', 'miigle_image', 'ElggFile');
	register_page_handler('image', 'MiigleImagesPageHandler');
	register_plugin_hook('search', 'object:miigle_image', 'SearchMiigleImagesHook');
}

function SearchMiigleImagesHook($hook, $type, $value, $params) {
	return false;
}

register_elgg_event_handler('init', 'system', 'MiigleImagesInit');
