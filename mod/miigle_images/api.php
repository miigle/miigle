<?php

function GetMimeType($file_contents) {
    return finfo_buffer(finfo_open(FILEINFO_MIME_TYPE), $file_contents);
}

//silly helper function I wrote that is used down the file
function _CreateFile(ElggFile $file_object, $file_contents) {
    if (!$file_object->getFilename()) {
        return false;
    }
    if (!$file_object->open('write')) {
        return false;
    }
    if (!$file_object->write($file_contents)) {
        return false;
    }
    if (!$file_object->close()) {
        return false;
    }
    if (!$file_object->save()) {
        return false;
    }

    return $file_object->guid;
} 

//Again, silly helper function that alleviates some grunt work.
function _GetUnusedFilename($file_dir, $base_filename, $filename_extension) {
    $temp_file = new ElggFile();
    $filename_number = 1;

    //if !$file_dir.endsWith('/'), append '/'
    if (substr($file_dir, -1) !== '/') {
        $file_dir .= '/';
    }

    do {
        //sprintf('/some/base/dir/base_filename_12.extension')
        $filename = sprintf('%s%s_%d.%s', $file_dir, $base_filename, $filename_number, $filename_extension);
        $temp_file->setFilename($filename);
        $filename_number += 1;
    } while ($temp_file->exists());

    return $filename;
}

/**
 * @param string $image_contents - result of file_get_contents on the filename
 * @return int|false the saved image guid
 */
function CreateMiigleImage($image_contents) {
    $image_file = new ElggFile();
    //$image_file->subtype = 'miigle_image';
    //why did I comment this out?
    $image_file->access_id = ACCESS_PUBLIC;
    $mime_type = GetMimeType($image_contents);
    $image_file->setMimeType($mime_type);

    //this is from engine/lib/filestore.php
    $accepted_formats = array(
        'image/jpeg' => 'jpeg',
        'image/pjpeg' => 'jpeg',
        'image/png' => 'png',
        'image/x-png' => 'png',
        'image/gif' => 'gif'
    );

    $file_extension = $accepted_formats[$mime_type];
    if (!$file_extension) {
        return false;
    }

    //comes out to be miigle_images/image_102348.extension
    $image_filename = _GetUnusedFilename('miigle_images', 'image', $file_extension);
    $image_file->setFilename($image_filename);

    $image_guid = _CreateFile($image_file, $image_contents);

    return $image_guid;
}