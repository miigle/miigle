<?php
$mime_type = $vars['image_mime_type'];
$image_contents = $vars['image_contents'];

header('Content-type: ' . $mime_type);
header('Expires: ' . date('r',time() + 864000));
header('Pragma: public');
header('Cache-Control: public');
/*
header('Cache-Control: no-cache, must-revalidate'); //HTTP 1.1
header('Pragma: no-cache'); //HTTP 1.0
header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
*/
header('Content-Length: ' . strlen($image_contents));

$image_chunks = str_split($image_contents, 1024);
foreach ($image_chunks as $chunk) {
    echo $chunk;
}