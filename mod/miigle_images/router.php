<?php
function MiigleImagesPageHandler($page) {
	global $CONFIG;

	$image_guid = intval($page[0]);
	$image_height = intval(get_input('h', 0));
	$image_width = intval(get_input('w', 0));

	require_once($CONFIG->pluginspath . 'miigle_images/routes/view_route.php');
}