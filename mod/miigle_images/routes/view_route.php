<?php
global $CONFIG;

$image_entity = get_entity($image_guid);
$model = array();

if ($image_entity->guid) {
	$image_filename = $image_entity->getFilenameOnFilestore();
	$backup_mime_type = $image_entity->getMimeType();
} else {
	//this is kinda ugly
	$image_filename = $CONFIG->pluginspath . 'profile/graphics/defaultmaster.gif';
	$backup_mime_type = 'image/gif';
}

if ($image_height !== 0 && $image_width !== 0) {
	$resized_image_contents = get_resized_image_from_existing_file($image_filename, $image_width, $image_height, true, 0, 0, 0, 0, true); 

	if ($resized_image_contents !== false) {
		//maybe store this file intelligently so next time we can just look it up
		//and not resize it again
		$model['image_contents'] = $resized_image_contents;
		$model['image_mime_type'] = 'image/jpeg';
	}
}

if (!isset($model['image_contents']) || !isset($model['image_mime_type'])) {
	$model['image_contents'] = file_get_contents($image_filename);
	$model['image_mime_type'] = $backup_mime_type;
}

require_once($CONFIG->pluginspath . 'miigle_images/controllers/view_controller.php');
