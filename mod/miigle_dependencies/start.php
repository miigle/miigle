<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/engine/start.php');
require_once(dirname(__FILE__) . '/api.php');

function MiigleDependenciesInit() {
    register_page_handler('miigle', 'MiiglePageHandler');
}

function MiiglePageHandler($page) {
	if (isset($page[0])) {
		$page[0] = '#';
		$new_page = join('/', $page);
		forward('pg/miigle' . $new_page);
		//elgg_dump($_REQUEST);
		//error_log(json_encode($page));
	}
	$body = elgg_view('miigle_dependencies/view');
	page_draw('', $body);
	return true;
}

register_elgg_event_handler('init', 'system', 'MiigleDependenciesInit', 0);
