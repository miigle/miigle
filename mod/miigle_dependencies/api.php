<?php
function GetEntity($guid) {
	return DictifyEntity(get_entity($guid));
}

expose_function('get_entity', 'GetEntity', 
    array(
        'guid' => array('type'=>'int', 'required'=>true),
    ),
    'Get an entity',
    'GET',
    false,
    false
);