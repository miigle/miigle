<script>
  var miigle_feed = {};
  miigle_feed.idea_category_counts_dict = <?php echo json_encode((object)MiigleIdea::GetIdeaCategoryCounts()); ?>;
  miigle_feed.idea_location_counts_dict = <?php echo json_encode((object)MiigleIdea::GetIdeaLocationCounts()); ?>;
</script>

<div>
  <div ng-view></div>
</div>
