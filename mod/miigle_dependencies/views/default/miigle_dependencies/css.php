<?php
$local = true;
$css_files = array(
    '/mod/miigle_dependencies/css/lightbox.css',
);

if (!isloggedin()) {
    $css_files = array_merge($css_files, array(
        '/mod/miigle_theme/vendor/nouislider/jquery.nouislider.min.css',
    ));
}

if ($local) {
    $css_files = array_merge($css_files, array(
        '/mod/miigle_dependencies/css/roboto.css',
        '/mod/miigle_dependencies/css/dist/css/bootstrap.css',
        '/mod/miigle_dependencies/css/css/font-awesome.css',
        '/mod/miigle_dependencies/css/tagmanager.css',
        '/mod/miigle_dependencies/jQuery-File-Upload/css/style.css',
        '/mod/miigle_dependencies/jQuery-File-Upload/css/jquery.fileupload.css',
        '/mod/miigle_dependencies/jQuery-File-Upload/css/jquery.fileupload-ui.css',
    ));
} else {
    $css_files = array_merge($css_files, array(
        'http://fonts.googleapis.com/css?family=Roboto:400,300,500',
        '//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css',
        '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css',
        '//cdnjs.cloudflare.com/ajax/libs/tagmanager/3.0.0/tagmanager.css',
        '//blueimp.github.io/jQuery-File-Upload/css/style.css',
        '//blueimp.github.io/jQuery-File-Upload/css/jquery.fileupload.css',
        '//blueimp.github.io/jQuery-File-Upload/css/jquery.fileupload-ui.css',
    ));
}

foreach ($css_files as $css_file) {
    echo "<link rel=\"stylesheet\" href=\"$css_file\" />\n";
}

