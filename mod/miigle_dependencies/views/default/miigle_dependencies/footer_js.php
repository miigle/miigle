<script src="/mod/miigle_dependencies/js/jquery.validate.js"></script>
<script src="/mod/miigle_ideas/js/jquery.ui.widget.js"></script>
<script src="/mod/miigle_notifications/js/miigle_notifications.js"></script>
<script src="/mod/miigle_dependencies/js/load-image.min.js"></script>
<script src="/mod/miigle_ideas/js/jquery.fileupload.js"></script>
<script src="/mod/miigle_ideas/js/jquery.fileupload-angular.js"></script>
<script src="/mod/miigle_ideas/js/jquery.fileupload-process.js"></script>
<script src="/mod/miigle_ideas/js/jquery.fileupload-image.js"></script>
<script src="/mod/miigle_ideas/js/jquery.fileupload-validate.js"></script>
<script src="/mod/miigle_dependencies/js/typeahead.js"></script>
<script src="/mod/miigle_dependencies/js/miigle_star_rating.js"></script>
<script src="/mod/miigle_dependencies/js/miigle.js"></script>
<script src="/mod/miigle_dependencies/js/miigle_location_dropdown.js"></script>
<script src="/mod/miigle_messages/js/miigle_messages.js"></script>
<script src="/mod/miigle_dependencies/js/modernizr.custom.js"></script>
<script src="/mod/miigle_dependencies/js/lightbox-2.6.min.js"></script>
<script src="/mod/miigle_dependencies/js/miigle_video.js"></script>
<script src="/mod/miigle_dependencies/js/miigle_button_checkbox.js"></script>
<script src="/mod/miigle_ideas/js/miigle_ideas.js"></script>
<script src="/mod/miigle_dependencies/js/miigle_image_upload.js"></script>
<script src="/mod/miigle_feed/js/miigle_feed.js"></script>
<script src="/mod/miigle_profile/js/miigle_profile.js"></script>

<?php if(!isloggedin()): ?>
    <script src="/mod/miigle_dependencies/js/jquery.form.min.js"></script>
    <script src="/mod/miigle_theme/vendor/nouislider/jquery.nouislider.min.js"></script>
    <script>
        $(document).ready(function() {
        
            $('#request_form').ajaxForm({
                beforeSend:function(){
                    $('#request_form').find('.noUiSlider').hide();
                    $('#request_form').find('.ajax-loader').show();
                },
                complete:function(xhr){
                    $('#request_form').find('.ajax-loader').hide();
                    if(xhr.responseText == 'success'){
                        $('#request_form').find('.modal-footer, .modal-body').fadeOut('slow', function(){
                            var html = $('#request_form').find('.success').html();
                            $('#request_form').addClass('success');
                            $('#request_form').find('.modal-body').html(html).fadeIn('slow');
                            //_gaq.push(['_trackEvent', 'Request_Invite_Submit', 'Submit', 'Success' ]);
                        });
                        
                    } else {
                        alert('Error submitting form. Please refresh and try again.');
                        //_gaq.push(['_trackEvent', 'Request_Invite_Submit', 'Submit', 'Error' ]);
                        console.log(xhr);
                    }
                },
                error:function(xhr){
                    alert('Error submitting form. Please refresh and try again.');
                    //_gaq.push(['_trackEvent', 'Request_Invite_Submit', 'Submit', 'Error' ]);
                    console.log(xhr);
                }
             });
             
            $('.noUiSlider').noUiSlider({
                range: [1,100],
                start: 1,
                handles:1
            }).on('change', function(e){
                var $this = $(this);
                if($this.val() == 100 && $this.parents('form').valid()){
                    $(e.target).parents('form').find('.human').val(1);
                    $(e.target).parents('form').submit();
                } else {
                    $this.val(0);
                }
            });
            
            $('.form-validate').each(function(){	
                // Validate
                $(this).validate({
                  showErrors: function(errorMap, errorList) {
                      $.each(errorList, function(key, value){
                        $(value.element).parents('.form-group').addClass('has-error');
                      });
                      this.defaultShowErrors();
                  },
                  unhighlight:function(element, errorClass, validClass){
                    $(element).parents('.form-group').removeClass('has-error');
                  }
                });			
            });	
            
        });
    </script>
<?php endif; ?>

<script>
  window.addthis_config = window.addthis_config || {};
  window.addthis_config.pubid = 'ra-52713b1954f95f2f';
</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js" async></script>
<script>
    // Include the UserVoice JavaScript SDK (only needed once on a page)
    UserVoice=window.UserVoice||[];(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/QboskhNBU7ZIQxBmPAcg.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})();

    //
    // UserVoice Javascript SDK developer documentation:
    // https://www.uservoice.com/o/javascript-sdk
    //

    // Set colors
    UserVoice.push(['set', {
      accent_color: '#448dd6',
      trigger_color: 'white',
      trigger_background_color: 'rgba(46, 49, 51, 0.6)'
    }]);

    // Identify the user and pass traits
    // To enable, replace sample data with actual user traits and uncomment the line
    UserVoice.push(['identify', {
      //email:      'john.doe@example.com', // User’s email address
      //name:       'John Doe', // User’s real name
      //created_at: 1364406966, // Unix timestamp for the date the user signed up
      //id:         123, // Optional: Unique id of the user (if set, this should not change)
      //type:       'Owner', // Optional: segment your users by type
      //account: {
      //  id:           123, // Optional: associate multiple users with a single account
      //  name:         'Acme, Co.', // Account name
      //  created_at:   1364406966, // Unix timestamp for the date the account was created
      //  monthly_rate: 9.99, // Decimal; monthly rate of the account
      //  ltv:          1495.00, // Decimal; lifetime value of the account
      //  plan:         'Enhanced' // Plan name for the account
      //}
    }]);

    // Add default trigger to the bottom-right corner of the window:
    UserVoice.push(['addTrigger', { mode: 'contact', trigger_position: 'bottom-right' }]);

    // Or, use your own custom trigger:
    //UserVoice.push(['addTrigger', '#id', { mode: 'contact' }]);

    // Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
    UserVoice.push(['autoprompt', {}]);
</script>
