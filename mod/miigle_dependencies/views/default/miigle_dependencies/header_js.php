<?php
$local = true;
$js_files = array(
    '/mod/miigle_dependencies/js/tagmanager.js',
    '/mod/miigle_dependencies/js/ng-infinite-scroll.js',
    '/mod/miigle_dependencies/js/miigle_email_share.js',
    '/mod/miigle_dependencies/js/miigle_twitter_share.js',
    '/mod/miigle_dependencies/js/miigle_status_update.js',
);

if ($local) {
    $js_files = array_merge(array(
        '/mod/miigle_dependencies/js/jquery.min.js',
        '/mod/miigle_dependencies/js/angular.min.js',
        '/mod/miigle_dependencies/js/angular-route.min.js',
        '/mod/miigle_dependencies/js/angular-sanitize.min.js',
        '/mod/miigle_dependencies/js/bootstrap.js',
        '/mod/miigle_dependencies/js/d3.v3.min.js',
    ), $js_files);
} else {
    $js_files = array_merge(array(
        '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
        '//ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular.min.js',
        '//code.angularjs.org/1.2.9/angular-route.min.js',
        '//code.angularjs.org/1.2.9/angular-sanitize.min.js',
        '//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js',
        '//d3js.org/d3.v3.min.js',
    ), $js_files);
}

foreach($js_files as $js_file) {
    echo "<script src=\"$js_file\"></script>\n";
}
?>

<script>
	var miigle_ideas = {};
    miigle_ideas.idea_categories = <?php echo json_encode(GetIdeaCategories()); ?>;
    miigle_ideas.idea_types = <?php echo json_encode(GetIdeaTypes()); ?>;
    miigle_ideas.idea_question_communities = <?php echo json_encode(GetIdeaQuestionCommunities()); ?>;
</script>
