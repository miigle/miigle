var miigle_module = angular.module('miigle', ['ngRoute', 'miigle.profile', 'miigle.feed', 'miigle.idea', 'miigle.shell']);

miigle_module.config(['$routeProvider', 
  function($routeProvider) {
    'use strict';

    $routeProvider
      .otherwise({
        redirectTo: '/ideafeed',
      });
  }
]);

var miigle_rpc_module = angular.module('miigle.rpc', []);

miigle_rpc_module.factory('RPCService', ['$http', '$q', 
  function($http, $q) {
    'use strict';

    var RPC = function(params) {
      var deferred = $q.defer();

      $http({
        method: 'POST',
        url: '/services/api/rest/json/',
        cache: false,
        responseType: 'json',
        params: params,
      })
      .success(function(data, status, headers, config) {
        if (data === null) {
          deferred.reject('Error: ' + JSON.stringify(config));
        } else if (data.status === 0) {
          deferred.resolve(data.result);
        } else {
          deferred.reject('Error: ' + JSON.stringify(data));
        }
      })
      .error(function(data, status, headers, config) {
        deferred.reject('Error: ' + JSON.stringify(data));
      });

      return deferred.promise;
    };

    return {
      RPC: RPC,
    };
  }
]);

miigle_module.run(['$rootScope', 
  function($rootScope) {
    'use strict';

    $rootScope.store = Object.create(null);
    $rootScope.loggedin_user_dict = miigle.getLoggedInUser();
  }
]);

Object.items = function(o) {
  var items_list = [];
  for (var key in o) {
    items_list.push([key, o[key]]);
  };

  return items_list;
};
