angular.module('miigle.profile.status_update', ['miigle.feed'])
    .directive('statusupdate', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_dependencies/templates/status_update.html',
            scope: {
                statusUpdateCallback: '&',
            },
        };
    })
    .controller('StatusUpdateController', function($scope, $http, FeedFactory) {
        $scope.status = '';

        $scope.postStatus = function() {
            $http({
                method: 'POST',
                url: '/services/api/rest/json',
                params: {
                    __elgg_ts: miigle.getActionTokenDict().__elgg_ts,
                    __elgg_token: miigle.getActionTokenDict().__elgg_token,
                    method: 'user.update_status',
                    status: $scope.status,
                },
                cache: false,
                responseType: 'json',
            })
            .success(function(data, status, headers, config) {
                FeedFactory.BreakFeedCache();
                FeedFactory.WarmCache();
                $scope.statusUpdateCallback()();
            });
        };
    })
    ;