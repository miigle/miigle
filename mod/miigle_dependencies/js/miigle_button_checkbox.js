angular.module('miigle.button_checkbox', [])
    .directive('buttoncheckbox', function() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      };

      function guid() {
          return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
          s4() + '-' + s4() + s4() + s4();
      }

      return {
            restrict: 'E',
            transclude: true,
            replace: true,
            templateUrl: '/mod/miigle_dependencies/templates/button_checkbox.html',
            scope: {
                checked: '=',
                activeClass: '=',
            },
            link: function($scope, $element, $attrs) {
                var id = guid();
                $element.find('label').attr('for', id);
                $element.find('input').attr('id', id);

                $scope.getActiveClasses = function() {
                    var class_string = '';

                    if ($scope.checked) {
                        class_string += $scope.activeClass + ' active';
                    }
                    return class_string;
                };
            },
        };
    })
    ;
