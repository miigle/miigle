angular.module('miigle.share.twitter', [])
    .directive('twittershare', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_dependencies/templates/twitter_share.html',
            scope: {
				tweetGenerator: '&',
                startupName: '=',
            },
        };
    })
    .controller('MiigleTwitterInviteController', function($scope) {
    	$scope.GenerateTweet = function(child_scope, twitter_handle) {
            return encodeURI(
                twitter_handle + 
                ' I signed up for Miigle.com -' +
                ' a social collaboration platform for #startups! Join me!' +
                ' #LetsMiigle via @miiglers').replace(/#/g, '%23');
    	};
    })
    .controller('IdeaTwitterShareController', function($scope) {
    	$scope.GenerateTweet = function(child_scope, twitter_handle) {
        	return encodeURI(
                twitter_handle + 
        		', you should check out ' + child_scope.startup_name + 
        		' at ' + window.location.href +
        		' via @miiglers').replace(/#/g, '%23');
    	};
    })
    .controller('TwitterShareController', function($scope) {
    	var init;

    	init = function() {
    		$scope.twitter_handle = '';
    		$scope.startup_name = $scope.startupName || '';

            //setting up form validation
            $validator = $('#twitter-share-form').validate({
            	debug: true,
            	validClass: 'has-success',
            	errorClass: 'error has-error',

            	rules: {
            		twitter_handle: {
            			required: true,
            		},
            		startup_name: {
            			required: true,
            		}
            	},

            	errorPlacement: function(error, element) {
            		if (element.parent('.input-group').length) {
            			error.insertAfter(element.parent());
            		} else {
            			error.insertAfter(element);
            		}
            	},
            	highlight: function(element, errorClass, validClass) {
            		$(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
            	},
            	unhighlight: function(element, errorClass, validClass) {
            		$(element).closest('.form-group').addClass(validClass);
            	},
            });
        };

        /* Try to place the popup near the user's pointer. Doesn't work in all browsers */
        /* The worst that happens is it ends up at 0, 0 */
        $scope.composeTweet = function(event) {
        	var tweet_link;
        	var bounding_rect = event.target.getBoundingClientRect();
        	var height = 450;
        	var width = 550;
        	var left = parseInt(bounding_rect.left) - width;
        	var top = parseInt(bounding_rect.top) - height;
        	var twitter_handle = '';

	        //erg erg erg this is ugly
	        //If the user forgets a @, I want to silently add it and not update the screen
	        //so I can't update $scope
        	if ($scope.twitter_handle.charAt(0) !== '@') {
        		twitter_handle += '@';
        	}
        	twitter_handle += $scope.twitter_handle;

        	//gen da link
			tweet_link = 'https://twitter.com/home?status=' + $scope.tweetGenerator()($scope, twitter_handle);

        	if ($('#twitter-share-form').valid()) {
        		window.open(tweet_link, '_blank', 
        			'location=no,menubar=no,scrollbars=no' + 
        			',height=' + height +
        			',width=' + width +
        			',top=' + top +
        			',left=' + left);
        	} else {
        		event.preventDefault();
        	}
        };

        init();
    })
    ;
