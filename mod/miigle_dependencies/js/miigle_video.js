angular.module('miigle.video', [])
    .directive('embedvideo', function() {
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="video-container"><iframe src="" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>',
            link: function (scope, element, attrs) {
                element = element.children();
                switch (attrs.videotype) {
                    case 'youtube':
                        element.attr('src', '//www.youtube.com/embed/' + attrs.videoid);
                        break;

                    case 'vimeo':
                        element.attr('src', '//player.vimeo.com/video/' + attrs.videoid);
                        break;

                    default:
                        break;
                }
            },
        };
    });
