
//location input typeahead init
function InitLocationDropdown($location_input, location_dict_container, callback) {
    var NO_RESULTS_STRING = 'No results';
    var location_valid = (location_dict_container.location_dict['fqcn'] !== '');
    var open = false;

    $.validator.addMethod('location', function(value, element) {
        //if they dropdown is visible, the location is "valid"
        //because the user is still selecting a location
        if (open) {
            return true;
        }

        return location_valid;
    }, 'Please select a valid location.');

    $location_input.typeahead({
        name: 'locations',
        limit: 10,
        remote: {
            dataType: 'json',
            url: '/services/api/rest/json/?method=location.autocomplete&search_term=%QUERY',
            cache: true,
            beforeSend: function(jqxhr, settings) {
                $location_input.addClass('input-loading');
            },
            filter: function(response_dict) {
                var key;
                var locations_list;
                var datum_array = [];

                $location_input.removeClass('input-loading');

                if (response_dict.status == 0) {
                    locations_list = response_dict.result;

                    $.each(locations_list, function(i, location) {
                        datum_array.push({
                            value: location.fqcn,
                            tokens: [location.city, location.country, 
                            location.region, location.fqcn],
                        });
                    });
                }

                if (datum_array.length === 0) {
                    datum_array.push({
                        value: NO_RESULTS_STRING,
                        tokens: []
                    });
                }

                return datum_array;
            },
        },
    });
    $('.tt-hint').addClass('form-control');

    //when they select something, fire a cross origin ajax call to 
    //look up more data on this location
    $location_input.on('typeahead:closed', function(event) {
        open = false;
        $location_input.valid();
    });
    $location_input.on('typeahead:opened', function(event) {
        open = true;
    });
    $location_input.on('typeahead:selected', function (event, datum) {
        //what the hell is this hackstorm datum.value?
        if (datum.value === '' || datum.value === NO_RESULTS_STRING) {
            location_valid = false;
            $location_input.valid(); //alert the user it's invalid
        } else {
            //set it as true right away, then false it if we get a bad response
            //this is for form validation.
            location_valid = true;
            $location_input.valid();

            //Kind of disgusting since we already did this...but it should be cached, right?. ugh. whatever.
            $.ajax({
                url: '/services/api/rest/json/?method=location.lookup',
                data: {search_term: datum.value},
                dataType: 'json',
                cache: true,
                complete: function(jqxhr, text_status) {
                    var response = JSON.parse(jqxhr.responseText);

                    if (response.status == 0) {
                        location_dict_container.location_dict = response.result;
                        if (callback) {
                            callback(response.result);
                        }
                    } else {
                        //oh god
                        console.log('oh god');
                        location_dict_container.location_dict = {};
                        location_valid = false;
                        $location_input.valid(); //alert the user it's invalid
                    }
                },
            });
        }
    });
}
