var miigle_image_upload_module = angular.module('miigle.image_upload', 
    ['blueimp.fileupload']);

miigle_image_upload_module.config(['$httpProvider', 'fileUploadProvider', 
  function ($httpProvider, fileUploadProvider) {
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    angular.extend(fileUploadProvider.defaults, {
      replaceFileInput: false,
      dataType: 'json',
      singleFileUploads: false,
      acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
      maxFileSize: 5000000, // 5 MB
      // Enable image resizing, except for Android and Opera,
      // which actually support image resizing, but fail to
      // send Blob objects via XHR requests:
      disableImageResize: /Android(?!.*Chrome)|Opera/
      .test(window.navigator.userAgent),
      previewMaxWidth: 100,
      previewMaxHeight: 100,
      previewCrop: true,
      pasteZone: null, //disabling pasting
      dropZone: null,
    });
  }
]);

miigle_image_upload_module.directive('imageupload', 
  function() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/mod/miigle_dependencies/templates/image_upload.html',

      link: function (scope, element, attrs) {
        scope.show_idea_icon = attrs.showIdeaIcon;
        scope.icon_guid = attrs.iconGuid;
        scope.input_id = attrs.inputId;
        scope.input_name = attrs.inputName;
        scope.ngController = 'ImageUploadController';
        scope.image_guid_list = attrs.imageGuidList;
        scope.show_image_list = attrs.showImageList;
        if (attrs.multipleInput !== undefined) {
          element.find('input').attr('multiple', '');
        }
      },
    };
  });

//Yeah we're screwing with the DOM in a controller. That's bad practice. I know.
//We can revisit it later if necessary, but right now it's *really* hard to do otherwise.
//Maybe I just suck at web development, but I couldn't think of another way to do this.
//You need to move the preview-div from the upload box over to the stakeholder-card-div
//because you're moving a <canvas> which you can't just copy. I tried. Doesn't work.
miigle_image_upload_module.controller('ImageUploadController', 
  ['$scope', '$element',
  function($scope, $element) {
    var $preview_div = $element.find('#icon-preview-div');

    $scope.PostAppendHook = function($element, thumbnail) {
    };

    //when they add an image, clear the previous thumbnail preview
    $scope.$on('fileuploadadd', function(e, data) {
      $preview_div.empty();
    });

    //once we have a resized preview icon, put it in the preview div
    $scope.$on('fileuploadprocessalways', function(e, data) {
      console.log('fileuploadprocessalways');
      $.each(data.files, function (index, file, thumbnail) {
        thumbnail = file.error || file.preview;
        $preview_div.append(thumbnail);
        $scope.PostAppendHook($element, thumbnail);
      });
    });
  }
]);