angular.module('miigle_star_rating', [])
    .directive('starrating', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_ideas/templates/rating_stars.html',
            scope: {
                stars: '=', //out of 5
            },
        };
    })
    ;