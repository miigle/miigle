angular.module('miigle.share.email', [])
    .directive('emailshare', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/mod/miigle_dependencies/templates/email_share.html',
            scope: {
                bodyGenerator: '&',
                subjectGenerator: '&',
            },
        };
    })
    .controller('MiigleEmailInviteController', function($scope) {
        $scope.GenerateEmailBody = function(child_scope) {
            return encodeURI(
                   'Hi ' + child_scope.first_name + '\n\n' +
                   'I just joined Miigle, a social platform where you can share, discover, and contribute to great ideas and startups from all over the world! Join us!\n\n' +
                   'I love it and so will you! Join me there by clicking on the link below request a priority invitation. ' +
                   'Enter "PRIORITY - MEMBER INVITE" in the comment section of the form.\n\n'  +
                   'http://miigle.com\n\n'+
                   'See you there!\n\n' +
                   miigle.getLoggedInUser().first_name);
        };

        $scope.GenerateEmailSubject = function(child_scope) {
            return encodeURI(
                miigle.getLoggedInUser().first_name + ' ' + miigle.getLoggedInUser().last_name + ' wants to invite you to Miigle');
        };
    })
    .controller('IdeaEmailShareController', function($scope) {
        $scope.GenerateEmailBody = function(child_scope) {
            return encodeURI(
                   'Hi ' + child_scope.first_name + '\n\n' +
                   'You should check out ' + $scope.idea.title + ' at ' + window.location.href + '\n' +
                   'See you there!\n' +
                   miigle.getLoggedInUser().first_name);
        };

        $scope.GenerateEmailSubject = function(child_scope) {
            return encodeURI(
                miigle.getLoggedInUser().first_name + ' ' + miigle.getLoggedInUser().last_name + ' wants to share ' + $scope.idea.title + ' with you');
        };
    })
    .controller('EmailShareController', function($scope) {
        var init;

        init = function() {
            $scope.email_address = '';
            $scope.first_name = '';

            //setting up form validation
            $validator = $('#email-share-form').validate({
                debug: true,
                validClass: 'has-success',
                errorClass: 'error has-error',

                rules: {
                    email_address: {
                        required: true,
                    },
                    first_name: {
                        required: true,
                    }
                },

                errorPlacement: function(error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).closest('.form-group').addClass(validClass);
                },
            });
        };


        /*
        If the form is invalid, prevent the mailto link from opening. If it's
        valid, we need to do some work. See, some browsers will replace mailto:
        links with something like gmail or hotmail links. So, we can't just
        plug in an email address using angular binding. We need to use a dummy
        link that the browser may or may not transform, then we sneak in and
        replace the fields we need.
        */
        $scope.composeEmail = function(event) {
            var email_link = event.target;

            if ($('#email-share-form').valid()) {
                email_link.href = email_link.href.replace('replaceemail', $scope.email_address)
                                                 .replace('replacesubject', $scope.subjectGenerator()($scope)) //DUDE WHAT THE FUCK FUNCTION()()??????
                                                 .replace('replacebody', $scope.bodyGenerator()($scope));
            } else {
                event.preventDefault();
            }
        };

        init();
    }) 
    ;
